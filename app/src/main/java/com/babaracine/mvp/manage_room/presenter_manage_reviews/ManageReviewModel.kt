package com.babaracine.mvp.manage_room.presenter_manage_reviews

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

import java.util.ArrayList

class ManageReviewModel : Serializable{

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: ArrayList<Datum>? = null


    inner class Datum : Serializable{

        @SerializedName("review_id")
        @Expose
        var reviewId: Int? = null
        @SerializedName("rating")
        @Expose
        var rating: Int? = null
        @SerializedName("comment")
        @Expose
        var comment: String? = null
        @SerializedName("full_name")
        @Expose
        var fullName: String? = null
        @SerializedName("date")
        @Expose
        var date: String? = null

    }

}

