package com.babaracine.mvp.manage_room.presenter

import com.babaracine.mvp.manage_room.model.PricingDetailModel
import com.babaracine.mvp.manage_room.model.SelectedAmenityModel
import com.babaracine.mvp.manage_room.model.SelectedRoomPhotoModel

interface BasicDataContractor {

    interface BasicDataPresenter{
        fun getBasicData()

        fun getBasicLocationAndPolicyData(roomId : String)
        fun getSelectedAmenityForSelectedRoom(roomId : String)
        fun getPricingDetailForSelectedRoom(roomId : String)
        fun getPhotosOfSelectedRoom(roomId : String)
    }

    interface BasicDataView{

        fun onBasicDataSuccess(basicDataModel: BasicDataModel)
        fun onBasicDataUnSuccess(message : String)
        fun onBasicDataInternetError()

        fun onGetBasicLocationAndPolicySuccess(basicLocationAndPolicyModel: BasicLocationAndPolicyModel)
        fun onGetBasicLocationAndPolicyUnSuccess(message: String)
        fun onGetBasicLocationAndPolicyInternetError()

        fun onGetSelectedAmenitySuccess(amenityModel: SelectedAmenityModel)
        fun onGetSelectedAmenityUnSuccess(message: String)
        fun onGetSelectedAmenityInternetError()

        fun onGetPricingDetailSuccess(pricingDetailModel: PricingDetailModel)
        fun onGetPricingDetailUnSuccess(message: String)
        fun onGetPricingDetailInternetError()

        fun onGetPhotoForSelectedRoomSuccess(selectedRoomPhotoModel: SelectedRoomPhotoModel)
        fun onGetPhotoForSelectedRoomUnSuccess(message: String)
        fun onGetPhotoForSelectedRoomInternetError()


    }


}