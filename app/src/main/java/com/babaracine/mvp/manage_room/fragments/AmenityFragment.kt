package com.babaracine.mvp.manage_room.fragments

import android.app.AlertDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.mvp.manage_room.ManageRoomActivity
import com.babaracine.mvp.manage_room.adapter.AmenitiesAdapter
import com.babaracine.mvp.manage_room.model.SelectedAmenityModel
import com.babaracine.mvp.manage_room.model.UpdateBasicDetailModel
import com.babaracine.mvp.manage_room.presenter.BasicDataContractor
import com.babaracine.mvp.manage_room.presenter.BasicDataModel
import com.babaracine.mvp.manage_room.presenter.BasicDataPresenterImpl
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomContractor
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomDetailModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomPresenterImpl
import kotlinx.android.synthetic.main.fragment_amenity.*
import org.json.JSONArray


class AmenityFragment : Fragment(), UpdateRoomContractor.UpdateBasicDetailView  {
    override fun onUpdateBasicDetailSuccess(updateBasicDetailModel: UpdateBasicDetailModel) {
        //no need to implement
    }

    override fun onPricingUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        //no need to implement
    }

    override fun onPricingUpdateUnSuccess(message: String) {
        //no need to implement
    }

    override fun onPricing1UpdateInternetError() {
        //no need to implement
    }

    override fun onUpdateBasicDetailUnSuccess(message: String) {
        // no need to implement
    }

    override fun onUpdateBasicDetailInternetError() {
        // no need to implement
    }

    override fun onLocationUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        // no need to implement
    }

    override fun onLocationUpdateUnSuccess(message: String) {
        // no need to implement
    }

    override fun onLocationUpdateInternetError() {
        // no need to implement
    }

    override fun onPolicyUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        // no need to implement
    }

    override fun onPolicyUpdateUnSuccess(message: String) {
        // no need to implement
    }

    override fun onPolicyUpdateInternetError() {
        // no need to implement
    }


    lateinit var updateRoomDetailModel: UpdateRoomDetailModel

    lateinit var arrayListAmenity : ArrayList<BasicDataModel.Data.Amenities.AmenityList>

    lateinit var selectedAmenityModel : SelectedAmenityModel
    var reviewStep : Int = 0

    lateinit var jsonArraySelectedAmenities : JSONArray

    lateinit var updateRoomPresenterImpl: UpdateRoomPresenterImpl

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        arrayListAmenity = arguments!!.getSerializable(ConstIntent.KEY_AMENITY_FRAGMENT) as ArrayList<BasicDataModel.Data.Amenities.AmenityList>
        reviewStep = arguments!!.getInt(ConstIntent.KEY_REVIEW_STEPS)
        if(reviewStep > 3) {
            selectedAmenityModel = arguments!!.getSerializable(ConstIntent.KEY_SELECTED_AMENITY_DATA) as SelectedAmenityModel
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_amenity, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setLayoutManager()
        updateRoomPresenterImpl = UpdateRoomPresenterImpl(activity!!,this)

        val isFromManageRoom = LoginManagerSession.getIsManageFrom()
        if(isFromManageRoom) {
            if (reviewStep > 3) {
                setAmenityAdapterWithValue()
            } else {
                setAmenityAdapter()
            }
        }else{
            setAmenityAdapter()
        }

        manageClickEvent()
    }

    override fun onAmenitiesUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        this.updateRoomDetailModel = updateRoomDetailModel
        reviewStep = updateRoomDetailModel.data!!.rooms!!.stepsCompleted!!
        showMessageAfterRoomUpdate(updateRoomDetailModel.message!!)
    }

    fun showMessageAfterRoomUpdate(msg: String) {

        val alertDialoge = AlertDialog.Builder(activity)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(activity!!.getString(R.string.title_ok)) { dialog, which ->

            (activity as ManageRoomActivity).setTabEnableOrDisable(reviewStep+1)
        }

        alertDialoge.show()


    }


    override fun onAmenitiesUpdateUnSuccess(message: String) {
        SnackNotify.showMessage(message,activity!!,frameLayoutFragAmenity)
    }

    override fun onAmenitiesUpdateInternetError() {
        SnackNotify.checkConnection(onRetryAmenityUpdate,frameLayoutFragAmenity)
    }

    private fun manageClickEvent() {
        btnUpdateRoomAmenities.setOnClickListener(View.OnClickListener { updateRoomAmenities() })
    }

    private fun updateRoomAmenities() {
        jsonArraySelectedAmenities = JSONArray()

        val isFromManageRoom = LoginManagerSession.getIsManageFrom()

        if(isFromManageRoom) {
            if (reviewStep > 3) {
                for (i in 0 until selectedAmenityModel.data!!.roomAmenities!!.size) {
                    if (selectedAmenityModel.data!!.roomAmenities!![i].selected == 1) {
                        jsonArraySelectedAmenities.put(selectedAmenityModel.data!!.roomAmenities!![i].amenityId!!)

                    }
                }
            }else{
                for (i in 0 until arrayListAmenity.size) {
                    if (arrayListAmenity[i].selected == 1) {
                        jsonArraySelectedAmenities.put(arrayListAmenity[i].amenityId!!)
                    }
                }

            }
        }else{
                for (i in 0 until arrayListAmenity.size) {
                    if (arrayListAmenity[i].selected == 1) {
                        jsonArraySelectedAmenities.put(arrayListAmenity[i].amenityId!!)
                    }
                }

        }

        if(jsonArraySelectedAmenities.length() > 0){
            updateRoomPresenterImpl.updateAmenitiesDetail(jsonArraySelectedAmenities)
        }else{
            AlertDialogHelper.showMessage(getString(R.string.select_atleast_aminity),activity!!)
        }
    }

    private fun setLayoutManager() {
        val linearLayoutManager = LinearLayoutManager(activity)
        recyclerViewAmenity.layoutManager = linearLayoutManager
    }

    private fun setAmenityAdapter() {
        selectedAmenityModel = SelectedAmenityModel()
        val amenityAdapter = AmenitiesAdapter(activity!!,arrayListAmenity,reviewStep)
        recyclerViewAmenity.setAdapter(amenityAdapter)
    }

    private fun setAmenityAdapterWithValue() {
        val amenityAdapter = AmenitiesAdapter(activity!!,arrayListAmenity,selectedAmenityModel.data!!.roomAmenities!!,reviewStep)
        recyclerViewAmenity.setAdapter(amenityAdapter)
    }

    internal val onRetryAmenityUpdate: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            updateRoomAmenities()
        }

    }








}
