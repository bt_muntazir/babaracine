package com.babaracine.mvp.manage_room.presenter_room_photo_update

import android.app.Activity
import android.net.Uri
import com.babaracine.R
import com.babaracine.common.application.BabaracineApplication
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.common.utility.FileUtils
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.manage_room.model.SelectedRoomPhotoModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomDetailModel
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RoomPhotoUpdatePresenterImpl(val activity: Activity, val roomPhotoUpdateView: RoomPhotoUpdateContractor.RoomPhotoUpdateView) : RoomPhotoUpdateContractor.RoomPhotoUpdatePresenter {
    override fun deleteRoomImage(photoId: Int) {
        try {
            ApiAdapter.getInstance(activity)
            deleteRoomPhoto(photoId)
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            roomPhotoUpdateView.onDeleteRoomPhotoInternetError()
        }
    }

    override fun updateRoomFeaturedImage(photoId: Int) {
        try {
            ApiAdapter.getInstance(activity)
            updateFeaturedImage(photoId)
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            roomPhotoUpdateView.onFeaturedPhotoUpdateInternetError()
        }
    }

    override fun upadteRoomPhotoDetails(filePath: Uri) {
        try {
            ApiAdapter.getInstance(activity)
            uploadPhotoOnServer(filePath)
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            roomPhotoUpdateView.onPhotoUpdateInternetError()
        }
    }

    private fun updateFeaturedImage(photoId: Int) {
        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_ROOM_ID,roomId)
            jsonObject.put(Const.PARAM_PHOTO_ID,photoId)

        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getRoomDetailResult = ApiAdapter.getApiService()!!.updateFeaturedPhoto("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomDetailResult.enqueue(object : retrofit2.Callback<UpdateRoomDetailModel>{

            override fun onResponse(call: Call<UpdateRoomDetailModel>?, response: Response<UpdateRoomDetailModel>?) {

                Progress.stop()

                try {
                    val featuredPhotoModel = response!!.body()
                    val message = featuredPhotoModel.message!!
                    if(featuredPhotoModel.status == 1){
                        roomPhotoUpdateView.onFeaturedPhotoUpdateSuccess(featuredPhotoModel)
                    }else if(featuredPhotoModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        roomPhotoUpdateView.onFeaturedPhotoUpdateUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    roomPhotoUpdateView.onFeaturedPhotoUpdateUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<UpdateRoomDetailModel>?, t: Throwable?) {
                Progress.stop()
                roomPhotoUpdateView.onFeaturedPhotoUpdateUnSuccess(activity.getString(R.string.error_server))
            }
        })


    }


   private fun deleteRoomPhoto(photoId: Int) {
        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }
            jsonObject.put(Const.PARAM_PHOTO_ID,photoId)

        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getRoomDetailResult = ApiAdapter.getApiService()!!.deletePhotoInRoomUpdate("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomDetailResult.enqueue(object : retrofit2.Callback<SelectedRoomPhotoModel>{

            override fun onResponse(call: Call<SelectedRoomPhotoModel>?, response: Response<SelectedRoomPhotoModel>?) {

                Progress.stop()

                try {
                    val roomPhotoModel = response!!.body()
                    val message = roomPhotoModel.message!!
                    if(roomPhotoModel.status == 1){
                        roomPhotoUpdateView.onDeleteRoomPhotoSuccess(roomPhotoModel)
                    }else if(roomPhotoModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        roomPhotoUpdateView.onDeleteRoomPhotoUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    roomPhotoUpdateView.onDeleteRoomPhotoUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<SelectedRoomPhotoModel>?, t: Throwable?) {
                Progress.stop()
                roomPhotoUpdateView.onDeleteRoomPhotoUnSuccess(activity.getString(R.string.error_server))
            }
        })


    }


    private fun uploadPhotoOnServer(filePath: Uri) {

        Progress.start(activity)

        val file = FileUtils.getFile(BabaracineApplication.getInstance()!!, filePath)
        val fbody = RequestBody.create(MediaType.parse("image/*"), file)
        val body = MultipartBody.Part.createFormData("name", file!!.name, fbody)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()
        val call : Call<SelectedRoomPhotoModel>
        if(!roomId.equals("0")) {
            val roomIdRequestBody = RequestBody.create(MediaType.parse("text/plain"),roomId)
            call = ApiAdapter.getApiService()!!.uploadRoomPhoto(authorizationToken, preferredLanguage,roomIdRequestBody, body)
        }else{
            val roomIdRequestBody = RequestBody.create(MediaType.parse("text/plain"),roomId)
            call = ApiAdapter.getApiService()!!.uploadRoomPhoto(authorizationToken, preferredLanguage,roomIdRequestBody, body)
        }



        call.enqueue(object : Callback<SelectedRoomPhotoModel> {
            override fun onResponse(call: Call<SelectedRoomPhotoModel>?, response: Response<SelectedRoomPhotoModel>?) {

                Progress.stop()
                val selectedRoomPhotoModel = response!!.body()
                try {
                    if (selectedRoomPhotoModel.status == 1) {
                        if (!selectedRoomPhotoModel.message.isNullOrEmpty())
                            roomPhotoUpdateView.onPhotoUpdateSuccess(selectedRoomPhotoModel)
                    }else if (selectedRoomPhotoModel.status == 2) {
                       AlertDialogHelper.showMessageToLogout(selectedRoomPhotoModel.message!!,activity)
                    } else {
                        if (!selectedRoomPhotoModel.message.isNullOrEmpty())
                            roomPhotoUpdateView.onPhotoUpdateUnSuccess(selectedRoomPhotoModel.message!!)
                    }
                } catch (ex: NullPointerException) {
                    roomPhotoUpdateView.onPhotoUpdateUnSuccess(activity.getString(R.string.error_server))
                    ex.printStackTrace()
                }

            }

            override fun onFailure(call: Call<SelectedRoomPhotoModel>?, t: Throwable?) {
                roomPhotoUpdateView.onPhotoUpdateUnSuccess(activity.getString(R.string.error_server))
                Progress.stop()
                t!!.printStackTrace()
            }


        })

    }
}