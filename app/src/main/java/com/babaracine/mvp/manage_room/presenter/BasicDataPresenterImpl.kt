package com.babaracine.mvp.manage_room.presenter

import android.app.Activity
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.manage_room.model.PricingDetailModel
import com.babaracine.mvp.manage_room.model.SelectedAmenityModel
import com.babaracine.mvp.manage_room.model.SelectedRoomPhotoModel
import com.babaracine.mvp.room_booking.presenter.RoomBookingConfirmAfterPaymentModel
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BasicDataPresenterImpl(val activity: Activity, val basicDataView: BasicDataContractor.BasicDataView) : BasicDataContractor.BasicDataPresenter {
    override fun getPhotosOfSelectedRoom(roomId: String) {
        try {
            ApiAdapter.getInstance(activity)
            getAllPhotoForSelectedRoom(roomId)
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            basicDataView.onGetPhotoForSelectedRoomInternetError()
        }
    }

    private fun getAllPhotoForSelectedRoom(roomId: String) {

        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }


        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject.toString())

        val getPhotoResult = ApiAdapter.getApiService()!!.getPhotoForManageRoom("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getPhotoResult.enqueue(object : retrofit2.Callback<SelectedRoomPhotoModel>{


            override fun onResponse(call: retrofit2.Call<SelectedRoomPhotoModel>?, response: Response<SelectedRoomPhotoModel>?) {

                Progress.stop()


                try {
                     val selectedPhotoModel = response!!.body()

                     val message = selectedPhotoModel.message!!
                     if(selectedPhotoModel.status == 1){
                         basicDataView.onGetPhotoForSelectedRoomSuccess(selectedPhotoModel)
                     }else if(selectedPhotoModel.status == 2){
                         AlertDialogHelper.showMessageToLogout(message,activity)
                     }else{
                         basicDataView.onGetPhotoForSelectedRoomUnSuccess(message)
                     }
                }catch (ex : NullPointerException){
                    basicDataView.onGetPhotoForSelectedRoomUnSuccess(activity.getString(R.string.error_server))
                }


            }


            override fun onFailure(call: retrofit2.Call<SelectedRoomPhotoModel>?, t: Throwable?) {
                Progress.stop()
                basicDataView.onGetPhotoForSelectedRoomUnSuccess(activity.getString(R.string.error_server))
            }


        })




    }

    override fun getSelectedAmenityForSelectedRoom(roomId: String) {
        try {
            ApiAdapter.getInstance(activity)
            getSelectedAmenity(roomId)
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            basicDataView.onGetSelectedAmenityInternetError()
        }
    }

    private fun getSelectedAmenity(roomId: String) {
        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }


        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject.toString())

        val getAmenityResult = ApiAdapter.getApiService()!!.getSelectedAmenity("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getAmenityResult.enqueue(object : retrofit2.Callback<SelectedAmenityModel>{


            override fun onResponse(call: retrofit2.Call<SelectedAmenityModel>?, response: Response<SelectedAmenityModel>?) {

                Progress.stop()


                try {
                    val selectedAmenityModel = response!!.body()

                    val message = selectedAmenityModel.message!!
                    if(selectedAmenityModel.status == 1){
                        basicDataView.onGetSelectedAmenitySuccess(selectedAmenityModel)
                    }else if(selectedAmenityModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        basicDataView.onGetSelectedAmenityUnSuccess(message)
                    }
                }catch (ex : NullPointerException){
                    basicDataView.onGetSelectedAmenityUnSuccess(activity.getString(R.string.error_server))
                }


            }


            override fun onFailure(call: retrofit2.Call<SelectedAmenityModel>?, t: Throwable?) {
                Progress.stop()
                basicDataView.onGetSelectedAmenityUnSuccess(activity.getString(R.string.error_server))
            }


        })


    }

    override fun getPricingDetailForSelectedRoom(roomId: String) {
        try {
            ApiAdapter.getInstance(activity)
            getPricingDetail(roomId)
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            basicDataView.onGetPricingDetailInternetError()
        }
    }

    private fun getPricingDetail(roomId: String) {
        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }


        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject.toString())

        val getPricingDetailSuccess = ApiAdapter.getApiService()!!.getPricingDetails("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getPricingDetailSuccess.enqueue(object : retrofit2.Callback<PricingDetailModel>{


            override fun onResponse(call: retrofit2.Call<PricingDetailModel>?, response: Response<PricingDetailModel>?) {

                Progress.stop()


                try {
                    val pricingDetailModel = response!!.body()

                    val message = pricingDetailModel.message!!
                    if(pricingDetailModel.status == 1){
                        basicDataView.onGetPricingDetailSuccess(pricingDetailModel)
                    }else if(pricingDetailModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        basicDataView.onGetPricingDetailUnSuccess(message)
                    }
                }catch (ex : NullPointerException){
                    basicDataView.onGetPricingDetailUnSuccess(activity.getString(R.string.error_server))
                }


            }


            override fun onFailure(call: retrofit2.Call<PricingDetailModel>?, t: Throwable?) {
                Progress.stop()
                basicDataView.onGetPricingDetailUnSuccess(activity.getString(R.string.error_server))
            }


        })


    }

    override fun getBasicLocationAndPolicyData(roomId: String) {
        try {
            ApiAdapter.getInstance(activity)
            getBasicDataLocationAndPolicyData(roomId)
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            basicDataView.onBasicDataInternetError()
        }
    }

    private fun getBasicDataLocationAndPolicyData(roomId: String) {
        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }


        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject.toString())

        val getBasicLocationData = ApiAdapter.getApiService()!!.getBasicLocationAndPolicyData("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getBasicLocationData.enqueue(object : retrofit2.Callback<BasicLocationAndPolicyModel>{


            override fun onResponse(call: retrofit2.Call<BasicLocationAndPolicyModel>?, response: Response<BasicLocationAndPolicyModel>?) {

                Progress.stop()


                try {
                    val basicLocationAndPolicyModel = response!!.body()

                    val message = basicLocationAndPolicyModel.message!!
                    if(basicLocationAndPolicyModel.status == 1){
                        basicDataView.onGetBasicLocationAndPolicySuccess(basicLocationAndPolicyModel)
                    }else if(basicLocationAndPolicyModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        basicDataView.onGetBasicLocationAndPolicyUnSuccess(message)
                    }
                }catch (ex : NullPointerException){
                    basicDataView.onGetBasicLocationAndPolicyUnSuccess(activity.getString(R.string.error_server))
                }


            }


            override fun onFailure(call: retrofit2.Call<BasicLocationAndPolicyModel>?, t: Throwable?) {
                Progress.stop()
                basicDataView.onGetBasicLocationAndPolicyUnSuccess(activity.getString(R.string.error_server))
            }


        })



    }


    override fun getBasicData() {
        try {
            ApiAdapter.getInstance(activity)
            getBasicDetailFromApi()
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            basicDataView.onBasicDataInternetError()
        }
    }


    private fun getBasicDetailFromApi() {
        Progress.start(activity)

        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()

        val url = Const.BASE_URL + "api/manageRooms/getBasicData.json"

        val getOutput = ApiAdapter.getApiService()!!.getBasicDetailForManageRoom("application/json", "no-cache", preferredLanguage, authorizationToken, url)

        getOutput.enqueue(object : Callback<BasicDataModel> {
            override fun onResponse(call: Call<BasicDataModel>, response: Response<BasicDataModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val basicDetailModel = response.body()
                    val message = basicDetailModel.message!!
                    if (basicDetailModel.status == 1) {
                        basicDataView.onBasicDataSuccess(basicDetailModel)
                    } else if (basicDetailModel.status == 2) {
                        AlertDialogHelper.showMessageToLogout(message, activity)
                    } else {
                        basicDataView.onBasicDataUnSuccess(message)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    basicDataView.onBasicDataUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<BasicDataModel>, t: Throwable) {
                Progress.stop()
                t.printStackTrace()
                basicDataView.onBasicDataUnSuccess(activity.getString(R.string.error_server))
            }
        })



    }


}