package com.babaracine.mvp.manage_room.presenter

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BasicLocationAndPolicyModel : Serializable {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data : Serializable{

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("description")
        @Expose
        var description: String? = null
        @SerializedName("bedrooms")
        @Expose
        var bedrooms: String? = null
        @SerializedName("beds")
        @Expose
        var beds: String? = null
        @SerializedName("bathrooms")
        @Expose
        var bathrooms: String? = null
        @SerializedName("max_occupants")
        @Expose
        var maxOccupants: Int? = null
        @SerializedName("property_type_id")
        @Expose
        var propertyTypeId: Int? = null
        @SerializedName("suitable_for")
        @Expose
        var suitableFor: Int? = null
        @SerializedName("room_type_id")
        @Expose
        var roomTypeId: Int? = null
        @SerializedName("bed_type_id")
        @Expose
        var bedTypeId: Int? = null
        @SerializedName("country")
        @Expose
        var country: String? = null
        @SerializedName("city")
        @Expose
        var city: String? = null
        @SerializedName("state")
        @Expose
        var state: String? = null
        @SerializedName("postal")
        @Expose
        var postal: String? = null
        @SerializedName("address_1")
        @Expose
        var address1: String? = null
        @SerializedName("address_2")
        @Expose
        var address2: String? = null
        @SerializedName("formated_address")
        @Expose
        var formatedAddress: String? = null
        @SerializedName("latitude")
        @Expose
        var latitude: String? = null
        @SerializedName("longitude")
        @Expose
        var longitude: String? = null
        @SerializedName("policy")
        @Expose
        var policy: String? = null
        @SerializedName("is_premium")
        @Expose
        var isPremium: String? = null
        @SerializedName("is_listed")
        @Expose
        var isListed: String? = null
        @SerializedName("currency")
        @Expose
        var currency: String? = null
        @SerializedName("status")
        @Expose
        var status: String? = null
        @SerializedName("steps_completed")
        @Expose
        var stepsCompleted: Int? = null
        @SerializedName("total_view")
        @Expose
        var totalView: Int? = null
        @SerializedName("deleted_at")
        @Expose
        var deletedAt: Any? = null
        @SerializedName("created")
        @Expose
        var created: String? = null
        @SerializedName("modified")
        @Expose
        var modified: String? = null
        @SerializedName("_locale")
        @Expose
        var locale: String? = null
        @SerializedName("_translations")
        @Expose
        var translations: Translations? = null


        inner class Translations : Serializable{

            @SerializedName("es")
            @Expose
            var es: Es? = null
            @SerializedName("fr")
            @Expose
            var fr: Fr? = null

            inner class Es : Serializable{

                @SerializedName("description")
                @Expose
                var description: String? = null
                @SerializedName("name")
                @Expose
                var name: String? = null
                @SerializedName("locale")
                @Expose
                var locale: String? = null

            }


            inner class Fr : Serializable{

                @SerializedName("description")
                @Expose
                var description: String? = null
                @SerializedName("name")
                @Expose
                var name: String? = null
                @SerializedName("locale")
                @Expose
                var locale: String? = null

            }


        }

    }

}







