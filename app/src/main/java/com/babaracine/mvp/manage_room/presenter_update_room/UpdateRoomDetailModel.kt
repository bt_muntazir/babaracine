package com.babaracine.mvp.manage_room.presenter_update_room

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateRoomDetailModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data {

        @SerializedName("rooms")
        @Expose
        var rooms: Rooms? = null

        inner class Rooms {

            @SerializedName("steps_completed")
            @Expose
            var stepsCompleted: Int? = null

        }

    }

}

