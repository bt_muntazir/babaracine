package com.babaracine.mvp.manage_room.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateBasicDetailModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data {

        @SerializedName("rooms")
        @Expose
        var rooms: Rooms? = null

        inner class Rooms {

            @SerializedName("bedrooms")
            @Expose
            var bedrooms: String? = null
            @SerializedName("beds")
            @Expose
            var beds: String? = null
            @SerializedName("bed_type_id")
            @Expose
            var bedTypeId: Int? = null
            @SerializedName("bathrooms")
            @Expose
            var bathrooms: String? = null
            @SerializedName("property_type_id")
            @Expose
            var propertyTypeId: Int? = null
            @SerializedName("room_type_id")
            @Expose
            var roomTypeId: Int? = null
            @SerializedName("max_occupants")
            @Expose
            var maxOccupants: Int? = null
            @SerializedName("description")
            @Expose
            var description: String? = null
            @SerializedName("name")
            @Expose
            var name: String? = null
            @SerializedName("steps_completed")
            @Expose
            var stepsCompleted: Int? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("_locale")
            @Expose
            var locale: String? = null
            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("room_featured_photo")
            @Expose
            var roomFeaturedPhoto: RoomFeaturedPhoto? = null

            inner class RoomFeaturedPhoto {

                @SerializedName("medium_thumb_image_url")
                @Expose
                var mediumThumbImageUrl: String? = null
                @SerializedName("image_url")
                @Expose
                var imageUrl: String? = null
                @SerializedName("thumb_image_url")
                @Expose
                var thumbImageUrl: String? = null

            }

        }

    }

}

