package com.babaracine.mvp.manage_room.presenter_disable_room

import com.babaracine.mvp.common_model.CommonModel

interface DisableRoomContractor {

    interface DisableRoomPresenter{

        fun getDisableRoomList()
        fun deleteDisableRoomListItem(itemId : Int)
        fun addDisableRoomDate(startDate : String, endDate : String,description : String)


    }

    interface DisableRoomView{

        fun onDisableRoomListSuccess(disableRoomListModel: DisableRoomListModel)
        fun onDisableRoomListUnSuccess(message : String)
        fun onDisableRoomListInternetError()

        fun onDeleteDisableRoomListItemSuccess(commonModel: CommonModel)
        fun onDeleteDisableRoomListItemUnSuccess(message : String)
        fun onDeleteDisableRoomListItemInternetError()

        fun onAddDisableRoomFromServerSuccess(disableRoomListModel: DisableRoomListModel)
        fun onAddDisableRoomFromServerUnSuccess(message : String)
        fun onAddDisableRoomFromServerInternetError()
    }



}