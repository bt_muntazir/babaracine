package com.babaracine.mvp.manage_room.presenter_disable_room

import android.app.Activity
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomDetailModel
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class DisableRoomPresenterImpl(val activity: Activity, val disableRoomView: DisableRoomContractor.DisableRoomView) : DisableRoomContractor.DisableRoomPresenter {
    override fun deleteDisableRoomListItem(itemId: Int) {
        try {
            ApiAdapter.getInstance(activity)
            deleteRoomFromList(itemId)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            disableRoomView.onDeleteDisableRoomListItemInternetError()
        }
    }

    private fun deleteRoomFromList(itemId: Int) {
        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }
            jsonObject.put(Const.PARAM_DISABLE_ID,itemId)

        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getDisableRoomResult = ApiAdapter.getApiService()!!.deletDisableRoom("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getDisableRoomResult.enqueue(object : retrofit2.Callback<CommonModel>{

            override fun onResponse(call: Call<CommonModel>?, response: Response<CommonModel>?) {

                Progress.stop()

                try {
                    val disableRoomListModel = response!!.body()
                    val message = disableRoomListModel.message!!
                    if(disableRoomListModel.status == 1){
                        disableRoomView.onDeleteDisableRoomListItemSuccess(disableRoomListModel)
                    }else if(disableRoomListModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        disableRoomView.onDeleteDisableRoomListItemUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    disableRoomView.onDeleteDisableRoomListItemUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<CommonModel>?, t: Throwable?) {
                Progress.stop()
                disableRoomView.onDeleteDisableRoomListItemUnSuccess(activity.getString(R.string.error_server))
            }
        })
    }

    override fun addDisableRoomDate(startDate: String, endDate: String, description: String) {
        try {
            ApiAdapter.getInstance(activity)
            addDisableRoom(startDate,endDate,description)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            disableRoomView.onAddDisableRoomFromServerInternetError()
        }
    }

    private fun addDisableRoom(startDate: String, endDate: String, description: String) {

        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }
            jsonObject.put(Const.PARAM_START_DATE,startDate)
            jsonObject.put(Const.PARAM_END_DATE,endDate)
            jsonObject.put(Const.PARAM_DESCRIPTION,description)


        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getDisableRoomResult = ApiAdapter.getApiService()!!.addDisableRoom("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getDisableRoomResult.enqueue(object : retrofit2.Callback<DisableRoomListModel>{

            override fun onResponse(call: Call<DisableRoomListModel>?, response: Response<DisableRoomListModel>?) {

                Progress.stop()

                try {
                    val disableRoomListModel = response!!.body()
                    val message = disableRoomListModel.message!!
                    if(disableRoomListModel.status == 1){
                        disableRoomView.onAddDisableRoomFromServerSuccess(disableRoomListModel)
                    }else if(disableRoomListModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        disableRoomView.onAddDisableRoomFromServerUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    disableRoomView.onAddDisableRoomFromServerUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<DisableRoomListModel>?, t: Throwable?) {
                Progress.stop()
                disableRoomView.onAddDisableRoomFromServerUnSuccess(activity.getString(R.string.error_server))
            }
        })


    }

    override fun getDisableRoomList() {
           try {
                ApiAdapter.getInstance(activity)
                getListDisabelRooms()
            }catch (ex : ApiAdapter.Companion.NoInternetException){
               disableRoomView.onDisableRoomListInternetError()
            }
        }

    private fun getListDisabelRooms() {
        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }

        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getDisableRoomResult = ApiAdapter.getApiService()!!.getDisableRoomList("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getDisableRoomResult.enqueue(object : retrofit2.Callback<DisableRoomListModel>{

            override fun onResponse(call: Call<DisableRoomListModel>?, response: Response<DisableRoomListModel>?) {

                Progress.stop()

                try {
                    val disableRoomListModel = response!!.body()
                    val message = disableRoomListModel.message!!
                    if(disableRoomListModel.status == 1){
                        disableRoomView.onDisableRoomListSuccess(disableRoomListModel)
                    }else if(disableRoomListModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        disableRoomView.onDisableRoomListUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    disableRoomView.onDisableRoomListUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<DisableRoomListModel>?, t: Throwable?) {
                Progress.stop()
                disableRoomView.onDisableRoomListUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }


}