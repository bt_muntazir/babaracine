package com.babaracine.mvp.manage_room.fragments


import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.babaracine.R
import com.babaracine.common.application.BabaracineApplication
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.manage_room.ManageRoomActivity

import com.babaracine.mvp.manage_room.adapter.RoomPhotoListingAdapter

import com.babaracine.mvp.manage_room.model.SelectedRoomPhotoModel
import com.babaracine.mvp.manage_room.presenter_room_photo_update.RoomPhotoUpdateContractor
import com.babaracine.mvp.manage_room.presenter_room_photo_update.RoomPhotoUpdatePresenterImpl
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomDetailModel
import kotlinx.android.synthetic.main.bottom_sheet_camera.*
import kotlinx.android.synthetic.main.content_fragment_photos.*
import kotlinx.android.synthetic.main.fragment_photos.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


class PhotosFragment : Fragment() , RoomPhotoUpdateContractor.RoomPhotoUpdateView {

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    var reviewStep : Int = 0
    var selectedRoomPhotoModel: SelectedRoomPhotoModel? = null

    var lastSelectedFeature: Int = 0

    private val IMAGE_DIRECTORY = "/Babaracine"

    var isNewImageUploading: Boolean = false

    lateinit var image_uri: Uri

    lateinit var roomPhotoUpdatePresenterImpl: RoomPhotoUpdatePresenterImpl

    var photoId : Int = 0
    lateinit var roomPhotoListingAdapter : RoomPhotoListingAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photos, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

         val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        getIntentData()
        roomPhotoUpdatePresenterImpl = RoomPhotoUpdatePresenterImpl(activity!!,this)
        bottomSheet()
        manageClickEvent()

        if(reviewStep > 5) {
            setLayoutManager()
            setAdapter()
        }

    }

    override fun onPhotoUpdateSuccess(selectedRoomPhotoModel: SelectedRoomPhotoModel) {
        this.selectedRoomPhotoModel = selectedRoomPhotoModel


        setLayoutManager()
        setAdapter()
        (activity as ManageRoomActivity).setTabEnableOrDisable(selectedRoomPhotoModel.data!!.rooms!!.stepsCompleted!!)
    }

    override fun onPhotoUpdateUnSuccess(message: String) {
        SnackNotify.showMessage(message,activity!!,coordinatorLayoutRoomPhoto)
    }

    override fun onPhotoUpdateInternetError() {
        SnackNotify.checkConnection(onRetryRoomPhoto,coordinatorLayoutRoomPhoto)
    }

    override fun onFeaturedPhotoUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        showMessageAfterFeaturedPhotoUpdate(updateRoomDetailModel.message!!)
    }

    override fun onDeleteRoomPhotoSuccess(selectedRoomPhotoModel: SelectedRoomPhotoModel) {
        this.selectedRoomPhotoModel = selectedRoomPhotoModel
        setAdapter()
    }

    override fun onDeleteRoomPhotoUnSuccess(message: String) {
        SnackNotify.showMessage(message,activity!!,coordinatorLayoutRoomPhoto)
    }

    override fun onDeleteRoomPhotoInternetError() {
        SnackNotify.checkConnection(onRetryDeletePhoto,coordinatorLayoutRoomPhoto)
    }

    private fun getIntentData(){

        if(arguments != null) {
            reviewStep = arguments!!.getInt(ConstIntent.KEY_REVIEW_STEPS)
            if (reviewStep > 5) {
                selectedRoomPhotoModel = arguments!!.getSerializable(ConstIntent.KEY_ROOM_PHOTOS) as SelectedRoomPhotoModel
            }
        }
    }


    fun showMessageAfterFeaturedPhotoUpdate(msg: String) {

        val alertDialoge = AlertDialog.Builder(activity!!)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(activity!!.getString(R.string.title_ok)) { dialog, which ->
            roomPhotoListingAdapter.notifyDataSetChanged()
        }

        alertDialoge.show()


    }

    override fun onFeaturedPhotoUpdateUnSuccess(message: String) {
        SnackNotify.showMessage(message,activity!!,coordinatorLayoutRoomPhoto)

    }

    override fun onFeaturedPhotoUpdateInternetError() {
        SnackNotify.checkConnection(onRetryFeaturedPhotoUpdate,coordinatorLayoutRoomPhoto)
    }

    private fun updateFeaturedPhoto(){
        roomPhotoUpdatePresenterImpl.updateRoomFeaturedImage(photoId)
    }

    private fun deletePhoto(){
        roomPhotoUpdatePresenterImpl.deleteRoomImage(photoId)
    }

    fun callFeaturedPhotoApi(photoId : Int){
        this.photoId = photoId
        updateFeaturedPhoto()
    }

    fun callDeletePhotoApi(photoId : Int){
        this.photoId = photoId
        deletePhoto()
    }


    private fun setAdapter() {
        roomPhotoListingAdapter = RoomPhotoListingAdapter(this.activity!!, selectedRoomPhotoModel!!.data!!.roomPhotos!!,this)
        recyclerViewRoomPhotos.setAdapter(roomPhotoListingAdapter)
    }

    private fun setLayoutManager() {
        val gridlayoutManager = GridLayoutManager(context, 2)
        recyclerViewRoomPhotos.layoutManager = gridlayoutManager as RecyclerView.LayoutManager?
        recyclerViewRoomPhotos.isNestedScrollingEnabled = true
        //recyclerViewRoomPhotos.addOnScrollListener(mRecyclerViewOnScrollListener)
    }

    private fun manageClickEvent() {
        btnUploadImage.setOnClickListener{
            openBottomSheet()
        }

        touch_outside.setOnClickListener{
            collapseBottomSheet()
        }

        relLayImageFromGallery.setOnClickListener { selectImageInAlbum() }
        relLayImageFromCamera.setOnClickListener { takePhoto() }
    }

    private fun bottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheetBehavior.setPeekHeight(0)
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    bottomSheetBehavior.setPeekHeight(0)
                    touch_outside.setVisibility(View.GONE)
                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    touch_outside.setVisibility(View.VISIBLE)
                } else {
                    touch_outside.setVisibility(View.GONE)

                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    private fun collapseBottomSheet() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)

        }
    }

    private fun openBottomSheet() {

        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        } else {
            bottomSheetBehavior.setPeekHeight(0)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
        }

    }

    fun selectImageInAlbum() {


        if (setupPermissions()) {
            isNewImageUploading = true

            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            if (intent.resolveActivity(activity!!.packageManager) != null) {
                startActivityForResult(intent, REQUEST_SELECT_IMAGE_IN_ALBUM)
            }
        } else {
            lastSelectedFeature = REQUEST_SELECT_IMAGE_IN_ALBUM
            makeRequest()
        }

        collapseBottomSheet()
    }

    fun takePhoto() {


        if (setupPermissions()) {

            isNewImageUploading = true

            val intent1 = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent1.resolveActivity(activity!!.packageManager) != null) {
                startActivityForResult(intent1, REQUEST_TAKE_PHOTO)
            }
        } else {
            lastSelectedFeature = REQUEST_TAKE_PHOTO
            makeRequest()
        }

        collapseBottomSheet()
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(activity as Activity,
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                2909)
    }

    private fun setupPermissions(): Boolean {
        val permission_camera = ContextCompat.checkSelfPermission(BabaracineApplication.getInstance()!!,
                Manifest.permission.CAMERA)

        val permission_write_storage = ContextCompat.checkSelfPermission(BabaracineApplication.getInstance()!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if ((permission_camera != PackageManager.PERMISSION_GRANTED) ||
                (permission_write_storage != PackageManager.PERMISSION_GRANTED)) {
            //Log.i(TAG, "Permission to record denied")
            return false
        } else {
            return true
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (setupPermissions()) {
            if (lastSelectedFeature == REQUEST_SELECT_IMAGE_IN_ALBUM) {
                selectImageInAlbum()
            } else if (lastSelectedFeature == REQUEST_TAKE_PHOTO) {
                takePhoto()
            }
        } else {
            isNewImageUploading = false
            //show message to enable it from setting
        }
    }

    companion object {
        private val REQUEST_TAKE_PHOTO = 321
        private val REQUEST_SELECT_IMAGE_IN_ALBUM = 123
    }


    //Only for main user
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_TAKE_PHOTO) {

            if (resultCode == AppCompatActivity.RESULT_OK) {
                val thumbnail = data!!.getExtras().get("data") as Bitmap
                // profileImageView.setImageBitmap(thumbnail)


                saveImage(thumbnail)


                Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == REQUEST_SELECT_IMAGE_IN_ALBUM) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                if (data != null) {
                    val contentURI = data.data
                    try {
                        //val bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), contentURI)
                        //val path = saveImage(bitmap)
                        image_uri=contentURI
                        uploadFile(image_uri)
                        //
                        //Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
                        //  profileImageView.setImageBitmap(bitmap)

                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(activity, "Failed!", Toast.LENGTH_SHORT).show()
                    }

                }
            }
        }
    }


    private fun uploadFile(file: Uri?) {
        roomPhotoUpdatePresenterImpl.upadteRoomPhotoDetails(file!!)
    }

    //only for main user
    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        var path = android.os.Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY


        val wallpaperDirectory = File(path)
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }

        try {
            val f = File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis().toString() + ".jpg")
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(activity,
                    arrayOf(f.getPath()),
                    arrayOf("image/jpeg"), null)
            fo.close()

            image_uri=Uri.fromFile(f)
            uploadFile(image_uri)
            // uploadFile(Uri.fromFile(f))
            // Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }



    internal val onRetryRoomPhoto: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            uploadFile(image_uri)
        }

    }

    internal val onRetryFeaturedPhotoUpdate: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            updateFeaturedPhoto()
        }

    }
    internal val onRetryDeletePhoto: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            deletePhoto()
        }

    }



}
