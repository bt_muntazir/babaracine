package com.babaracine.mvp.manage_room.adapter

import android.app.Activity

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton

import java.util.ArrayList
import com.babaracine.R
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.mvp.manage_room.model.SelectedAmenityModel
import com.babaracine.mvp.manage_room.presenter.BasicDataModel
import com.babaracine.mvp.room_detail.presenter.RoomDetailModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_amenities_manage_room.view.*
import kotlin.concurrent.timer


/**
 * Created by Braintech on 7/4/2018.
 */
class AmenitiesAdapter() : RecyclerView.Adapter<AmenitiesAdapter.ViewHolder>() {

    var context: Context? = null
    var arrayListAmenity: ArrayList<BasicDataModel.Data.Amenities.AmenityList>? = null
    var arrayListSelectedAmenities: ArrayList<SelectedAmenityModel.Data.RoomAmenity>? = null
    var reviewStep: Int = 0

    constructor(context: Context, arrayListAmenity: ArrayList<BasicDataModel.Data.Amenities.AmenityList>, arrayListSelectedAmenities: ArrayList<SelectedAmenityModel.Data.RoomAmenity>, reviewStep: Int) : this() {
        this.context = context
        this.arrayListAmenity = arrayListAmenity
        this.arrayListSelectedAmenities = arrayListSelectedAmenities
        this.reviewStep = reviewStep
    }

    constructor(context: Context, arrayListAmenity: ArrayList<BasicDataModel.Data.Amenities.AmenityList>, reviewStep: Int) : this() {
        this.context = context
        this.arrayListAmenity = arrayListAmenity
        this.reviewStep = reviewStep
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_amenities_manage_room, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val isFromManageRoom = LoginManagerSession.getIsManageFrom()

        if (isFromManageRoom) {
            if (reviewStep > 3) {
                val dataModel = arrayListSelectedAmenities!![position]

                val itemName = dataModel.amenityName!!

                holder.txtViewAmenitiesItemName!!.text = itemName

                if (dataModel.selected == 1) {
                    holder.checkBoxAmenitiesItem.isChecked = true
                }

                holder.checkBoxAmenitiesItem.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        if (isChecked) {
                            arrayListSelectedAmenities!![position].selected = 1
                        } else {
                            arrayListSelectedAmenities!![position].selected = 0
                        }
                    }
                })


            } else {

                val dataModel = arrayListAmenity!![position]

                val itemName = dataModel.amenityName!!

                holder.txtViewAmenitiesItemName!!.text = itemName

                holder.checkBoxAmenitiesItem.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
                    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                        if (isChecked) {
                            arrayListAmenity!![position].selected = 1
                        } else {
                            arrayListAmenity!![position].selected = 0
                        }
                    }
                })
            }
        } else {

            val dataModel = arrayListAmenity!![position]

            val itemName = dataModel.amenityName!!

            holder.txtViewAmenitiesItemName!!.text = itemName

            holder.checkBoxAmenitiesItem.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    if (isChecked) {
                        arrayListAmenity!![position].selected = 1
                    } else {
                        arrayListAmenity!![position].selected = 0
                    }
                }
            })
        }
    }


    override fun getItemCount(): Int {
        if (reviewStep > 3) {
            return arrayListSelectedAmenities!!.size
        }
        return arrayListAmenity!!.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val checkBoxAmenitiesItem = itemView.checkBoxAmenitiesItem
        val txtViewAmenitiesItemName = itemView.txtViewAmenitiesItemName

    }


}