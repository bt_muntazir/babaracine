package com.babaracine.mvp.manage_room.fragments

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.manage_room.adapter.DisableRoomListAdapter
import com.babaracine.mvp.manage_room.presenter_disable_room.DisableRoomContractor
import com.babaracine.mvp.manage_room.presenter_disable_room.DisableRoomListModel
import com.babaracine.mvp.manage_room.presenter_disable_room.DisableRoomPresenterImpl
import kotlinx.android.synthetic.main.fragment_disable_rooms.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class DisableRoomsFragment : Fragment() , DisableRoomContractor.DisableRoomView {

    lateinit var arrayListDisableRoom: ArrayList<DisableRoomListModel.Datum>
    lateinit var disableRoomPresenterImpl: DisableRoomPresenterImpl
    var itemId : Int = 0
    var startDate : String = ""
    var endDate : String = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_disable_rooms, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arrayListDisableRoom = ArrayList()
        disableRoomPresenterImpl = DisableRoomPresenterImpl(activity!!, this)
        manageClickEvents()
        setLayoutManager()
        getIntentData()

    }

    override fun onResume() {
        super.onResume()

    }

    private fun getIntentData() {
        if(arguments != null){
            arrayListDisableRoom = arguments!!.getSerializable(ConstIntent.KEY_ARRAY_LIST_DISABLE_ROOM
            ) as ArrayList<DisableRoomListModel.Datum>

            if(arrayListDisableRoom!=null && arrayListDisableRoom.size>0){
                setAdapter()
            }

        }

    }

    private fun manageClickEvents() {
        txtViewStartDate.setOnClickListener(View.OnClickListener { openStartDatePicker() })
        txtViewEndDate.setOnClickListener(View.OnClickListener {

            if(Utils.isEmptyOrNull(startDate)){
            AlertDialogHelper.showMessage("please first select start date", activity!!)
            }else{
            openEndDatePicker()
            } })
        btnDisableRoom.setOnClickListener(View.OnClickListener {

            onClickBtnDisableRoom() })
    }

    override fun onDisableRoomListSuccess(disableRoomListModel: DisableRoomListModel) {
        this.arrayListDisableRoom = disableRoomListModel.data!!
        setAdapter()
    }

    override fun onDisableRoomListUnSuccess(message: String) {
        arrayListDisableRoom = ArrayList()
        setAdapter()
    }

    override fun onDisableRoomListInternetError() {
        SnackNotify.checkConnection(onRetryDisableRoomList,relLayDisableRoom)
    }

    override fun onDeleteDisableRoomListItemSuccess(commonModel: CommonModel) {
        /*this.arrayListDisableRoom = disableRoomListModel.data!!
        setAdapter()*/
        showMessageAfterSuccessfullyDelete(commonModel.message!!)
    }

    override fun onDeleteDisableRoomListItemUnSuccess(message: String) {
        SnackNotify.showMessage(message,activity!!,relLayDisableRoom)
    }

    override fun onDeleteDisableRoomListItemInternetError() {
        SnackNotify.checkConnection(onRetryDeleteItem,relLayDisableRoom)
    }

    override fun onAddDisableRoomFromServerSuccess(disableRoomListModel: DisableRoomListModel) {
        this.arrayListDisableRoom = disableRoomListModel.data!!
        setAdapter()
    }

    override fun onAddDisableRoomFromServerUnSuccess(message: String) {
        SnackNotify.showMessage(message,activity!!,relLayDisableRoom)
    }

    override fun onAddDisableRoomFromServerInternetError() {
        SnackNotify.checkConnection(onRetryAddDisableRoom,relLayDisableRoom)
    }

    private fun showMessageAfterSuccessfullyDelete(msg: String) {

        val alertDialoge = AlertDialog.Builder(context)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(activity!!.getString(R.string.title_ok)) { dialog, which ->
            getDisableRoomResult()
            dialog.dismiss()
        }

        alertDialoge.show()


    }



    fun openStartDatePicker() {

        Utils.hideKeyboardIfOpen(activity!!)
        // Get Current Date
        val calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)


        val datePickerDialog = DatePickerDialog(activity!!,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val calendar = Calendar.getInstance()
                    calendar.set(year, monthOfYear, dayOfMonth)

                    val format = SimpleDateFormat("yyyy-MM-dd")

                    startDate = format.format(calendar.time)

                    txtViewStartDate.text = startDate
                    /*val dateArray = startDate.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                    val years = Integer.parseInt(dateArray[0])
                    val month = Integer.parseInt(dateArray[1])
                    val date = Integer.parseInt(dateArray[2])
                    startingMonthName = arrayListMonthsName[month-1]
                    openEndDatePicker(date)*/
                    //Log.e("startDate",startDate)
                    //txtViewDobDate.text = format.format(calendar.time)
                    //dobUpdatedDate = format.format(calendar.time)


                    //txtViewDatePicker.setText(rideDate)
                }, mYear, mMonth, mDay)

        datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        datePickerDialog.setTitle("Start Date")
        datePickerDialog.show()


    }

     fun openEndDatePicker() {

         Utils.hideKeyboardIfOpen(activity!!)
         // Get Current Date
         val calendar = Calendar.getInstance()
         val mYear = calendar.get(Calendar.YEAR)
         val mMonth = calendar.get(Calendar.MONTH)
         val mDay = calendar.get(Calendar.DAY_OF_MONTH)
         var numberOfDays: String = "0"

         val datePickerDialog = DatePickerDialog(activity!!,
                 DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                     val calendar = Calendar.getInstance()
                     calendar.set(year, monthOfYear, dayOfMonth)

                     val format = SimpleDateFormat("yyyy-MM-dd")

                     endDate = format.format(calendar.time)


                     /*val dateArray = endDate.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                     val years = Integer.parseInt(dateArray[0])
                     val month = Integer.parseInt(dateArray[1])
                     val date = Integer.parseInt(dateArray[2])
                     endingMonthName = arrayListMonthsName[month - 1]
                     val monthName = endingMonthName
                     val completeDate = startDate.toString() + "-" + date.toString() + " " + monthName
                     */txtViewEndDate.text = endDate


 //txtViewDatePicker.setText(rideDate)
                 }, mYear, mMonth, mDay)

         calendar.add(Calendar.DAY_OF_YEAR, 1)
         val tomorrow = calendar.time

         val myFormat = SimpleDateFormat("yyyy-MM-dd")
         var number: Int = 0
         try {
             val currentDate = Calendar.getInstance().time
             val date1 = myFormat.parse(this.startDate)
             val tomorrowDate = myFormat.format(tomorrow)
             if (this.startDate.equals(tomorrowDate)) {
                 number = 1
             } else {
                 val diff = date1.getTime() - currentDate.getTime()
                 numberOfDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toString()
                 if (numberOfDays.toInt() == 0) {
                     number = 0
                 } else {
                     number = numberOfDays.toInt() + 1
                 }
             }
         } catch (e: ParseException) {
             e.printStackTrace()
         }


         datePickerDialog.datePicker.minDate = (System.currentTimeMillis() + (number.toLong() * 24 * 60 * 60 * 1000))
         datePickerDialog.setTitle("End Date")
         datePickerDialog.show()


     }




    private fun onClickBtnDisableRoom(){

        val description = edtTextDescription.text.toString()
        if(isValidate(startDate,endDate,description)){
            disableRoomPresenterImpl.addDisableRoomDate(startDate,endDate,description)
        }

    }

    private fun isValidate(startDate: String, endDate: String, description: String): Boolean {

        if(Utils.isEmptyOrNull(startDate)){
            AlertDialogHelper.showMessage("Please select start date.",activity!!)
            return false
        }else if(Utils.isEmptyOrNull(endDate)){
            AlertDialogHelper.showMessage("Please select end date.",activity!!)
            return false
        }else if(Utils.isEmptyOrNull(description)){
            AlertDialogHelper.showMessage("Please enter description.",activity!!)
            return false
        }

        return true
    }

    private fun setLayoutManager() {
        val linearLayoutManager = LinearLayoutManager(activity)
        recyclerViewDisableItemList.layoutManager = linearLayoutManager
    }


    private fun getDisableRoomResult(){
        disableRoomPresenterImpl.getDisableRoomList()
    }

    private fun callItemDeleteApi(){
        disableRoomPresenterImpl.deleteDisableRoomListItem(itemId)
    }

    private fun setAdapter() {
        val disableRoomAdapter = DisableRoomListAdapter(activity!!,arrayListDisableRoom,this)
        recyclerViewDisableItemList.setAdapter(disableRoomAdapter)
    }

    fun getDisableRoomItemIdForDelete(itemId : Int){
        this.itemId = itemId
        callItemDeleteApi()
    }

    internal val onRetryDisableRoomList: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getDisableRoomResult()
        }

    }
    internal val onRetryDeleteItem: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            callItemDeleteApi()
        }

    }
    internal val onRetryAddDisableRoom: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getDisableRoomResult()
        }

    }



}
