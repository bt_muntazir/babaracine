package com.babaracine.mvp.manage_room.presenter_disable_room

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

import java.util.ArrayList

class DisableRoomListModel : Serializable{

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: ArrayList<Datum>? = null

    inner class Datum : Serializable{

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("room_id")
        @Expose
        var roomId: Int? = null
        @SerializedName("start_date")
        @Expose
        var startDate: String? = null
        @SerializedName("end_date")
        @Expose
        var endDate: String? = null
        @SerializedName("description")
        @Expose
        var description: String? = null

    }

}
