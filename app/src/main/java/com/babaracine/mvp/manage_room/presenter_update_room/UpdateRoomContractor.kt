package com.babaracine.mvp.manage_room.presenter_update_room

import android.net.Uri
import com.babaracine.mvp.manage_room.model.SelectedRoomPhotoModel
import com.babaracine.mvp.manage_room.model.UpdateBasicDetailModel
import org.json.JSONArray

interface UpdateRoomContractor {

    interface UpdateBasicDetailPresenter{

        fun updateBasicDetails(roomId : String, bedrooms : String,beds : String,bedType : String, bathrooms : String,propertyTypeId : String,roomTypeId : String,maxOccupants : String,name : String,description : String,suitableFor : String, translation : String)
        fun updateRoomLocationDetails(address1 : String, address2: String,city : String,state : String,postal : String,country : String,latitude : String,longitude : String)
        fun updateRoomPolicyDetails(policy : String,translationObject : String)
        fun updateAmenitiesDetail(jsonArraySelectedAmenities: JSONArray)
        fun upadteRoomProcingDetails(currencyCode : String,nightlyPrice : String,minimumDat : String,longTermNightlyPrice : String,taxName : String,taxPercentage : String,depositPercentage : String,cancellationType : String,cancellationCharge : String,cancellationDescription : String)

    }

    interface UpdateBasicDetailView{

        fun onUpdateBasicDetailSuccess(updateBasicDetailModel: UpdateBasicDetailModel)
        fun onUpdateBasicDetailUnSuccess(message : String)
        fun onUpdateBasicDetailInternetError()

        fun onLocationUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel)
        fun onLocationUpdateUnSuccess(message : String)
        fun onLocationUpdateInternetError()

        fun onPolicyUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel)
        fun onPolicyUpdateUnSuccess(message : String)
        fun onPolicyUpdateInternetError()

        fun onAmenitiesUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel)
        fun onAmenitiesUpdateUnSuccess(message : String)
        fun onAmenitiesUpdateInternetError()

        fun onPricingUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel)
        fun onPricingUpdateUnSuccess(message : String)
        fun onPricing1UpdateInternetError()

    }


}