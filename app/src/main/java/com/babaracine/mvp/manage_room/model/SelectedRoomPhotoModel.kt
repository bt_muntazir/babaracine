package com.babaracine.mvp.manage_room.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

import java.util.ArrayList

class SelectedRoomPhotoModel : Serializable {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data : Serializable{

        @SerializedName("rooms")
        @Expose
        var rooms: Rooms? = null
        @SerializedName("room_photos")
        @Expose
        var roomPhotos: ArrayList<RoomPhoto>? = null

        inner class RoomPhoto : Serializable{

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("room_id")
            @Expose
            var roomId: Int? = null
            @SerializedName("name")
            @Expose
            var name: String? = null
            @SerializedName("featured")
            @Expose
            var featured: Int? = null
            @SerializedName("medium_thumb_image_url")
            @Expose
            var mediumThumbImageUrl: String? = null
            @SerializedName("thumb_image_url")
            @Expose
            var thumbImageUrl: String? = null
            @SerializedName("image_url")
            @Expose
            var imageUrl: String? = null

        }


        inner class Rooms : Serializable{

            @SerializedName("steps_completed")
            @Expose
            var stepsCompleted: Int? = null

        }

    }

}

