package com.babaracine.mvp.manage_room.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

import java.util.ArrayList

class PricingDetailModel : Serializable {


    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data : Serializable{

        @SerializedName("rooms")
        @Expose
        var rooms: Rooms? = null
        @SerializedName("pricing")
        @Expose
        var pricing: ArrayList<Pricing>? = null

        inner class Pricing : Serializable{

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("currency_code")
            @Expose
            var currencyCode: String? = null
            @SerializedName("room_id")
            @Expose
            var roomId: Int? = null
            @SerializedName("nightly_price")
            @Expose
            var nightlyPrice: Int? = null
            @SerializedName("minimum_day")
            @Expose
            var minimumDay: Int? = null
            @SerializedName("long_term_nightly_price")
            @Expose
            var longTermNightlyPrice: Int? = null
            @SerializedName("deposit_frequency")
            @Expose
            var depositFrequency: String? = null
            @SerializedName("deposit_percent")
            @Expose
            var depositPercent: Int? = null
            @SerializedName("deposit_enable")
            @Expose
            var depositEnable: String? = null
            @SerializedName("cancellation_type")
            @Expose
            var cancellationType: String? = null
            @SerializedName("cancellation_charge")
            @Expose
            var cancellationCharge: String? = null
            @SerializedName("cancellation_desc")
            @Expose
            var cancellationDesc: String? = null
            @SerializedName("tax_name")
            @Expose
            var taxName: String? = null
            @SerializedName("tax_percentage")
            @Expose
            var taxPercentage: Int? = null
            @SerializedName("deleted_at")
            @Expose
            var deletedAt: Any? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null

        }


        inner class Rooms : Serializable{

            @SerializedName("steps_completed")
            @Expose
            var stepsCompleted: Int? = null

        }

    }

}
