package com.babaracine.mvp.manage_room.fragments

import android.app.AlertDialog
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.babaracine.R
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.mvp.manage_room.presenter.BasicLocationAndPolicyModel
import kotlinx.android.synthetic.main.fragment_policy.*
import android.text.TextUtils
import android.text.Html
import android.text.Spanned
import android.util.Log
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.manage_room.ManageRoomActivity
import com.babaracine.mvp.manage_room.model.UpdateBasicDetailModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomContractor
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomDetailModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomPresenterImpl
import com.google.gson.Gson
import org.jsoup.Jsoup





class PolicyFragment : Fragment() , UpdateRoomContractor.UpdateBasicDetailView {



    lateinit var updateRoomDetailModel: UpdateRoomDetailModel

    lateinit var updateRoomPresenterImpl: UpdateRoomPresenterImpl

    lateinit var policyData : String

    lateinit var basicLocationAndPolicyModel : BasicLocationAndPolicyModel.Data
    var reviewStep : Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_policy, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getIntentData()
        updateRoomPresenterImpl = UpdateRoomPresenterImpl(activity!!,this)
        manageClickEvent()
        val isFromManageRoom = LoginManagerSession.getIsManageFrom()
        if(isFromManageRoom) {
            if (reviewStep > 2) {
                setData()
            }
        }
    }

    override fun onPolicyUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        this.updateRoomDetailModel = updateRoomDetailModel
        reviewStep = updateRoomDetailModel.data!!.rooms!!.stepsCompleted!!
        showMessageAfterRoomUpdate(updateRoomDetailModel.message!!)
    }

    fun showMessageAfterRoomUpdate(msg: String) {

        val alertDialoge = AlertDialog.Builder(activity)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(activity!!.getString(R.string.title_ok)) { dialog, which ->

            (activity as ManageRoomActivity).setTabEnableOrDisable(reviewStep+1)
        }
        alertDialoge.show()
    }

    override fun onPolicyUpdateUnSuccess(message: String) {
        SnackNotify.showMessage(message,activity!!,relLayPolicy)
    }

    override fun onPolicyUpdateInternetError() {
        SnackNotify.checkConnection(onRetryPolicyUpdate,relLayPolicy)
    }

    override fun onUpdateBasicDetailSuccess(updateBasicDetailModel: UpdateBasicDetailModel) {
        //no need to implement
    }

    override fun onPricingUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        //no need to implement
    }

    override fun onPricingUpdateUnSuccess(message: String) {
        //no need to implement
    }

    override fun onPricing1UpdateInternetError() {
        //no need to implement
    }

    override fun onAmenitiesUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        // no need to implement
    }

    override fun onAmenitiesUpdateUnSuccess(message: String) {
        // no need to implement
    }

    override fun onAmenitiesUpdateInternetError() {
        // no need to implement
    }


    override fun onUpdateBasicDetailUnSuccess(message: String) {
        //no need to implement
    }

    override fun onUpdateBasicDetailInternetError() {
        //no need to implement
    }

    override fun onLocationUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        //no need to implement
    }

    override fun onLocationUpdateUnSuccess(message: String) {
        //no need to implement
    }

    override fun onLocationUpdateInternetError() {
        //no need to implement
    }

    private fun getIntentData(){


        if(arguments != null) {
            reviewStep = arguments!!.getInt(ConstIntent.KEY_REVIEW_STEPS)
            val isFromManageRoom = LoginManagerSession.getIsManageFrom()
            if(isFromManageRoom) {
                if (reviewStep > 2) {
                    basicLocationAndPolicyModel = arguments!!.getSerializable(ConstIntent.KEY_BASIC_DETAIL_FRAGMENT) as BasicLocationAndPolicyModel.Data
                }
            }
        }
    }

    private fun manageClickEvent() {
        btnUpdateRoomPolicy.setOnClickListener(View.OnClickListener { updateRoomPolicy() })
    }

    private fun updateRoomPolicy() {
        policyData = edtTextPolicy.text.toString()

        val isFromManageRoom = LoginManagerSession.getIsManageFrom()

        if(isValidate()) {
            val htmlString = TextUtils.htmlEncode(policyData)
            updateRoomPresenterImpl.updateRoomPolicyDetails(htmlString,"")
        }
        val htmlString = TextUtils.htmlEncode(policyData)


    }

    private fun isValidate(): Boolean {

        if(Utils.isEmptyOrNull(policyData)){
            edtTextPolicy.setError(getString(R.string.empty_room_policy))
            edtTextPolicy.requestFocus()
            return false
        }
        return true

    }

    private fun setData() {

        val policyTextFromHtml = Jsoup.parse(basicLocationAndPolicyModel.policy!!).text()
        edtTextPolicy.setText(policyTextFromHtml)

    }

    internal val onRetryPolicyUpdate: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            updateRoomPolicy()
        }

    }
}
