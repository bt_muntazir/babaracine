package com.babaracine.mvp.manage_room.presenter_update_room

import android.app.Activity
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.manage_room.model.UpdateBasicDetailModel
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class UpdateRoomPresenterImpl(val activity: Activity, val updateBasicDetailView: UpdateRoomContractor.UpdateBasicDetailView) : UpdateRoomContractor.UpdateBasicDetailPresenter{
    override fun upadteRoomProcingDetails(currencyCode: String, nightlyPrice: String, minimumDate: String, longTermNightlyPrice: String, taxName: String, taxPercentage: String, depositPercentage: String, cancellationType: String, cancellationCharge: String, cancellationDescription: String) {
        try {
            ApiAdapter.getInstance(activity)
            pricingDetailUpdate(currencyCode,nightlyPrice,minimumDate,longTermNightlyPrice,taxName,taxPercentage,depositPercentage,cancellationType,cancellationCharge,cancellationDescription)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            updateBasicDetailView.onPricing1UpdateInternetError()
        }
    }

    private fun pricingDetailUpdate(currencyCode: String, nightlyPrice: String, minimumDate: String, longTermNightlyPrice: String, taxName: String, taxPercentage: String, depositPercentage: String, cancellationType: String, cancellationCharge: String, cancellationDescription: String) {
        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }
            jsonObject.put(Const.PARAM_CURRENCY_CODE,currencyCode)
            jsonObject.put(Const.PARAM_NIGHTLY_PRICE,nightlyPrice)
            jsonObject.put(Const.PARAM_MINIMUM_DAY,minimumDate)
            jsonObject.put(Const.PARAM_LONG_TERM_NIGHTLY_PRICE,longTermNightlyPrice)
            jsonObject.put(Const.PARAM_TAX_NAME,taxName)
            jsonObject.put(Const.PARAM_TAX_PERCENTAGE,taxPercentage)
            jsonObject.put(Const.PARAM_DEPOSIT_PERCENTAGE,depositPercentage)
            jsonObject.put(Const.PARAM_CANCELLATION_TYPE,cancellationType)
            jsonObject.put(Const.PARAM_CANCELLATION_CHARGE,cancellationCharge)
            jsonObject.put(Const.PARAM_CANCELLATION_DESC,cancellationDescription)
        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getRoomDetailResult = ApiAdapter.getApiService()!!.updatePricingDetail("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomDetailResult.enqueue(object : retrofit2.Callback<UpdateRoomDetailModel>{

            override fun onResponse(call: Call<UpdateRoomDetailModel>?, response: Response<UpdateRoomDetailModel>?) {

                Progress.stop()

                try {
                    val updateRoomDetailModel = response!!.body()
                    val message = updateRoomDetailModel.message!!
                    if(updateRoomDetailModel.status == 1){
                        updateBasicDetailView.onPricingUpdateSuccess(updateRoomDetailModel)
                    }else if(updateRoomDetailModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        updateBasicDetailView.onPricingUpdateUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    updateBasicDetailView.onPricingUpdateUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<UpdateRoomDetailModel>?, t: Throwable?) {
                Progress.stop()
                updateBasicDetailView.onPricingUpdateUnSuccess(activity.getString(R.string.error_server))
            }
        })


    }

    override fun updateAmenitiesDetail(jsonArraySelectedAmenities: JSONArray) {
        try {
            ApiAdapter.getInstance(activity)
            amenityUpdate(jsonArraySelectedAmenities)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            updateBasicDetailView.onAmenitiesUpdateInternetError()
        }
    }

    private fun amenityUpdate(jsonArraySelectedAmenities: JSONArray) {
        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }
            jsonObject.put(Const.PARAM_ARRAYLIST_SELECTED_AMENITY,jsonArraySelectedAmenities)
        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getRoomDetailResult = ApiAdapter.getApiService()!!.updateAmenityDetails("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomDetailResult.enqueue(object : retrofit2.Callback<UpdateRoomDetailModel>{

            override fun onResponse(call: Call<UpdateRoomDetailModel>?, response: Response<UpdateRoomDetailModel>?) {

                Progress.stop()

                try {
                    val updateRoomDetailModel = response!!.body()
                    val message = updateRoomDetailModel.message!!
                    if(updateRoomDetailModel.status == 1){
                        updateBasicDetailView.onAmenitiesUpdateSuccess(updateRoomDetailModel)
                    }else if(updateRoomDetailModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        updateBasicDetailView.onAmenitiesUpdateUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    updateBasicDetailView.onAmenitiesUpdateUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<UpdateRoomDetailModel>?, t: Throwable?) {
                Progress.stop()
                updateBasicDetailView.onAmenitiesUpdateUnSuccess(activity.getString(R.string.error_server))
            }
        })



    }

    override fun updateRoomPolicyDetails(policy: String, translationObject: String) {
        try {
            ApiAdapter.getInstance(activity)
            policyUpdate(policy, translationObject)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            updateBasicDetailView.onPolicyUpdateInternetError()
        }
    }

    private fun policyUpdate(policy: String, translationObject: String) {


        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }
            jsonObject.put(Const.PARAM_POLICY,policy)
                //jsonObject.put(Const.PARAM_TRANSLATIONS, JSONObject(translationObject))

        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getRoomDetailResult = ApiAdapter.getApiService()!!.updatePolicyDetails("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomDetailResult.enqueue(object : retrofit2.Callback<UpdateRoomDetailModel>{

            override fun onResponse(call: Call<UpdateRoomDetailModel>?, response: Response<UpdateRoomDetailModel>?) {

                Progress.stop()

                try {
                    val updateRoomDetailModel = response!!.body()
                    val message = updateRoomDetailModel.message!!
                    if(updateRoomDetailModel.status == 1){
                        updateBasicDetailView.onPolicyUpdateSuccess(updateRoomDetailModel)
                    }else if(updateRoomDetailModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        updateBasicDetailView.onPolicyUpdateUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    updateBasicDetailView.onPolicyUpdateUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<UpdateRoomDetailModel>?, t: Throwable?) {
                Progress.stop()
                updateBasicDetailView.onPolicyUpdateUnSuccess(activity.getString(R.string.error_server))
            }
        })





    }

    override fun updateRoomLocationDetails(address1: String, address2: String, city: String, state: String, postal: String, country: String, latitude: String, longitude: String) {
        try {
            ApiAdapter.getInstance(activity)
            locationUpdate(address1,address2,city,state,postal,country,latitude,longitude)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            updateBasicDetailView.onLocationUpdateInternetError()
        }
    }

    private fun locationUpdate(address1: String, address2: String, city: String, state: String, postal: String, country: String, latitude: String, longitude: String) {


        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()
        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }
            jsonObject.put(Const.PARAM_ADDRESS_1,address1)
            if(!Utils.isEmptyOrNull(address2)) {
                jsonObject.put(Const.PARAM_ADDRESS_2, address2)
            }
            jsonObject.put(Const.PARAM_CITY,city)
            jsonObject.put(Const.PARAM_STATE,state)
            jsonObject.put(Const.PARAM_POSTAL,postal)
            jsonObject.put(Const.PARAM_COUNTRY,country)
            jsonObject.put(Const.PARAM_LATITUDE,latitude)
            jsonObject.put(Const.PARAM_LONGITUDE,longitude)
        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getRoomDetailResult = ApiAdapter.getApiService()!!.updateLocationDetails("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomDetailResult.enqueue(object : retrofit2.Callback<UpdateRoomDetailModel>{

            override fun onResponse(call: Call<UpdateRoomDetailModel>?, response: Response<UpdateRoomDetailModel>?) {

                Progress.stop()

                try {
                    val updateRoomDetailModel = response!!.body()
                    val message = updateRoomDetailModel.message!!
                    if(updateRoomDetailModel.status == 1){
                        updateBasicDetailView.onLocationUpdateSuccess(updateRoomDetailModel)
                    }else if(updateRoomDetailModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        updateBasicDetailView.onLocationUpdateUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    updateBasicDetailView.onLocationUpdateUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<UpdateRoomDetailModel>?, t: Throwable?) {
                Progress.stop()
                updateBasicDetailView.onLocationUpdateUnSuccess(activity.getString(R.string.error_server))
            }
        })





    }

    override fun updateBasicDetails(roomId: String, bedrooms: String, beds: String, bedType: String, bathrooms: String, propertyTypeId: String, roomTypeId: String, maxOccupants: String, name: String, description: String, suitableFor: String, translation: String) {
        try {
            ApiAdapter.getInstance(activity)
            updateBasicDetailFromApi(roomId,bedrooms,beds,bedType,bathrooms,propertyTypeId,roomTypeId,maxOccupants,name,description,suitableFor,translation)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            updateBasicDetailView.onUpdateBasicDetailInternetError()
        }
    }

    private fun updateBasicDetailFromApi(roomId: String, bedrooms: String, beds: String, bedType: String, bathrooms: String, propertyTypeId: String, roomTypeId: String, maxOccupants: String, name: String, description: String, suitableFor: String, translation: String) {

        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }
            jsonObject.put(Const.PARAM_BEDROOMS,bedrooms)
            jsonObject.put(Const.PARAM_BEDS,beds)
            jsonObject.put(Const.PARAM_BED_TYPE_ID,bedType)
            jsonObject.put(Const.PARAM_BATHROOMS,bathrooms)
            jsonObject.put(Const.PARAM_PROPERTY_TYPE_ID,propertyTypeId)
            jsonObject.put(Const.PARAM_ROOM_TYPE_ID,roomTypeId)
            jsonObject.put(Const.PARAM_MAX_OCCUPANTS,maxOccupants)
            jsonObject.put(Const.PARAM_DESCRIPTION,description)
            jsonObject.put(Const.PARAM_SUITABLE_FOR,suitableFor)
            /*if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_TRANSLATIONS,JSONObject(translation))
            }*/
            jsonObject.put(Const.PARAM_NAME,name)

        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getRoomDetailResult = ApiAdapter.getApiService()!!.updateBasicDetailsOfRoom("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomDetailResult.enqueue(object : retrofit2.Callback<UpdateBasicDetailModel>{

            override fun onResponse(call: Call<UpdateBasicDetailModel>?, response: Response<UpdateBasicDetailModel>?) {

                Progress.stop()

                try {
                    val updateBasicDetailModel = response!!.body()
                    val message = updateBasicDetailModel.message!!
                    if(updateBasicDetailModel.status == 1){
                        updateBasicDetailView.onUpdateBasicDetailSuccess(updateBasicDetailModel)
                    }else if(updateBasicDetailModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        updateBasicDetailView.onUpdateBasicDetailUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    updateBasicDetailView.onUpdateBasicDetailUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<UpdateBasicDetailModel>?, t: Throwable?) {
                Progress.stop()
                updateBasicDetailView.onUpdateBasicDetailUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }


}