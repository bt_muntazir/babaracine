package com.babaracine.mvp.manage_room.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatSpinner
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView

import com.babaracine.R
import com.babaracine.common.adapter.AdapterSpinner
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.Const
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.manage_room.ManageRoomActivity
import com.babaracine.mvp.manage_room.model.UpdateBasicDetailModel

import com.babaracine.mvp.manage_room.presenter.BasicDataModel
import com.babaracine.mvp.manage_room.presenter.BasicLocationAndPolicyModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomDetailModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomContractor
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomPresenterImpl
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_basic_details.*


class BasicDetailsFragment : Fragment(), UpdateRoomContractor.UpdateBasicDetailView{
    override fun onPricingUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        //no need to implement
    }

    override fun onPricingUpdateUnSuccess(message: String) {
        //no need to implement
    }

    override fun onPricing1UpdateInternetError() {
        //no need to implement
    }

    override fun onAmenitiesUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        //no need to implement
    }

    override fun onAmenitiesUpdateUnSuccess(message: String) {
        //no need to implement
    }

    override fun onAmenitiesUpdateInternetError() {
        //no need to implement
    }

    override fun onPolicyUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        //no need to implement
    }

    override fun onPolicyUpdateUnSuccess(message: String) {
        //no need to implement
    }

    override fun onPolicyUpdateInternetError() {
        //no need to implement
    }

    override fun onLocationUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        // no need to implement
    }

    override fun onLocationUpdateUnSuccess(message: String) {
        // no need to implement
    }

    override fun onLocationUpdateInternetError() {
        // no need to implement
    }

    lateinit var arrayListBedroomsHash: ArrayList<HashMap<String, String>>
    lateinit var arrayListBedrooms: ArrayList<String>

    lateinit var arrayListBedsHash: ArrayList<HashMap<String, String>>
    lateinit var arrayListBeds: ArrayList<String>

    lateinit var arrayListBathroomsHash: ArrayList<HashMap<String, String>>
    lateinit var arrayListBathrooms: ArrayList<String>

    lateinit var arrayListAccomodatesHash: ArrayList<HashMap<String, String>>
    lateinit var arrayListAccomodates: ArrayList<String>

    lateinit var arrayListBedTypeHash: ArrayList<HashMap<String, String>>
    lateinit var arrayListPropertyTypeHash: ArrayList<HashMap<String, String>>
    lateinit var arrayListSuitableForHash: ArrayList<HashMap<String, String>>
    lateinit var arrayListRoomTypeHash: ArrayList<HashMap<String, String>>

    lateinit var basicDataModel : BasicDataModel.Data

    lateinit var selectedBedroomsId : String
    lateinit var selectedBedroomsName : String
    lateinit var selectedBedId : String
    lateinit var selectedBedName : String
    lateinit var selectedBedTypeId : String
    lateinit var selectedBedTypeName : String
    lateinit var selectedBathroomId : String
    lateinit var selectedBathroomName : String
    lateinit var selectedPropertyTypeId : String
    lateinit var selectedPropertyTypeName : String
    lateinit var selectedSuitableForId : String
    lateinit var selectedSuitableForName : String
    lateinit var selectedRoomTypeId : String
    lateinit var selectedRoomTypeName : String
    lateinit var selectedAccommodatesId : String
    lateinit var selectedAccommodatesName : String

    lateinit var basicLocationAndPolicyModel : BasicLocationAndPolicyModel.Data

    lateinit var updateBasicDataPresenterImpl: UpdateRoomPresenterImpl

    lateinit var updateRoomDetailModel: UpdateRoomDetailModel

     var reviewSteps : Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


         // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_basic_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        manageClickEvents()



        bindArrayList()
        updateBasicDataPresenterImpl = UpdateRoomPresenterImpl(activity!!, this)
        setBedroomsAdapter()

        getIntentData()

        setDynamicAdapter()
        val isFromManageRoom = LoginManagerSession.getIsManageFrom()
        if(isFromManageRoom){
            setAdapterValue()
        }


    }

    private fun getIntentData() {
        if(arguments != null){
            basicDataModel = arguments!!.getSerializable(ConstIntent.KEY_BASIC_DATA_FRAGMENT) as BasicDataModel.Data
            val isFromManageRoom = LoginManagerSession.getIsManageFrom()
            if(isFromManageRoom) {
                basicLocationAndPolicyModel = arguments!!.getSerializable(ConstIntent.KEY_BASIC_DETAIL_FRAGMENT) as BasicLocationAndPolicyModel.Data
            }

        }
    }

    override fun onUpdateBasicDetailSuccess(updateBasicDetailModel: UpdateBasicDetailModel) {
        //this.updateRoomDetailModel = updateRoomDetailModel
        reviewSteps = updateBasicDetailModel.data!!.rooms!!.stepsCompleted!!
        val roomId = LoginManagerSession.getRoomIdForManageRoom()
        if(roomId.equals("0")){
            LoginManagerSession.setRoomIdForManageRoom(updateBasicDetailModel.data!!.rooms!!.id!!.toString())
        }
        //SnackNotify.showMessage(updateRoomDetailModel.message!!,activity!!,relLayBasicDetail)
        showMessage(updateBasicDetailModel.message!!)

    }

    fun showMessage(msg: String) {

        val alertDialoge = AlertDialog.Builder(activity)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(activity!!.getString(R.string.title_ok)) { dialog, which ->

            (activity as ManageRoomActivity).setTabEnableOrDisable(reviewSteps+1)
        }

        alertDialoge.show()


    }


    override fun onUpdateBasicDetailUnSuccess(message: String) {
        SnackNotify.showMessage(message,activity!!,relLayBasicDetail)
    }

    override fun onUpdateBasicDetailInternetError() {
        SnackNotify.checkConnection(onRetryUpdateBasicDetails, relLayBasicDetail)
    }


    private fun setAdapterValue() {

        selectedBedroomsName = basicLocationAndPolicyModel.bedrooms!!.toString()
        selectedBedName = basicLocationAndPolicyModel.beds!!.toString()

        selectedBedTypeId = basicLocationAndPolicyModel.bedTypeId!!.toString()
        selectedBathroomName = basicLocationAndPolicyModel.bathrooms!!.toString()

        selectedPropertyTypeId = basicLocationAndPolicyModel.propertyTypeId!!.toString()

        selectedSuitableForName = basicLocationAndPolicyModel.suitableFor!!.toString()

        selectedRoomTypeId = basicLocationAndPolicyModel.roomTypeId!!.toString()

        selectedAccommodatesName = basicLocationAndPolicyModel.maxOccupants!!.toString()
        spinnerBedrooms.setSelection(getIndexWithValue(spinnerBedrooms, selectedBedroomsName))
        spinnerBeds.setSelection(getIndexWithValue(spinnerBeds,selectedBedName ))
        spinnerBedType.setSelection(getIndexWithId(spinnerBedType, selectedBedTypeId))
        spinnerBathrooms.setSelection(getIndexWithValue(spinnerBathrooms, selectedBathroomName))
        spinnerPropertyType.setSelection(getIndexWithId(spinnerPropertyType, selectedPropertyTypeId))
        spinnerSuitableFor.setSelection(getIndexWithId(spinnerSuitableFor,selectedSuitableForName ))
        spinnerRoomType.setSelection(getIndexWithId(spinnerRoomType, selectedRoomTypeId))
        spinnerAccommodates.setSelection(getIndexWithId(spinnerAccommodates, selectedAccommodatesName))

        val listingName = basicLocationAndPolicyModel.name!!
        val descriptionListing = basicLocationAndPolicyModel.description!!
        edtTextDescListingName.setText(listingName)
        edtTextDescription.setText(descriptionListing)

    }

    fun getIndexWithValue(spinner : AppCompatSpinner,compare_value : String ) : Int {

        for (i in 0 until spinner.count){
            if(spinner.getItemAtPosition(i).toString().equals(compare_value,true)){
                return i
            }

        }
        return 0
    }

    fun getIndexWithId(spinner : AppCompatSpinner,compare_value : String ) : Int{

        for (i in 0 until spinner.count){
            if(spinner.getItemIdAtPosition(i).toString().equals(compare_value,true)){
                return i
            }

        }
        return 0
    }

    private fun manageClickEvents() {
        spinnerBedrooms?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedBedroomsId = arrayListBedroomsHash.get(position).get(Const.KEY_ID).toString()
                selectedBedroomsName = arrayListBedroomsHash.get(position).get(Const.KEY_NAME).toString()
            }
        }

        spinnerBeds?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedBedId = arrayListBedsHash.get(position).get(Const.KEY_ID).toString()
                selectedBedName = arrayListBedsHash.get(position).get(Const.KEY_NAME).toString()
            }
        }

        spinnerBedType?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedBedTypeId = arrayListBedTypeHash.get(position).get(Const.KEY_ID).toString()
                selectedBedTypeName = arrayListBedTypeHash.get(position).get(Const.KEY_NAME).toString()
            }
        }

        spinnerBathrooms?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedBathroomId = arrayListBathroomsHash.get(position).get(Const.KEY_ID).toString()
                selectedBathroomName = arrayListBathroomsHash.get(position).get(Const.KEY_NAME).toString()
            }
        }

        spinnerPropertyType?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedPropertyTypeId = arrayListPropertyTypeHash.get(position).get(Const.KEY_ID).toString()
                selectedPropertyTypeName = arrayListPropertyTypeHash.get(position).get(Const.KEY_NAME).toString()
            }
        }

        spinnerSuitableFor?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedSuitableForId = arrayListSuitableForHash.get(position).get(Const.KEY_ID).toString()
                selectedSuitableForName = arrayListSuitableForHash.get(position).get(Const.KEY_NAME).toString()
            }
        }

        spinnerRoomType?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedRoomTypeId = arrayListRoomTypeHash.get(position).get(Const.KEY_ID).toString()
                selectedRoomTypeName = arrayListRoomTypeHash.get(position).get(Const.KEY_NAME).toString()
            }
        }

        spinnerAccommodates?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedAccommodatesId = arrayListAccomodatesHash.get(position).get(Const.KEY_ID).toString()
                selectedAccommodatesName = arrayListAccomodatesHash.get(position).get(Const.KEY_NAME).toString()
            }
        }


        btnUpdateRoomBasicDetails.setOnClickListener(View.OnClickListener { onClickBtnUpdateBasicDetails() })

    }

    private fun onClickBtnUpdateBasicDetails() {
        val listingName = edtTextDescListingName.text.toString()
        val listingDesc = edtTextDescription.text.toString()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()

        var translationJaon = ""
        if(!roomId.equals("0")) {
            translationJaon = Gson().toJson(basicLocationAndPolicyModel.translations)
        }
        //Log.e("json",translationJaon)
       /* val jsonObjectEs = JSONObject()
        jsonObjectEs.put("description","g")
        jsonObjectEs.put("name","d")
        jsonObjectEs.put("locale","d")

        val jsonObjectFr = JSONObject()
        jsonObjectFr.put("description","g")
        jsonObjectFr.put("name","d")
        jsonObjectFr.put("locale","d")

        val jsonObjectTranslation = JSONObject()
        jsonObjectTranslation.put("es",jsonObjectEs)
        jsonObjectTranslation.put("fr",jsonObjectFr)*/

        if(isValidate(listingName,listingDesc)){
            updateBasicDataPresenterImpl.updateBasicDetails(roomId,selectedBedroomsName,selectedBedName,selectedBedTypeId,selectedBathroomName,selectedPropertyTypeId,selectedRoomTypeId,selectedAccommodatesName,listingName,listingDesc,selectedSuitableForName,translationJaon!!)
        }
    }

    private fun isValidate(listingName: String, listingDesc: String): Boolean {

        if(selectedBedroomsName.equals(getString(R.string.select_bedrooms),true)){
            SnackNotify.showMessage(getString(R.string.please_select_bedrooms),activity!!,relLayBasicDetail)
            return false
        }else if(selectedBedName.equals(getString(R.string.select_no_of_beds),true)){
            SnackNotify.showMessage(getString(R.string.please_select_beds),activity!!,relLayBasicDetail)
            return false
        }else if(selectedBedTypeId.equals("0",true)){
            SnackNotify.showMessage(getString(R.string.please_select_bed_type),activity!!,relLayBasicDetail)
            return false
        }else if(selectedBathroomName.equals(getString(R.string.select_no_of_bathrooms),true)){
            SnackNotify.showMessage(getString(R.string.please_select_bathroom),activity!!,relLayBasicDetail)
            return false
        }else if(selectedPropertyTypeId.equals("0",true)){
            SnackNotify.showMessage(getString(R.string.please_select_property_type),activity!!,relLayBasicDetail)
            return false
        }else if(selectedRoomTypeId.equals("0",true)){
            SnackNotify.showMessage(getString(R.string.please_select_room_type),activity!!,relLayBasicDetail)
            return false
        }else if(selectedAccommodatesName.equals(getString(R.string.select_no_of_people_accom),true)){
            SnackNotify.showMessage(getString(R.string.please_select_accomod),activity!!,relLayBasicDetail)
            return false
        }else if(Utils.isEmptyOrNull(listingName)){
            edtTextDescListingName.setError(getString(R.string.please_enter_name))
            edtTextDescListingName.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(listingDesc)){
            edtTextDescription.setError(getString(R.string.please_enter_description))
            edtTextDescription.requestFocus()
            return false
        }
        return true
    }


    private fun setDynamicAdapter() {

        for (i in 0 until basicDataModel.bedType!!.bedTypeList!!.size) {
            val hashMapBedType = java.util.HashMap<String, String>()
            val name = basicDataModel.bedType!!.bedTypeList!![i].bedTypeName!!
            val id = basicDataModel.bedType!!.bedTypeList!![i].bedTypeId
            hashMapBedType.put(Const.KEY_ID, id.toString())
            hashMapBedType.put(Const.KEY_NAME, name)
            arrayListBedTypeHash.add(hashMapBedType)
        }


        val adapterBedType = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListBedTypeHash, "black")
        spinnerBedType.adapter = adapterBedType

        for (i in 0 until basicDataModel.propertyTypes!!.propertyTypesList!!.size) {
            val hashMapPropertyType = java.util.HashMap<String, String>()
            val name = basicDataModel.propertyTypes!!.propertyTypesList!![i].propertyTypeName!!
            val id = basicDataModel.propertyTypes!!.propertyTypesList!![i].propertyTypeId
            hashMapPropertyType.put(Const.KEY_ID, id.toString())
            hashMapPropertyType.put(Const.KEY_NAME, name)
            arrayListPropertyTypeHash.add(hashMapPropertyType)
        }


        val adapterPropertyType = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListPropertyTypeHash, "black")
        spinnerPropertyType.adapter = adapterPropertyType

        for (i in 0 until basicDataModel.suitableFor!!.suitableForList!!.size) {
            val hashMapSuitableFor = java.util.HashMap<String, String>()
            val name = basicDataModel.suitableFor!!.suitableForList!![i].suitableForName!!
            val id = basicDataModel.suitableFor!!.suitableForList!![i].suitableForId

            hashMapSuitableFor.put(Const.KEY_ID, id.toString())
            hashMapSuitableFor.put(Const.KEY_NAME,name)
            arrayListSuitableForHash.add(hashMapSuitableFor)
        }


        val adapterSuitableFor = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListSuitableForHash, "black")
        spinnerSuitableFor.adapter = adapterSuitableFor


        for (i in 0 until basicDataModel.roomType!!.roomTypeList!!.size) {
            val hashMapRoomType = java.util.HashMap<String, String>()
            val name = basicDataModel.roomType!!.roomTypeList!![i].roomTypeName!!
            val id = basicDataModel.roomType!!.roomTypeList!![i].roomTypeId

            hashMapRoomType.put(Const.KEY_ID, id.toString())
            hashMapRoomType.put(Const.KEY_NAME, name)
            arrayListRoomTypeHash.add(hashMapRoomType)
        }

        val adapterRoomType = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListRoomTypeHash, "black")
        spinnerRoomType.adapter = adapterRoomType

    }




    private fun bindArrayList() {
        arrayListBedrooms = ArrayList()
        arrayListBedrooms.add(getString(R.string.select_bedrooms))
        arrayListBedrooms.add("Stdio")

        for (i in 1 until 11) {
            arrayListBedrooms.add(i.toString())
        }


        arrayListBeds = ArrayList()
        arrayListBeds.add(getString(R.string.select_no_of_beds))
        for (i in 1 until 16) {
            arrayListBeds.add(i.toString())
        }
        arrayListBeds.add("16+")

        arrayListAccomodates = ArrayList()
        arrayListAccomodates.add(getString(R.string.select_no_of_people_accom))
        for (i in 1 until 16) {
            arrayListAccomodates.add(i.toString())
        }
        arrayListAccomodates.add("16+")


        arrayListBathrooms = ArrayList()
        arrayListBathrooms.add(getString(R.string.select_no_of_bathrooms))
        for (i in 0 until 3) {
            arrayListBathrooms.add(i.toString())
        }
    }

    private fun setBedroomsAdapter() {

        arrayListBedroomsHash = ArrayList()

        for (index in 0 until arrayListBedrooms.size) {
            val hashMapBedrooms = java.util.HashMap<String, String>()
            hashMapBedrooms.put(Const.KEY_ID, index.toString())
            hashMapBedrooms.put(Const.KEY_NAME, arrayListBedrooms[index])
            arrayListBedroomsHash.add(hashMapBedrooms)
        }

        val adapterBedrooms = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListBedroomsHash, "black")
        spinnerBedrooms.adapter = adapterBedrooms

        arrayListBedsHash = ArrayList()
        for (index in 0 until arrayListBeds.size) {
            val hashMapBeds = java.util.HashMap<String, String>()
            hashMapBeds.put(Const.KEY_ID, index.toString())
            hashMapBeds.put(Const.KEY_NAME, arrayListBeds[index])
            arrayListBedsHash.add(hashMapBeds)
        }

        val adapterBeds = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListBedsHash, "black")
        spinnerBeds.adapter = adapterBeds


        arrayListBathroomsHash = ArrayList()
        for (index in 0 until arrayListBathrooms.size) {
            val hashMapBathrooms = java.util.HashMap<String, String>()
            hashMapBathrooms.put(Const.KEY_ID, index.toString())
            hashMapBathrooms.put(Const.KEY_NAME, arrayListBathrooms[index])
            arrayListBathroomsHash.add(hashMapBathrooms)
        }

        val adapterBathrooms = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListBathroomsHash, "black")
        spinnerBathrooms.adapter = adapterBathrooms


        arrayListAccomodatesHash = ArrayList()
        for (index in 0 until arrayListAccomodates.size) {
            val hashMapAccommodates = java.util.HashMap<String, String>()
            hashMapAccommodates.put(Const.KEY_ID, index.toString())
            hashMapAccommodates.put(Const.KEY_NAME, arrayListAccomodates[index])
            arrayListAccomodatesHash.add(hashMapAccommodates)
        }

        val adapterAccomodates = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListAccomodatesHash, "black")
        spinnerAccommodates.adapter = adapterAccomodates


        arrayListBedTypeHash = ArrayList()
        val hashMapBedType = java.util.HashMap<String, String>()
        hashMapBedType.put(Const.KEY_ID, "0")
        hashMapBedType.put(Const.KEY_NAME, getString(R.string.please_select_bed_type))
        arrayListBedTypeHash.add(hashMapBedType)


        val adapterBedType = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListBedTypeHash, "black")
        spinnerBedType.adapter = adapterBedType

        arrayListPropertyTypeHash = ArrayList()
        val hashMapPropertyType = java.util.HashMap<String, String>()
        hashMapPropertyType.put(Const.KEY_ID, "0")
        hashMapPropertyType.put(Const.KEY_NAME, getString(R.string.select_property_type))
        arrayListPropertyTypeHash.add(hashMapPropertyType)


        val adapterPropertyType = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListPropertyTypeHash, "black")
        spinnerPropertyType.adapter = adapterPropertyType

        arrayListSuitableForHash = ArrayList()
        val hashMapSuitableFor = java.util.HashMap<String, String>()
        hashMapSuitableFor.put(Const.KEY_ID, "0")
        hashMapSuitableFor.put(Const.KEY_NAME, getString(R.string.all))
        arrayListSuitableForHash.add(hashMapSuitableFor)


        val adapterSuitableFor = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListSuitableForHash, "black")
        spinnerSuitableFor.adapter = adapterSuitableFor


        arrayListRoomTypeHash = ArrayList()
        val hashMapRoomType = java.util.HashMap<String, String>()
        hashMapRoomType.put(Const.KEY_ID, "0")
        hashMapRoomType.put(Const.KEY_NAME, getString(R.string.select_room_type))
        arrayListRoomTypeHash.add(hashMapRoomType)


        val adapterRoomType = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListRoomTypeHash, "black")
        spinnerRoomType.adapter = adapterRoomType

    }

    internal val onRetryUpdateBasicDetails : OnClickInterface = object : OnClickInterface{
        override fun onClick() {
            onClickBtnUpdateBasicDetails()
        }

    }


}
