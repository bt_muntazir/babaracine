package com.babaracine.mvp.manage_room.presenter_manage_reviews

import android.app.Activity
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.manage_room.presenter_disable_room.DisableRoomListModel
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class ManageReviewPresenterImpl(val activity: Activity,val manageReviewsView: ManageReviewsContractor.ManageReviewsView) : ManageReviewsContractor.ManageReviewsPresenter  {
    override fun getReviewList(roomId : String) {
        try {
            ApiAdapter.getInstance(activity)
            getAllReviews(roomId)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            manageReviewsView.onReviewInternetError()
        }
    }

    private fun getAllReviews(roomId : String) {
        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }

        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getReviewResult = ApiAdapter.getApiService()!!.getAllReviews("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getReviewResult.enqueue(object : retrofit2.Callback<ManageReviewModel>{

            override fun onResponse(call: Call<ManageReviewModel>?, response: Response<ManageReviewModel>?) {

                Progress.stop()

                try {
                    val manageReviewModel = response!!.body()
                    val message = manageReviewModel.message!!
                    if(manageReviewModel.status == 1){
                        manageReviewsView.onReviewListSuccess(manageReviewModel)
                    }else if(manageReviewModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        manageReviewsView.onReviewUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    manageReviewsView.onReviewUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<ManageReviewModel>?, t: Throwable?) {
                Progress.stop()
                manageReviewsView.onReviewUnSuccess(activity.getString(R.string.error_server))
            }
        })
    }

    override fun delteReviewFromList(reviewId : Int) {
        try {
            ApiAdapter.getInstance(activity)
            deleteReviewFromList(reviewId)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            manageReviewsView.onDeleteReviewInternetError()
        }
    }

    private fun deleteReviewFromList(reviewId: Int) {

        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val roomId = LoginManagerSession.getRoomIdForManageRoom()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            if(!roomId.equals("0")) {
                jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            }
            jsonObject.put(Const.PARAM_REVIEW_ID,reviewId)

        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getDeleteReviewResult = ApiAdapter.getApiService()!!.deletReviewFromList("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getDeleteReviewResult.enqueue(object : retrofit2.Callback<CommonModel>{

            override fun onResponse(call: Call<CommonModel>?, response: Response<CommonModel>?) {

                Progress.stop()

                try {
                    val deleterReviewResult = response!!.body()
                    val message = deleterReviewResult.message!!
                    if(deleterReviewResult.status == 1){
                        manageReviewsView.onDeleteRevieSuccess(deleterReviewResult)
                    }else if(deleterReviewResult.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        manageReviewsView.onDeleteReviewUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    manageReviewsView.onDeleteReviewUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<CommonModel>?, t: Throwable?) {
                Progress.stop()
                manageReviewsView.onDeleteReviewUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }
}