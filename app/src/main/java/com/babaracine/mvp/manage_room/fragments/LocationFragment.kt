package com.babaracine.mvp.manage_room.fragments

import android.app.AlertDialog
import android.content.Context
import android.location.Address
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatSpinner
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView

import com.babaracine.R
import com.babaracine.common.adapter.AdapterSpinner
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.Const
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.mvp.manage_room.presenter.BasicLocationAndPolicyModel
import com.babaracine.mvp.review_steps.presenter.presenter_country.CountryPresenterImpl
import com.babaracine.mvp.review_steps.presenter.presenter_country.GetCountryModel
import kotlinx.android.synthetic.main.fragment_location.*
import java.util.HashMap
import com.google.android.gms.maps.model.LatLng
import android.location.Geocoder
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.manage_room.ManageRoomActivity
import com.babaracine.mvp.manage_room.model.UpdateBasicDetailModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomDetailModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomContractor
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomPresenterImpl
import java.io.IOException


class LocationFragment : Fragment(), UpdateRoomContractor.UpdateBasicDetailView {
    override fun onUpdateBasicDetailSuccess(updateBasicDetailModel: UpdateBasicDetailModel) {
        //no need to implement
    }

    override fun onPricingUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        //no need to implement
    }

    override fun onPricingUpdateUnSuccess(message: String) {
        //no need to implement
    }

    override fun onPricing1UpdateInternetError() {
        //no need to implement
    }

    override fun onAmenitiesUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        // no need to implement
    }

    override fun onAmenitiesUpdateUnSuccess(message: String) {
        // no need to implement
    }

    override fun onAmenitiesUpdateInternetError() {
        // no need to implement
    }

    override fun onPolicyUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        //no need to implement
    }

    override fun onPolicyUpdateUnSuccess(message: String) {
        //no need to implement
    }

    override fun onPolicyUpdateInternetError() {
        //no need to implement
    }

    lateinit var countryPresenterImpl: CountryPresenterImpl

    var selectedCountryId = "0"
    lateinit var selectedCountryName: String
    lateinit var arrayListCountry: ArrayList<HashMap<String, String>>

    lateinit var arrayListCountryList: ArrayList<GetCountryModel.Datum>

    lateinit var basicLocationAndPolicyModel: BasicLocationAndPolicyModel.Data

    var reviewStep: Int = 0

    lateinit var updateRoomDetailModel: UpdateRoomDetailModel

    lateinit var updateRoomPresenterImpl: UpdateRoomPresenterImpl

    lateinit var address1 : String
    lateinit var address2 : String
    lateinit var city : String
    lateinit var state : String
    lateinit var postal : String
    lateinit var country : String
    var latitude = ""
    var longitude = ""





    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getIntentData()
        setCountryAdapter()
        bindCountryInDropdown()
        updateRoomPresenterImpl = UpdateRoomPresenterImpl(activity!!, this)
        manageClickEvent()

        //getAllCountryList()
        val isFromManageRoom = LoginManagerSession.getIsManageFrom()
        if(isFromManageRoom) {
            if (reviewStep > 1) {
                setPageData()
            }
        }
    }

    private fun getIntentData(){
        if(arguments != null) {
            arrayListCountryList = arguments!!.getSerializable(ConstIntent.KEY_LOCATION_PAGE_COUNTRY_LIST) as ArrayList<GetCountryModel.Datum>
            reviewStep = arguments!!.getInt(ConstIntent.KEY_REVIEW_STEPS)
            val isFromManageRoom = LoginManagerSession.getIsManageFrom()
            if(isFromManageRoom) {
                if (reviewStep > 1) {
                    basicLocationAndPolicyModel = arguments!!.getSerializable(ConstIntent.KEY_BASIC_DETAIL_FRAGMENT) as BasicLocationAndPolicyModel.Data
                }
            }
        }
    }


    override fun onUpdateBasicDetailUnSuccess(message: String) {
        // no need to implement
    }

    override fun onUpdateBasicDetailInternetError() {
        // no need to implement
    }

    override fun onLocationUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        this.updateRoomDetailModel = updateRoomDetailModel
        reviewStep = updateRoomDetailModel.data!!.rooms!!.stepsCompleted!!

        showMessageAfterRoomUpdate(updateRoomDetailModel.message!!)

    }

    fun showMessageAfterRoomUpdate(msg: String) {

        val alertDialoge = AlertDialog.Builder(activity)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(activity!!.getString(R.string.title_ok)) { dialog, which ->

            (activity as ManageRoomActivity).setTabEnableOrDisable(reviewStep+1)
        }

        alertDialoge.show()


    }

    override fun onLocationUpdateUnSuccess(message: String) {
        SnackNotify.showMessage(message, activity!!, relLayFragLocation)
    }

    override fun onLocationUpdateInternetError() {
        SnackNotify.checkConnection(onRetryUpdateLocation, relLayFragLocation)
    }


    fun getLocationFromAddress(context: Context, strAddress: String): LatLng? {

        val coder = Geocoder(context)
        val address: List<Address>?
        var p1: LatLng? = null

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5)
            if (address == null) {
                return null
            }

            val location = address[0]
            p1 = LatLng(location.getLatitude(), location.getLongitude())

        } catch (ex: IOException) {

            ex.printStackTrace()
        }

        return p1
    }

    fun getLatitudeAndLongitudeFromZipcode(){

        val zipcode = edtTextZipPostalCode.text.toString().trim()

        val geocoder = Geocoder(activity!!)
        try {
            val addresses = geocoder.getFromLocationName(zipcode, 1)
            if (addresses != null && !addresses.isEmpty()) {
                val address = addresses[0]
                // Use the address as needed
                latitude = address.latitude.toString()
                longitude = address.longitude.toString()
                /*val message = String.format("Latitude: %f, Longitude: %f",
                        address.latitude, address.longitude)*/
            } else {
                // Display appropriate message when Geocoder services are not available
                //SnackNotify.showMessage("Please enter valid zip code",activity!!,relLayFragLocation)
            }
        } catch (e: IOException) {
            // handle exception
        }

    }

    private fun setPageData() {

        address1 = basicLocationAndPolicyModel.address1.toString()
        address2 = basicLocationAndPolicyModel.address2.toString()
        city = basicLocationAndPolicyModel.city.toString()
        state = basicLocationAndPolicyModel.state.toString()
        postal = basicLocationAndPolicyModel.postal.toString()
        selectedCountryName = basicLocationAndPolicyModel.country!!.toString()

        latitude = basicLocationAndPolicyModel.latitude!!
        longitude = basicLocationAndPolicyModel.longitude!!

        spinnerCountryLocation.setSelection(getIndexWithValue(spinnerCountryLocation, selectedCountryName))
        edtTextAddressLine1.setText(address1)
        edtTextAddressLine2.setText(address2)
        edtTextCityTownDistrict.setText(city)
        edtTextStateProvinceCountryRegion.setText(state)
        edtTextZipPostalCode.setText(postal)

    }

    fun getIndexWithValue(spinner: AppCompatSpinner, compare_value: String): Int {

        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString().equals(compare_value, true)) {
                return i
            }

        }
        return 0
    }

    private fun manageClickEvent() {
        spinnerCountryLocation?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedCountryId = arrayListCountry.get(position).get(Const.KEY_ID).toString()
                selectedCountryName = arrayListCountry.get(position).get(Const.KEY_NAME).toString()
            }
        }

        btnUpdateRoomLocations.setOnClickListener(View.OnClickListener { updateRoomLocationDetails() })

    }

    private fun updateRoomLocationDetails() {

          address1 = edtTextAddressLine1.text.toString()
        address2 = edtTextAddressLine2.text.toString()
        city = edtTextCityTownDistrict.text.toString()
        state = edtTextStateProvinceCountryRegion.text.toString()
        postal = edtTextZipPostalCode.text.toString()
        getLatitudeAndLongitudeFromZipcode()
        if(isValidate()) {
            updateRoomPresenterImpl.updateRoomLocationDetails(address1,address2,city,state,postal,selectedCountryName,latitude,longitude)
        }
    }

    private fun isValidate(): Boolean {
        if(selectedCountryName.equals(getString(R.string.hint_country))){
            AlertDialogHelper.showMessage(getString(R.string.error_country),activity!!)
            return false
        }else if(Utils.isEmptyOrNull(address1)){
            edtTextAddressLine1.setError(getString(R.string.error_address_1))
            edtTextAddressLine1.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(city)){
            edtTextCityTownDistrict.setError(getString(R.string.error_city))
            edtTextCityTownDistrict.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(state)){
            edtTextStateProvinceCountryRegion.setError(getString(R.string.error_state))
            edtTextStateProvinceCountryRegion.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(postal)){
            edtTextZipPostalCode.setError(getString(R.string.error_zip_code))
            edtTextZipPostalCode.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(latitude) || Utils.isEmptyOrNull(longitude)){
            edtTextZipPostalCode.setError(getString(R.string.invalid_zip_code))
            edtTextZipPostalCode.requestFocus()
            return false
        }
        return true
    }


    private fun bindCountryInDropdown() {

        for (i in 0 until arrayListCountryList.size) {

            val name = arrayListCountryList.get(i).countryName!!
            val id = arrayListCountryList.get(i).countryId!!.toString()
            val hashMap: HashMap<String, String> = HashMap()
            hashMap.put(Const.KEY_ID, id)
            hashMap.put(Const.KEY_NAME, name)
            arrayListCountry.add(hashMap)
        }

        val adapterCountry = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListCountry, "black")
        spinnerCountryLocation.setAdapter(adapterCountry)


    }


    private fun setCountryAdapter() {

        arrayListCountry = ArrayList()
        val hashMapMonth = java.util.HashMap<String, String>()
        hashMapMonth.put(Const.KEY_ID, "0")
        hashMapMonth.put(Const.KEY_NAME, getString(R.string.hint_country))
        arrayListCountry.add(hashMapMonth)

        val adapterMonth = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListCountry, "black")
        spinnerCountryLocation.setAdapter(adapterMonth)

    }

    internal val onRetryUpdateLocation: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            updateRoomLocationDetails()
        }

    }


}
