package com.babaracine.mvp.manage_room.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

import java.util.ArrayList

class SelectedAmenityModel : Serializable {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data : Serializable{

        @SerializedName("rooms")
        @Expose
        var rooms: Rooms? = null
        @SerializedName("room_amenities")
        @Expose
        var roomAmenities: ArrayList<RoomAmenity>? = null

        inner class RoomAmenity : Serializable{

            @SerializedName("amenity_id")
            @Expose
            var amenityId: Int? = null
            @SerializedName("amenity_name")
            @Expose
            var amenityName: String? = null
            @SerializedName("selected")
            @Expose
            var selected: Int? = null

        }


        inner class Rooms : Serializable{

            @SerializedName("steps_completed")
            @Expose
            var stepsCompleted: Int? = null

        }

    }

}