package com.babaracine.mvp.manage_room.adapter

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babaracine.R
import com.babaracine.mvp.dashboard.model.NotificationModel
import com.babaracine.mvp.dashboard.model.TripsModel
import com.babaracine.mvp.manage_room.fragments.DisableRoomsFragment
import com.babaracine.mvp.manage_room.presenter_disable_room.DisableRoomListModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_disable_room_list.view.*
import kotlinx.android.synthetic.main.item_notification.view.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Braintech on 06-09-2018.
 */
class DisableRoomListAdapter : RecyclerView.Adapter<DisableRoomListAdapter.ViewHolder> {

    var context: Context
    var arrayListDisableRoomList: ArrayList<DisableRoomListModel.Datum>
    lateinit var disableRoomsFragment : DisableRoomsFragment

    constructor(context: Context, arrayListDisableRoomList: ArrayList<DisableRoomListModel.Datum>,disableRoomsFragment: DisableRoomsFragment) {
        this.context = context
        this.arrayListDisableRoomList = arrayListDisableRoomList
        this.disableRoomsFragment = disableRoomsFragment
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_disable_room_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = arrayListDisableRoomList[position]

        holder.txtViewStartDateListItem!!.text = dataModel.startDate!!
        holder.txtViewEndDateListItem!!.text = dataModel.endDate!!
        holder.txtViewSerialNumberItem!!.text = (position+1).toString()




         holder.txtViewActionItem.setOnClickListener(){
             val itemId = dataModel.id!!
             disableRoomsFragment.getDisableRoomItemIdForDelete(itemId)
         }
    }

    override fun getItemCount(): Int {
        return arrayListDisableRoomList.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val txtViewSerialNumberItem = itemView.txtViewSerialNumberItem
        val txtViewStartDateListItem = itemView.txtViewStartDateListItem
        val txtViewEndDateListItem = itemView.txtViewEndDateListItem
        val txtViewActionItem = itemView.txtViewActionItem



    }


}