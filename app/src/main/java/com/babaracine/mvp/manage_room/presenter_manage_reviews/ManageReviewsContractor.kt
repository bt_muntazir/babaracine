package com.babaracine.mvp.manage_room.presenter_manage_reviews

import com.babaracine.mvp.common_model.CommonModel

interface ManageReviewsContractor {


    interface ManageReviewsPresenter{

        fun getReviewList(roomId : String)
        fun delteReviewFromList(reviewId : Int)

    }

    interface ManageReviewsView{

        fun onReviewListSuccess(manageReviewModel: ManageReviewModel)
        fun onReviewUnSuccess(message : String)
        fun onReviewInternetError()

        fun onDeleteRevieSuccess(commonModel: CommonModel)
        fun onDeleteReviewUnSuccess(message : String)
        fun onDeleteReviewInternetError()

    }



}