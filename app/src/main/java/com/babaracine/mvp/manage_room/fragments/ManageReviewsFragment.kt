package com.babaracine.mvp.manage_room.fragments

import android.app.AlertDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.babaracine.R
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.manage_room.adapter.ManageReviewAdapter
import com.babaracine.mvp.manage_room.presenter_manage_reviews.ManageReviewModel
import com.babaracine.mvp.manage_room.presenter_manage_reviews.ManageReviewPresenterImpl
import com.babaracine.mvp.manage_room.presenter_manage_reviews.ManageReviewsContractor
import kotlinx.android.synthetic.main.activity_manage_room.*
import kotlinx.android.synthetic.main.fragment_manage_reviews.*

class ManageReviewsFragment : Fragment(),ManageReviewsContractor.ManageReviewsView {

    var reviewId : Int = 0
    lateinit var manageReviewPresenterImpl: ManageReviewPresenterImpl
    lateinit var arrayListReviewList : ArrayList<ManageReviewModel.Datum>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manage_reviews, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arrayListReviewList = ArrayList()
        manageReviewPresenterImpl = ManageReviewPresenterImpl(activity!!, this)
        getIntentData()

    }

    private fun getIntentData() {
        if(arguments != null){
            arrayListReviewList = arguments!!.getSerializable(ConstIntent.KEY_ARRAY_LIST_REVIEW) as ArrayList<ManageReviewModel.Datum>
            setLayoutManager()

            if(arrayListReviewList!=null && arrayListReviewList.size>0){
                setAdapter()
            }

        }

    }

    override fun onReviewListSuccess(manageReviewModel: ManageReviewModel) {
        this.arrayListReviewList = manageReviewModel.data!!

        if(arrayListReviewList!=null && arrayListReviewList.size>0){
            setAdapter()
        }
    }

    override fun onReviewUnSuccess(message: String) {
     //  SnackNotify.showMessage(message,activity!!,frameManageReviews)

        arrayListReviewList= ArrayList()
        if(arrayListReviewList!=null && arrayListReviewList.size>0){
            setAdapter()
        }
    }

    override fun onReviewInternetError() {
        SnackNotify.checkConnection(onRetryReviewList,relLayManageRoom)
    }

    override fun onDeleteRevieSuccess(commonModel: CommonModel) {
        showMessageAfterSuccessfullyDelete(commonModel.message!!)
    }

    override fun onDeleteReviewUnSuccess(message: String) {
        SnackNotify.showMessage(message,activity!!,frameManageReviews)
    }

    override fun onDeleteReviewInternetError() {
        SnackNotify.checkConnection(onRetryDeleteReviewItem,frameManageReviews)
    }

    private fun showMessageAfterSuccessfullyDelete(msg: String) {

        val alertDialoge = AlertDialog.Builder(context)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(activity!!.getString(R.string.title_ok)) { dialog, which ->
            getAllReviewFromApi()
            dialog.dismiss()
        }

        alertDialoge.show()


    }

    private fun setLayoutManager() {
        val linearLayoutManager = LinearLayoutManager(activity)
        recyclerViewManageReviews.layoutManager = linearLayoutManager
    }

    private fun setAdapter() {
        val manageReviewAdapter = ManageReviewAdapter(activity!!,arrayListReviewList,this)
        recyclerViewManageReviews.setAdapter(manageReviewAdapter)
    }

    private fun getAllReviewFromApi(){
        val roomId = LoginManagerSession.getRoomIdForManageRoom()
        manageReviewPresenterImpl.getReviewList(roomId)
    }


    private fun deleteReviewFromList(){
        manageReviewPresenterImpl.delteReviewFromList(reviewId)
    }

    fun callDeleteApiForDeleteReview(reviewId : Int){
        this.reviewId = reviewId
        deleteReviewFromList()
    }



    internal val onRetryDeleteReviewItem: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            deleteReviewFromList()
        }

    }

    internal val onRetryReviewList: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getAllReviewFromApi()
        }

    }



}
