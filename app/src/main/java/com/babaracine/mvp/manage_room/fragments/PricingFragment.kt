package com.babaracine.mvp.manage_room.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatSpinner
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView

import com.babaracine.R
import com.babaracine.common.adapter.AdapterSpinner
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.Const
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.manage_room.ManageRoomActivity
import com.babaracine.mvp.manage_room.model.PricingDetailModel
import com.babaracine.mvp.manage_room.model.SelectedRoomPhotoModel
import com.babaracine.mvp.manage_room.model.UpdateBasicDetailModel
import com.babaracine.mvp.manage_room.presenter.BasicDataContractor
import com.babaracine.mvp.manage_room.presenter.BasicDataModel
import com.babaracine.mvp.manage_room.presenter.BasicDataPresenterImpl
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomContractor
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomDetailModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomPresenterImpl
import kotlinx.android.synthetic.main.fragment_location.*
import kotlinx.android.synthetic.main.fragment_picing.*

class PricingFragment : Fragment(), UpdateRoomContractor.UpdateBasicDetailView {

    lateinit var arrayListChargeableTypeHash: ArrayList<HashMap<String, String>>
    lateinit var arrayListCurrencyTypeHash: ArrayList<HashMap<String, String>>

    lateinit var basicDataPresenterImpl: BasicDataPresenterImpl

    lateinit var arrayListCurrency: ArrayList<BasicDataModel.Data.Currency.CurrencyList>

    lateinit var selectedCurrencyTypeId: String
    lateinit var selectedCurrencyTypeName: String
    var selectedChargeableTypeId: String = "0"
    var selectedChargeableTypeName: String = "Free"

    lateinit var pricingDetailModel: PricingDetailModel

    var reviewStep: Int = 0

    lateinit var updateRoomDetailModel: UpdateRoomDetailModel
    lateinit var updateRoomPresenterImpl: UpdateRoomPresenterImpl


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_picing, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getIntenteData()
        updateRoomPresenterImpl = UpdateRoomPresenterImpl(activity!!, this)
        manageClickEvents()
        setChargableAdapter()
        setCurrencyAdapter()

        setData()


    }

    override fun onUpdateBasicDetailSuccess(updateBasicDetailModel: UpdateBasicDetailModel) {
        // no need to implement
    }

    override fun onUpdateBasicDetailUnSuccess(message: String) {
        // no need to implement
    }

    override fun onUpdateBasicDetailInternetError() {
        // no need to implement
    }

    override fun onLocationUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        // no need to implement
    }

    override fun onLocationUpdateUnSuccess(message: String) {
        // no need to implement
    }

    override fun onLocationUpdateInternetError() {
        // no need to implement
    }

    override fun onPolicyUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        // no need to implement
    }

    override fun onPolicyUpdateUnSuccess(message: String) {
        // no need to implement
    }

    override fun onPolicyUpdateInternetError() {
        // no need to implement
    }

    override fun onAmenitiesUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        // no need to implement
    }

    override fun onAmenitiesUpdateUnSuccess(message: String) {
        // no need to implement
    }

    override fun onAmenitiesUpdateInternetError() {
        // no need to implement
    }

    override fun onPricingUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel) {
        this.updateRoomDetailModel = updateRoomDetailModel
        reviewStep = updateRoomDetailModel.data!!.rooms!!.stepsCompleted!!
        showMessageAfterRoomUpdate(updateRoomDetailModel.message!!)
    }

    fun showMessageAfterRoomUpdate(msg: String) {

        val alertDialoge = AlertDialog.Builder(activity)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(activity!!.getString(R.string.title_ok)) { dialog, which ->

            (activity as ManageRoomActivity).setTabEnableOrDisable(reviewStep+1)
        }
        alertDialoge.show()
    }

    override fun onPricingUpdateUnSuccess(message: String) {
        AlertDialogHelper.showMessage(message, activity!!)
    }

    override fun onPricing1UpdateInternetError() {
        SnackNotify.checkConnection(onRetryRoomPriceUpdate, framLayoutFragPricing)
    }

    private fun getIntenteData() {
        if (arguments != null) {
            arrayListCurrency = arguments!!.getSerializable(ConstIntent.KEY_PRICING_FRAGMENT) as ArrayList<BasicDataModel.Data.Currency.CurrencyList>
            reviewStep = arguments!!.getInt(ConstIntent.KEY_REVIEW_STEPS)
            if (reviewStep > 4) {
                pricingDetailModel = arguments!!.getSerializable(ConstIntent.KEY_PRICING_DETAILS) as PricingDetailModel
            }
        }
    }


    private fun updateRoomPricingDetails() {
        val nightlyPrice = edtTextNightlyPrice.text.toString()
        val minimumPrice = edtTextMinimumPrice.text.toString()
        val nightlyPriceForLongTerm = edtTextNightlyPriceForLongTermBooking.text.toString()
        val taxName = edtTextTaxName.text.toString()
        val taxPercentage = edtTextTextPercent.text.toString()
        val depositPercentage = edtTextDepositPercent.text.toString()
        val cancellationCharge = edtTextCancellationCharge.text.toString()
        val cancellationDescription = edtTextDescription.text.toString()

        if (isValidData(nightlyPrice, minimumPrice, nightlyPriceForLongTerm, taxName, taxPercentage, depositPercentage, cancellationCharge, cancellationDescription)) {
            updateRoomPresenterImpl.upadteRoomProcingDetails(selectedCurrencyTypeId, nightlyPrice, minimumPrice, nightlyPriceForLongTerm, taxName, taxPercentage, depositPercentage, selectedChargeableTypeName, cancellationCharge, cancellationDescription)
        }
    }

    private fun isValidData(nightlyPrice: String, minimumPrice: String, nightlyPriceForLongTerm: String, taxName: String, taxPercentage: String, depositPercentage: String, cancellationCharge: String, cancellationDescription: String): Boolean {

        if (selectedCurrencyTypeId.equals("0")) {
            AlertDialogHelper.showMessage(activity!!.getString(R.string.error_currency), activity!!)
            return false
        } else if (Utils.isEmptyOrNull(nightlyPrice)) {
            edtTextNightlyPrice.setError(activity!!.getString(R.string.error_nightly_price))
            edtTextNightlyPrice.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(minimumPrice)) {
            edtTextMinimumPrice.setError(activity!!.getString(R.string.error_minimum_price))
            edtTextMinimumPrice.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(nightlyPriceForLongTerm)) {
            edtTextNightlyPriceForLongTermBooking.setError(activity!!.getString(R.string.error_nightly_price_for_long_term))
            edtTextNightlyPriceForLongTermBooking.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(taxName)) {
            edtTextTaxName.setError(activity!!.getString(R.string.error_tax_name))
            edtTextTaxName.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(taxPercentage)) {
            edtTextTextPercent.setError(activity!!.getString(R.string.error_tax_percent))
            edtTextTextPercent.requestFocus()
            return false
        } else if (taxPercentage.toInt() > 100) {
            edtTextTextPercent.setError(activity!!.getString(R.string.error_tax_percent_more_than_100))
            edtTextTextPercent.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(depositPercentage)) {
            edtTextDepositPercent.setError(activity!!.getString(R.string.error_deposit_percent))
            edtTextDepositPercent.requestFocus()
            return false
        } else if (depositPercentage.toInt() > 100) {
            edtTextDepositPercent.setError(activity!!.getString(R.string.error_deposit_percent_more_than_100))
            edtTextDepositPercent.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(cancellationCharge)) {
            edtTextCancellationCharge.setError(activity!!.getString(R.string.error_cancellation_percent))
            edtTextCancellationCharge.requestFocus()
            return false
        } else if (cancellationCharge.toInt() > 100) {
            edtTextCancellationCharge.setError(activity!!.getString(R.string.error_cancellation_percent_more_than_100))
            edtTextCancellationCharge.requestFocus()
            return false
        }

        return true
    }


    private fun setData() {
        val isFromManageRoom = LoginManagerSession.getIsManageFrom()

        if (isFromManageRoom) {
            if (reviewStep > 4) {

                val data = pricingDetailModel.data!!.pricing!!
                if (data.size > 0) {
                    spinnerCurrency.setSelection(getIndexWithId(spinnerCurrency, data[0].currencyCode!!))
                    spinnerChargeableType.setSelection(getIndexWithValue(spinnerChargeableType, data[0].cancellationType!!))

                    edtTextNightlyPrice.setText(data[0].nightlyPrice.toString())
                    edtTextMinimumPrice.setText(data[0].minimumDay.toString())
                    edtTextNightlyPriceForLongTermBooking.setText(data[0].longTermNightlyPrice.toString())
                    edtTextTaxName.setText(data[0].taxName.toString())
                    edtTextTextPercent.setText(data[0].taxPercentage.toString())
                    edtTextDepositPercent.setText(data[0].depositPercent.toString())
                    edtTextCancellationCharge.setText(data[0].cancellationCharge.toString())
                    edtTextDescription.setText(data[0].cancellationDesc.toString())
                }
            }
        }
    }


    fun getIndexWithValue(spinner: AppCompatSpinner, compare_value: String): Int {

        for (i in 0 until spinner.count) {
            Log.e("currency value ", spinner.getItemAtPosition(i).toString())

            if (spinner.getItemAtPosition(i).toString().equals(compare_value, true)) {
                return i
            }

        }
        return 0
    }

    fun getIndexWithId(spinner: AppCompatSpinner, compare_value: String): Int {

        for (i in 0 until spinner.count) {

            Log.e("currency value ", spinner.getItemIdAtPosition(i).toString())
            if (spinner.getItemIdAtPosition(i).toString().equals(compare_value, true)) {
                return i
            }

        }
        return 0
    }

    private fun manageClickEvents() {
        spinnerCurrency?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedCurrencyTypeId = arrayListCurrencyTypeHash.get(position).get(Const.KEY_ID).toString()
                selectedCurrencyTypeName = arrayListCurrencyTypeHash.get(position).get(Const.KEY_NAME).toString()
            }
        }

        spinnerChargeableType?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedChargeableTypeId = arrayListChargeableTypeHash.get(position).get(Const.KEY_ID).toString()
                selectedChargeableTypeName = arrayListChargeableTypeHash.get(position).get(Const.KEY_NAME).toString()
            }
        }

        btnUpdateRoomPricing.setOnClickListener(View.OnClickListener { updateRoomPricingDetails() })
    }


    private fun setCurrencyAdapter() {
        for (i in 0 until arrayListCurrency.size) {
            val hashMapCurrencyType = java.util.HashMap<String, String>()
            val name = arrayListCurrency[i].currencyName!!
            val id = arrayListCurrency[i].currencyCode!!


            hashMapCurrencyType.put(Const.KEY_ID, id)
            hashMapCurrencyType.put(Const.KEY_NAME, name)
            arrayListCurrencyTypeHash.add(hashMapCurrencyType)
        }


        val adapterCurrencyType = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListCurrencyTypeHash, "black")
        spinnerCurrency.adapter = adapterCurrencyType
    }


    private fun setChargableAdapter() {
        arrayListChargeableTypeHash = ArrayList()
        val hashMapRoomType = java.util.HashMap<String, String>()
        hashMapRoomType.put(Const.KEY_ID, "0")
        hashMapRoomType.put(Const.KEY_NAME, getString(R.string.title_free))
        arrayListChargeableTypeHash.add(hashMapRoomType)
        val hashMapRoomType1 = java.util.HashMap<String, String>()
        hashMapRoomType1.put(Const.KEY_ID, "1")
        hashMapRoomType1.put(Const.KEY_NAME, getString(R.string.chargeable))
        arrayListChargeableTypeHash.add(hashMapRoomType1)


        val adapterChargeable = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListChargeableTypeHash, "black")
        spinnerChargeableType.adapter = adapterChargeable

        arrayListCurrencyTypeHash = ArrayList()
        val hashMapCurrencyType = java.util.HashMap<String, String>()
        hashMapCurrencyType.put(Const.KEY_ID, "0")
        hashMapCurrencyType.put(Const.KEY_NAME, getString(R.string.select_currency_type))
        arrayListCurrencyTypeHash.add(hashMapCurrencyType)

        val adapterCurrencyType = AdapterSpinner(activity!!, R.layout.spinner_item, arrayListCurrencyTypeHash, "black")
        spinnerCurrency.adapter = adapterCurrencyType
    }

    internal val onRetryRoomPriceUpdate: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            updateRoomPricingDetails()
        }

    }


}
