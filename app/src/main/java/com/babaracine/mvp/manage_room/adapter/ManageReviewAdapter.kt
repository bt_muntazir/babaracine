package com.babaracine.mvp.manage_room.adapter

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babaracine.R
import com.babaracine.mvp.dashboard.model.NotificationModel
import com.babaracine.mvp.dashboard.model.TripsModel
import com.babaracine.mvp.manage_room.fragments.ManageReviewsFragment
import com.babaracine.mvp.manage_room.presenter_manage_reviews.ManageReviewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_manage_room_reviews.view.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Braintech on 06-09-2018.
 */
class ManageReviewAdapter : RecyclerView.Adapter<ManageReviewAdapter.ViewHolder> {

    var context: Context
    var arrayListReviews: ArrayList<ManageReviewModel.Datum>
    var manageReviewsFragment: ManageReviewsFragment

    constructor(context: Context, arrayListReviews: ArrayList<ManageReviewModel.Datum>, manageReviewsFragment: ManageReviewsFragment) {
        this.context = context
        this.arrayListReviews = arrayListReviews
        this.manageReviewsFragment = manageReviewsFragment
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_manage_room_reviews, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = arrayListReviews[position]

        holder.txtViewSerialNumber.text = (position + 1).toString()
        holder.txtViewGuestName.text = dataModel.fullName!!
        holder.txtViewDate.text = dataModel.date!!
        holder.txtViewGuestComment.text = dataModel.comment!!
        if (dataModel.rating != null) {
            holder.ratingBar.rating = dataModel.rating!!.toFloat()
        }


        holder.txtViewDelete.setOnClickListener() {
            val reviewId = dataModel.reviewId!!
            manageReviewsFragment.callDeleteApiForDeleteReview(reviewId)
        }
    }

    override fun getItemCount(): Int {
        return arrayListReviews.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val txtViewSerialNumber = itemView.txtViewSerialNumber
        val txtViewGuestName = itemView.txtViewGuestName
        val txtViewDate = itemView.txtViewDate
        val ratingBar = itemView.ratingBar
        val txtViewDelete = itemView.txtViewDelete
        val txtViewGuestComment = itemView.txtViewGuestComment


    }


}