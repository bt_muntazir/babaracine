package com.babaracine.mvp.manage_room.presenter

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

import java.util.ArrayList

class BasicDataModel : Serializable{

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data : Serializable{

        @SerializedName("property_types")
        @Expose
        var propertyTypes: PropertyTypes? = null
        @SerializedName("suitable_for")
        @Expose
        var suitableFor: SuitableFor? = null
        @SerializedName("room_type")
        @Expose
        var roomType: RoomType? = null
        @SerializedName("bed_type")
        @Expose
        var bedType: BedType? = null
        @SerializedName("amenities")
        @Expose
        var amenities: Amenities? = null
        @SerializedName("currency")
        @Expose
        var currency: Currency? = null

        inner class BedType : Serializable{

            @SerializedName("status")
            @Expose
            var status: Int? = null
            @SerializedName("message")
            @Expose
            var message: String? = null
            @SerializedName("bed_type_list")
            @Expose
            var bedTypeList: ArrayList<BedTypeList>? = null

            inner class BedTypeList : Serializable{

                @SerializedName("bed_type_id")
                @Expose
                var bedTypeId: Int? = null
                @SerializedName("bed_type_name")
                @Expose
                var bedTypeName: String? = null

            }

        }

        inner class Amenities : Serializable{

            @SerializedName("status")
            @Expose
            var status: Int? = null
            @SerializedName("message")
            @Expose
            var message: String? = null
            @SerializedName("amenity_list")
            @Expose
            var amenityList: ArrayList<AmenityList>? = null

            inner class AmenityList : Serializable{

                @SerializedName("amenity_id")
                @Expose
                var amenityId: Int? = null
                @SerializedName("amenity_name")
                @Expose
                var amenityName: String? = null
                @SerializedName("selected")
                @Expose
                var selected: Int? = null

            }

        }

        inner class Currency : Serializable{

            @SerializedName("status")
            @Expose
            var status: Int? = null
            @SerializedName("message")
            @Expose
            var message: String? = null
            @SerializedName("currency_list")
            @Expose
            var currencyList: ArrayList<CurrencyList>? = null

            inner class CurrencyList : Serializable{

                @SerializedName("currency_code")
                @Expose
                var currencyCode: String? = null
                @SerializedName("currency_name")
                @Expose
                var currencyName: String? = null

            }

        }

        inner class PropertyTypes : Serializable{

            @SerializedName("status")
            @Expose
            var status: Int? = null
            @SerializedName("message")
            @Expose
            var message: String? = null
            @SerializedName("property_types_list")
            @Expose
            var propertyTypesList: ArrayList<PropertyTypesList>? = null

            inner class PropertyTypesList : Serializable{

                @SerializedName("property_type_id")
                @Expose
                var propertyTypeId: Int? = null
                @SerializedName("property_type_name")
                @Expose
                var propertyTypeName: String? = null

            }

        }

        inner class RoomType : Serializable{

            @SerializedName("status")
            @Expose
            var status: Int? = null
            @SerializedName("message")
            @Expose
            var message: String? = null
            @SerializedName("room_type_list")
            @Expose
            var roomTypeList: ArrayList<RoomTypeList>? = null

            inner class RoomTypeList : Serializable{

                @SerializedName("room_type_id")
                @Expose
                var roomTypeId: Int? = null
                @SerializedName("room_type_name")
                @Expose
                var roomTypeName: String? = null

            }

        }

        inner class SuitableFor : Serializable{

            @SerializedName("status")
            @Expose
            var status: Int? = null
            @SerializedName("message")
            @Expose
            var message: String? = null
            @SerializedName("suitable_for_list")
            @Expose
            var suitableForList: ArrayList<SuitableForList>? = null

            inner class SuitableForList : Serializable{

                @SerializedName("suitable_for_id")
                @Expose
                var suitableForId: Int? = null
                @SerializedName("suitable_for_name")
                @Expose
                var suitableForName: String? = null

            }

        }

    }

}






