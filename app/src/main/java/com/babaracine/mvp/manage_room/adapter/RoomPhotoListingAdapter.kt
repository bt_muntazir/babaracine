package com.babaracine.mvp.manage_room.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.AlertDialogLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.dashboard.model.RoomListingModel
import com.babaracine.mvp.manage_room.fragments.PhotosFragment
import com.babaracine.mvp.manage_room.model.SelectedRoomPhotoModel
import com.babaracine.mvp.room_detail.RoomDetailActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_room_photos.view.*

/**
 * Created by Braintech on 14-08-2018.
 */
class RoomPhotoListingAdapter : RecyclerView.Adapter<RoomPhotoListingAdapter.ViewHolder> {


    var context: Context
    var arrayListRoomPhotoData: ArrayList<SelectedRoomPhotoModel.Data.RoomPhoto>
    lateinit var photosFragment: PhotosFragment

    constructor(context: Context, arrayListRoomPhotoData: ArrayList<SelectedRoomPhotoModel.Data.RoomPhoto>,photosFragment: PhotosFragment) {
        this.context = context
        this.arrayListRoomPhotoData = arrayListRoomPhotoData
        this.photosFragment = photosFragment
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_room_photos, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        val dataModel = arrayListRoomPhotoData[position]

        if(!Utils.isEmptyOrNull(dataModel.thumbImageUrl)) {
            Picasso.with(context).load(dataModel.thumbImageUrl).into(holder.imgViewItem)
        }

        if(dataModel.featured == 1){
            holder.checkBoxFeaturedImage.isChecked = true
        }else{
            holder.checkBoxFeaturedImage.isChecked = false
        }

        holder.checkBoxFeaturedImage.setOnClickListener{

            if(holder.checkBoxFeaturedImage.isChecked){
                val photoId = dataModel.id
                photosFragment.callFeaturedPhotoApi(photoId!!)
            }else{
                holder.checkBoxFeaturedImage.isChecked = true
            }

            for (i in 0 until arrayListRoomPhotoData.size){
                if(i == position){
                    arrayListRoomPhotoData[i].featured = 1
                }else{
                    arrayListRoomPhotoData[i].featured = 0
                }
            }
        }

        holder.btnDelete.setOnClickListener() {
            if(!holder.checkBoxFeaturedImage.isChecked){
                val photoId = dataModel.id!!
                photosFragment.callDeletePhotoApi(photoId)
            }else{
                AlertDialogHelper.showMessage("Featured Image cannot be deleted",context)
            }
        }
    }



    override fun getItemCount(): Int {
        return arrayListRoomPhotoData.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val imgViewItem = itemView.imgViewItem
        val checkBoxFeaturedImage = itemView.checkBoxFeaturedImage
        val btnDelete = itemView.btnDelete


    }


}