package com.babaracine.mvp.manage_room.presenter_room_photo_update

import android.net.Uri
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.manage_room.model.SelectedRoomPhotoModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomDetailModel

interface RoomPhotoUpdateContractor {

    interface RoomPhotoUpdatePresenter{

        fun upadteRoomPhotoDetails(filePath: Uri)
        fun updateRoomFeaturedImage(photoId : Int)
        fun deleteRoomImage(photoId : Int)
    }

    interface RoomPhotoUpdateView{


        fun onPhotoUpdateSuccess(selectedRoomPhotoModel: SelectedRoomPhotoModel)
        fun onPhotoUpdateUnSuccess(message : String)
        fun onPhotoUpdateInternetError()

        fun onFeaturedPhotoUpdateSuccess(updateRoomDetailModel: UpdateRoomDetailModel)
        fun onFeaturedPhotoUpdateUnSuccess(message : String)
        fun onFeaturedPhotoUpdateInternetError()

        fun onDeleteRoomPhotoSuccess(selectedRoomPhotoModel: SelectedRoomPhotoModel)
        fun onDeleteRoomPhotoUnSuccess(message : String)
        fun onDeleteRoomPhotoInternetError()

    }


}