package com.babaracine.mvp.manage_room

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import com.babaracine.R
import kotlinx.android.synthetic.main.activity_manage_room.*

import android.support.v4.app.FragmentPagerAdapter
import android.util.Log

import com.babaracine.mvp.manage_room.fragments.*

import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.mvp.manage_room.presenter.BasicDataContractor
import com.babaracine.mvp.manage_room.presenter.BasicDataModel
import com.babaracine.mvp.manage_room.presenter.BasicDataPresenterImpl
import com.babaracine.mvp.manage_room.presenter.BasicLocationAndPolicyModel
import com.babaracine.mvp.room_booking.presenter.RoomBookingModel
import com.babaracine.R.id.tabLayout
import com.babaracine.R.id.tabs
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.LinearLayout
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.manage_room.model.PricingDetailModel
import com.babaracine.mvp.manage_room.model.SelectedAmenityModel
import com.babaracine.mvp.manage_room.model.SelectedRoomPhotoModel
import com.babaracine.mvp.manage_room.presenter_disable_room.DisableRoomContractor
import com.babaracine.mvp.manage_room.presenter_disable_room.DisableRoomListModel
import com.babaracine.mvp.manage_room.presenter_disable_room.DisableRoomPresenterImpl
import com.babaracine.mvp.manage_room.presenter_manage_reviews.ManageReviewModel
import com.babaracine.mvp.manage_room.presenter_manage_reviews.ManageReviewPresenterImpl
import com.babaracine.mvp.manage_room.presenter_manage_reviews.ManageReviewsContractor
import com.babaracine.mvp.review_steps.presenter.presenter_country.CountryContractor
import com.babaracine.mvp.review_steps.presenter.presenter_country.CountryPresenterImpl
import com.babaracine.mvp.review_steps.presenter.presenter_country.GetCountryModel
import kotlinx.android.synthetic.main.fragment_location.*
import kotlinx.android.synthetic.main.toolbar_with_back_and_title.*
import okhttp3.internal.Util


class ManageRoomActivity : BaseActivity(), BasicDataContractor.BasicDataView, CountryContractor.CountryView,ManageReviewsContractor.ManageReviewsView, DisableRoomContractor.DisableRoomView {

    lateinit var manageReviewPresenterImpl: ManageReviewPresenterImpl
    lateinit var basicDataPresenterImpl: BasicDataPresenterImpl
    lateinit var basicDataModel: BasicDataModel.Data

    lateinit var arrayListAmenity: ArrayList<BasicDataModel.Data.Amenities.AmenityList>
    lateinit var arrayListCurrency: ArrayList<BasicDataModel.Data.Currency.CurrencyList>

    var roomId: String = "0"

    var reviewStep = 0

    lateinit var arrayListCountryList: ArrayList<GetCountryModel.Datum>

    lateinit var basicLocationAndPolicyModel: BasicLocationAndPolicyModel.Data

    lateinit var countryPresenterImpl: CountryPresenterImpl

    var pricingDetailModel: PricingDetailModel? = null

    var amenityModel: SelectedAmenityModel? = null

    lateinit var selectedRoomPhotoModel: SelectedRoomPhotoModel
    lateinit var arrayListReviewList : ArrayList<ManageReviewModel.Datum>
    lateinit var arrayListDisableRoom: ArrayList<DisableRoomListModel.Datum>
    lateinit var disableRoomPresenterImpl: DisableRoomPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_room)


        if(LoginManagerSession.getIsManageFrom()){

            txtViewToolTitle.text= getString(R.string.activity_manage_room)
        }else{

            txtViewToolTitle.text= getString(R.string.add_property)
        }

        getIntentData()
        tabs.setupWithViewPager(viewpager)
        arrayListReviewList = ArrayList()
        arrayListDisableRoom = ArrayList()
        disableRoomPresenterImpl = DisableRoomPresenterImpl(this, this)
        manageReviewPresenterImpl = ManageReviewPresenterImpl(this, this)
        basicDataPresenterImpl = BasicDataPresenterImpl(this, this)
        countryPresenterImpl = CountryPresenterImpl(this, this)

        getBasicDeatilForSetAdapter()
        manageClickEvent()

        /*tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                Log.e("onTabSelected", tab.text.toString())
                Log.e("onTabSelected", tab.toString())
                Log.e("reviewStep", reviewStep.toString())
                *//* val tabStrip = tabs.getChildAt(0) as LinearLayout
                 val reviewStepIntValue = (reviewStep+1)
                 //if (reviewStep.equals("1")) {
                     for (i in reviewStepIntValue until tabStrip.childCount) {
                         tabStrip.getChildAt(tab.position).setOnTouchListener(object : View.OnTouchListener {
                             override fun onTouch(v: View, event: MotionEvent): Boolean {
                                 return true
                             }
                         })
                     }
                 //}*//*


            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                Log.e("onTabUnselected", tab.toString())
                Log.e("onTabUnselected", tab.toString())
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                Log.e("onTabReselected", tab.toString())
                Log.e("onTabReselected", tab.toString())
            }
        })*/
    }


    override fun onDisableRoomListSuccess(disableRoomListModel: DisableRoomListModel) {
        this.arrayListDisableRoom = disableRoomListModel.data!!
        getBasicDeatils()
    }

    override fun onDisableRoomListUnSuccess(message: String) {
        arrayListDisableRoom = ArrayList()
        getBasicDeatils()
    }

    override fun onDisableRoomListInternetError() {
        SnackNotify.checkConnection(onRetryDisableRoomList,relLayManageRoom)
    }

    override fun onDeleteDisableRoomListItemSuccess(commonModel: CommonModel) {
        //no need to implement
    }

    override fun onDeleteDisableRoomListItemUnSuccess(message: String) {
        //no need to implement
    }

    override fun onDeleteDisableRoomListItemInternetError() {
        //no need to implement
    }

    override fun onAddDisableRoomFromServerSuccess(disableRoomListModel: DisableRoomListModel) {
        // no need to implement
    }

    override fun onAddDisableRoomFromServerUnSuccess(message: String) {
        // no need to implement
    }

    override fun onAddDisableRoomFromServerInternetError() {
        // no need to implement
    }



    override fun onReviewListSuccess(manageReviewModel: ManageReviewModel)    {
        arrayListReviewList= manageReviewModel.data!!
        //getBasicDeatils()
        getDisableRoomResult()
    }

    override fun onReviewUnSuccess(message: String) {
        arrayListReviewList = ArrayList()
        //getBasicDeatils()
        getDisableRoomResult()
    }

    override fun onReviewInternetError() {
        SnackNotify.checkConnection(onRetryReviewList,relLayManageRoom)
    }

    override fun onDeleteRevieSuccess(commonModel: CommonModel) {
        //no need to implement
    }

    override fun onDeleteReviewUnSuccess(message: String) {
        // no need to implement
    }

    override fun onDeleteReviewInternetError() {
        //no need to implement
    }

    private fun getDisableRoomResult(){
        disableRoomPresenterImpl.getDisableRoomList()
    }

    private fun manageClickEvent() {
        imgViewToolBack.setOnClickListener{finish()}
    }

    private fun getAllCountryList() {
        countryPresenterImpl.getCountryList()
    }

    override fun onGetCountrySuccess(countryModel: GetCountryModel) {
        this.arrayListCountryList = countryModel.data!!
        if(!roomId.equals("0")){
            getAllReviewFromApi()
        }else{
            setupViewPager(viewpager)
        }
    }

    override fun onGetCountryUnsuccess(message: String) {
        SnackNotify.showMessage(message, this, relLayManageRoom)
    }

    override fun onGetCountryInternetError() {
        SnackNotify.checkConnection(onRetryCountry, relLayManageRoom)
    }

    override fun onGetPhotoForSelectedRoomSuccess(selectedRoomPhotoModel: SelectedRoomPhotoModel) {
        this.selectedRoomPhotoModel = selectedRoomPhotoModel
        reviewStep = selectedRoomPhotoModel.data!!.rooms!!.stepsCompleted!!


        if (viewpager != null) {
            setupViewPager(viewpager)
        }
    }

    override fun onGetPhotoForSelectedRoomUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, relLayManageRoom)
    }

    override fun onGetPhotoForSelectedRoomInternetError() {
        SnackNotify.checkConnection(onRetryPhotos, relLayManageRoom)
    }


    override fun onGetSelectedAmenitySuccess(amenityModel: SelectedAmenityModel) {
        this.amenityModel = amenityModel
        reviewStep = amenityModel.data!!.rooms!!.stepsCompleted!!
        if (reviewStep > 4) {
            getPricingDetails()
        } else {
            if (viewpager != null) {
                setupViewPager(viewpager)
            }
        }

        //setTabEnableOrDisable()
    }

    private fun getPricingDetails() {
        basicDataPresenterImpl.getPricingDetailForSelectedRoom(roomId)
    }

    override fun onGetSelectedAmenityUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, relLayManageRoom)
    }

    override fun onGetSelectedAmenityInternetError() {
        SnackNotify.checkConnection(onRetryAmenityData, relLayManageRoom)
    }

    override fun onGetPricingDetailSuccess(pricingDetailModel: PricingDetailModel) {
        this.pricingDetailModel = pricingDetailModel
        reviewStep = pricingDetailModel.data!!.rooms!!.stepsCompleted!!
        if (reviewStep > 5) {
            getRoomPhotos()
        } else {
            if (viewpager != null) {
                setupViewPager(viewpager)
            }
        }

       // setTabEnableOrDisable()
    }

    private fun getRoomPhotos() {
        basicDataPresenterImpl.getPhotosOfSelectedRoom(roomId)
    }

    override fun onGetPricingDetailUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, relLayManageRoom)
    }

    override fun onGetPricingDetailInternetError() {
        SnackNotify.checkConnection(onRetryPricingData, relLayManageRoom)
    }


    fun setTabEnableOrDisable(stepsCompleted : Int) {
        reviewStep = stepsCompleted
        val tabStrip = tabs.getChildAt(0) as LinearLayout

        if (reviewStep > 5) {
            for (i in 0 until tabStrip.childCount) {
                // tabStrip.getChildAt(i).setEnabled(true)
                tabStrip.getChildAt(i).setOnTouchListener(object : View.OnTouchListener {
                    override fun onTouch(v: View, event: MotionEvent): Boolean {
                        return false
                    }
                })
            }
        } else {
            val reviewStepIntValue = (reviewStep + 1)
            //if (reviewStep.equals("1")) {
            for (i in 0 until reviewStep) {
                // tabStrip.getChildAt(i).setEnabled(true)
                tabStrip.getChildAt(i).setOnTouchListener(object : View.OnTouchListener {
                    override fun onTouch(v: View, event: MotionEvent): Boolean {
                        return false
                    }
                })
            }
            for (i in reviewStepIntValue until tabStrip.childCount) {
                tabStrip.getChildAt(i).setOnTouchListener(object : View.OnTouchListener {
                    override fun onTouch(v: View, event: MotionEvent): Boolean {
                        return true
                    }
                })
            }
        }
    }


    override fun onBasicDataUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, relLayManageRoom)
    }

    override fun onBasicDataInternetError() {
        SnackNotify.checkConnection(onRetryBasicDetail, relLayManageRoom)
    }

    private fun getIntentData(){
        if (intent.hasExtra(ConstIntent.KEY_ROOM_ID)) {
            roomId = intent.getIntExtra(ConstIntent.KEY_ROOM_ID, 0).toString()

            LoginManagerSession.setRoomIdForManageRoom(roomId)
        }else{
            LoginManagerSession.setRoomIdForManageRoom("0")
        }
    }


    override fun onBasicDataSuccess(basicDataModel: BasicDataModel) {
        this.basicDataModel = basicDataModel.data!!
        arrayListAmenity = basicDataModel.data!!.amenities!!.amenityList!!
        arrayListCurrency = basicDataModel.data!!.currency!!.currencyList!!

        getAllCountryList()

    }

    private fun getBasicDeatils() {
        basicDataPresenterImpl.getBasicLocationAndPolicyData(roomId)
    }

    private fun getAllReviewFromApi(){
        manageReviewPresenterImpl.getReviewList(roomId)
    }

    override fun onGetBasicLocationAndPolicySuccess(basicLocationAndPolicyModel: BasicLocationAndPolicyModel) {
        this.basicLocationAndPolicyModel = basicLocationAndPolicyModel.data!!
        reviewStep = this.basicLocationAndPolicyModel.stepsCompleted!!

        //setTabEnableOrDisable()
        if (reviewStep > 3) {
            getAmenityData()
        } else {
            if (viewpager != null) {
                setupViewPager(viewpager)
            }
        }
    }

    private fun getAmenityData() {
        basicDataPresenterImpl.getSelectedAmenityForSelectedRoom(roomId)
    }

    override fun onGetBasicLocationAndPolicyUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, relLayManageRoom)
    }

    override fun onGetBasicLocationAndPolicyInternetError() {
        SnackNotify.checkConnection(onRetryBasicDetailLocationAndPloicy, relLayManageRoom)
    }


    private fun getBasicDeatilForSetAdapter() {
        basicDataPresenterImpl.getBasicData()
    }

    fun setupViewPager(viewPager: ViewPager) {

        if(roomId.equals("0")){
            val bundleBasicDetail = Bundle()
            bundleBasicDetail.putSerializable(ConstIntent.KEY_BASIC_DATA_FRAGMENT, basicDataModel)
            val basicDetailsFragment = BasicDetailsFragment()
            basicDetailsFragment.arguments = bundleBasicDetail

            val bundleAmenityPage = Bundle()
            bundleAmenityPage.putSerializable(ConstIntent.KEY_AMENITY_FRAGMENT, arrayListAmenity)
            bundleAmenityPage.putInt(ConstIntent.KEY_REVIEW_STEPS, reviewStep)
            val amenityFragment = AmenityFragment()
            amenityFragment.arguments = bundleAmenityPage

            val bundlePricingPage = Bundle()
            bundlePricingPage.putSerializable(ConstIntent.KEY_PRICING_FRAGMENT, arrayListCurrency)
            bundlePricingPage.putInt(ConstIntent.KEY_REVIEW_STEPS, reviewStep)
            val pricingFragment = PricingFragment()
            pricingFragment.arguments = bundlePricingPage

            val bundleLocationPage = Bundle()
            bundleLocationPage.putInt(ConstIntent.KEY_REVIEW_STEPS, reviewStep)
            bundleLocationPage.putSerializable(ConstIntent.KEY_LOCATION_PAGE_COUNTRY_LIST, arrayListCountryList)
            val locationFragment = LocationFragment()
            locationFragment.arguments = bundleLocationPage

            /*val bundlePolicyPage = Bundle()
            bundlePolicyPage.putInt(ConstIntent.KEY_REVIEW_STEPS, reviewStep)
            val policyFragment = PolicyFragment()
            policyFragment.arguments = bundlePolicyPage*/

            /*val photosFragment = PhotosFragment()
            val bundlePhotoPage = Bundle()
            bundlePhotoPage.putInt(ConstIntent.KEY_REVIEW_STEPS, reviewStep)
            photosFragment.arguments = bundlePhotoPage*/


            /*val manageReviewsFragment = ManageReviewsFragment()
            val bundleManageReviews = Bundle()
            if(!roomId.equals("0")) {
                bundleManageReviews.putSerializable(ConstIntent.KEY_ARRAY_LIST_REVIEW, arrayListReviewList)
            }
            manageReviewsFragment.arguments = bundleManageReviews*/


            val adapter = Adapter(supportFragmentManager)
            adapter.addFragment(basicDetailsFragment, getString(R.string.basic_detail))
            adapter.addFragment(locationFragment, getString(R.string.location))
            adapter.addFragment(PolicyFragment(), getString(R.string.policy))
            adapter.addFragment(amenityFragment, getString(R.string.amenity))
            adapter.addFragment(pricingFragment, getString(R.string.title_pricing))
            adapter.addFragment(PhotosFragment(), getString(R.string.photos))
            //adapter.addFragment(BookingCalenderFragment(), "Booking  Calender")
            adapter.addFragment(DisableRoomsFragment(), getString(R.string.disable_room))
            adapter.addFragment(ManageReviewsFragment(), getString(R.string.manage_reviews))
            viewpager!!.setAdapter(adapter)
        }else {
            val bundleBasicDetail = Bundle()
            bundleBasicDetail.putSerializable(ConstIntent.KEY_BASIC_DATA_FRAGMENT, basicDataModel)
            bundleBasicDetail.putSerializable(ConstIntent.KEY_BASIC_DETAIL_FRAGMENT, basicLocationAndPolicyModel)
            val basicDetailsFragment = BasicDetailsFragment()
            basicDetailsFragment.arguments = bundleBasicDetail

            val bundleAmenityPage = Bundle()
            bundleAmenityPage.putSerializable(ConstIntent.KEY_AMENITY_FRAGMENT, arrayListAmenity)
            bundleAmenityPage.putInt(ConstIntent.KEY_REVIEW_STEPS, reviewStep)
            if (reviewStep > 3) {
                bundleAmenityPage.putSerializable(ConstIntent.KEY_SELECTED_AMENITY_DATA, amenityModel)
            }
            val amenityFragment = AmenityFragment()
            amenityFragment.arguments = bundleAmenityPage

            val bundlePricingPage = Bundle()
            bundlePricingPage.putSerializable(ConstIntent.KEY_PRICING_FRAGMENT, arrayListCurrency)
            bundlePricingPage.putInt(ConstIntent.KEY_REVIEW_STEPS, reviewStep)
            if (reviewStep > 4) {
                bundlePricingPage.putSerializable(ConstIntent.KEY_PRICING_DETAILS, pricingDetailModel)
            }
            val pricingFragment = PricingFragment()
            pricingFragment.arguments = bundlePricingPage

            val bundleLocationPage = Bundle()
            bundleLocationPage.putInt(ConstIntent.KEY_REVIEW_STEPS, reviewStep)
            if (reviewStep > 1) {
                bundleLocationPage.putSerializable(ConstIntent.KEY_BASIC_DETAIL_FRAGMENT, basicLocationAndPolicyModel)
            }
            bundleLocationPage.putSerializable(ConstIntent.KEY_LOCATION_PAGE_COUNTRY_LIST, arrayListCountryList)
            val locationFragment = LocationFragment()
            locationFragment.arguments = bundleLocationPage

            val bundlePolicyPage = Bundle()
            bundlePolicyPage.putInt(ConstIntent.KEY_REVIEW_STEPS, reviewStep)
            if (reviewStep > 2) {
                bundlePolicyPage.putSerializable(ConstIntent.KEY_BASIC_DETAIL_FRAGMENT, basicLocationAndPolicyModel)
            }
            val policyFragment = PolicyFragment()
            policyFragment.arguments = bundlePolicyPage

            val photosFragment = PhotosFragment()
            val bundlePhotoPage = Bundle()
            bundlePhotoPage.putInt(ConstIntent.KEY_REVIEW_STEPS, reviewStep)
            if (reviewStep > 5) {
                bundlePhotoPage.putSerializable(ConstIntent.KEY_ROOM_PHOTOS, selectedRoomPhotoModel)
            }
            photosFragment.arguments = bundlePhotoPage


            val manageReviewsFragment = ManageReviewsFragment()
            val bundleManageReviews = Bundle()
            bundleManageReviews.putSerializable(ConstIntent.KEY_ARRAY_LIST_REVIEW, arrayListReviewList)
            manageReviewsFragment.arguments = bundleManageReviews

            val disableRoomsFragment = DisableRoomsFragment()
            val bundleDisableRoom = Bundle()
            bundleDisableRoom.putSerializable(ConstIntent.KEY_ARRAY_LIST_DISABLE_ROOM, arrayListDisableRoom)
            disableRoomsFragment.arguments = bundleDisableRoom


            val adapter = Adapter(supportFragmentManager)
        /*    adapter.addFragment(basicDetailsFragment, "Basic Detail")
            adapter.addFragment(locationFragment, "Location")
            adapter.addFragment(policyFragment, "Policy")
            adapter.addFragment(amenityFragment, "Amenity")
            adapter.addFragment(pricingFragment, "Pricing")
            adapter.addFragment(photosFragment, "Photos")
            //adapter.addFragment(BookingCalenderFragment(), "Booking  Calender")
            adapter.addFragment(disableRoomsFragment, "Disable  Rooms")
            adapter.addFragment(manageReviewsFragment, "Manage Reviews")*/

            adapter.addFragment(basicDetailsFragment, getString(R.string.basic_detail))
            adapter.addFragment(locationFragment, getString(R.string.location))
            adapter.addFragment(PolicyFragment(), getString(R.string.policy))
            adapter.addFragment(amenityFragment, getString(R.string.amenity))
            adapter.addFragment(pricingFragment, getString(R.string.title_pricing))
            adapter.addFragment(PhotosFragment(), getString(R.string.photos))
            //adapter.addFragment(BookingCalenderFragment(), "Booking  Calender")
            adapter.addFragment(DisableRoomsFragment(), getString(R.string.disable_room))
            adapter.addFragment(ManageReviewsFragment(), getString(R.string.manage_reviews))

            viewpager!!.setAdapter(adapter)
        }

        setTabEnableOrDisable(reviewStep)
    }


    internal val onRetryBasicDetail: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getBasicDeatilForSetAdapter()
        }

    }
    internal val onRetryAmenityData: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getAmenityData()
        }

    }

    internal val onRetryPricingData: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getPricingDetails()
        }

    }
    internal val onRetryBasicDetailLocationAndPloicy: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getBasicDeatils()
        }

    }

    internal val onRetryPhotos: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getRoomPhotos()
        }

    }

    internal val onRetryCountry: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getAllCountryList()
        }

    }

    internal val onRetryReviewList: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getAllReviewFromApi()
        }

    }

    internal val onRetryDisableRoomList: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getDisableRoomResult()
        }

    }


}

internal class Adapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private val mFragments = ArrayList<Fragment>()
    private val mFragmentTitles = ArrayList<String>()

    fun addFragment(fragment: Fragment, title: String) {
        mFragments.add(fragment)
        mFragmentTitles.add(title)
    }

    override fun getItem(position: Int): Fragment {
        return mFragments.get(position)

    }

    override fun getCount(): Int {
        return mFragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitles.get(position)
    }

}

