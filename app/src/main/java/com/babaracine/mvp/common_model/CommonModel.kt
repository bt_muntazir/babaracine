package com.babaracine.mvp.common_model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CommonModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: List<Any>? = null

}