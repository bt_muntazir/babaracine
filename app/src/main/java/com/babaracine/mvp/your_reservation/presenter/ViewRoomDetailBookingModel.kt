package com.babaracine.mvp.your_reservation.presenter

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

/**
 * Created by Braintech on 07-09-2018.
 */

class ViewRoomDetailBookingModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null



    inner class Data {

        @SerializedName("bookings")
        @Expose
        var bookings: Bookings? = null
        @SerializedName("room")
        @Expose
        var room: Room? = null

        @SerializedName("owner")
        @Expose
        var owner: Owner? = null

        inner class Bookings {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("user_id")
            @Expose
            var userId: Int? = null
            @SerializedName("owner_id")
            @Expose
            var ownerId: Int? = null
            @SerializedName("room_id")
            @Expose
            var roomId: Int? = null
            @SerializedName("number_of_adults")
            @Expose
            var numberOfAdults: Int? = null
            @SerializedName("number_of_children")
            @Expose
            var numberOfChildren: Int? = null
            @SerializedName("status")
            @Expose
            var status: Int? = null
            @SerializedName("start_date")
            @Expose
            var startDate: String? = null
            @SerializedName("end_date")
            @Expose
            var endDate: String? = null
            @SerializedName("email_confirmation")
            @Expose
            var emailConfirmation: Any? = null
            @SerializedName("booking_notes")
            @Expose
            var bookingNotes: String? = null
            @SerializedName("authorize")
            @Expose
            var authorize: Int? = null
            @SerializedName("authorization_id")
            @Expose
            var authorizationId: String? = null
            @SerializedName("payer_id")
            @Expose
            var payerId: String? = null
            @SerializedName("review_complete")
            @Expose
            var reviewComplete: Int? = null
            @SerializedName("review_token")
            @Expose
            var reviewToken: Any? = null
            @SerializedName("guest_review_complete")
            @Expose
            var guestReviewComplete: Int? = null
            @SerializedName("guest_review_token")
            @Expose
            var guestReviewToken: String? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("booking_transactions")
            @Expose
            var bookingTransactions: ArrayList<BookingTransaction>? = null
            @SerializedName("booking_invoice")
            @Expose
            var bookingInvoice: BookingInvoice? = null
            @SerializedName("booking_traveller")
            @Expose
            var bookingTraveller: BookingTraveller? = null

            inner class BookingTransaction {

                @SerializedName("id")
                @Expose
                var id: Int? = null
                @SerializedName("booking_id")
                @Expose
                var bookingId: Int? = null
                @SerializedName("txn_id")
                @Expose
                var txnId: String? = null
                @SerializedName("status")
                @Expose
                var status: String? = null
                @SerializedName("type")
                @Expose
                var type: String? = null
                @SerializedName("date")
                @Expose
                var date: String? = null
                @SerializedName("description")
                @Expose
                var description: String? = null
                @SerializedName("amount")
                @Expose
                var amount: Int? = null
                @SerializedName("currency")
                @Expose
                var currency: String? = null
                @SerializedName("created")
                @Expose
                var created: String? = null
                @SerializedName("modified")
                @Expose
                var modified: String? = null

            }

            inner class BookingInvoice {

                @SerializedName("id")
                @Expose
                var id: Int? = null
                @SerializedName("booking_id")
                @Expose
                var bookingId: Int? = null
                @SerializedName("deposit_percent")
                @Expose
                var depositPercent: Int? = null
                @SerializedName("deposit_amount")
                @Expose
                var depositAmount: Double? = null
                @SerializedName("grand_total")
                @Expose
                var grandTotal: Int? = null
                @SerializedName("remaining_amount")
                @Expose
                var remainingAmount: Double? = null
                @SerializedName("tax_amount")
                @Expose
                var taxAmount: Int? = null
                @SerializedName("tax_name")
                @Expose
                var taxName: String? = null
                @SerializedName("tax_percent")
                @Expose
                var taxPercent: Int? = null
                @SerializedName("modified")
                @Expose
                var modified: String? = null
                @SerializedName("created")
                @Expose
                var created: String? = null

            }


            inner class BookingTraveller {

                @SerializedName("id")
                @Expose
                var id: Int? = null
                @SerializedName("booking_id")
                @Expose
                var bookingId: Int? = null
                @SerializedName("user_id")
                @Expose
                var userId: Int? = null
                @SerializedName("email_address")
                @Expose
                var emailAddress: String? = null
                @SerializedName("first_name")
                @Expose
                var firstName: String? = null
                @SerializedName("last_name")
                @Expose
                var lastName: String? = null
                @SerializedName("address")
                @Expose
                var address: String? = null
                @SerializedName("city")
                @Expose
                var city: String? = null
                @SerializedName("state")
                @Expose
                var state: String? = null
                @SerializedName("zip_code")
                @Expose
                var zipCode: String? = null
                @SerializedName("country")
                @Expose
                var country: String? = null
                @SerializedName("phone")
                @Expose
                var phone: String? = null
                @SerializedName("modified")
                @Expose
                var modified: String? = null
                @SerializedName("created")
                @Expose
                var created: String? = null
                @SerializedName("full_name")
                @Expose
                var fullName: String? = null

            }

        }

        inner class Room {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("user_id")
            @Expose
            var userId: Int? = null
            @SerializedName("name")
            @Expose
            var name: String? = null
            @SerializedName("description")
            @Expose
            var description: String? = null
            @SerializedName("bedrooms")
            @Expose
            var bedrooms: String? = null
            @SerializedName("beds")
            @Expose
            var beds: String? = null
            @SerializedName("bathrooms")
            @Expose
            var bathrooms: String? = null
            @SerializedName("max_occupants")
            @Expose
            var maxOccupants: Int? = null
            @SerializedName("property_type_id")
            @Expose
            var propertyTypeId: Int? = null
            @SerializedName("suitable_for")
            @Expose
            var suitableFor: Any? = null
            @SerializedName("room_type_id")
            @Expose
            var roomTypeId: Int? = null
            @SerializedName("bed_type_id")
            @Expose
            var bedTypeId: Int? = null
            @SerializedName("country")
            @Expose
            var country: String? = null
            @SerializedName("city")
            @Expose
            var city: String? = null
            @SerializedName("state")
            @Expose
            var state: String? = null
            @SerializedName("postal")
            @Expose
            var postal: String? = null
            @SerializedName("address_1")
            @Expose
            var address1: String? = null
            @SerializedName("address_2")
            @Expose
            var address2: String? = null
            @SerializedName("formated_address")
            @Expose
            var formatedAddress: String? = null
            @SerializedName("latitude")
            @Expose
            var latitude: String? = null
            @SerializedName("longitude")
            @Expose
            var longitude: String? = null
            @SerializedName("policy")
            @Expose
            var policy: String? = null
            @SerializedName("is_premium")
            @Expose
            var isPremium: String? = null
            @SerializedName("is_ArrayListed")
            @Expose
            var isArrayListed: String? = null
            @SerializedName("currency")
            @Expose
            var currency: String? = null
            @SerializedName("status")
            @Expose
            var status: String? = null
            @SerializedName("steps_completed")
            @Expose
            var stepsCompleted: Int? = null
            @SerializedName("total_view")
            @Expose
            var totalView: Int? = null
            @SerializedName("deleted_at")
            @Expose
            var deletedAt: Any? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("room_pricing")
            @Expose
            var roomPricing: RoomPricing? = null
            @SerializedName("owner")
            @Expose
            var owner: Owner? = null


            inner class RoomPricing {

                @SerializedName("id")
                @Expose
                var id: Int? = null
                @SerializedName("currency_code")
                @Expose
                var currencyCode: String? = null
                @SerializedName("room_id")
                @Expose
                var roomId: Int? = null
                @SerializedName("nightly_price")
                @Expose
                var nightlyPrice: Int? = null
                @SerializedName("minimum_day")
                @Expose
                var minimumDay: Int? = null
                @SerializedName("long_term_nightly_price")
                @Expose
                var longTermNightlyPrice: Int? = null
                @SerializedName("deposit_frequency")
                @Expose
                var depositFrequency: String? = null
                @SerializedName("deposit_percent")
                @Expose
                var depositPercent: Int? = null
                @SerializedName("deposit_enable")
                @Expose
                var depositEnable: String? = null
                @SerializedName("cancellation_type")
                @Expose
                var cancellationType: String? = null
                @SerializedName("cancellation_charge")
                @Expose
                var cancellationCharge: String? = null
                @SerializedName("cancellation_desc")
                @Expose
                var cancellationDesc: String? = null
                @SerializedName("tax_name")
                @Expose
                var taxName: String? = null
                @SerializedName("tax_percentage")
                @Expose
                var taxPercentage: Int? = null
                @SerializedName("deleted_at")
                @Expose
                var deletedAt: Any? = null
                @SerializedName("created")
                @Expose
                var created: String? = null
                @SerializedName("modified")
                @Expose
                var modified: String? = null
                @SerializedName("_locale")
                @Expose
                var locale: String? = null

            }

        }

        inner class Owner {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("role")
            @Expose
            var role: String? = null
            @SerializedName("username")
            @Expose
            var username: String? = null
            @SerializedName("email")
            @Expose
            var email: String? = null
            @SerializedName("first_name")
            @Expose
            var firstName: String? = null
            @SerializedName("last_name")
            @Expose
            var lastName: String? = null
            @SerializedName("gender")
            @Expose
            var gender: String? = null
            @SerializedName("dob")
            @Expose
            var dob: String? = null
            @SerializedName("country_code")
            @Expose
            var countryCode: String? = null
            @SerializedName("mobile_number")
            @Expose
            var mobileNumber: String? = null
            @SerializedName("contact_number")
            @Expose
            var contactNumber: String? = null
            @SerializedName("profile_picture")
            @Expose
            var profilePicture: String? = null
            @SerializedName("status")
            @Expose
            var status: String? = null
            @SerializedName("email_token")
            @Expose
            var emailToken: String? = null
            @SerializedName("email_verified")
            @Expose
            var emailVerified: Int? = null
            @SerializedName("id_proof")
            @Expose
            var idProof: String? = null
            @SerializedName("id_proof_verified")
            @Expose
            var idProofVerified: String? = null
            @SerializedName("last_login")
            @Expose
            var lastLogin: Any? = null
            @SerializedName("passKey")
            @Expose
            var passKey: String? = null
            @SerializedName("time_out")
            @Expose
            var timeOut: Any? = null
            @SerializedName("deleted_at")
            @Expose
            var deletedAt: Any? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("full_name")
            @Expose
            var fullName: String? = null
            @SerializedName("profile_picture_path")
            @Expose
            var profilePicturePath: String? = null
            @SerializedName("full_image_url")
            @Expose
            var fullImageUrl: String? = null
            @SerializedName("id_proof_path")
            @Expose
            var idProofPath: Boolean? = null
            @SerializedName("date_of_birth")
            @Expose
            var dateOfBirth: String? = null

        }


    }


}
