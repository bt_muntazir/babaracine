package com.babaracine.mvp.your_reservation.presenter

import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.login.presenter.LoginModel
import com.babaracine.mvp.your_reservation.YourReservationModel

/**
 * Created by Braintech on 27-08-2018.
 */
class YourReservationContractor {

    interface YourReservationPresenter{

        fun onYourReservation()

        fun ownerConfirmRoomBook(bookingId:Int)

        fun cancelRoomBook(bookingId:Int)

        fun viewDetailRoomBook(bookingId:Int)

    }

    interface YourReservationView{

        fun onYourReservationSuccess(yourReservationModel: YourReservationModel)
        fun onYourReservationUnSuccess(message : String)
        fun onYourReservationInternetError()

        fun onConfirmRoomBookSuccess(commonModel: CommonModel)
        fun onConfirmRoomBookUnSuccess(message : String)
        fun onConfirmRoomBookInternetError()

        fun onCancelRoomBookSuccess(commonModel: CommonModel)
        fun onCancelRoomBookUnSuccess(message : String)
        fun onCancelRoomBookInternetError()

        fun onViewDetailRoomBookSuccess(viewRoomDetailBookingModel: ViewRoomDetailBookingModel)
        fun onViewDetailRoomBookUnSuccess(message : String)
        fun onViewDetailRoomBookInternetError()
    }
}