package com.babaracine.mvp.your_reservation.presenter

import android.app.Activity
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.login.presenter.LoginContractor
import com.babaracine.mvp.login.presenter.LoginModel
import com.babaracine.mvp.your_reservation.YourReservationModel
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Braintech on 27-08-2018.
 */
class YourReservationPresenterImpl : YourReservationContractor.YourReservationPresenter {


    var activity: Activity
    var yourReservationView: YourReservationContractor.YourReservationView

    constructor(activity: Activity, yourReservationView: YourReservationContractor.YourReservationView) {

        this.activity = activity
        this.yourReservationView = yourReservationView
    }

    override fun onYourReservation() {

        try {
            ApiAdapter.getInstance(activity)
            yourReservationData()
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            yourReservationView.onYourReservationInternetError()
        }
    }

    override fun ownerConfirmRoomBook(bookingId: Int) {
        try {
            ApiAdapter.getInstance(activity)
            ownerConfirmRoomBookData(bookingId)
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            yourReservationView.onConfirmRoomBookInternetError()
        }
    }

    override fun cancelRoomBook(bookingId: Int) {
        try {
            ApiAdapter.getInstance(activity)
            cancelRoomBookData(bookingId)
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            yourReservationView.onCancelRoomBookInternetError()
        }
    }

    override fun viewDetailRoomBook(bookingId: Int) {
        try {
            ApiAdapter.getInstance(activity)
            viewDetailRoomBookData(bookingId)
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            yourReservationView.onViewDetailRoomBookInternetError()
        }
    }


    private fun ownerConfirmRoomBookData(bookingId: Int) {

        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject: JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_BOOKING_ID, bookingId)

        } catch (ex: JSONException) {
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString())

        val getOutput = ApiAdapter.getApiService()!!.confirmRoomBook("application/json", "no-cache", preferredLanguage, authorizationToken, body)

        getOutput.enqueue(object : Callback<CommonModel> {
            override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val commonModel = response.body()

                    if (commonModel.status == 1) {
                        yourReservationView.onConfirmRoomBookSuccess(commonModel)
                    } else if (commonModel.status == 2) {

                        AlertDialogHelper.showMessageToLogout(commonModel.message!!, activity)
                    } else {
                        yourReservationView.onConfirmRoomBookUnSuccess(commonModel.message!!)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    yourReservationView.onConfirmRoomBookUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                Progress.stop()
                yourReservationView.onConfirmRoomBookUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }

    private fun yourReservationData() {

        Progress.start(activity)

        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()

//        val deviceId = Utils.getDeviceId(activity)

        //      var fcmToken= FcmSession(activity);

        val url = Const.BASE_URL + "api/rooms/reservationList.json"

        val getOutput = ApiAdapter.getApiService()!!.yourReservation("application/json", "no-cache", preferredLanguage, authorizationToken, url)

        getOutput.enqueue(object : Callback<YourReservationModel> {
            override fun onResponse(call: Call<YourReservationModel>, response: Response<YourReservationModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val yourReservationModel = response.body()

                    if (yourReservationModel.status == 1) {
                        yourReservationView.onYourReservationSuccess(yourReservationModel)
                    } else if (yourReservationModel.status == 2) {

                        AlertDialogHelper.showMessageToLogout(yourReservationModel.message!!, activity)
                    } else {
                        yourReservationView.onYourReservationUnSuccess(yourReservationModel.message!!)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    yourReservationView.onYourReservationUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<YourReservationModel>, t: Throwable) {
                Progress.stop()
                yourReservationView.onYourReservationUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }

    private fun cancelRoomBookData(bookingId: Int) {

        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject: JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_BOOKING_ID, bookingId)
            jsonObject.put(Const.PARAM_REQUESTED_BY, "Owner")

        } catch (ex: JSONException) {
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString())

        val getOutput = ApiAdapter.getApiService()!!.cancelRoomBook("application/json", "no-cache", preferredLanguage, authorizationToken, body)


        getOutput.enqueue(object : Callback<CommonModel> {
            override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val commonModel = response.body()

                    if (commonModel.status == 1) {
                        yourReservationView.onCancelRoomBookSuccess(commonModel)
                    } else if (commonModel.status == 2) {

                        AlertDialogHelper.showMessageToLogout(commonModel.message!!, activity)
                    } else {
                        yourReservationView.onCancelRoomBookUnSuccess(commonModel.message!!)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    yourReservationView.onCancelRoomBookUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                Progress.stop()
                yourReservationView.onCancelRoomBookUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }

    private fun viewDetailRoomBookData(bookingId: Int) {

        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject: JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_BOOKING_ID, bookingId)

        } catch (ex: JSONException) {
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString())

        val getOutput = ApiAdapter.getApiService()!!.viewDetailRoomBook("application/json", "no-cache", preferredLanguage, authorizationToken, body)


        getOutput.enqueue(object : Callback<ViewRoomDetailBookingModel> {
            override fun onResponse(call: Call<ViewRoomDetailBookingModel>, response: Response<ViewRoomDetailBookingModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val yourReservationModel = response.body()

                    if (yourReservationModel.status == 1) {
                        yourReservationView.onViewDetailRoomBookSuccess(yourReservationModel)
                    } else if (yourReservationModel.status == 2) {

                        AlertDialogHelper.showMessageToLogout(yourReservationModel.message!!, activity)
                    } else {
                        yourReservationView.onViewDetailRoomBookUnSuccess(yourReservationModel.message!!)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    yourReservationView.onViewDetailRoomBookUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<ViewRoomDetailBookingModel>, t: Throwable) {
                Progress.stop()
                t.printStackTrace()
                yourReservationView.onViewDetailRoomBookUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }
}