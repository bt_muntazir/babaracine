package com.babaracine.mvp.your_reservation

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.babaracine.R
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.dashboard.adapter.RoomListingAdapter
import com.babaracine.mvp.login.presenter.LoginModel
import com.babaracine.mvp.your_reservation.presenter.ViewRoomDetailBookingModel
import com.babaracine.mvp.your_reservation.presenter.YourReservationContractor
import com.babaracine.mvp.your_reservation.presenter.YourReservationPresenterImpl
import kotlinx.android.synthetic.main.activity_your_reservation.*
import kotlinx.android.synthetic.main.toolbar_with_back_and_title.*
import okhttp3.internal.Util

class YourReservationActivity : BaseActivity(), YourReservationContractor.YourReservationView {


    lateinit var yourReservationPresenterImpl: YourReservationPresenterImpl

    lateinit var yourReservationAdapter: YourReservationAdapter

    lateinit var yourReservationModel: YourReservationModel

    lateinit var viewRoomDetailBookingModel: ViewRoomDetailBookingModel

    internal lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_your_reservation)

        yourReservationPresenterImpl = YourReservationPresenterImpl(this, this)

        setToolbarTitle()

        setLayoutManager()

        getYourReservationDataApi()

        manageClickEvent()

    }

    private fun setAdapter() {
        yourReservationAdapter = YourReservationAdapter(this, yourReservationModel.data!!.bookings!!)
        recyclerViewYourReservationRooms.setAdapter(yourReservationAdapter)
    }


    private fun setLayoutManager() {

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerViewYourReservationRooms.layoutManager = layoutManager
        recyclerViewYourReservationRooms.isNestedScrollingEnabled = true
    }


    override fun onYourReservationSuccess(yourReservationModel: YourReservationModel) {

        this.yourReservationModel = yourReservationModel
        setAdapter()
    }

    override fun onYourReservationUnSuccess(message: String) {

        SnackNotify.showMessage(message, this, constrainLayoutReservation);
    }

    override fun onYourReservationInternetError() {

    }

    override fun onConfirmRoomBookSuccess(commonModel: CommonModel) {
        SnackNotify.showMessage(commonModel.message!!, this, constrainLayoutReservation)
    }

    override fun onConfirmRoomBookUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, constrainLayoutReservation)
    }

    override fun onConfirmRoomBookInternetError() {

    }

    override fun onCancelRoomBookSuccess(commonModel: CommonModel) {
        SnackNotify.showMessage(commonModel.message!!, this, constrainLayoutReservation)
    }

    override fun onCancelRoomBookUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, constrainLayoutReservation)
    }

    override fun onCancelRoomBookInternetError() {

    }

    override fun onViewDetailRoomBookSuccess(viewRoomDetailBookingModel: ViewRoomDetailBookingModel) {

        this.viewRoomDetailBookingModel = viewRoomDetailBookingModel

        showDialogForViewRoomdetails()

    }



    override fun onViewDetailRoomBookUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, constrainLayoutReservation)
    }

    override fun onViewDetailRoomBookInternetError() {

    }

    public fun getConfirmRoomBooking(bookingId: Int) {

        yourReservationPresenterImpl.ownerConfirmRoomBook(bookingId)

    }

    public fun getCancelRoomBooking(bookingId: Int) {

        yourReservationPresenterImpl.cancelRoomBook(bookingId)
    }

    public fun getViewDetailRoomBooking(bookingId: Int) {
        yourReservationPresenterImpl.viewDetailRoomBook(bookingId)

    }

    fun getYourReservationDataApi() {

        yourReservationPresenterImpl.onYourReservation()
    }

    private fun manageClickEvent() {

        imgViewToolBack.setOnClickListener { finish() }
    }

    private fun setToolbarTitle() {

        txtViewToolTitle.text = getString(R.string.activity_your_reservation)
    }

    fun showDialogForViewRoomdetails() {

        dialog = Dialog(this, R.style.CustomDialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_view_room_book)
        dialog.getWindow()!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.alpha(Color.BLACK)))

        val imgViewClose = dialog.findViewById<View>(R.id.imgViewClose) as ImageView

        val linLayoutBookingId = dialog.findViewById<View>(R.id.linLayoutBookingId) as LinearLayout
        val txtViewBookingId = dialog.findViewById<View>(R.id.txtViewBookingId) as TextView

        val linLayoutBookingStatus = dialog.findViewById<View>(R.id.linLayoutBookingStatus) as LinearLayout
        val txtViewBookingStatus = dialog.findViewById<View>(R.id.txtViewBookingStatus) as TextView

        val linLayoutCheckIn = dialog.findViewById<View>(R.id.linLayoutCheckIn) as LinearLayout
        val txtViewCheckIn = dialog.findViewById<View>(R.id.txtViewCheckIn) as TextView

        val linLayoutCheckOut = dialog.findViewById<View>(R.id.linLayoutCheckOut) as LinearLayout
        val txtViewCheckOut = dialog.findViewById<View>(R.id.txtViewCheckOut) as TextView

        val linLayoutRoomName = dialog.findViewById<View>(R.id.linLayoutRoomName) as LinearLayout
        val txtViewRoomName = dialog.findViewById<View>(R.id.txtViewRoomName) as TextView

        val linLayoutBedRooms = dialog.findViewById<View>(R.id.linLayoutBedRooms) as LinearLayout
        val txtViewBedRooms = dialog.findViewById<View>(R.id.txtViewBedRooms) as TextView

        val linLayoutAddress = dialog.findViewById<View>(R.id.linLayoutAddress) as LinearLayout
        val txtViewAddress = dialog.findViewById<View>(R.id.txtViewAddress) as TextView

        val linLayoutCity = dialog.findViewById<View>(R.id.linLayoutCity) as LinearLayout
        val txtViewCity = dialog.findViewById<View>(R.id.txtViewCity) as TextView

        val linLayoutState = dialog.findViewById<View>(R.id.linLayoutState) as LinearLayout
        val txtViewState = dialog.findViewById<View>(R.id.txtViewState) as TextView

        val linLayoutCountry = dialog.findViewById<View>(R.id.linLayoutCountry) as LinearLayout
        val txtViewCountry = dialog.findViewById<View>(R.id.txtViewCountry) as TextView

        val linLayoutZipCode = dialog.findViewById<View>(R.id.linLayoutZipCode) as LinearLayout
        val txtViewZipCode = dialog.findViewById<View>(R.id.txtViewZipCode) as TextView

        val linLayoutOwnerName = dialog.findViewById<View>(R.id.linLayoutOwnerName) as LinearLayout
        val txtViewOwnerName = dialog.findViewById<View>(R.id.txtViewOwnerName) as TextView

        val linLayoutEmail = dialog.findViewById<View>(R.id.linLayoutEmail) as LinearLayout
        val txtViewEmail = dialog.findViewById<View>(R.id.txtViewEmail) as TextView

        val linLayoutOwnerMobileNumber = dialog.findViewById<View>(R.id.linLayoutOwnerMobileNumber) as LinearLayout
        val txtViewOwnerMobileNumber = dialog.findViewById<View>(R.id.txtViewOwnerMobileNumber) as TextView

        val linLayoutRoomCharge = dialog.findViewById<View>(R.id.linLayoutRoomCharge) as LinearLayout
        val txtViewRoomCharge = dialog.findViewById<View>(R.id.txtViewRoomCharge) as TextView

        val linLayoutTaxAmount = dialog.findViewById<View>(R.id.linLayoutTaxAmount) as LinearLayout
        val txtViewTaxAmount = dialog.findViewById<View>(R.id.txtViewTaxAmount) as TextView

        val linLayoutGrandTotal = dialog.findViewById<View>(R.id.linLayoutGrandTotal) as LinearLayout
        val txtViewGrandTotal = dialog.findViewById<View>(R.id.txtViewGrandTotal) as TextView

        val linLayoutDepositAmount = dialog.findViewById<View>(R.id.linLayoutDepositAmount) as LinearLayout
        val txtViewDepositAmount = dialog.findViewById<View>(R.id.txtViewDepositAmount) as TextView

        val linLayoutRemainingAmount = dialog.findViewById<View>(R.id.linLayoutRemainingAmount) as LinearLayout
        val txtViewRemainingAmount = dialog.findViewById<View>(R.id.txtViewRemainingAmount) as TextView

        val linLayoutTransactionId = dialog.findViewById<View>(R.id.linLayoutTransactionId) as LinearLayout
        val txtViewTransactionId = dialog.findViewById<View>(R.id.txtViewTransactionId) as TextView

        val linLayoutTransactionStatus = dialog.findViewById<View>(R.id.linLayoutTransactionStatus) as LinearLayout
        val txtViewTransactionStatus = dialog.findViewById<View>(R.id.txtViewTransactionStatus) as TextView

        setLayoutManager()

        /*set the booking Id*/
        if (viewRoomDetailBookingModel.data!!.room!!.id!! > 0) {

            linLayoutBookingId.visibility = View.VISIBLE
            txtViewBookingId.text = viewRoomDetailBookingModel.data!!.room!!.id!!.toString()

        } else {

            linLayoutBookingId.visibility = View.GONE
        }


        /*set the booking Status*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.status!!)) {

            linLayoutBookingStatus.visibility = View.VISIBLE
            txtViewBookingStatus.text = viewRoomDetailBookingModel.data!!.room!!.status!!

        } else {

            linLayoutBookingStatus.visibility = View.GONE
        }

        /*set the Check in Date*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.bookings!!.startDate!!)) {

            linLayoutCheckIn.visibility = View.VISIBLE
            txtViewCheckIn.text = viewRoomDetailBookingModel.data!!.bookings!!.startDate!!

        } else {

            linLayoutCheckIn.visibility = View.GONE
        }

        /*set the Check out Date*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.bookings!!.endDate!!)) {

            linLayoutCheckOut.visibility = View.VISIBLE
            txtViewCheckOut.text = viewRoomDetailBookingModel.data!!.bookings!!.endDate!!

        } else {

            linLayoutCheckOut.visibility = View.GONE
        }

        /*set the Bed Room Name*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.name!!)) {

            linLayoutRoomName.visibility = View.VISIBLE
            txtViewRoomName.text = viewRoomDetailBookingModel.data!!.room!!.name!!

        } else {

            linLayoutRoomName.visibility = View.GONE
        }


        /*set the Bed Room*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.bedrooms!!)) {

            linLayoutBedRooms.visibility = View.VISIBLE
            txtViewBedRooms.text = viewRoomDetailBookingModel.data!!.room!!.bedrooms!!

        } else {

            linLayoutCheckOut.visibility = View.GONE
        }

        /*set the Address*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.address1!!)) {

            linLayoutAddress.visibility = View.VISIBLE
            txtViewAddress.text = viewRoomDetailBookingModel.data!!.room!!.address1!!

        } else {

            linLayoutAddress.visibility = View.GONE
        }

        /*set the City*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.city!!)) {

            linLayoutCity.visibility = View.VISIBLE
            txtViewCity.text = viewRoomDetailBookingModel.data!!.room!!.city!!

        } else {

            linLayoutCity.visibility = View.GONE
        }

        /*set the State*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.state!!)) {

            linLayoutState.visibility = View.VISIBLE
            txtViewState.text = viewRoomDetailBookingModel.data!!.room!!.state!!

        } else {

            linLayoutState.visibility = View.GONE
        }

        /*set the Country*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.country!!)) {

            linLayoutCountry.visibility = View.VISIBLE
            txtViewCountry.text = viewRoomDetailBookingModel.data!!.room!!.country!!

        } else {

            linLayoutCountry.visibility = View.GONE
        }

        /*  set the postal code*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.postal!!)) {

            linLayoutZipCode.visibility = View.VISIBLE
            txtViewZipCode.text = viewRoomDetailBookingModel.data!!.room!!.postal!!.toString()

        } else {

            linLayoutZipCode.visibility = View.GONE
        }

        /*set the owner name*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.owner!!.username!!)) {

            linLayoutOwnerName.visibility = View.VISIBLE
            txtViewOwnerName.text = viewRoomDetailBookingModel.data!!.owner!!.username!!.toString()

        } else {

            linLayoutOwnerName.visibility = View.GONE
        }

        /*set the email*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.owner!!.email!!)) {

            linLayoutEmail.visibility = View.VISIBLE
            txtViewEmail.text = viewRoomDetailBookingModel.data!!.owner!!.email!!.toString()

        } else {

            linLayoutEmail.visibility = View.GONE
        }


        /*set the mobile numbner*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.owner!!.mobileNumber!!)) {

            linLayoutOwnerMobileNumber.visibility = View.VISIBLE
            txtViewOwnerMobileNumber.text = viewRoomDetailBookingModel.data!!.owner!!.mobileNumber!!.toString()

        } else {

            linLayoutOwnerMobileNumber.visibility = View.GONE
        }

        /*set the room charge*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.grandTotal!! > 0) {

            linLayoutRoomCharge.visibility = View.VISIBLE
            txtViewRoomCharge.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.grandTotal!!.toString()

        } else {

            linLayoutRoomCharge.visibility = View.GONE
        }

        /*set the tax amount*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.taxAmount!! > 0) {

            linLayoutTaxAmount.visibility = View.VISIBLE
            txtViewTaxAmount.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.taxAmount!!.toString()

        } else {

            linLayoutTaxAmount.visibility = View.GONE
        }

        /*set the grand total*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.grandTotal!! > 0) {

            linLayoutGrandTotal.visibility = View.VISIBLE
            txtViewGrandTotal.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.grandTotal!!.toString()

        } else {

            linLayoutGrandTotal.visibility = View.GONE
        }

        /*set the deposit amount*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.depositAmount!! > 0) {

            linLayoutDepositAmount.visibility = View.VISIBLE
            txtViewDepositAmount.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.depositAmount!!.toString()

        } else {

            linLayoutDepositAmount.visibility = View.GONE
        }

        /*set the remaining amount*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.remainingAmount!! > 0) {

            linLayoutRemainingAmount.visibility = View.VISIBLE
            txtViewRemainingAmount.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.remainingAmount!!.toString()

        } else {

            linLayoutRemainingAmount.visibility = View.GONE
        }

        /*  set the transaction Id*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingTransactions!!.size > 0) {

            linLayoutTransactionId.visibility = View.VISIBLE
            txtViewTransactionId.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingTransactions!!.get(0)!!.txnId

        } else {

            linLayoutTransactionId.visibility = View.GONE
        }

        /*   set the transaction status*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingTransactions!!.size > 0) {

            linLayoutTransactionStatus.visibility = View.VISIBLE
            txtViewTransactionStatus.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingTransactions!!.get(0)!!.status

        } else {

            linLayoutTransactionStatus.visibility = View.GONE
        }


        imgViewClose.setOnClickListener { dialog.dismiss() }

        dialog.show()
    }
}
