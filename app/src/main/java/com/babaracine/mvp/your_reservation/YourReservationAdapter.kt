package com.babaracine.mvp.your_reservation

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babaracine.R
import com.babaracine.common.request_response.Const
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_your_reservation.view.*

/**
 * Created by Braintech on 27-08-2018.
 */
class YourReservationAdapter : RecyclerView.Adapter<YourReservationAdapter.ViewHolder> {

    var context: Context
    var arrayListYoursReservationData: ArrayList<YourReservationModel.Data.Booking>

    constructor(context: Context, arrayListYoursReservationData: ArrayList<YourReservationModel.Data.Booking>) {
        this.context = context
        this.arrayListYoursReservationData = arrayListYoursReservationData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_your_reservation, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = arrayListYoursReservationData[position]

        holder.txtViewHotelName!!.text = dataModel.roomName
        holder.txtViewCheckInTime!!.text = dataModel.startDate
        holder.txtViewCheckOutTime!!.text = dataModel.endDate

        if (dataModel.status == Const.STATUS_CONFIRMED) {

            holder.txtViewStatus!!.text = context.getString(R.string.confirmed)
            holder.btnConfirm.alpha=.5f
            holder.btnConfirm.isEnabled=false

        } else if (dataModel.status == Const.STATUS_ARRIVED) {
            holder.txtViewStatus!!.text = context.getString(R.string.arrived)
        } else if (dataModel.status == Const.STATUS_DEPARTED) {
            holder.txtViewStatus!!.text = context.getString(R.string.departed)
        } else if (dataModel.status == Const.STATUS_ARCHIVED) {
            holder.txtViewStatus!!.text = context.getString(R.string.archived)
        } else if (dataModel.status == Const.STATUS_CANCEL) {
            holder.txtViewStatus!!.text = context.getString(R.string.cancel)

            holder.btnCancel.alpha=.5f
            holder.btnCancel.isEnabled=false
            holder.btnCancel.text=context.getString(R.string.cancelled)

            holder.btnConfirm.alpha=.5f
            holder.btnConfirm.isEnabled=false

        } else if (dataModel.status == Const.STATUS_DISABLED) {
            holder.txtViewStatus!!.text = context.getString(R.string.disabled)
        } else if (dataModel.status == Const.STATUS_PAYMENT_PENDING) {
            holder.txtViewStatus!!.text = context.getString(R.string.payment_pending)
        } else if (dataModel.status == Const.STATUS_EXPIRE) {
            holder.txtViewStatus!!.text = context.getString(R.string.expired)

            holder.btnCancel.alpha=.8f
            holder.btnCancel.isEnabled=false

            holder.btnConfirm.alpha=.5f
            holder.btnConfirm.isEnabled=false
        }

        holder.txtViewGuestName!!.text = dataModel.guestName

        Picasso.with(context).load(dataModel.imageUrl).into(holder.imgViewHotelImage)

        holder.btnConfirm.setOnClickListener()
        {
            holder.btnConfirm.alpha=.5f
            holder.btnConfirm.isEnabled=false

            (context as YourReservationActivity).getConfirmRoomBooking(dataModel.bookingId!!)
        }

        holder.btnCancel.setOnClickListener()
        {
            holder.btnCancel.alpha=.8f
            holder.btnCancel.isEnabled=false
            holder.btnCancel.text=context.getString(R.string.cancelled)

            holder.btnConfirm.alpha=.5f
            holder.btnConfirm.isEnabled=false

            (context as YourReservationActivity).getCancelRoomBooking(dataModel.bookingId!!)
        }

        holder.btnView.setOnClickListener()
        {
            (context as YourReservationActivity).getViewDetailRoomBooking(dataModel.bookingId!!)
        }
    }

    override fun getItemCount(): Int {
        return arrayListYoursReservationData.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val itemLayout = itemView.itemLayout
        val txtViewHotelName = itemView.txtViewHotelName
        val imgViewHotelImage = itemView.imgViewHotelImage
        val txtViewCheckInTime = itemView.txtViewCheckInTime
        val txtViewCheckIn = itemView.txtViewCheckIn
        val txtViewCheckOut = itemView.txtViewCheckOut
        val txtViewCheckOutTime = itemView.txtViewCheckOutTime
        val txtViewGuestNameTitle = itemView.txtViewGuestNameTitle
        val txtViewGuestName = itemView.txtViewGuestName
        val btnView = itemView.btnView
        val txtViewStatus = itemView.txtViewStatus
        val btnConfirm = itemView.btnConfirm
        val btnCancel = itemView.btnCancel

    }
}