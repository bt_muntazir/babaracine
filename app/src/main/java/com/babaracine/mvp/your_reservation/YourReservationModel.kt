package com.babaracine.mvp.your_reservation

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

class YourReservationModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data {

        @SerializedName("bookings")
        @Expose
        var bookings: ArrayList<Booking>? = null


        inner class Booking {

            @SerializedName("booking_id")
            @Expose
            var bookingId: Int? = null
            @SerializedName("room_id")
            @Expose
            var roomId: Int? = null
            @SerializedName("owner_id")
            @Expose
            var ownerId: Int? = null
            @SerializedName("user_id")
            @Expose
            var userId: Int? = null
            @SerializedName("guest_name")
            @Expose
            var guestName: String? = null
            @SerializedName("room_name")
            @Expose
            var roomName: String? = null
            @SerializedName("start_date")
            @Expose
            var startDate: String? = null
            @SerializedName("end_date")
            @Expose
            var endDate: String? = null
            @SerializedName("status")
            @Expose
            var status: Int? = null

            @SerializedName("thumb_image_url")
            @Expose
            var thumbImageUrl: String? = null

            @SerializedName("medium_thumb_image_url")
            @Expose
            var mediumThumbImageUrl: String? = null

            @SerializedName("image_url")
            @Expose
            var imageUrl: String? = null

        }


    }
}