package com.babaracine.mvp.terms_and_condition

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.babaracine.R
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.interfaces.SnakeOnClick
import com.babaracine.common.utility.SnackNotify
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.terms_and_condition.presenter.TermsAndConditionContractor
import com.babaracine.mvp.terms_and_condition.presenter.TermsAndConditionModel
import com.babaracine.mvp.terms_and_condition.presenter.TermsAndConditionPresenterImpl
import kotlinx.android.synthetic.main.activity_terms_and_condition.*
import kotlinx.android.synthetic.main.toolbar_with_back_and_title.*

class TermsAndConditionActivity : BaseActivity(), TermsAndConditionContractor.TermsAndConditionView {

    lateinit var termsAndConditionPresenterImpl : TermsAndConditionPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_and_condition)

        termsAndConditionPresenterImpl = TermsAndConditionPresenterImpl(this,this)
        txtViewToolTitle.text = getString(R.string.activity_terms_and_condition)
        getTermsAndConditionData()

        imgViewToolBack.setOnClickListener { finish() }
    }

    private fun getTermsAndConditionData() {
        termsAndConditionPresenterImpl.onTermsAndCondition()
    }

    override fun onTermsAndConditionSuccess(termsAndConditionModel: TermsAndConditionModel) {


        val webContent = termsAndConditionModel.data!!.termsConditions!!.content
        webViewTermsAndCondition.loadData(webContent, "text/html", "UTF-8")

    }

    override fun onTermsAndConditionUnSuccess(message: String) {
        SnackNotify.showMessage(message,this,constraintLayoutTermsAndCondition)
    }

    override fun onTermsAndConditionInternetError() {
        SnackNotify.checkConnection(OnRetryTermsAndCondition, constraintLayoutTermsAndCondition)
    }

    internal val OnRetryTermsAndCondition : OnClickInterface = object : OnClickInterface{
        override fun onClick() {
            getTermsAndConditionData()
        }

    }

}
