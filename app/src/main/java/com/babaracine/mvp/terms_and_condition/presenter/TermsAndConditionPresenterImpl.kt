package com.babaracine.mvp.terms_and_condition.presenter

import android.app.Activity
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TermsAndConditionPresenterImpl : TermsAndConditionContractor.TermsAndConditionPresenter {

    var activity: Activity
    var termsAndConditionView: TermsAndConditionContractor.TermsAndConditionView

    constructor(activity: Activity, termsAndConditionView: TermsAndConditionContractor.TermsAndConditionView) {

        this.activity = activity
        this.termsAndConditionView = termsAndConditionView
    }

    override fun onTermsAndCondition() {

        try {
            ApiAdapter.getInstance(activity)
         cmcData()
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            termsAndConditionView.onTermsAndConditionInternetError()
        }
    }

    private fun cmcData() {

        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()

        val url = Const.BASE_URL + "api/users/cmsPages.json"

        val getTermsAndConditionResult = ApiAdapter.getApiService()!!.termsAndCondition("application/json", "no-cache", preferredLanguage, url)

        getTermsAndConditionResult.enqueue(object : Callback<TermsAndConditionModel> {
            override fun onResponse(call: Call<TermsAndConditionModel>, response: Response<TermsAndConditionModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val termsAndConditionModel = response.body()

                    if (termsAndConditionModel.status == 1) {
                        termsAndConditionView.onTermsAndConditionSuccess(termsAndConditionModel)
                    } else if (termsAndConditionModel.status == 2) {

                        AlertDialogHelper.showMessageToLogout(termsAndConditionModel.message!!, activity)
                    } else {
                        termsAndConditionView.onTermsAndConditionUnSuccess(termsAndConditionModel.message!!)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    termsAndConditionView.onTermsAndConditionUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<TermsAndConditionModel>, t: Throwable) {
                Progress.stop()
                termsAndConditionView.onTermsAndConditionUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }


}