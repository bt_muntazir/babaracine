package com.babaracine.mvp.terms_and_condition.presenter

interface TermsAndConditionContractor {

    interface TermsAndConditionPresenter{

        fun onTermsAndCondition()

    }

    interface TermsAndConditionView{

        fun onTermsAndConditionSuccess(termsAndConditionModel: TermsAndConditionModel)
        fun onTermsAndConditionUnSuccess(message : String)
        fun onTermsAndConditionInternetError()


    }

}