package com.babaracine.mvp.terms_and_condition.presenter

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList


class TermsAndConditionModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data {

        @SerializedName("terms_conditions")
        @Expose
        var termsConditions: TermsConditions? = null
        @SerializedName("about_us")
        @Expose
        var aboutUs: AboutUs? = null
        @SerializedName("faqs")
        @Expose
        var faqs: ArrayList<Faq>? = null

        inner class Faq {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("category_name")
            @Expose
            var categoryName: String? = null
            @SerializedName("status")
            @Expose
            var status: String? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("faqs")
            @Expose
            var faqs: ArrayList<Faq_>? = null
            @SerializedName("_locale")
            @Expose
            var locale: String? = null


            inner class Faq_ {

                @SerializedName("id")
                @Expose
                var id: Int? = null
                @SerializedName("faq_category_id")
                @Expose
                var faqCategoryId: Int? = null
                @SerializedName("question")
                @Expose
                var question: String? = null
                @SerializedName("answer")
                @Expose
                var answer: String? = null
                @SerializedName("status")
                @Expose
                var status: String? = null
                @SerializedName("created")
                @Expose
                var created: String? = null
                @SerializedName("modified")
                @Expose
                var modified: String? = null
                @SerializedName("_locale")
                @Expose
                var locale: String? = null

            }

        }

        inner class TermsConditions {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("page_name")
            @Expose
            var pageName: String? = null
            @SerializedName("title")
            @Expose
            var title: String? = null
            @SerializedName("url")
            @Expose
            var url: String? = null
            @SerializedName("meta_keyword")
            @Expose
            var metaKeyword: String? = null
            @SerializedName("meta_desc")
            @Expose
            var metaDesc: String? = null
            @SerializedName("content")
            @Expose
            var content: String? = null
            @SerializedName("status")
            @Expose
            var status: String? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("_locale")
            @Expose
            var locale: String? = null

        }

        inner class AboutUs {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("page_name")
            @Expose
            var pageName: String? = null
            @SerializedName("title")
            @Expose
            var title: String? = null
            @SerializedName("url")
            @Expose
            var url: String? = null
            @SerializedName("meta_keyword")
            @Expose
            var metaKeyword: String? = null
            @SerializedName("meta_desc")
            @Expose
            var metaDesc: String? = null
            @SerializedName("content")
            @Expose
            var content: String? = null
            @SerializedName("status")
            @Expose
            var status: String? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("_locale")
            @Expose
            var locale: String? = null

        }

    }


}


