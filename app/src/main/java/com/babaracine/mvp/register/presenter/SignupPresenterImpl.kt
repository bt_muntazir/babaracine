package com.babaracine.mvp.register.presenter

import android.app.Activity
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.login.presenter.LoginModel
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignupPresenterImpl : SignupContractor.SignupPresenter {

    var activity: Activity
    var signupView: SignupContractor.SignupView

    constructor(activity: Activity, signupView: SignupContractor.SignupView) {

        this.activity = activity
        this.signupView = signupView
    }


    override fun onSignupApi(firstName: String, lastName: String, mobileNumber: String, email: String, gender: String, dob: String, password: String, confirmPassword: String, subscription: String) {
        try {
            ApiAdapter.getInstance(activity)
            signup(firstName,lastName,mobileNumber,email,gender,dob,password,confirmPassword,subscription)
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            signupView.onSignupInternetError()
        }
    }

    private fun signup(firstName: String, lastName: String, mobileNumber: String, email: String, gender: String, dob: String, password: String, confirmPassword: String, subscription: String) {

        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()

        var jsonObject: JSONObject? = null

        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_FIRST_NAME, firstName)
            jsonObject.put(Const.PARAM_LAST_NAME, lastName)
            jsonObject.put(Const.PARAM_MOBILE_NUMBER, mobileNumber)
            jsonObject.put(Const.PARAM_EMAIL, email)
            jsonObject.put(Const.PARAM_GENDER, gender)
            jsonObject.put(Const.PARAM_DOB, dob)
            jsonObject.put(Const.PARAM_PASSWORD, password)
            jsonObject.put(Const.PARAM_CONFIRM_PASSWORD, confirmPassword)
            jsonObject.put(Const.PARAM_SUBSCRIPTION, subscription)
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject!!.toString())

        val getSignupResult = ApiAdapter.getApiService()!!.signup("application/json", "no-cache", preferredLanguage, body)

        getSignupResult.enqueue(object : Callback<SignupModel> {
            override fun onResponse(call: Call<SignupModel>, response: Response<SignupModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val signupModel = response.body()

                    if (signupModel.status == 1) {
                        signupView.onSignupSuccess(signupModel)
                    } else {
                        signupView.onSignupUnSuccess(signupModel.message!!)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    signupView.onSignupUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<SignupModel>, t: Throwable) {
                Progress.stop()
                signupView.onSignupUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }
}