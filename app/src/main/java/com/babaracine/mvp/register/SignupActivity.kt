package com.babaracine.mvp.register

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.TextView
import com.babaracine.R
import com.babaracine.common.adapter.AdapterSpinner
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.Const
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.login.LoginActivity
import com.babaracine.mvp.register.presenter.SignupContractor
import com.babaracine.mvp.register.presenter.SignupModel
import com.babaracine.mvp.register.presenter.SignupPresenterImpl
import kotlinx.android.synthetic.main.activity_signup2.*
import kotlinx.android.synthetic.main.content_signup.*
import java.util.*
import kotlin.collections.ArrayList
import java.text.SimpleDateFormat


class SignupActivity : BaseActivity(),SignupContractor.SignupView {

    lateinit var arrayListDay: ArrayList<HashMap<String, String>>
    lateinit var arrayListMonth: ArrayList<HashMap<String, String>>
    lateinit var arrayListYear: ArrayList<HashMap<String, String>>

    var dayId = "0"
    lateinit var day : String
    var monthId ="0"
    lateinit var month : String
    var yearId = "0"
    lateinit var year : String

    var selectedGenderValue = "Male"


    lateinit var arrayListDays: ArrayList<String>
    lateinit var arrayListMonths: ArrayList<String>
    lateinit var arrayListYears: ArrayList<String>

    lateinit var signupPresenterImpl: SignupPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup2)

        signupPresenterImpl = SignupPresenterImpl(this,this)

        manageClickEvent()
        bindMonthAndYear()
        setAdapterDate()
    }

    override fun onSignupSuccess(signupModel: SignupModel) {
        alertForClickOkAndFinish(signupModel.message)
    }

    override fun onSignupUnSuccess(message: String) {
        SnackNotify.showMessage(message,this,constraintLayoutSignup)
    }

    override fun onSignupInternetError() {
        SnackNotify.checkConnection(onRetrySignup,constraintLayoutSignup)
    }

    fun alertForClickOkAndFinish(msg: String) {

        val alertDialoge = AlertDialog.Builder(this)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(getString(R.string.title_ok)) { dialog, which ->

            val intent = Intent(this,LoginActivity::class.java)
            startActivity(intent)
        }
        alertDialoge.show()
    }



    private fun manageClickEvent() {
        txtViewAlreadyHaveAnAccount.setOnClickListener(View.OnClickListener { onClickAlreadyHaveAnAccount() })

        imgViewBack.setOnClickListener(View.OnClickListener { onClickBackArrow() })

        btnSignup.setOnClickListener(View.OnClickListener { onClickBtnSignup() })

        spinnerMonth?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position != 0) {
                    monthId = arrayListMonth.get(position).get(Const.KEY_ID).toString()
                    month = arrayListMonth.get(position).get(Const.KEY_NAME).toString()
                }
            }
        }

        spinnerDay?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position != 0) {
                    dayId = arrayListDay.get(position).get(Const.KEY_ID).toString()
                    day = arrayListDay.get(position).get(Const.KEY_NAME).toString()
                }
            }
        }

        spinnerYear?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if(position != 0) {
                    yearId = arrayListYear.get(position).get(Const.KEY_ID).toString()
                    year = arrayListYear.get(position).get(Const.KEY_NAME).toString()
                }
            }
        }

        imgViewBack.setOnClickListener { finish() }
    }

    private fun onClickBtnSignup() {

        val selectedRadioid = radiGroupGender.checkedRadioButtonId
        when (selectedRadioid) {
            R.id.radioBtnMale -> {
                selectedGenderValue = getString(R.string.tag_male)
            }
            R.id.radioBtnFemale -> {
                selectedGenderValue = getString(R.string.tag_female)
            }
            R.id.radioBtnOther -> {
                selectedGenderValue = getString(R.string.tag_other)
            }
        }



        val firstName = edtTextFirstName.text.toString().trim()
        val lastName = edtTextLastName.text.toString().trim()
        val mobileNumber = edtTextMobileNumber.text.toString().trim()
        val email = edtTextEmail.text.toString().trim()
        val password = edtTextPassword.text.toString().trim()
        val confirmPassword = edtTextConfirmPassword.text.toString().trim()
        val subscriptionValue : String
        if(checkBoxSendMarketingMsg.isChecked){
            subscriptionValue = "1"
        }else{
            subscriptionValue = "2"
        }

        if(isValidaData(firstName,lastName,mobileNumber,email,password,confirmPassword)){


            val cal = Calendar.getInstance()
            cal.set(Calendar.YEAR, year.toInt())
            cal.set(Calendar.DAY_OF_MONTH, day.toInt()-1)
            cal.set(Calendar.MONTH, monthId.toInt())
            val selectedBirthdayDate = SimpleDateFormat("yyyy-MM-dd").format(cal.time)

            //Log.e("selectedBirthdayDate",selectedBirthdayDate)

            signupPresenterImpl.onSignupApi(firstName,lastName,mobileNumber,email,selectedGenderValue, selectedBirthdayDate,password,confirmPassword,subscriptionValue)

/*
            val intent = Intent(this,LoginActivity::class.java)
            startActivity(intent)*/

        }

    }

    private fun isValidaData(firstName: String, lastName: String, mobileNumber: String, email: String, password: String, confirmPassword: String): Boolean {
        if(Utils.isEmptyOrNull(firstName)){
            edtTextFirstName.setError(getString(R.string.error_first_name))
            edtTextFirstName.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(lastName)){
            edtTextLastName.setError(getString(R.string.error_last_name))
            edtTextLastName.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(mobileNumber)){
            edtTextMobileNumber.setError(getString(R.string.error_mobile_number))
            edtTextMobileNumber.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(email)){
            edtTextEmail.setError(getString(R.string.error_email))
            edtTextEmail.requestFocus()
            return false
        }else if(!Utils.isValidEmail(email)){
            edtTextEmail.setError(getString(R.string.error_valid_email))
            edtTextEmail.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(password)){
            edtTextPassword.setError(getString(R.string.error_password))
            edtTextPassword.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(confirmPassword)){
            edtTextConfirmPassword.setError(getString(R.string.error_confirm_password))
            edtTextConfirmPassword.requestFocus()
            return false
        }else if(!Utils.isPasswordMatch(password,confirmPassword)){
            edtTextConfirmPassword.setError(getString(R.string.error_password_confrm_pass_not_match))
            edtTextConfirmPassword.requestFocus()
            return false
        }else if(dayId.equals("0")){
            SnackNotify.showMessage(getString(R.string.error_day),this,constraintLayoutSignup)
            return false
        }else if(monthId.equals("0")){
            SnackNotify.showMessage(getString(R.string.error_month),this,constraintLayoutSignup)
            return false
        }else if(yearId.equals("0")){
            SnackNotify.showMessage(getString(R.string.error_year),this,constraintLayoutSignup)
            return false
        }
        return true
    }

    private fun onClickBackArrow() {
        finish()
    }

    private fun onClickAlreadyHaveAnAccount() {
        val intent = Intent(this@SignupActivity, LoginActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    fun bindMonthAndYear() {

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)-18
        val month = c.get(Calendar.MONTH)

/*
        this.month = (month+1)!!.toString()
        this.year = year!!.toString()

*/
        arrayListDays = ArrayList()
        arrayListDays.add("01")
        arrayListDays.add("02")
        arrayListDays.add("03")
        arrayListDays.add("04")
        arrayListDays.add("05")
        arrayListDays.add("06")
        arrayListDays.add("07")
        arrayListDays.add("08")
        arrayListDays.add("09")
        arrayListDays.add("10")
        arrayListDays.add("11")
        arrayListDays.add("12")
        arrayListDays.add("13")
        arrayListDays.add("14")
        arrayListDays.add("15")
        arrayListDays.add("16")
        arrayListDays.add("17")
        arrayListDays.add("18")
        arrayListDays.add("19")
        arrayListDays.add("20")
        arrayListDays.add("21")
        arrayListDays.add("22")
        arrayListDays.add("23")
        arrayListDays.add("24")
        arrayListDays.add("25")
        arrayListDays.add("26")
        arrayListDays.add("27")
        arrayListDays.add("28")
        arrayListDays.add("29")
        arrayListDays.add("30")
        arrayListDays.add("31")

        arrayListMonths = ArrayList()
        arrayListMonths.add(getString(R.string.january))
        arrayListMonths.add(getString(R.string.february))
        arrayListMonths.add(getString(R.string.march))
        arrayListMonths.add(getString(R.string.april))
        arrayListMonths.add(getString(R.string.may))
        arrayListMonths.add(getString(R.string.june))
        arrayListMonths.add(getString(R.string.july))
        arrayListMonths.add(getString(R.string.august))
        arrayListMonths.add(getString(R.string.september))
        arrayListMonths.add(getString(R.string.october))
        arrayListMonths.add(getString(R.string.november))
        arrayListMonths.add(getString(R.string.december))

        arrayListYears = ArrayList()

        for (i in 0..80) {
            arrayListYears.add((year - i).toString())
        }

    }

    private fun setAdapterDate() {
        arrayListMonth = ArrayList()
        val hashMapMonth = java.util.HashMap<String, String>()
        hashMapMonth.put(Const.KEY_ID, "0")
        hashMapMonth.put(Const.KEY_NAME, getString(R.string.tag_month))
        arrayListMonth.add(hashMapMonth)

        val adapterMonth = AdapterSpinner(this, R.layout.spinner_item, arrayListMonth,"white")
        spinnerMonth.setAdapter(adapterMonth)


        arrayListYear = ArrayList()
        val hashMapYear = java.util.HashMap<String, String>()
        hashMapYear.put(Const.KEY_ID, "0")
        hashMapYear.put(Const.KEY_NAME, getString(R.string.tag_year))
        arrayListYear.add(hashMapYear)

        val adapterYear = AdapterSpinner(this, R.layout.spinner_item, arrayListYear,"white")
        spinnerYear.setAdapter(adapterYear)


        arrayListDay = ArrayList()
        val hashMapDay = java.util.HashMap<String, String>()
        hashMapDay.put(Const.KEY_ID, "0")
        hashMapDay.put(Const.KEY_NAME, getString(R.string.tag_day))
        arrayListDay.add(hashMapDay)

        val adapterDay = AdapterSpinner(this, R.layout.spinner_item, arrayListDay,"white")
        spinnerDay.setAdapter(adapterDay)


        for (i in 0 until arrayListMonths.size) {

            val name = arrayListMonths.get(i)
            val hashMap: HashMap<String, String> = HashMap()
            hashMap.put(Const.KEY_ID, (i + 1).toString())
            hashMap.put(Const.KEY_NAME, name)
            arrayListMonth.add(hashMap)
        }

        for (i in 0 until arrayListYears.size) {

            val name = arrayListYears.get(i)
            val hashMap: HashMap<String, String> = HashMap()
            hashMap.put(Const.KEY_ID, (i + 1).toString())
            hashMap.put(Const.KEY_NAME, name)
            arrayListYear.add(hashMap)
        }


        for (i in 0 until arrayListDays.size) {

            val name = arrayListDays.get(i)
            val hashMap: HashMap<String, String> = HashMap()
            hashMap.put(Const.KEY_ID, (i + 1).toString())
            hashMap.put(Const.KEY_NAME, name)
            arrayListDay.add(hashMap)
        }


        val adapterMonths = AdapterSpinner(this, R.layout.spinner_item, arrayListMonth,"white")
        spinnerMonth.setAdapter(adapterMonths)

        val adapterYears = AdapterSpinner(this, R.layout.spinner_item, arrayListYear,"white")
        spinnerYear.setAdapter(adapterYears)

        val adapterDays = AdapterSpinner(this, R.layout.spinner_item, arrayListDay,"white")
        spinnerDay.setAdapter(adapterDays)
/*
        for (i in 0 until arrayListMonths!!.size) {

            if (month!!.equals((i + 1).toString())) {

                spinnerMonth.setSelection(i + 1)

            }
        }*/
/*
        for (i in 0 until arrayListYears!!.size) {

            val name = arrayListYears!!.get(i)

            if (year!!.equals(name, true)) {

                spinnerYear.setSelection(i + 1)

            }
        }
    */

    }


    internal var onRetrySignup: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            onClickBtnSignup()
        }
    }
}
