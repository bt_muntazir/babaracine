package com.babaracine.mvp.register.presenter

interface SignupContractor {

    interface SignupPresenter{

        fun onSignupApi(firstName : String,lastName : String,mobileNumber : String,email : String,gender : String,dob : String,password : String,confirmPassword : String,subscription : String)

    }

    interface SignupView{

        fun onSignupSuccess(signupModel: SignupModel)
        fun onSignupUnSuccess(message : String)
        fun onSignupInternetError()
    }
}