package com.babaracine.mvp.property_list

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.babaracine.R
import com.babaracine.common.adapter.AdapterSpinner
import com.babaracine.common.request_response.Const
import com.babaracine.common.utility.SnackNotify
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.property_list.presenter.PropertyListContractor
import com.babaracine.mvp.property_list.presenter.PropertyListPresenterImpl
import com.babaracine.mvp.your_reservation.YourReservationAdapter
import com.babaracine.mvp.your_reservation.YourReservationModel
import kotlinx.android.synthetic.main.activity_property_list.*
import kotlinx.android.synthetic.main.activity_your_reservation.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import java.util.HashMap

class PropertyListActivity : BaseActivity(), PropertyListContractor.PropertyListView {


    lateinit var propertyListPresenterImpl: PropertyListPresenterImpl
    lateinit var propertyListAdapter: PropertyListAdapter
    lateinit var propertyListModel: PropertyListModel

    lateinit var arrayListFilter: ArrayList<String>
    lateinit var arrayListHashFilter: ArrayList<HashMap<String, String>>

    var filterType = ""

    var roomId: Int = 0
    var listedUnlistedRoom: String = ""

    //for paging
    var isEndReached: Boolean = false
    var mIsLoading: Boolean = false
    var page = 0
    lateinit var linearLayoutManager: LinearLayoutManager

    lateinit var arrayListRoomData: ArrayList<PropertyListModel.Data.Room>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_list)

        propertyListPresenterImpl = PropertyListPresenterImpl(this, this)

        setToolbarTitle()

        setLayoutManager()

        arrayListRoomData = ArrayList()

        bindFilterList()

        setFilterAdapterSection()

        setAdapter()

        manageClickEvents()

        getPropertyListDataApi(true)
    }

    fun bindFilterList() {

        arrayListHashFilter = java.util.ArrayList<java.util.HashMap<String, String>>()

        val hashMapMonth = java.util.HashMap<String, String>()
        hashMapMonth.put(Const.KEY_ID, "0")
        hashMapMonth.put(Const.KEY_NAME, getString(R.string.sort_by))
        arrayListHashFilter.add(hashMapMonth)

        arrayListFilter = ArrayList()
        arrayListFilter.add(getString(R.string.asc))
        arrayListFilter.add(getString(R.string.desc))
    }

    private fun setFilterAdapterSection() {

        for (i in 0 until arrayListFilter!!.size) {

            val name = arrayListFilter!!.get(i)
            val hashMap: HashMap<String, String> = HashMap()
            hashMap.put(Const.KEY_ID, (i + 1).toString())
            hashMap.put(Const.KEY_NAME, name!!)
            arrayListHashFilter.add(hashMap)
        }

        val adapterMonth = AdapterSpinner(this, R.layout.spinner_item, arrayListHashFilter, "black")
        spinnerFilter.setAdapter(adapterMonth)
    }


    override fun onPropertyListSuccess(propertyListModel: PropertyListModel) {

        this.propertyListModel = propertyListModel

        if (propertyListModel.data != null) {

            arrayListRoomData = propertyListModel.data!!.rooms!!

            //setAdapter()

            if (arrayListRoomData != null) {
                if (arrayListRoomData.size < 10) {
                    isEndReached = true
                } else {
                    isEndReached = false
                }
                //setAdapter()
                propertyListAdapter.updateArrayListData(arrayListRoomData, page)

            }

            Handler().postDelayed(Runnable { mIsLoading = false }, 1000)
        }


    }

    override fun onPropertyListUnSuccess(message: String) {

        SnackNotify.showMessage(message, this, constrainLayoutPropertyList)
    }

    override fun onPropertyListInternetError() {

    }

    override fun onRoomListUnlistSuccess(coomonModel: CommonModel) {

        page = 0
        getPropertyListDataApi(true)
    }

    override fun onRoomListUnlistUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, constrainLayoutPropertyList)
    }

    override fun onRoomListUnlistInternetError() {


    }


    public fun callListedUnlisted(roomId: Int, listedUnlistedRoom: String) {

        this.roomId = roomId
        this.listedUnlistedRoom = listedUnlistedRoom

        callListedUnlistedDataApi()

    }

    fun callListedUnlistedDataApi() {

        propertyListPresenterImpl.onRoomListUnlistData(roomId,listedUnlistedRoom)

    }

    fun manageClickEvents() {

        imgViewToolBack.setOnClickListener { finish() }

        spinnerFilter?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position != 0) {

                    val selectedId = arrayListHashFilter.get(position).get(Const.KEY_ID).toString()
                    filterType = arrayListHashFilter.get(position).get(Const.KEY_NAME).toString()
                    val selectedNamePlusOne = selectedId.toInt() + 1

                    if (filterType.equals(getString(R.string.asc))) {
                        page = 0
                        getPropertyListDataApi(true)
                    } else if (filterType.equals(getString(R.string.desc))) {
                        page = 0
                        getPropertyListDataApi(true)
                    }
                }
            }
        }
    }

    private fun getPropertyListDataApi(isRetry: Boolean) {

        mIsLoading = true
        if (isRetry) {
            if (page == 0) {
                page = 1
            }
            propertyListPresenterImpl.onPropertyList(filterType, page)
        } else {
            page = page + 1
            propertyListPresenterImpl.onPropertyList(filterType, page)
        }


    }


    private fun setLayoutManager() {

        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerViewPropertyList.layoutManager = linearLayoutManager as RecyclerView.LayoutManager?
        recyclerViewPropertyList.isNestedScrollingEnabled = true
        recyclerViewPropertyList.addOnScrollListener(mRecyclerViewOnScrollListener);
    }

    // implement paging
    private val mRecyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView,
                                          newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = linearLayoutManager.getChildCount()
            val totalItemCount = linearLayoutManager.getItemCount()
            val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()

            if (!mIsLoading && !isEndReached) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                    getPropertyListDataApi(false)
                }
            }
        }
    }


    private fun setAdapter() {
        propertyListAdapter = PropertyListAdapter(this, arrayListRoomData)
        recyclerViewPropertyList.setAdapter(propertyListAdapter)
    }


    private fun setToolbarTitle() {

        txtViewToolTitle.text = getString(R.string.activity_property_list)
    }
}
