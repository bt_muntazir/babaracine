package com.babaracine.mvp.property_list

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.mvp.manage_room.ManageRoomActivity
import com.babaracine.mvp.your_reservation.YourReservationAdapter
import com.babaracine.mvp.your_reservation.YourReservationModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_property_list.view.*

/**
 * Created by Braintech on 27-08-2018.
 */
class PropertyListAdapter : RecyclerView.Adapter<PropertyListAdapter.ViewHolder> {

    var context: Context
    var arrayListPropertyListingData: ArrayList<PropertyListModel.Data.Room>

    constructor(context: Context, arrayListPropertyListingData: ArrayList<PropertyListModel.Data.Room>) {
        this.context = context
        this.arrayListPropertyListingData = arrayListPropertyListingData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_property_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = arrayListPropertyListingData[position]

        holder.txtViewHotelName!!.text = dataModel.name
        //   holder.txtViewManageRoom!!.text = dataModel.locale

        if (dataModel.stepsCompleted!! < 6) {

            dataModel.isListedRoom = false

            holder.txtViewListed!!.text = dataModel.status
            holder.txtViewListed!!.setBackground(ContextCompat.getDrawable(context, R.drawable.drawable_button))
            /*holder.txtViewListed!!.isEnabled = false
            holder.txtViewListed!!.alpha = .5f*/


        } else {

            /*holder.txtViewListed!!.isEnabled = true
            holder.txtViewListed!!.alpha = 1f
*/
            if (dataModel.isListed.equals("Listed")) {

                dataModel.isListedRoom = true

                holder.txtViewListed!!.text = dataModel.isListed
                holder.txtViewListed!!.setBackground(ContextCompat.getDrawable(context, R.drawable.drawable_button_green))
            } else {

                dataModel.isListedRoom = false

                holder.txtViewListed!!.text = dataModel.isListed
                holder.txtViewListed!!.setBackground(ContextCompat.getDrawable(context, R.drawable.drawable_button))

            }
        }

        if (dataModel.roomFeaturedPhoto != null) {

            Picasso.with(context).load(dataModel.roomFeaturedPhoto!!.imageUrl).into(holder.imgViewRoom)

        }

        holder.txtViewManageRoom.setOnClickListener()
        {
            LoginManagerSession.setIsManageFrom(true)
            val intent = Intent(context, ManageRoomActivity::class.java)
            intent.putExtra(ConstIntent.KEY_ROOM_ID, dataModel.id)
            context.startActivity(intent)
        }


        holder.txtViewListed.setOnClickListener()
        {
            if (dataModel.stepsCompleted!! >= 6) {
                if (dataModel.isListed.equals("Listed")) {
                    (context as PropertyListActivity).callListedUnlisted(dataModel.id!!, "Unlisted")
                } else {
                    (context as PropertyListActivity).callListedUnlisted(dataModel.id!!, "Listed")
                }
            }
            else{
                AlertDialogHelper.showMessage(context.getString(R.string.complete_necessory_steps_to_change_status),context)

            }

        }
    }

    fun updateArrayListData(arrayListPendingNew: java.util.ArrayList<PropertyListModel.Data.Room>?, page: Int) {

        if (arrayListPendingNew != null && arrayListPropertyListingData != null) {
            // arrayListPending.addAll(arrayListPendingNew);

            if (page == 1) {

                arrayListPropertyListingData.clear()
            }

            for (i in arrayListPendingNew.indices) {
                arrayListPropertyListingData.add(arrayListPendingNew[i])
            }

            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return arrayListPropertyListingData.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val itemLayout = itemView.itemLayout
        val imgViewRoom = itemView.imgViewRoom
        val txtViewHotelName = itemView.txtViewHotelName
        val txtViewManageRoom = itemView.txtViewManageRoom
        val txtViewListed = itemView.txtViewListed

    }
}