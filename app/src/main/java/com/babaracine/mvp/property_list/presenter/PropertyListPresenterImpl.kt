package com.babaracine.mvp.property_list.presenter

import android.app.Activity
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.property_list.PropertyListModel
import com.babaracine.mvp.your_reservation.YourReservationModel
import com.babaracine.mvp.your_reservation.presenter.YourReservationContractor
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Braintech on 27-08-2018.
 */
class PropertyListPresenterImpl : PropertyListContractor.PropertyListPresenter {


    var activity: Activity
    var propertyListView: PropertyListContractor.PropertyListView

    constructor(activity: Activity, propertyListView: PropertyListContractor.PropertyListView) {

        this.activity = activity
        this.propertyListView = propertyListView
    }

    override fun onPropertyList(filterType: String, page: Int) {

        try {
            ApiAdapter.getInstance(activity)
            propertyListData(filterType, page)
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            propertyListView.onPropertyListInternetError()
        }
    }

    override fun onRoomListUnlistData(roomId: Int, listedUnlistedRoom: String) {
        try {
            ApiAdapter.getInstance(activity)
            changeRoomStatusListedUnlisted(roomId, listedUnlistedRoom)
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            propertyListView.onRoomListUnlistInternetError()
        }
    }

    private fun propertyListData(filterType: String, page: Int) {

        Progress.start(activity)

        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()


        // val url = Const.BASE_URL + "api/Properties/listing.json"

        val getLoginOutput = ApiAdapter.getApiService()!!.propertyList("application/json", "no-cache", preferredLanguage, authorizationToken, filterType, page)

        getLoginOutput.enqueue(object : Callback<PropertyListModel> {
            override fun onResponse(call: Call<PropertyListModel>, response: Response<PropertyListModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val propertyListModel = response.body()

                    if (propertyListModel.status == 1) {
                        propertyListView.onPropertyListSuccess(propertyListModel)
                    } else if (propertyListModel.status == 2) {

                        AlertDialogHelper.showMessageToLogout(propertyListModel.message!!, activity)
                    } else {
                        propertyListView.onPropertyListUnSuccess(propertyListModel.message!!)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    propertyListView.onPropertyListUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<PropertyListModel>, t: Throwable) {
                Progress.stop()
                propertyListView.onPropertyListUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }

    private fun changeRoomStatusListedUnlisted(roomId: Int, listedUnlistedRoom: String) {

        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()

        var jsonObject: JSONObject? = null
        try {
            jsonObject = JSONObject()

            jsonObject.put(Const.PARAM_ROOM_ID, roomId)
            jsonObject.put(Const.PARAM_STATUS, listedUnlistedRoom)

        } catch (ex: JSONException) {
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject!!.toString())

        val getOutput = ApiAdapter.getApiService()!!.updateRoomStatusListedUnlisted("application/json", "no-cache", preferredLanguage, authorizationToken, body)


        getOutput.enqueue(object : Callback<CommonModel> {
            override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val commonModel = response.body()

                    if (commonModel.status == 1) {
                        propertyListView.onRoomListUnlistSuccess(commonModel)
                    } else if (commonModel.status == 2) {

                        AlertDialogHelper.showMessageToLogout(commonModel.message!!, activity)
                    } else {
                        propertyListView.onRoomListUnlistUnSuccess(commonModel.message!!)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    propertyListView.onRoomListUnlistUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                Progress.stop()
                propertyListView.onRoomListUnlistUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }
}