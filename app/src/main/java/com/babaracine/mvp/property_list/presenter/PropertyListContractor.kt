package com.babaracine.mvp.property_list.presenter

import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.property_list.PropertyListModel
import com.babaracine.mvp.your_reservation.YourReservationModel

/**
 * Created by Braintech on 27-08-2018.
 */
class PropertyListContractor {

    interface PropertyListPresenter{

        fun onPropertyList(filterType:String,page:Int)

        fun onRoomListUnlistData(roomId:Int,listedUnlistedRoom:String)

    }

    interface PropertyListView{

        fun onPropertyListSuccess(propertyListModel: PropertyListModel)
        fun onPropertyListUnSuccess(message : String)
        fun onPropertyListInternetError()

        fun onRoomListUnlistSuccess(coomonModel: CommonModel)
        fun onRoomListUnlistUnSuccess(message : String)
        fun onRoomListUnlistInternetError()
    }
}