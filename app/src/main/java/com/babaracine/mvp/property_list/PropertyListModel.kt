package com.babaracine.mvp.property_list

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

/**
 * Created by Braintech on 27-08-2018.
 */

class PropertyListModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null


    inner class Data {

        @SerializedName("rooms")
        @Expose
        var rooms: ArrayList<Room>? = null


        inner class Room {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("name")
            @Expose
            var name: String? = null
            @SerializedName("description")
            @Expose
            var description: String? = null
            @SerializedName("user_id")
            @Expose
            var userId: Int? = null
            @SerializedName("status")
            @Expose
            var status: String? = null
            @SerializedName("is_listed")
            @Expose
            var isListed: String? = null
            @SerializedName("steps_completed")
            @Expose
            var stepsCompleted: Int? = null
            @SerializedName("room_featured_photo")
            @Expose
            var roomFeaturedPhoto: RoomFeaturedPhoto? = null
            @SerializedName("_locale")
            @Expose
            var locale: String? = null

            var isListedRoom: Boolean? = null


            inner class RoomFeaturedPhoto {

                @SerializedName("name")
                @Expose
                var name: String? = null
                @SerializedName("dir")
                @Expose
                var dir: String? = null
                @SerializedName("medium_thumb_image_url")
                @Expose
                var mediumThumbImageUrl: String? = null
                @SerializedName("thumb_image_url")
                @Expose
                var thumbImageUrl: String? = null
                @SerializedName("image_url")
                @Expose
                var imageUrl: String? = null

            }
        }
    }
}