package com.babaracine.mvp.base_activity

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
 import android.media.MediaFormat.KEY_LANGUAGE
import com.babaracine.common.application.BabaracineApplication
import com.babaracine.common.language.ContextWrapper
import com.babaracine.common.request_response.Const
import com.babaracine.common.utility.PrefUtil
import java.util.*


public open class BaseActivity: AppCompatActivity() {


   // var strLocales = arrayOf("es", "fr", "es")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }




   override protected fun attachBaseContext(newBase: Context) {
        val lang_code = Const.strLocales[PrefUtil.getInt(BabaracineApplication.getInstance() as Context, Const.KEY_LANGUAGE, 0)] //load it from SharedPref
        // Context context = changeLang(newBase, lang_code);

        val context = ContextWrapper.wrap(newBase, Locale(lang_code))
        //super.attachBaseContext(context);
        super.attachBaseContext(context)
    }
}