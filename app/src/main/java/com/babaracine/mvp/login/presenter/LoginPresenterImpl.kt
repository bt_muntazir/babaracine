package com.babaracine.mvp.login.presenter

import android.app.Activity
import android.util.Log
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.common.session.FcmSession
import com.babaracine.common.utility.Utils
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenterImpl : LoginContractor.LoginPresenter {


    var activity: Activity
    var loginView: LoginContractor.LoginView

    constructor(activity: Activity, loginView: LoginContractor.LoginView) {

        this.activity = activity
        this.loginView = loginView
    }

    override fun onLoginPerform(email: String, password: String) {

        try {
            ApiAdapter.getInstance(activity)
            login(email, password)
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            loginView.onLoginInternetError()
        }
    }

    private fun login(email: String, password: String) {

        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()

        val deviceId = Utils.getDeviceId(activity)

              val fcmToken= FcmSession(activity).fcmToken

        var jsonObject: JSONObject? = null

        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_EMAIL, email)
            jsonObject.put(Const.PARAM_PASSWORD, password)
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A")
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId)
          //  jsonObject.put(Const.PARAM_DEVICE_TOKEN, fcmToken)
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, "evvvULC8jhA:APA91bHLx1i7ddgSvzMaPR8_7l8MyB_URiJwr2Os1pp8yQbBHXMH2MS50L5CQ7aFhRKJIpp-3Ve4Shso-Eejbxq9PozakYjQLa0GLPX21zQzf36P7cns5ZbLFlFzTyukSlYX2WXAxae9")
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject!!.toString())

        val getLoginOutput = ApiAdapter.getApiService()!!.login("application/json", "no-cache", preferredLanguage, body)

        getLoginOutput.enqueue(object : Callback<LoginModel> {
            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {

                Progress.stop()

                try {

                    Log.e("response", "" + response.body().status);
                    Log.e("response", "" + response.code());
                    //getting whole data from response
                    val loginModel = response.body()

                    if (loginModel.status == 1) {
                        loginView.onLoginSuccess(loginModel)
                    } else {
                        loginView.onLoginUnSuccess(loginModel.message!!)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    loginView.onLoginUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                t.printStackTrace()
                Progress.stop()
                loginView.onLoginUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }
}