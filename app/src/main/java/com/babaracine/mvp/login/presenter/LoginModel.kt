package com.babaracine.mvp.login.presenter


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class LoginModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data {

        @SerializedName("token")
        @Expose
        var token: String? = null
        @SerializedName("user")
        @Expose
        var user: User? = null

        inner class User {

            @SerializedName("first_name")
            @Expose
            var firstName: String? = null
            @SerializedName("last_name")
            @Expose
            var lastName: String? = null
            @SerializedName("full_name")
            @Expose
            var fullName: String? = null
            @SerializedName("user_id")
            @Expose
            var userId: Int? = null
            @SerializedName("email")
            @Expose
            var email: String? = null
            @SerializedName("gender")
            @Expose
            var gender: String? = null
            @SerializedName("full_image_url")
            @Expose
            var fullImageUrl: String? = null
            @SerializedName("provider")
            @Expose
            var provider: String? = null
            @SerializedName("is_social_login")
            @Expose
            var isSocialLogin: Boolean? = null

        }

    }

}


