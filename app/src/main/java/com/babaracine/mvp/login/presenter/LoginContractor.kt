package com.babaracine.mvp.login.presenter

interface LoginContractor {

    interface LoginPresenter{

        fun onLoginPerform(email : String, password : String)

    }

    interface LoginView{

        fun onLoginSuccess(loginModel: LoginModel)
        fun onLoginUnSuccess(message : String)
        fun onLoginInternetError()
    }

}