package com.babaracine.mvp.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.method.PasswordTransformationMethod
import android.view.View
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.dashboard.DashboardActivity
import com.babaracine.mvp.forgot_password.ForgotPasswordActivity
import com.babaracine.mvp.login.presenter.LoginContractor
import com.babaracine.mvp.login.presenter.LoginModel
import com.babaracine.mvp.login.presenter.LoginPresenterImpl
import kotlinx.android.synthetic.main.activity_login.*
import android.widget.Toast
import android.location.Geocoder
import java.io.IOException


class LoginActivity : BaseActivity(), LoginContractor.LoginView {


    lateinit var loginPresenterImpl: LoginPresenterImpl
    var isPasswordView: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginPresenterImpl = LoginPresenterImpl(this, this)
        manageClickEvent()

    }

    override fun onLoginSuccess(loginModel: LoginModel) {
        LoginManagerSession.createLoginSession(loginModel)
        LoginManagerSession.setAuthenticationToken(loginModel.data!!.token.toString())

        val intent = Intent(this, DashboardActivity::class.java)
        finishAffinity()
        startActivity(intent)

    }

    override fun onLoginUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, constraintLayoutLogin)
    }

    override fun onLoginInternetError() {
        SnackNotify.checkConnection(onRetryLogin, constraintLayoutLogin)
    }


    private fun manageClickEvent() {
        imgViewBack.setOnClickListener(View.OnClickListener { imgViewBackClick() })
        imgViewEye.setOnClickListener(View.OnClickListener { imgViewEyeClick() })

        txtViewForgotPassword.setOnClickListener(View.OnClickListener { txtViewForgotPasswordClick() })

        btnLogin.setOnClickListener(View.OnClickListener { btnLoginClick() })



    }


    fun abc(){

        val email = edtTextEmail.text.toString().trim()

        val geocoder = Geocoder(this)
        val zip = email
        try {
            val addresses = geocoder.getFromLocationName(zip, 1)
            if (addresses != null && !addresses.isEmpty()) {
                val address = addresses[0]
                // Use the address as needed
                val message = String.format("Latitude: %f, Longitude: %f",
                        address.latitude, address.longitude)
                Toast.makeText(this, message, Toast.LENGTH_LONG).show()
            } else {
                // Display appropriate message when Geocoder services are not available
                Toast.makeText(this, "Unable to geocode zipcode", Toast.LENGTH_LONG).show()
            }
        } catch (e: IOException) {
            // handle exception
        }

    }

    private fun imgViewBackClick() {
        finish()
    }

    private fun btnLoginClick() {



        val email = edtTextEmail.text.toString().trim()
        val password = edtTextPassword.text.toString().trim()

        if (isValidData(email, password)) {
            loginPresenterImpl.onLoginPerform(email, password)
        }

    }

    private fun isValidData(email: String, password: String): Boolean {

        if (Utils.isEmptyOrNull(email)) {
            edtTextEmail.setError(getString(R.string.error_email_mobile))
            edtTextEmail.requestFocus()
            return false
        }/*else if (!Utils.isValidEmail(email)){
            edtTextEmail.setError("Please enter valid email.")
            edtTextEmail.requestFocus()
            return false
        }*/ else if (Utils.isValidEmail(password)) {
            edtTextPassword.setError(getString(R.string.error_password))
            edtTextPassword.requestFocus()
            return false
        }
        return true
    }

    private fun txtViewForgotPasswordClick() {

        val intent = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
        startActivity(intent)

    }


    private fun imgViewEyeClick() {

        if (isPasswordView) {
            edtTextPassword.setTransformationMethod(PasswordTransformationMethod())
            val length = edtTextPassword.getText().toString().length
            edtTextPassword.setSelection(length)
            isPasswordView = false
            imgViewEye.setImageResource(R.mipmap.ic_eye_show)
        } else {
            edtTextPassword.setTransformationMethod(null)
            val length = edtTextPassword.getText().toString().length
            edtTextPassword.setSelection(length)
            isPasswordView = true
            imgViewEye.setImageResource(R.mipmap.ic_eye_hide)
        }

    }

    internal var onRetryLogin: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            btnLoginClick()
        }
    }

}
