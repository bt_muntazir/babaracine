package com.babaracine.mvp.Payment

import android.content.Intent
import android.media.session.MediaSession
import android.os.Bundle
import android.util.Log
import android.view.View
import com.babaracine.R
import com.babaracine.common.helpers.Progress
import com.babaracine.common.helpers.ValidateCardHelper
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.review_steps.CongratulationActivity
import com.babaracine.mvp.room_booking.presenter.RoomBookingConfirmAfterPaymentModel
import com.babaracine.mvp.room_booking.presenter.RoomBookingContractor
import com.babaracine.mvp.room_booking.presenter.RoomBookingModel
import com.babaracine.mvp.room_booking.presenter.RoomBookingPresenterImpl
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.activity_room_booking.*
import kotlinx.android.synthetic.main.toolbar_with_back_and_title.*

class PaymentActivity : BaseActivity(), RoomBookingContractor.RoomBookingView {


    override fun onRoomBookingSuccess(roomBookingModel: RoomBookingModel) {

        //Not Used
    }

    override fun onRoomBookingUnSuccess(message: String) {
        //Not Used
    }

    override fun onRoomBookingInternetError() {
        //Not Used
    }

    private val PUBLISHABLE_KEY = "pk_test_2PJGmfWtOaPyREqk0imOc9o4"


    lateinit var bookingId: String
    lateinit var tokenId: String

    lateinit var roomSessionId: String
    lateinit var startDate: String
    lateinit var endDate: String
    lateinit var numberOfDays: String


    lateinit var taxName: String
    var taxPercentage: Int = 0
    var depositPercentage: Int = 0

    var roomPerNightPrice: Int = 0
    lateinit var cancelationType: String
    lateinit var cancellationCharge: String
    lateinit var currencyCode: String
    lateinit var roomId: String


    lateinit var roomBookingPresenterImpl: RoomBookingPresenterImpl

    internal var cardNumber: String? = null
    internal var name: String? = null
    internal var expMonth = 0
    internal var expYear = 0
    internal var cvv: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        txtViewToolTitle.text = getString(R.string.make_payment)

        roomBookingPresenterImpl = RoomBookingPresenterImpl(this, this)

        manageClickEvent()

        getIntentData()


    }


    override fun onRoomBookingConfirmAfterPaymentSuccess(roomBookingConfirmAfterPaymentModel: RoomBookingConfirmAfterPaymentModel) {
        val intent = Intent(this@PaymentActivity, CongratulationActivity::class.java)
        intent.putExtra(ConstIntent.KEY_ROOM_CONFIRMATION_MESSAGE, roomBookingConfirmAfterPaymentModel.message!!)
        startActivity(intent)
    }

    override fun onRoomBookingConfirmAfterPaymentUnSuccess(message: String) {

        SnackNotify.showMessage(message, this, relLayMain)
    }

    override fun onRoomBookingConfirmAfterPaymentInternetError() {
        SnackNotify.checkConnection(onRetryRoomBookingAfterPayment, relLayMain)
    }

    private fun getIntentData() {

        if (intent.hasExtra(ConstIntent.KEY_ROOM_SESSION_ID)) {
            roomSessionId = intent.getStringExtra(ConstIntent.KEY_ROOM_SESSION_ID)
            bookingId = intent.getStringExtra(ConstIntent.KEY_ROOM_BOOKING_ID)

        }
    }

    fun manageClickEvent() {

        btnMakePayment.setOnClickListener(View.OnClickListener { btnMakePaymentClicked() })

    }

    private fun callingRoomBookingConfirmApi() {
        roomBookingPresenterImpl.afterPaymentRoomConfirm(roomSessionId, tokenId, bookingId)

        //  roomBookingPresenterImpl.getRoomBookingData(roomSessionId, firstName, lastName, email, address, state, city, zipCode, phoneNumber, selectedCountryName, bookingNotes)

    }


    fun btnMakePaymentClicked() {

        cardNumber = edtTextCardNumber.text.toString()
        name = edtTextHolderName.text.toString()
        var month = edtTextMonth.text.toString()
        var year = edtTextYear.text.toString()
        cvv = edtTextCvv.text.toString()

        if (isValidData(month, year)) {

            pay(month,year)
        }
    }

    private fun isValidData(month: String, year: String): Boolean {

        if (Utils.isEmptyOrNull(name)) {
            edtTextHolderName.setError(getString(R.string.empty_card_name))
            edtTextHolderName.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(cardNumber)) {
            edtTextCardNumber.setError(getString(R.string.empty_card_number))
            edtTextCardNumber.requestFocus()
            return false
        } else if (cardNumber!!.length < 16) {
            edtTextCardNumber.setError(getString(R.string.error_card_no))
            edtTextCardNumber.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(month)) {
            edtTextMonth.setError(getString(R.string.empty_month))
            edtTextMonth.requestFocus()
            return false
        } else if (month.length < 2 || (month.toInt() > 12)) {
            edtTextMonth.setError(getString(R.string.error_month))
            edtTextMonth.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(year)) {
            edtTextYear.setError(getString(R.string.empty_year))
            edtTextYear.requestFocus()
            return false
        } else if (year.length < 4) {
            edtTextYear.setError(getString(R.string.error_year))
            edtTextYear.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(cvv)) {
            edtTextCvv.setError(getString(R.string.empty_cvv))
            edtTextCvv.requestFocus()
            return false
        } else if (cvv!!.length < 3) {
            edtTextCvv.setError(getString(R.string.invalid_cvv))
            edtTextCvv.requestFocus()
            return false
        }
        return true
    }

    private fun pay(month: String,year: String) {

        expMonth = month.toInt()
        expYear = year.toInt()


        //String currency = getCurrency();
        val cardToSave = Card(cardNumber, expMonth, expYear, cvv)
        // cardToSave.setCurrency(CURRENCY_UNSPECIFIED.toLowerCase());
        cardToSave.name = name

        val message = ValidateCardHelper.validateCardData(this, cardToSave)
        if (message.equals("true", ignoreCase = true)) {
            Progress.start(this)

            Stripe(this).createToken(
                    cardToSave,
                    PUBLISHABLE_KEY,
                    object : TokenCallback {

                        override fun onError(error: Exception) {
                            // SnackNotify.showMessage(error.localizedMessage,this,relLayMain)
                            Progress.stop()
                        }

                        override fun onSuccess(token: Token) {

                            Progress.stop()

                            tokenId = token.getId()

                            Log.e("TokenId", "" + tokenId)

                            if (!Utils.isEmptyOrNull(tokenId)) {
                                callingRoomBookingConfirmApi()
                            }
                        }
                    })
        } else
            SnackNotify.showMessage(message, this, relLayMain)
    }

    internal val onRetryRoomBookingAfterPayment: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            callingRoomBookingConfirmApi()
        }
    }

}
