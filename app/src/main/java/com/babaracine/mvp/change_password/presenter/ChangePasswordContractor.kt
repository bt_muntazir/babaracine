package com.babaracine.mvp.change_password.presenter

import com.babaracine.mvp.common_model.CommonModel

interface ChangePasswordContractor {

    interface ChangePasswordPresenter{

        fun onChangePasswordDataSubmit(oldPassword : String,newPassword : String, confirmPassword : String)

    }

    interface ChangePasswordView{

        fun onChangePasswordSuccess(changePasswordModel : CommonModel)
        fun onChangePasswordUnSuccess(message : String)
        fun onChangePasswordInternetError()



    }



}