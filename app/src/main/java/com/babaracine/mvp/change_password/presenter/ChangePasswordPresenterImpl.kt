package com.babaracine.mvp.change_password.presenter

import android.app.Activity
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.common_model.CommonModel
import okhttp3.Callback

import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class ChangePasswordPresenterImpl(val activity: Activity, val changePasswordView: ChangePasswordContractor.ChangePasswordView) : ChangePasswordContractor.ChangePasswordPresenter {

    override fun onChangePasswordDataSubmit(oldPassword: String, newPassword: String, confirmPassword: String) {
        try {
            ApiAdapter.getInstance(activity)
            onChangePasswordApiCall(oldPassword, newPassword, confirmPassword)
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            changePasswordView.onChangePasswordInternetError()
        }
    }

    private fun onChangePasswordApiCall(oldPassword: String, newPassword: String, confirmPassword: String) {

        Progress.start(activity)

        val userId = LoginManagerSession.getUserData().data!!.user!!.userId
        val PreferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject: JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_USED_ID, userId)
            jsonObject.put(Const.PARAM_OLD_PASSWORD, oldPassword)
            jsonObject.put(Const.PARAM_NEW_PASSWORD, newPassword)
            jsonObject.put(Const.PARAM_CONFIRM_PASSWORD, confirmPassword)
        } catch (ex: JSONException) {
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getChangePasswordResult = ApiAdapter.getApiService()!!.changePassword("application/json","no-cache",PreferredLanguage,authorizationToken,body)

        getChangePasswordResult.enqueue(object : retrofit2.Callback<CommonModel> {


            override fun onResponse(call: Call<CommonModel>?, response: Response<CommonModel>?) {

                Progress.stop()

                try {
                    val changePasswordModel = response!!.body()
                    val message = changePasswordModel.message!!
                    if(changePasswordModel.status == 1){
                        changePasswordView.onChangePasswordSuccess(changePasswordModel)
                    }else if(changePasswordModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        changePasswordView.onChangePasswordUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    exp.printStackTrace()
                    changePasswordView.onChangePasswordUnSuccess(activity.getString(R.string.error_server))
                }


            }


            override fun onFailure(call: Call<CommonModel>?, t: Throwable?) {
                Progress.stop()
                changePasswordView.onChangePasswordUnSuccess(activity.getString(R.string.error_server))
            }



        })

    }
}