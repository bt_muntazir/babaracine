package com.babaracine.mvp.change_password

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.change_password.presenter.ChangePasswordContractor
import com.babaracine.mvp.change_password.presenter.ChangePasswordModel
import com.babaracine.mvp.change_password.presenter.ChangePasswordPresenterImpl
import com.babaracine.mvp.common_model.CommonModel
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.toolbar_with_back_and_title.*

class ChangePasswordActivity : BaseActivity(),ChangePasswordContractor.ChangePasswordView {

    lateinit var changePasswordPresenterImpl : ChangePasswordPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        changePasswordPresenterImpl = ChangePasswordPresenterImpl(this,this)

        manageClickEvent()
    }

    override fun onChangePasswordUnSuccess(message : String) {
        SnackNotify.showMessage(message,this,constraintLayoutChangePassword)
    }

    override fun onChangePasswordInternetError() {
        SnackNotify.checkConnection(onRetryChangePassword,constraintLayoutChangePassword)
    }

    override fun onChangePasswordSuccess(changePasswordModel : CommonModel) {
        AlertDialogHelper.showMessageToFinish(changePasswordModel.message!!,this)
    }


    private fun manageClickEvent() {

        txtViewToolTitle.text = getString(R.string.activity_change_password)

        imgViewToolBack.setOnClickListener { finish() }

        btnConfirm.setOnClickListener{ onClickBtnConfirm() }
    }

    private fun onClickBtnConfirm() {

        val oldPassword = edtTextOldPassword.text.toString().trim()
        val newPassword = edtTextNewPassword.text.toString().trim()
        val confirmPassword = edtTextConfirmPassword.text.toString().trim()

        if(isValidData(oldPassword,newPassword,confirmPassword)){
            changePasswordPresenterImpl.onChangePasswordDataSubmit(oldPassword,newPassword,confirmPassword)
        }

    }

    private fun isValidData(oldPassword: String, newPassword: String, confirmPassword: String): Boolean {

        if(Utils.isEmptyOrNull(oldPassword)){
            edtTextOldPassword.error = getString(R.string.error_old_password)
            edtTextOldPassword.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(newPassword)){
            edtTextNewPassword.error = getString(R.string.error_new_password)
            edtTextNewPassword.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(confirmPassword)){
            edtTextConfirmPassword.error = getString(R.string.error_confirm_password)
            edtTextConfirmPassword.requestFocus()
            return false
        }else if(!Utils.isPasswordMatch(newPassword,confirmPassword)){
            edtTextConfirmPassword.error = getString(R.string.error_password_confrm_pass_not_match)
            edtTextConfirmPassword.requestFocus()
            return false
        }
        return true
    }

    internal var onRetryChangePassword : OnClickInterface = object : OnClickInterface{
        override fun onClick() {
            onClickBtnConfirm()
        }
    }

}
