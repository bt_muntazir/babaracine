package com.babaracine.mvp.update_profile.presenter

import android.app.Activity
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.login.presenter.LoginModel
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpdateProfilePresenterImpl : UpdateProfileContractor.EditProfilePresenter {



    var activity: Activity
    var updateProfileView: UpdateProfileContractor.EditProfileView

    constructor(activity: Activity, updateProfileView: UpdateProfileContractor.EditProfileView) {

        this.activity = activity
        this.updateProfileView = updateProfileView
    }

    override fun onEditProfile(firstName: String, lastName: String, email: String, phoneNumber: String, gender: String, dob: String) {
        try {
            ApiAdapter.getInstance(activity)
            editProfileData(firstName,lastName,phoneNumber,email,gender,dob)
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            updateProfileView.onEditProfileInternetError()
        }
    }

    private fun editProfileData(firstName: String, lastName: String, phoneNumber: String, email: String, gender: String, dob: String) {

        Progress.start(activity)



        val userData = LoginManagerSession.getUserData()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val userId = userData.data!!.user!!.userId!!

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()

        var jsonObject: JSONObject? = null

        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_USED_ID, userId)
            jsonObject.put(Const.PARAM_FIRST_NAME, firstName)
            jsonObject.put(Const.PARAM_LAST_NAME, lastName)
            jsonObject.put(Const.PARAM_MOBILE_NUMBER, phoneNumber)
            jsonObject.put(Const.PARAM_EMAIL, email)
            jsonObject.put(Const.PARAM_GENDER, gender)
            jsonObject.put(Const.PARAM_DOB, dob)

        } catch (ex: JSONException) {
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject!!.toString())

        val getEditProfileResult = ApiAdapter.getApiService()!!.editProfile("application/json", "no-cache",preferredLanguage,authorizationToken, body)

        getEditProfileResult.enqueue(object : Callback<LoginModel> {
            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val editProfileModel = response.body()
                    val message = editProfileModel.message!!
                    if (editProfileModel.status == 1) {
                        updateProfileView.onEditProfileSuccess(editProfileModel)
                    }else if(editProfileModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    } else {
                        updateProfileView.onEditProfileUnSuccess(message)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    updateProfileView.onEditProfileUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                Progress.stop()
                updateProfileView.onEditProfileUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }

    override fun onGetProfileDetails() {
        try {
            ApiAdapter.getInstance(activity)
            getProfileData()
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            updateProfileView.getProfileDetailInternetError()
        }
    }

    private fun getProfileData() {
        Progress.start(activity)



        val userData = LoginManagerSession.getUserData()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val userId = userData.data!!.user!!.userId!!
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()

        var jsonObject: JSONObject? = null

        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_USED_ID, userId)


        } catch (ex: JSONException) {
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject!!.toString())

        val getEditProfileResult = ApiAdapter.getApiService()!!.getProfile("application/json", "no-cache",preferredLanguage,authorizationToken, body)

        getEditProfileResult.enqueue(object : Callback<GetProfileDataModel> {
            override fun onResponse(call: Call<GetProfileDataModel>, response: Response<GetProfileDataModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val getProfileDataModel = response.body()
                    val message = getProfileDataModel.message!!
                    if (getProfileDataModel.status == 1) {
                        updateProfileView.getProfileDataSuccess(getProfileDataModel)
                    }else if(getProfileDataModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    } else {
                        updateProfileView.getProfileDataUnSuccess(message)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    updateProfileView.getProfileDataUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<GetProfileDataModel>, t: Throwable) {
                Progress.stop()
                updateProfileView.getProfileDataUnSuccess(activity.getString(R.string.error_server))
            }
        })
    }


}