package com.babaracine.mvp.update_profile

import android.app.DatePickerDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.login.presenter.LoginModel
import com.babaracine.mvp.update_profile.presenter.UpdateProfileContractor
import com.babaracine.mvp.update_profile.presenter.UpdateProfilePresenterImpl
import com.babaracine.mvp.update_profile.presenter.GetProfileDataModel
import kotlinx.android.synthetic.main.activity_update_profile.*
import kotlinx.android.synthetic.main.toolbar_with_back_and_title.*
import java.text.SimpleDateFormat
import java.util.*





class UpdateProfileActivity : BaseActivity(), UpdateProfileContractor.EditProfileView{

    var dobUpdatedDate : String = ""
    var selectedGenderValue : String = ""

    lateinit var getProfileDataModel: GetProfileDataModel

    lateinit var updateProfilePresenterImpl: UpdateProfilePresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_profile)

        txtViewToolTitle.text = getString(R.string.activity_edit_profile)

        updateProfilePresenterImpl = UpdateProfilePresenterImpl(this,this)

        getProfileData()
        manageClickEvent()
    }

    private fun getProfileData() {
        updateProfilePresenterImpl.onGetProfileDetails()
    }

    override fun getProfileDataSuccess(getProfileDataModel: GetProfileDataModel) {
        this.getProfileDataModel= getProfileDataModel

        setProfileData()

    }

    private fun setProfileData() {
        val data = getProfileDataModel.data!!

        edtTextFirstName.setText(data.firstName.toString())
        edtTextLastName.setText(data.lastName.toString())
        edtTextEmail.setText(data.email.toString())
        edtTextPhone.setText(data.mobileNumber.toString())

        dobUpdatedDate = data.dateOfBirth.toString()
        txtViewDobDate.setText(dobUpdatedDate)
        if(data.gender.equals(getString(R.string.tag_male),true)){
            radioBtnMale.isChecked = true
        }else if(data.gender.equals(getString(R.string.tag_female),true)){
            radioBtnFemale.isChecked = true
        }else{
            radioBtnOther.isChecked = true
        }




    }

    override fun getProfileDataUnSuccess(message: String) {
        SnackNotify.showMessage(message,this,constraintLayoutEditProfile)
    }

    override fun getProfileDetailInternetError() {
        SnackNotify.checkConnection(onRetryGetProfileData,constraintLayoutEditProfile)
    }


    override fun onEditProfileSuccess(editProfileModel: LoginModel) {
        LoginManagerSession.createLoginSession(editProfileModel)
        AlertDialogHelper.showMessageToFinish(editProfileModel.message!!,this)


    }

    override fun onEditProfileUnSuccess(message: String) {
        SnackNotify.showMessage(message,this,constraintLayoutEditProfile)
    }

    override fun onEditProfileInternetError() {
        SnackNotify.checkConnection(onRetryEditProfile,constraintLayoutEditProfile)
    }

    private fun manageClickEvent() {

        imgViewToolBack.setOnClickListener{
            finish()
        }

        btnSubmit.setOnClickListener(View.OnClickListener { onClickBtnSubmit() })

        txtViewDobDate.setOnClickListener(View.OnClickListener { openDatePicker() })
    }

    fun openDatePicker() {

        Utils.hideKeyboardIfOpen(this)
        // Get Current Date
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.YEAR, -18)
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)


        val datePickerDialog = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val calendar = Calendar.getInstance()
                    calendar.set(year, monthOfYear, dayOfMonth)

                    val format = SimpleDateFormat("yyyy-MM-dd")

                    txtViewDobDate.text = format.format(calendar.time)
                    dobUpdatedDate = format.format(calendar.time)


                    //txtViewDatePicker.setText(rideDate)
                }, mYear, mMonth, mDay)


        datePickerDialog.datePicker.maxDate = calendar.timeInMillis
        datePickerDialog.show()


    }

    private fun onClickBtnSubmit() {

        val selectedRadioid = radiGroupGender.checkedRadioButtonId
        when (selectedRadioid) {
            R.id.radioBtnMale -> {
                selectedGenderValue = getString(R.string.tag_male)
            }
            R.id.radioBtnFemale -> {
                selectedGenderValue = getString(R.string.tag_female)
            }
            R.id.radioBtnOther -> {
                selectedGenderValue = getString(R.string.tag_other)
            }
        }

        val firstName = edtTextFirstName.text.toString().trim()
        val lastName = edtTextLastName.text.toString().trim()
        val phoneNumber = edtTextPhone.text.toString().trim()
        val email = edtTextEmail.text.toString().trim()

        if(isDataValid(firstName,lastName,phoneNumber,email)){
            updateProfilePresenterImpl.onEditProfile(firstName,lastName,email,phoneNumber,selectedGenderValue,dobUpdatedDate)
        }
    }

    private fun isDataValid(firstName: String, lastName: String, phoneNumber: String, email: String): Boolean {

        if(Utils.isEmptyOrNull(firstName)){
            edtTextFirstName.setError(getString(R.string.error_first_name))
            edtTextFirstName.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(lastName)){
            edtTextLastName.setError(getString(R.string.error_last_name))
            edtTextLastName.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(email)){
            edtTextEmail.setError(getString(R.string.error_email))
            edtTextEmail.requestFocus()
            return false
        }else if(!Utils.isValidEmail(email)){
            edtTextEmail.setError(getString(R.string.error_valid_email))
            edtTextEmail.requestFocus()
            return false
        }else if(Utils.isEmptyOrNull(phoneNumber)){
            edtTextPhone.setError(getString(R.string.error_mobile_number))
            edtTextPhone.requestFocus()
            return false
        }else if(dobUpdatedDate.equals("")){
            SnackNotify.showMessage(getString(R.string.select_dob),this,constraintLayoutEditProfile)
            return false
        }
        return true
    }

    internal var onRetryEditProfile : OnClickInterface = object : OnClickInterface{
        override fun onClick() {
            onClickBtnSubmit()
        }

    }

    internal var onRetryGetProfileData : OnClickInterface = object : OnClickInterface{
        override fun onClick() {
            getProfileData()
        }

    }

}
