package com.babaracine.mvp.update_profile.presenter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetProfileDataModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("mobile_number")
        @Expose
        private String mobileNumber;
        @SerializedName("full_name")
        @Expose
        private String fullName;
        @SerializedName("profile_picture_path")
        @Expose
        private String profilePicturePath;
        @SerializedName("full_image_url")
        @Expose
        private String fullImageUrl;
        @SerializedName("id_proof_path")
        @Expose
        private Boolean idProofPath;
        @SerializedName("date_of_birth")
        @Expose
        private String dateOfBirth;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getProfilePicturePath() {
            return profilePicturePath;
        }

        public void setProfilePicturePath(String profilePicturePath) {
            this.profilePicturePath = profilePicturePath;
        }

        public String getFullImageUrl() {
            return fullImageUrl;
        }

        public void setFullImageUrl(String fullImageUrl) {
            this.fullImageUrl = fullImageUrl;
        }

        public Boolean getIdProofPath() {
            return idProofPath;
        }

        public void setIdProofPath(Boolean idProofPath) {
            this.idProofPath = idProofPath;
        }

        public String getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

    }

}
