package com.babaracine.mvp.update_profile.presenter

import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.login.presenter.LoginModel

interface UpdateProfileContractor {

    interface EditProfilePresenter{

        fun onEditProfile(firstName : String, lastName : String,email : String,phoneNumber : String,gender : String,dob: String)
        fun onGetProfileDetails()

    }

    interface EditProfileView{

        fun onEditProfileSuccess(editProfileModel: LoginModel)
        fun onEditProfileUnSuccess(message : String)
        fun onEditProfileInternetError()

        fun getProfileDataSuccess(getProfileDataModel: GetProfileDataModel)
        fun getProfileDataUnSuccess(message: String)
        fun getProfileDetailInternetError()

    }
}