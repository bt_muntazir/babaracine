package com.babaracine.mvp.review_steps.presenter


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CheckAvailabilityModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null


    inner class Data {

        @SerializedName("booking_session_id")
        @Expose
        var bookingSessionId: String? = null

    }
}