package com.babaracine.mvp.review_steps.presenter.presenter_country

import android.app.Activity
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CountryPresenterImpl(val activity: Activity, val countryView: CountryContractor.CountryView) : CountryContractor.CountryPresenter {


    override fun getCountryList() {

        try {
            ApiAdapter.getInstance(activity)
            onGetCountryList()
        } catch (exp: ApiAdapter.Companion.NoInternetException) {
            countryView.onGetCountryInternetError()
        }

    }

    private fun onGetCountryList() {

        Progress.start(activity)

        val url = Const.BASE_URL + "api/bookings/getCountries.json"

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val getCountryList = ApiAdapter.getApiService()!!.countryList("application/json", "no-cache", preferredLanguage, authorizationToken, url)

        getCountryList.enqueue(object : Callback<GetCountryModel> {
            override fun onResponse(call: Call<GetCountryModel>, response: Response<GetCountryModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val countryModel = response.body()
                    val message = countryModel.message!!
                    if (countryModel.status == 1) {
                        countryView.onGetCountrySuccess(countryModel)
                    } else if (countryModel.status == 2) {
                        AlertDialogHelper.showMessageToLogout(message, activity)
                    } else {
                        countryView.onGetCountryUnsuccess(message)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    countryView.onGetCountryUnsuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<GetCountryModel>, t: Throwable) {
                Progress.stop()
                countryView.onGetCountryUnsuccess(activity.getString(R.string.error_server))
            }
        })


    }
}