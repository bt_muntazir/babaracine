package com.babaracine.mvp.review_steps

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.babaracine.R
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.room_booking.RoomBookingActivity
import kotlinx.android.synthetic.main.activity_review_step2.*
import kotlinx.android.synthetic.main.custom_dialog_price_details.*
import kotlinx.android.synthetic.main.toolbar_with_back_and_title.*

import java.util.concurrent.TimeUnit
import java.text.ParseException
import java.text.SimpleDateFormat


class ReviewStep2Activity : BaseActivity() {

    var roomBookingLongTermPrice: Int = 0
    var roomBookingMinimumDay: Int = 0

    lateinit var roomSessionId: String
    lateinit var roomDetails: String
    lateinit var housingPolicy: String
    lateinit var startDate: String
    lateinit var endDate: String
    lateinit var numberOfDays: String

    lateinit var startingMonthName: String
    lateinit var endingMonthName: String

    lateinit var taxName: String
    var taxPercentage: Int = 0
    var depositPercentage: Int = 0

    var roomPerNightPrice: Int = 0
    lateinit var cancelationType: String
    lateinit var cancellationCharge: String
    lateinit var currencyCode: String
    lateinit var roomId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_review_step2)
        getIntentData()
        manageClickEvents()
    }

    private fun getIntentData() {
        if (intent.hasExtra(ConstIntent.KEY_ROOM_SESSION_ID)) {
            roomSessionId = intent.getStringExtra(ConstIntent.KEY_ROOM_SESSION_ID)
            roomDetails = intent.getStringExtra(ConstIntent.KEY_ROOM_DETAILS)
            housingPolicy = intent.getStringExtra(ConstIntent.KEY_HOUSING_RULES)
            startDate = intent.getStringExtra(ConstIntent.KEY_CHECK_IN_DATE)
            endDate = intent.getStringExtra(ConstIntent.KEY_CHECK_OUT_DATE)

            startingMonthName = intent.getStringExtra(ConstIntent.KEY_STARTING_MONTH_NAME)
            endingMonthName = intent.getStringExtra(ConstIntent.KEY_ENDING_MONTH_NAME)

            cancelationType = intent.getStringExtra(ConstIntent.KEY_CANCELATION_TYPE)
            cancellationCharge = intent.getStringExtra(ConstIntent.KEY_CANCELATION_CHARGE)
            roomPerNightPrice = intent.getIntExtra(ConstIntent.KEY_PRICE, 0)
            currencyCode = intent.getStringExtra(ConstIntent.KEY_CURRENCY_CODE)

            taxName = intent.getStringExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_NAME)
            taxPercentage = intent.getIntExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_PERCENTAGE, 0)
            depositPercentage = intent.getIntExtra(ConstIntent.KEY_ROOM_BOOKING_DEPOSIT_PERCENTAGE, 0)

            roomId = intent.getStringExtra(ConstIntent.KEY_ROOM_ID)
            roomBookingLongTermPrice = intent.getIntExtra(ConstIntent.KEY_ROOM_LONG_BOOKING_NIGHTLY_PRICE, 0)
            roomBookingMinimumDay = intent.getIntExtra(ConstIntent.KEY_ROOM_BOOKING_MINIMUM_DAY, 0)
            setData()
        }
    }

    private fun setData() {
        txtViewRoomDetail.text = roomDetails

        webViewHousingRules.loadData(housingPolicy, "text/html", "UTF-8")

        val myFormat = SimpleDateFormat("yyyy-MM-dd")

        try {
            if (startDate.equals(endDate)) {
                numberOfDays = "1"
            } else {
                val date1 = myFormat.parse(startDate)
                val date2 = myFormat.parse(endDate)
                val diff = date2.getTime() - date1.getTime()
                numberOfDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toString()
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val endDateArray = endDate.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val endYear = Integer.parseInt(endDateArray[0])
        val endMonth = Integer.parseInt(endDateArray[1])
        val endDay = Integer.parseInt(endDateArray[2])

        val startDateArray = startDate.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        val startYear = Integer.parseInt(startDateArray[0])
        val startMonth = Integer.parseInt(startDateArray[1])
        val startDay = Integer.parseInt(startDateArray[2])

        val completeStartingDate = startingMonthName + ", " + startDay + "-" + startYear
        val completeEndingDate = endingMonthName + ", " + endDay + "-" + endYear

        txtViewCheckInDate.text = completeStartingDate
        txtViewCheckOutDate.text = completeEndingDate

        val completePrice: String
        if (numberOfDays.toInt() >= roomBookingMinimumDay) {
            completePrice = Utils.getCurrenctSymbol(currencyCode, this) + " " + (roomBookingLongTermPrice * numberOfDays.toInt()) + " " + getString(R.string.title_for) + numberOfDays + " " + getString(R.string.night) + "."
        } else {
            completePrice = Utils.getCurrenctSymbol(currencyCode, this) + " " + (roomPerNightPrice * numberOfDays.toInt()) + " " + getString(R.string.title_for) + numberOfDays + " " + getString(R.string.night) + "."
        }
        txtViewPrice.text = completePrice

    }

    private fun manageClickEvents() {

        imgViewToolBack.setOnClickListener { finish() }

        btnNext.setOnClickListener(View.OnClickListener { onClickBtnNext() })

        txtViewSeePricingDetails.setOnClickListener(View.OnClickListener { showAlertForSeePricingDetails() })
    }

    private fun onClickBtnNext() {

        val intent = Intent(this, RoomBookingActivity::class.java)

        intent.putExtra(ConstIntent.KEY_ROOM_SESSION_ID, roomSessionId)
        intent.putExtra(ConstIntent.KEY_ROOM_DETAILS, roomDetails)
        intent.putExtra(ConstIntent.KEY_HOUSING_RULES, housingPolicy)
        intent.putExtra(ConstIntent.KEY_CHECK_IN_DATE, startDate)
        intent.putExtra(ConstIntent.KEY_CHECK_OUT_DATE, endDate)

        intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_DEPOSIT_PERCENTAGE, depositPercentage)
        intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_PERCENTAGE, taxPercentage)
        intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_NAME, taxName)


        intent.putExtra(ConstIntent.KEY_CANCELATION_TYPE, cancelationType)
        intent.putExtra(ConstIntent.KEY_CANCELATION_CHARGE, cancellationCharge)

        intent.putExtra(ConstIntent.KEY_PRICE, roomPerNightPrice)
        intent.putExtra(ConstIntent.KEY_CURRENCY_CODE, currencyCode)
        intent.putExtra(ConstIntent.KEY_ROOM_ID, roomId)
        intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_MINIMUM_DAY, roomBookingMinimumDay)
        intent.putExtra(ConstIntent.KEY_ROOM_LONG_BOOKING_NIGHTLY_PRICE, roomBookingLongTermPrice)
        intent.putExtra(ConstIntent.KEY_NUMBER_OF_DAYS, numberOfDays)


        startActivity(intent)
    }

    fun showAlertForSeePricingDetails() {

        try {

            val customDialog = Dialog(this)

            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            customDialog.setContentView(R.layout.custom_dialog_price_details)
            customDialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            //customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            customDialog.setCancelable(true)

            val price: String
            val totalPrice: Int
            if (numberOfDays.toInt() >= roomBookingMinimumDay) {
                price = Utils.getCurrenctSymbol(currencyCode, this) + " " + roomBookingLongTermPrice.toString()
                totalPrice = numberOfDays.toInt() * roomBookingLongTermPrice
            } else {
                price = Utils.getCurrenctSymbol(currencyCode, this) + " " + roomPerNightPrice.toString()
                totalPrice = numberOfDays.toInt() * roomPerNightPrice
            }

            customDialog.txtViewPricePerNight.text = price
            customDialog.txtViewNoOfNights.text = numberOfDays

            customDialog.txtViewTotal.text = Utils.getCurrenctSymbol(currencyCode, this) + " " + totalPrice.toString()
            val gstPercentage = getString(R.string.gst) + " (" + taxPercentage + "%)"
            customDialog.txtViewTaxTitle.text = gstPercentage

            val calculateTaxAmount = ((totalPrice * taxPercentage) / 100)
            customDialog.txtViewTax.text = Utils.getCurrenctSymbol(currencyCode, this) + " " + calculateTaxAmount

            val depositPercentageTitle = getString(R.string.deposit) + " (" + depositPercentage + "%)"

            customDialog.txtViewDepositTitle.text = depositPercentageTitle
            val calculateDepositAmount = ((totalPrice * depositPercentage) / 100)
            customDialog.txtViewDeposit.text = Utils.getCurrenctSymbol(currencyCode, this) + " " + calculateDepositAmount

            val calculateRemainingAmount = (totalPrice + calculateTaxAmount - calculateDepositAmount)
            customDialog.txtViewRemainingAmount.text = Utils.getCurrenctSymbol(currencyCode, this) + " " + calculateRemainingAmount

            val cancellationChargeAmount: String
            if (cancelationType.equals("free", true)) {
                cancellationChargeAmount = getString(R.string.free_cancellation)
            } else {
                cancellationChargeAmount = getString(R.string.cancellation_charge) + " (" + cancellationCharge + "%)"
            }

            customDialog.txtViewCancellationCharge.text = cancellationChargeAmount


            customDialog.imgViewClose.setOnClickListener {
                customDialog.dismiss()
            }


            customDialog.create()
            customDialog.show()
        } catch (e: RuntimeException) {
            e.printStackTrace()
        }

    }
}
