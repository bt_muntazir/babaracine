package com.babaracine.mvp.review_steps

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.review_steps.presenter.CheckAvailabilityContractor
import com.babaracine.mvp.review_steps.presenter.CheckAvailabilityModel
import com.babaracine.mvp.review_steps.presenter.CheckAvailabilityPresenterImpl
import com.squareup.picasso.Picasso
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.activity_room_check_availability.*
import kotlinx.android.synthetic.main.custom_dialog_guest.*
import kotlinx.android.synthetic.main.toolbar_with_back_and_title.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

import kotlin.collections.ArrayList


class RoomCheckAvailabilityActivity : BaseActivity(), CheckAvailabilityContractor.CheckAvailabilityView, DatePickerDialog.OnDateSetListener {


    var startDate = ""
    var endDate = ""

    lateinit var roomName: String
    var roomPerNightPrice: Int = 0
    var maximumNumberOfGuest: Int = 0
    var roomBookingMinimumDay: Int = 0
    var roomBookingLongTermPrice: Int = 0
    lateinit var roomId: String
    lateinit var currencyCode: String
    lateinit var imageUrl: String
    lateinit var cancelationType: String
    lateinit var cancellationCharge: String
    lateinit var roomDetails: String
    lateinit var housingPolicy: String
    lateinit var checkInDisableDates: ArrayList<String>
    lateinit var checkOutDisableDates: ArrayList<String>

    lateinit var startingMonthName: String
    lateinit var endingMonthName: String
    var startDateDay: Int = 0

    var isStartDateCalOpen: Boolean = true

    lateinit var taxName: String
    var taxPercentage: Int = 0
    var depositPercentage: Int = 0

    var averageRating: Int = 0

    var numberOfAdult = "0"
    var numberOfChildren = "0"

    lateinit var arrayListMonthsName: ArrayList<String>

    lateinit var checkAvailabilityPresenterImpl: CheckAvailabilityPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_room_check_availability)

        checkAvailabilityPresenterImpl = CheckAvailabilityPresenterImpl(this, this)
        getIntentData()

        bindMonthsNameInArray()
        manageClickEvents()
    }

    override fun onCheckAvailabilitySuccess(checkAvailabilityModel: CheckAvailabilityModel) {
        //AlertDialogHelper.showMessage(checkAvailabilityModel.message!!,this)
        alertWhenRoomIsAvailable(checkAvailabilityModel.message!!, checkAvailabilityModel.data!!.bookingSessionId!!)
    }

    override fun onCheckAvailabilityUnSuccess(message: String) {
        AlertDialogHelper.showMessage(message, this)
    }

    override fun onCheckAvailabilityInternetError() {
        SnackNotify.checkConnection(onRetryCheckRoomAvailability, constraintLayoutCheckAvailability)
    }

    fun alertWhenRoomIsAvailable(msg: String, sessionId: String) {

        val alertDialoge = AlertDialog.Builder(this)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(getString(R.string.title_ok)) { dialog, which ->

            val intent = Intent(this@RoomCheckAvailabilityActivity, ReviewStep2Activity::class.java)
            intent.putExtra(ConstIntent.KEY_ROOM_SESSION_ID, sessionId)
            intent.putExtra(ConstIntent.KEY_ROOM_DETAILS, roomDetails)
            intent.putExtra(ConstIntent.KEY_HOUSING_RULES, housingPolicy)
            intent.putExtra(ConstIntent.KEY_CHECK_IN_DATE, startDate)
            intent.putExtra(ConstIntent.KEY_CHECK_OUT_DATE, endDate)

            intent.putExtra(ConstIntent.KEY_STARTING_MONTH_NAME, startingMonthName)
            intent.putExtra(ConstIntent.KEY_ENDING_MONTH_NAME, endingMonthName)

            intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_DEPOSIT_PERCENTAGE, depositPercentage)
            intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_PERCENTAGE, taxPercentage)
            intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_NAME, taxName)


            intent.putExtra(ConstIntent.KEY_CANCELATION_TYPE, cancelationType)
            intent.putExtra(ConstIntent.KEY_CANCELATION_CHARGE, cancellationCharge)

            intent.putExtra(ConstIntent.KEY_PRICE, roomPerNightPrice)
            intent.putExtra(ConstIntent.KEY_CURRENCY_CODE, currencyCode)
            intent.putExtra(ConstIntent.KEY_ROOM_ID, roomId)
            intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_MINIMUM_DAY, roomBookingMinimumDay)
            intent.putExtra(ConstIntent.KEY_ROOM_LONG_BOOKING_NIGHTLY_PRICE, roomBookingLongTermPrice)

            startActivity(intent)
        }

        alertDialoge.show()
    }

    private fun getIntentData() {

        if (intent != null) {
            if (intent.hasExtra(ConstIntent.KEY_ROOM_NAME)) {
                roomName = intent.getStringExtra(ConstIntent.KEY_ROOM_NAME)
                imageUrl = intent.getStringExtra(ConstIntent.KEY_IMGAGE_URL)
                cancelationType = intent.getStringExtra(ConstIntent.KEY_CANCELATION_TYPE)
                cancellationCharge = intent.getStringExtra(ConstIntent.KEY_CANCELATION_CHARGE)
                currencyCode = intent.getStringExtra(ConstIntent.KEY_CURRENCY_CODE)
                roomPerNightPrice = intent.getIntExtra(ConstIntent.KEY_PRICE, 0)
                averageRating = intent.getIntExtra(ConstIntent.KEY_RATING, 0)
                roomId = intent.getStringExtra(ConstIntent.KEY_ROOM_ID)
                roomDetails = intent.getStringExtra(ConstIntent.KEY_ROOM_DETAILS)
                housingPolicy = intent.getStringExtra(ConstIntent.KEY_HOUSING_RULES)
                taxName = intent.getStringExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_NAME)
                taxPercentage = intent.getIntExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_PERCENTAGE, 0)
                roomBookingLongTermPrice = intent.getIntExtra(ConstIntent.KEY_ROOM_LONG_BOOKING_NIGHTLY_PRICE, 0)
                roomBookingMinimumDay = intent.getIntExtra(ConstIntent.KEY_ROOM_BOOKING_MINIMUM_DAY, 0)
                depositPercentage = intent.getIntExtra(ConstIntent.KEY_ROOM_BOOKING_DEPOSIT_PERCENTAGE, 0)
                maximumNumberOfGuest = intent.getIntExtra(ConstIntent.KEY_MAXIMUM_GUEST_ALLOWED, 0)
                checkInDisableDates = intent.getStringArrayListExtra(ConstIntent.KEY_CHECK_IN_DISABLE_DATES)
                checkOutDisableDates = intent.getStringArrayListExtra(ConstIntent.KEY_CHECK_OUT_DISABLE_DATES)
            }
            setData()
        }

    }

    private fun setData() {
        txtViewRoomName.text = roomName
        val cancelationValue: String


        if (cancelationType.equals("free", true)) {
            cancelationValue = getString(R.string.free_cancellation)
        } else {
            cancelationValue = getString(R.string.cancellation_charge) + " (" + cancellationCharge + "%)"
        }

        txtViewItemCancelationCharge.text = cancelationValue
        ratingBar.rating = averageRating.toFloat()
        val completePrice = Utils.getCurrenctSymbol(currencyCode, this) + " " + roomPerNightPrice
        txtViewPrice.text = completePrice

        if (!Utils.isEmptyOrNull(imageUrl)) {
            Picasso.with(this).load(imageUrl).into(imgViewRoomImage)
        }
    }

    private fun bindMonthsNameInArray() {

        arrayListMonthsName = ArrayList()
        arrayListMonthsName.add(getString(R.string.january))
        arrayListMonthsName.add(getString(R.string.february))
        arrayListMonthsName.add(getString(R.string.march))
        arrayListMonthsName.add(getString(R.string.april))
        arrayListMonthsName.add(getString(R.string.may))
        arrayListMonthsName.add(getString(R.string.june))
        arrayListMonthsName.add(getString(R.string.july))
        arrayListMonthsName.add(getString(R.string.august))
        arrayListMonthsName.add(getString(R.string.september))
        arrayListMonthsName.add(getString(R.string.october))
        arrayListMonthsName.add(getString(R.string.november))
        arrayListMonthsName.add(getString(R.string.december))
    }

    private fun manageClickEvents() {

        imgViewToolBack.setOnClickListener { finish() }

        btnCheckAvailability.setOnClickListener(View.OnClickListener { onCheckRoomAvailability() })
        txtViewDate.setOnClickListener(View.OnClickListener { onClickDateSelect() })
        txtViewGuest.setOnClickListener(View.OnClickListener { showAlertForChooseUser() })
    }

    private fun onClickDateSelect() {
        openStartDatePicker()
    }

    private fun openStartDatePicker() {

        isStartDateCalOpen = true

        var calendar = Calendar.getInstance()


        val dpd = DatePickerDialog.newInstance(
                this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        )

        dpd.minDate = calendar


        dpd.show(this.getFragmentManager(), "DatePickerDialog")

        var date: Date? = null

        startingMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault())

        val sdf = SimpleDateFormat("yyyy-MM-dd")

        //val list = arrayListOf("2018-10-08", "2018-10-10", "2018-10-15")
        val list = checkInDisableDates

        val dates = ArrayList<Calendar>()

        for (i in 0 until list.size) {
            date = sdf.parse(list[i])
            dates.add(dateToCalendar(date))
            /* calendar = dateToCalendar(date)
             System.out.println(calendar.time)*/
        }


        /* val dates = ArrayList<Calendar>()
         dates.add(calendar)*/
        val disabledDays1 = dates.toTypedArray()
        dpd.setDisabledDays(disabledDays1)
        dpd.setTitle(getString(R.string.check_in_date))
        // dpd.minDate = calendar

    }

    private fun dateToCalendar(date: Date?): Calendar {
        val calendar = Calendar.getInstance()
        calendar.time = date
        return calendar
    }

    override fun onDateSet(view: com.wdullaer.materialdatetimepicker.date.DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        // val date = dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year

        if (isStartDateCalOpen) {

            val date = "" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth

            startDateDay = dayOfMonth

            //  openEndDatePicker(date)
            // Log.e("Start date", "" + date)

            startDate = date

            openEndDatePicker(date)

        } else {

            val date = "" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth

            endDate = date


            txtViewDate.text = "" + startDateDay + "-" + dayOfMonth + " " + endingMonthName


            //  openEndDatePicker(date)
            Log.e("End date", "" + date + "  " + endingMonthName)
        }
    }


    private fun openEndDatePicker(startDate: String) {

        isStartDateCalOpen = false

        var calendar = Calendar.getInstance()


        val dpd = DatePickerDialog.newInstance(
                this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        )

        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrow = calendar.time
        var numberOfDays: String = "0"

        val myFormat = SimpleDateFormat("yyyy-MM-dd")
        var number: Int = 0

        try {
            val currentDate = Calendar.getInstance().getTime()
            val date1 = myFormat.parse(startDate)
            val tomorrowDate = myFormat.format(tomorrow)
            if (this.startDate.equals(tomorrowDate)) {
                number = 1
            } else {
                val diff = date1.getTime() - currentDate.getTime()
                numberOfDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toString()
                if (numberOfDays.toInt() == 0) {
                    number = 0
                } else {
                    number = numberOfDays.toInt() + 1
                }
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        calendar.add(Calendar.DAY_OF_YEAR, number)

        endingMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault())

        dpd.minDate = calendar

        dpd.show(this.getFragmentManager(), "DatePickerDialog")

        var date: Date? = null

        val sdf = SimpleDateFormat("yyyy-MM-dd")

        //val list = arrayListOf("2018-10-08", "2018-10-10", "2018-10-15", "2018-10-07", "2018-10-06", "2018-10-27")
        val list = checkOutDisableDates

        val dates = ArrayList<Calendar>()

        for (i in 0 until list.size) {
            date = sdf.parse(list[i])
            dates.add(dateToCalendar(date))
            /* calendar = dateToCalendar(date)
             System.out.println(calendar.time)*/
        }


        /* val dates = ArrayList<Calendar>()
         dates.add(calendar)*/
        val disabledDays1 = dates.toTypedArray()
        dpd.setDisabledDays(disabledDays1)
        dpd.setTitle(getString(R.string.check_out_date))
        // dpd.minDate = calendar

    }

    /*fun openStartDatePicker() {

        Utils.hideKeyboardIfOpen(this)
        // Get Current Date
        val calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)


        val datePickerDialog = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val calendar = Calendar.getInstance()
                    calendar.set(year, monthOfYear, dayOfMonth)

                    val format = SimpleDateFormat("yyyy-MM-dd")

                    startDate = format.format(calendar.time)


                    val dateArray = startDate.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                    val years = Integer.parseInt(dateArray[0])
                    val month = Integer.parseInt(dateArray[1])
                    val date = Integer.parseInt(dateArray[2])
                    startingMonthName = arrayListMonthsName[month-1]
                    openEndDatePicker(date)
                    //Log.e("startDate",startDate)
                    //txtViewDobDate.text = format.format(calendar.time)
                    //dobUpdatedDate = format.format(calendar.time)


                    //txtViewDatePicker.setText(rideDate)
                }, mYear, mMonth, mDay)

        datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        datePickerDialog.setTitle("Check-In Date")
        datePickerDialog.show()


    }*/

    /* fun openEndDatePicker(startDate: Int) {

         Utils.hideKeyboardIfOpen(this)
         // Get Current Date
         val calendar = Calendar.getInstance()
         val mYear = calendar.get(Calendar.YEAR)
         val mMonth = calendar.get(Calendar.MONTH)
         val mDay = calendar.get(Calendar.DAY_OF_MONTH)
         var numberOfDays: String = "0"

         val datePickerDialog = DatePickerDialog(this,
                 DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                     val calendar = Calendar.getInstance()
                     calendar.set(year, monthOfYear, dayOfMonth)

                     val format = SimpleDateFormat("yyyy-MM-dd")

                     endDate = format.format(calendar.time)


                     val dateArray = endDate.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                     val years = Integer.parseInt(dateArray[0])
                     val month = Integer.parseInt(dateArray[1])
                     val date = Integer.parseInt(dateArray[2])
                     endingMonthName = arrayListMonthsName[month - 1]
                     val monthName = endingMonthName
                     val completeDate = startDate.toString() + "-" + date.toString() + " " + monthName
                     txtViewDate.text = completeDate


 //txtViewDatePicker.setText(rideDate)
                 }, mYear, mMonth, mDay)

         calendar.add(Calendar.DAY_OF_YEAR, 1)
         val tomorrow = calendar.time

         val myFormat = SimpleDateFormat("yyyy-MM-dd")
         var number: Int = 0
         try {
             val currentDate = Calendar.getInstance().time
             val date1 = myFormat.parse(this.startDate)
             val tomorrowDate = myFormat.format(tomorrow)
             if (this.startDate.equals(tomorrowDate)) {
                 number = 1
             } else {
                 val diff = date1.getTime() - currentDate.getTime()
                 numberOfDays = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toString()
                 if (numberOfDays.toInt() == 0) {
                     number = 0
                 } else {
                     number = numberOfDays.toInt() + 1
                 }
             }
         } catch (e: ParseException) {
             e.printStackTrace()
         }


         datePickerDialog.datePicker.minDate = (System.currentTimeMillis() + (number.toLong() * 24 * 60 * 60 * 1000))
         datePickerDialog.setTitle("Check-Out Date")
         datePickerDialog.show()


     }*/

    fun showAlertForChooseUser() {

        try {

            val customDialog = Dialog(this)

            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            customDialog.setContentView(R.layout.custom_dialog_guest)
            customDialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            //customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            customDialog.setCancelable(false)

            if (!Utils.isEmptyOrNull(numberOfAdult) && !(numberOfAdult.equals("0"))) {
                customDialog.edtTextNoAdult.setText(numberOfAdult)
            }
            if (!Utils.isEmptyOrNull(numberOfChildren) && !(numberOfChildren.equals("0"))) {
                customDialog.edtTextNoChildren.setText(numberOfChildren)
            }

            val btnConfirm = customDialog.btnConfirm
            btnConfirm.setOnClickListener {

                Utils.hideKeyboardIfOpen(this)

                numberOfAdult = customDialog.edtTextNoAdult.text.toString().trim()
                numberOfChildren = customDialog.edtTextNoChildren.text.toString().trim()
                if (Utils.isEmptyOrNull(numberOfAdult)) {
                    AlertDialogHelper.showMessage(getString(R.string.error_no_of_guest), this)
                } else {
                    if (numberOfChildren.equals("", true)) {
                        numberOfChildren = "0"
                    }

                    val totalGuest = numberOfAdult.toInt() + numberOfChildren.toInt()
                    if (totalGuest > maximumNumberOfGuest) {
                        AlertDialogHelper.showMessage(getString(R.string.error_maximum_no_guest) + " " + maximumNumberOfGuest + " guest only.", this)
                        numberOfChildren = "0"
                    } else {
                        var completeGuest: String = ""
                        if (Utils.isEmptyOrNull(numberOfChildren)) {
                            completeGuest = numberOfAdult + " " + getString(R.string.adult) + ", " + 0 + " " + getString(R.string.children)
                        } else {
                            completeGuest = numberOfAdult + " " + getString(R.string.adult) + " , " + numberOfChildren + " " + getString(R.string.children)
                        }
                        txtViewGuest.text = completeGuest
                        customDialog.dismiss()
                    }
                }
            }

            customDialog.imgViewClose.setOnClickListener {

                if (numberOfAdult.equals("", true)) {
                    customDialog.edtTextNoAdult.setText(numberOfAdult)
                    customDialog.edtTextNoChildren.setText(numberOfChildren)
                    if (numberOfChildren.equals("", true)) {
                        txtViewGuest.text = numberOfAdult + " " + getString(R.string.adult) + " , " + numberOfChildren + " " + getString(R.string.children)
                    } else {
                        txtViewGuest.text = numberOfAdult + " " + getString(R.string.adult) + ", " + 0 + " " + getString(R.string.children)
                    }
                }
                customDialog.dismiss()
            }

            customDialog.create()
            customDialog.show()
        } catch (e: RuntimeException) {
            e.printStackTrace()
        }

    }

    private fun onCheckRoomAvailability() {

        /* val intent = Intent(this, ReviewStep2Activity::class.java)
         startActivity(intent)*/

        val date = txtViewDate.text.toString().trim()
        val guest = txtViewGuest.text.toString().trim()

        if (isValidate(date, guest)) {
            checkAvailabilityPresenterImpl.getCheckAvailabilityOfRoom(roomId, startDate, endDate, numberOfAdult, numberOfChildren)
        }
    }

    private fun isValidate(date: String, guest: String): Boolean {

        if (date.equals(getString(R.string.select_date), true)) {
            SnackNotify.showMessage(getString(R.string.select_checkin_checkout_dates), this, constraintLayoutCheckAvailability)
            return false
        } else if (guest.equals(getString(R.string.enter_guest), true)) {
            SnackNotify.showMessage(getString(R.string.select_no_of_guest), this, constraintLayoutCheckAvailability)
            return false
        }
        return true
    }

    internal val onRetryCheckRoomAvailability: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            onCheckRoomAvailability()
        }
    }
}
