package com.babaracine.mvp.review_steps.presenter.presenter_country

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

import java.util.ArrayList


class GetCountryModel : Serializable{

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: ArrayList<Datum>? = null


    inner class Datum : Serializable{

        @SerializedName("country_id")
        @Expose
        var countryId: Int? = null
        @SerializedName("country_name")
        @Expose
        var countryName: String? = null

    }
}

