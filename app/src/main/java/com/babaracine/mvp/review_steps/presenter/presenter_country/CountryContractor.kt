package com.babaracine.mvp.review_steps.presenter.presenter_country

interface CountryContractor {

    interface CountryPresenter{

        fun getCountryList()

    }

    interface CountryView{

        fun onGetCountrySuccess(getCountryModel: GetCountryModel)
        fun onGetCountryUnsuccess(message : String)
        fun onGetCountryInternetError()

    }

}