package com.babaracine.mvp.review_steps.presenter

interface CheckAvailabilityContractor {

    interface CheckAvailabilityPresenter{

        fun getCheckAvailabilityOfRoom(roomId : String,startDate : String,endDate : String,numberOfAdults : String, numberOfChildren : String)
    }

    interface CheckAvailabilityView{

        fun onCheckAvailabilitySuccess(checkAvailabilityModel: CheckAvailabilityModel)
        fun onCheckAvailabilityUnSuccess(message : String)
        fun onCheckAvailabilityInternetError()

    }


}