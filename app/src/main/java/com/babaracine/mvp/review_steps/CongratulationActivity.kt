package com.babaracine.mvp.review_steps

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.babaracine.R
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.dashboard.DashboardActivity
import kotlinx.android.synthetic.main.activity_congratulation.*

class CongratulationActivity : BaseActivity() {

    lateinit var roomConfirmationMessage : String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_congratulation)

        getIntentData()
        manageClickEvent()
    }

    private fun getIntentData() {
        if(intent.hasExtra(ConstIntent.KEY_ROOM_CONFIRMATION_MESSAGE)){
            roomConfirmationMessage = intent.getStringExtra(ConstIntent.KEY_ROOM_CONFIRMATION_MESSAGE)
            setData()
        }
    }

    private fun setData() {
        txtViewCongratulationMessage.text = roomConfirmationMessage
    }

    private fun manageClickEvent() {

        btnBackToHome.setOnClickListener {
            val intent = Intent(this@CongratulationActivity, DashboardActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }
    }
}
