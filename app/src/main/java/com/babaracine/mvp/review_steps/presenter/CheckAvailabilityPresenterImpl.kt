package com.babaracine.mvp.review_steps.presenter

import android.app.Activity
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.room_detail.presenter.RoomDetailModel
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class CheckAvailabilityPresenterImpl(val activity: Activity, val checkAvailabilityView: CheckAvailabilityContractor.CheckAvailabilityView) : CheckAvailabilityContractor.CheckAvailabilityPresenter {


    override fun getCheckAvailabilityOfRoom(roomId: String, startDate: String, endDate: String, numberOfAdults: String, numberOfChildren: String) {

        try {
            ApiAdapter.getInstance(activity)
            checkRoomAvailability(roomId,startDate,endDate,numberOfAdults,numberOfChildren)
        }catch (exp : ApiAdapter.Companion.NoInternetException){
            checkAvailabilityView.onCheckAvailabilityInternetError()
        }
    }

    private fun checkRoomAvailability(roomId: String, startDate: String, endDate: String, numberOfAdults: String, numberOfChildren: String) {

        Progress.start(activity)


        val userId = LoginManagerSession.getUserData().data!!.user!!.userId
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_USED_ID,userId)
            jsonObject.put(Const.PARAM_ROOM_ID,roomId)
            jsonObject.put(Const.PARAM_START_DATE,startDate)
            jsonObject.put(Const.PARAM_END_DATE,endDate)
            jsonObject.put(Const.PARAM_NUMBER_OF_ADULT,numberOfAdults)
            jsonObject.put(Const.PARAM_NUMBER_OF_CHILDREN,numberOfChildren)

        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getRoomDetailResult = ApiAdapter.getApiService()!!.checkAvailabilityRoom("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomDetailResult.enqueue(object : retrofit2.Callback<CheckAvailabilityModel>{

            override fun onResponse(call: Call<CheckAvailabilityModel>?, response: Response<CheckAvailabilityModel>?) {

                Progress.stop()
                val checkAvailabilityModel = response!!.body()
                val message = checkAvailabilityModel.message!!
                try {
                    if(checkAvailabilityModel.status == 1){
                        checkAvailabilityView.onCheckAvailabilitySuccess(checkAvailabilityModel)
                    }else if(checkAvailabilityModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        checkAvailabilityView.onCheckAvailabilityUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    exp.printStackTrace()
                    checkAvailabilityView.onCheckAvailabilityUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<CheckAvailabilityModel>?, t: Throwable?) {
                Progress.stop()
                t!!.printStackTrace()
                checkAvailabilityView.onCheckAvailabilityUnSuccess(activity.getString(R.string.error_server))
            }
        })


    }
}