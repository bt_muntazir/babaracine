package com.babaracine.mvp.welcome

import android.content.Intent
import android.os.Bundle

import android.view.View
import com.babaracine.R


import com.babaracine.common.helpers.Progress
import com.babaracine.mvp.login.LoginActivity
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.content_welcome.*
import org.json.JSONException
import java.util.*
import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Color
import android.text.InputType
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan

import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress.Companion.dialog
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.dashboard.DashboardActivity
import com.babaracine.mvp.login.presenter.LoginModel
import com.babaracine.mvp.register.SignupActivity
import com.babaracine.mvp.terms_and_condition.TermsAndConditionActivity

import com.babaracine.mvp.welcome.presenter.SocialLoginContractor
import com.babaracine.mvp.welcome.presenter.SocialLoginPresenterImpl
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import kotlinx.android.synthetic.main.activity_welcome.*
import java.io.File


class WelcomeActivity : BaseActivity(), GoogleApiClient.OnConnectionFailedListener, SocialLoginContractor.SocialLoginView {


    internal lateinit var mCallbackManager: CallbackManager
    internal lateinit var userEmail: String
    internal lateinit var fullName: String
    internal lateinit var userId: String
    internal lateinit var first_name: String
    internal lateinit var last_name: String

    var RC_SIGN_IN = 1001

    //lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleApiClient: GoogleApiClient

    lateinit var socialLoginPresenterImpl : SocialLoginPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_welcome)

        addClickableSomePartOfTermsAndServiceText()

        socialLoginPresenterImpl = SocialLoginPresenterImpl(this,this)
        //Facebook
        //FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(application)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()


        manageClickEvent()


    }

    override fun onSocialLoginSuccess(socialLoginModel: LoginModel) {

        LoginManagerSession.createLoginSession(socialLoginModel)
        LoginManagerSession.setAuthenticationToken(socialLoginModel.data!!.token!!)

        val intent = Intent(this, DashboardActivity::class.java)
        startActivity(intent)
    }

    override fun onSocialLoginUnSuccess(message: String) {
        SnackNotify.showMessage(message,this,constraintLayoutWelcome)
    }

    override fun onSocialLoginInternetError() {
        SnackNotify.checkConnection(onRetrySocialLogin,constraintLayoutWelcome)
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        //Log.d("bett", "onConnectionFailed:" + ConnectionResult);
    }

    private fun addClickableSomePartOfTermsAndServiceText() {

        val ss = SpannableString(getString(R.string.title_terms_and_service))
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View?) {
                startActivity( Intent(this@WelcomeActivity, TermsAndConditionActivity::class.java))
            }

            override fun updateDrawState(ds: TextPaint?) {
                super.updateDrawState(ds)
                ds!!.color = Color.WHITE
            }
        }
        ss.setSpan(clickableSpan, 79, 170, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        txtViewTermsOfServices.setMovementMethod(LinkMovementMethod.getInstance())
        txtViewTermsOfServices.setHighlightColor(Color.WHITE)
        txtViewTermsOfServices.text = ss

}



    private fun googleSignIn() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun manageClickEvent() {

        txtViewAlreadyHaveAnAccount.setOnClickListener(View.OnClickListener { loginClick() })

        txtViewFacebook.setOnClickListener(View.OnClickListener { onClickFacebookLogin() })

        txtViewGoogle.setOnClickListener(View.OnClickListener { googleSignIn() })

        txtViewCreateAccount.setOnClickListener(View.OnClickListener { onClickCreateAccount() })

        txtViewAlreadyHaveAnAccount.setOnClickListener(View.OnClickListener { onClickAlreadyHaveAnAccountLogin() })

        imgViewCross.setOnClickListener{ finishAffinity() }
    }

    private fun onClickAlreadyHaveAnAccountLogin() {
        val intent = Intent(this@WelcomeActivity, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun onClickCreateAccount() {
        val intent = Intent(this, SignupActivity::class.java)
        startActivity(intent)
    }

    private fun onClickFacebookLogin() {
        mCallbackManager = CallbackManager.Factory.create()
        Progress.start(this)

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(*arrayOf("email")))

        LoginManager.getInstance().registerCallback(mCallbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResults: LoginResult) {

                        Progress.stop()
                        val request = GraphRequest.newMeRequest(
                                loginResults.accessToken
                        ) { `object`, response ->
                            // Application code
                            userEmail = ""
                            fullName = ""
                            userId = ""
                            first_name = ""
                            last_name = ""

                            try {
                                if (`object`.has("email"))
                                    userEmail = `object`.getString("email")
                                if (`object`.has("name")) {
                                    fullName = `object`.getString("name")
                                }
                                if (`object`.has("id"))
                                    userId = `object`.getString("id")
                                if (`object`.has("first_name"))
                                    first_name = `object`.getString("first_name")
                                if (`object`.has("last_name"))
                                    last_name = `object`.getString("last_name")
                                //userId=object.getString();
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }

                            if(userEmail.equals("",true)){
                                alertForTakeEmail()
                            }else {
                                socialLoginPresenterImpl.onSocialLoginApi(first_name, last_name, userEmail, "Facebook", userId)
                            }
                            //setIntentDataAndSend()
                            //log out user before login to server
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut()
                            }
                        }
                        val parameters = Bundle()
                        parameters.putString("fields", "id,name,email,gender,first_name,last_name")
                        request.parameters = parameters
                        request.executeAsync()

                    }

                    override fun onCancel() {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut()
                        }
                        Progress.stop()
                        // Log.e("dd", "facebook login canceled");
                    }


                    override fun onError(e: FacebookException) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut()
                        }
                        e.printStackTrace()
                        Progress.stop()

                    }
                })

    }


    fun alertForTakeEmail() {

        val input = EditText(this)
        input.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        dialog = AlertDialog.Builder(this)
                .setCancelable(false)
                .setView(input)
                .setTitle("Enter Email")
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel) { dialog, which -> finishAffinity() }
                .create()

        dialog!!.setOnShowListener(DialogInterface.OnShowListener {
            val button = (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener {
                Utils.hideKeyboardIfOpen(this)
                userEmail = input.text.toString()
                if (isValidate(userEmail)) {
                    socialLoginPresenterImpl.onSocialLoginApi(first_name, last_name, userEmail, "Facebook", userId)
                    //loginView.getFacebookLoginSuccess(userEmail,fullName,userId,first_name,last_name);
                }
            }
        })
        dialog!!.show()
    }


    private fun isValidate(userEmail: String): Boolean {

        if (Utils.isEmptyOrNull(userEmail)) {
            AlertDialogHelper.showMessage(getString(R.string.error_email),this)
            return false
        } else if (!Utils.isValidEmail(userEmail)) {
            AlertDialogHelper.showMessage(getString(R.string.error_valid_email),this)
            return false
        }
        return true

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {

        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data)
        }

    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        //  Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess) run {
            // Signed in successfully, show authenticated UI.

            var email: String? = ""
            var userID: String? = ""
            var name: String? = ""
            var first_name = ""
            var last_name = ""
            try {
                val acct = result.signInAccount
                email = acct!!.email
                name = acct.displayName
                userID = acct.id

                Log.e("email", email)
                Log.e("name", name)
                Log.e("userid", userID)

                val parts = name!!.split("\\s+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                Log.d("Length-->", "" + parts.size)
                if (parts.size == 2) {
                    first_name = parts[0]
                    last_name = parts[1]
                    Log.e("First-->", "" + first_name)
                    Log.e("Last-->", "" + last_name)

                } else if (parts.size == 3) {
                    first_name = parts[0]
                    last_name = parts[2]
                    Log.e("First-->", "" + first_name)
                    Log.e("Last-->", "" + last_name)
                }

                socialLoginPresenterImpl.onSocialLoginApi(first_name,last_name,email!!,"Google",userID!!)

            } catch (e: NullPointerException) {

            }

            try {
                signOut()
                revokeAccess()
            } catch (e: IllegalStateException) {
                clearApplicationData()
            } catch (e: RuntimeException) {
                clearApplicationData()
            }

            //Call social Login API

            //callSocialLoginMethod(name, email, userID, first_name, last_name)


        }
    }

    private fun signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback {}
    }

    // [START revokeAccess]
    private fun revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback {}
    }

    fun clearApplicationData() {
        val cache = getCacheDir()
        val appDir = File(cache.getParent())
        if (appDir.exists()) {
            val children = appDir.list()
            for (s in children!!) {
                if (s != "lib") {
                    deleteDir(File(appDir, s))
                    Log.i("TAG", "**************** File /data/data/APP_PACKAGE/$s DELETED *******************")
                }
            }
        }
    }

    fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children!!.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
        }
        return dir!!.delete()
    }

    private fun loginClick() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    internal val onRetrySocialLogin : OnClickInterface = object : OnClickInterface{
        override fun onClick() {
            onClickFacebookLogin()
        }

    }

}
