package com.babaracine.mvp.welcome.presenter

import com.babaracine.mvp.login.presenter.LoginModel

interface SocialLoginContractor {

    interface SocialLoginPresenter{

        fun onSocialLoginApi(firstName : String, lastName : String,email : String,provider : String,identifier : String)

    }

    interface SocialLoginView{

        fun onSocialLoginSuccess(socialLoginModel : LoginModel)
        fun onSocialLoginUnSuccess(message : String)
        fun onSocialLoginInternetError()

    }



}