package com.babaracine.mvp.welcome.presenter

import android.app.Activity
import com.babaracine.R
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.common.session.FcmSession
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.login.presenter.LoginModel
import okhttp3.Callback
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class SocialLoginPresenterImpl(val activity : Activity,val socialLoginView : SocialLoginContractor.SocialLoginView ) : SocialLoginContractor.SocialLoginPresenter {


    override fun onSocialLoginApi(firstName: String, lastName: String, email: String, provider: String, identifier: String) {

        try{
            ApiAdapter.getInstance(activity)
            getSocialLoginData(firstName,lastName,email,provider,identifier)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            socialLoginView.onSocialLoginInternetError()
        }

    }

    private fun getSocialLoginData(firstName: String, lastName: String, email: String, provider: String, identifier: String) {

        Progress.start(activity)
        val preferredLanguage = LoginManagerSession.getPreferredLanguage()

        val deviceId = Utils.getDeviceId(activity)

        val fcmToken= FcmSession(activity).fcmToken

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_FIRST_NAME,firstName)
            jsonObject.put(Const.PARAM_LAST_NAME,lastName)
            jsonObject.put(Const.PARAM_EMAIL,email)
            jsonObject.put(Const.PARAM_PROVIDER,provider)
            jsonObject.put(Const.PARAM_IDENTIFIER,identifier)
            jsonObject.put(Const.PARAM_DEVICE_TYPE, "A")
            jsonObject.put(Const.PARAM_DEVICE_ID, deviceId)
            jsonObject.put(Const.PARAM_DEVICE_TOKEN, fcmToken)
        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset = utf-8"),jsonObject.toString())

        val getSocialLoginResult = ApiAdapter.getApiService()!!.socialLogin("application/json","no-cache",preferredLanguage,body)

        getSocialLoginResult.enqueue(object : retrofit2.Callback<LoginModel>{

            override fun onResponse(call: Call<LoginModel>?, response: Response<LoginModel>?) {

                Progress.stop()
                val socialLoginModel = response!!.body()
                val message = socialLoginModel.message!!
                try {
                    if(socialLoginModel.status == 1){
                        socialLoginView.onSocialLoginSuccess(socialLoginModel)
                    }else{
                        socialLoginView.onSocialLoginUnSuccess(message)
                    }
                }catch (ex : NullPointerException){
                    socialLoginView.onSocialLoginUnSuccess(activity.getString(R.string.error_server))
                }


            }

            override fun onFailure(call: Call<LoginModel>?, t: Throwable?) {
                Progress.stop()
                socialLoginView.onSocialLoginUnSuccess(activity.getString(R.string.error_server))
            }


        })

    }
}