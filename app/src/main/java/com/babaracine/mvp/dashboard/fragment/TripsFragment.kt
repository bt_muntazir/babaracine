package com.babaracine.mvp.dashboard.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife

import com.babaracine.R
import com.babaracine.common.utility.SnackNotify
import com.babaracine.mvp.dashboard.adapter.TripsAdapter
import com.babaracine.mvp.dashboard.model.TripsModel
import com.babaracine.mvp.dashboard.presenter.TripsContractor
import com.babaracine.mvp.dashboard.presenter.TripsPresenterImpl
import kotlinx.android.synthetic.main.fragment_trips.*


class TripsFragment : Fragment(), TripsContractor.TripsView {

    lateinit var tripsPresenterImpl: TripsPresenterImpl
    lateinit var tripsAdapter: TripsAdapter

    lateinit var tripsModel: TripsModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_trips, container, false)

        ButterKnife.bind(this, view)
        return view;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tripsPresenterImpl = TripsPresenterImpl(this.activity!!, this)

        setLayoutManager()

        getYourTripsDataApi()

    }


    private fun setAdapter() {
        tripsAdapter = TripsAdapter(this.activity!!, tripsModel!!.data!!.bookings!!)
        recylerViewTrips.setAdapter(tripsAdapter)
    }


    private fun setLayoutManager() {

        val layoutManager = LinearLayoutManager(this.activity!!, LinearLayoutManager.VERTICAL, false)
        recylerViewTrips.layoutManager = layoutManager as RecyclerView.LayoutManager?
        recylerViewTrips.isNestedScrollingEnabled = true
    }

    fun getYourTripsDataApi() {

        tripsPresenterImpl.onTrips()
    }


    override fun onTripsSuccess(tripsModel: TripsModel) {

        this.tripsModel = tripsModel

        setAdapter()
    }

    override fun onTripsUnSuccess(message: String) {

        SnackNotify.showMessage(message, this!!.activity!!,notificationItemLayout )
    }

    override fun onTripsInternetError() {

    }

}
