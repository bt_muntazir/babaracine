package com.babaracine.mvp.dashboard.presenter

import android.app.Activity
import android.widget.Adapter
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.dashboard.model.NotificationModel
import com.babaracine.mvp.dashboard.model.RoomListingModel
import com.babaracine.mvp.property_list.PropertyListModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Braintech on 05-09-2018.
 */
class NotificationPresenterImpl : NotificationContractor.NotificationPresenter {

    var notificationView: NotificationContractor.NotificationView

    var activity: Activity

    constructor(activity: Activity, notificationView: NotificationContractor.NotificationView) {

        this.activity = activity
        this.notificationView = notificationView

    }

    override fun getNotification(page: Int) {

        try {
            ApiAdapter.getInstance(activity)
            getNotificationData(page)

        } catch (e: ApiAdapter.Companion.NoInternetException) {
            notificationView.onNotificationInternetError()
        }
    }

    private fun getNotificationData(page:Int) {

        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val getOutput = ApiAdapter.getApiService()!!.ownerNotification("application/json", "no-cache", preferredLanguage,authorizationToken,page )

        getOutput.enqueue(object : Callback<NotificationModel> {
            override fun onResponse(call: Call<NotificationModel>, response: Response<NotificationModel>) {

                try {

                    Progress.stop()
                    //getting whole data from response
                    val notificationModel = response.body()
                    val message = notificationModel.message!!
                    if (notificationModel.status == 1) {
                        notificationView.onNotificationSuccess(notificationModel)
                    }else if(notificationModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    } else {
                        notificationView.onNotificationUnSuccess(message)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    Progress.stop()

                    notificationView.onNotificationUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<NotificationModel>, t: Throwable) {
                Progress.stop()
                notificationView.onNotificationUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }
}