package com.babaracine.mvp.dashboard.presenter

import com.babaracine.mvp.dashboard.model.RoomListingModel
import com.babaracine.mvp.dashboard.model.TripsModel


/**
 * Created by Braintech on 29-08-2018.
 */
class TripsContractor {

    interface TripsPresenter {

        fun onTrips()
    }

    interface TripsView {

        fun onTripsSuccess(tripsModel: TripsModel)
        fun onTripsUnSuccess(message: String)
        fun onTripsInternetError()

    }
}