package com.babaracine.mvp.dashboard.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babaracine.R
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.dashboard.model.RoomListingModel
import com.babaracine.mvp.room_detail.RoomDetailActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_dashboard.view.*

/**
 * Created by Braintech on 14-08-2018.
 */
class RoomListingAdapter : RecyclerView.Adapter<RoomListingAdapter.ViewHolder> {


    var context: Context
    var arrayListRoomListingData: ArrayList<RoomListingModel.Datum>

    constructor(context: Context, arrayListRoomListingData: ArrayList<RoomListingModel.Datum>) {
        this.context = context
        this.arrayListRoomListingData = arrayListRoomListingData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_dashboard, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = arrayListRoomListingData[position]

        holder.txtViewName!!.text = dataModel.roomName
        holder.txtViewPrice!!.text = Utils.getCurrenctSymbol(dataModel.currencyCode!!, context) + " " + dataModel.nightlyPrice + " per night."

        holder.txtViewFreeCancellation!!.text = dataModel.cancellationType + " "+ context.getString(R.string.cancellation)

        holder.ratingBar!!.rating = dataModel.averageRating!!.toFloat()

        Picasso.with(context).load(dataModel.thumbImageUrl).into(holder.imgViewItem)

        holder.itemLayout.setOnClickListener()
        {
            val intent = Intent(context, RoomDetailActivity::class.java)
            intent.putExtra(ConstIntent.KEY_ROOM_ID, dataModel.roomId)
            context.startActivity(intent)
        }
    }

    fun updateArrayListData(arrayListPendingNew: java.util.ArrayList<RoomListingModel.Datum>?, page: Int) {

        if (arrayListPendingNew != null && arrayListRoomListingData != null) {
            // arrayListPending.addAll(arrayListPendingNew);

            if (page == 1) {

                arrayListRoomListingData.clear()
            }

            for (i in arrayListPendingNew.indices) {
                arrayListRoomListingData.add(arrayListPendingNew[i])
            }

            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return arrayListRoomListingData.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val itemLayout = itemView.itemLayout
        val imgViewItem = itemView.imgViewItem
        val txtViewName = itemView.txtViewName
        val txtViewPrice = itemView.txtViewPrice
        val txtViewFreeCancellation = itemView.txtViewFreeCancellation
        val ratingBar = itemView.ratingBar

    }


}