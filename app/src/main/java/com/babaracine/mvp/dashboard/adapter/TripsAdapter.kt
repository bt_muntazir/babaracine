package com.babaracine.mvp.dashboard.adapter

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babaracine.R
import com.babaracine.common.request_response.Const
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.mvp.dashboard.DashboardActivity
import com.babaracine.mvp.dashboard.model.RoomListingModel
import com.babaracine.mvp.dashboard.model.TripsModel
import com.babaracine.mvp.room_detail.RoomDetailActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_trips.view.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Braintech on 29-08-2018.
 */
class TripsAdapter : RecyclerView.Adapter<TripsAdapter.ViewHolder> {

    var context: Context
    var arrayListTripsBooking: ArrayList<TripsModel.Data.Booking>

    constructor(context: Context, arrayListTripsBooking: ArrayList<TripsModel.Data.Booking>) {
        this.context = context
        this.arrayListTripsBooking = arrayListTripsBooking
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_trips, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = arrayListTripsBooking[position]

        holder.txtViewName!!.text = dataModel.roomName
        holder.txtViewDateTime!!.text = dataModel.startDate
        holder.txtViewAddress!!.text = dataModel.address1

        holder.txtViewCheckInTime!!.text=dataModel.startDate
        holder.txtViewCheckOutTime!!.text=dataModel.endDate

        if (dataModel.status == Const.STATUS_CONFIRMED) {



        } else if (dataModel.status == Const.STATUS_ARRIVED) {

        } else if (dataModel.status == Const.STATUS_DEPARTED) {

        } else if (dataModel.status == Const.STATUS_ARCHIVED) {

        } else if (dataModel.status == Const.STATUS_CANCEL) {

            holder.btnCancel.alpha=.8f
            holder.btnCancel.isEnabled=false
            holder.btnCancel.text=context.getString(R.string.cancelled)

        } else if (dataModel.status == Const.STATUS_DISABLED) {

        } else if (dataModel.status == Const.STATUS_PAYMENT_PENDING) {

        } else if (dataModel.status == Const.STATUS_EXPIRE) {

            holder.btnCancel.alpha=.8f
            holder.btnCancel.isEnabled=false

        }


        val c = Calendar.getInstance().time
        // println("Current time => " + c)

        val df = SimpleDateFormat("yyyy-MM-dd")
        val currentDate = df.format(c)

        val startDate = dataModel.startDate

        Log.e("startDate=", startDate + " currentDate" + currentDate)

        if (currentDate.equals(startDate)) {

            holder.txtViewDay!!.text = "Today"
            Picasso.with(context).load(R.mipmap.ic_key).into(holder.imgViewTop)
            holder.vieLineVertical.setBackgroundColor(ContextCompat.getColor(context, R.color.color_buttton))


        } else {

            val input_date = dataModel.startDate
            val format1 = SimpleDateFormat("yyyy-MM-dd")
            val dt1 = format1.parse(input_date)
            /*    val format2 = SimpleDateFormat("EEEE")
                val finalDay = format2.format(dt1)*/
            val dayOfTheWeek = DateFormat.format("EEEE", dt1) as String // Thursday

            holder.txtViewDay!!.text = dayOfTheWeek
            Picasso.with(context).load(R.mipmap.ic_glass).into(holder.imgViewTop)
            holder.vieLineVertical.setBackgroundColor(ContextCompat.getColor(context, R.color.color_light_grey_drawable))


        }
/*
        val input_date = dataModel.startDate
        val format1 = SimpleDateFormat("yyyy-MM-dd")
        val dt1 = format1.parse(input_date)
        val format2 = SimpleDateFormat("EEEE")
        val finalDay = format2.format(dt1)

        val dayOfTheWeek = DateFormat.format("EEEE", dt1) as String // Thursday
        val day = DateFormat.format("dd", dt1) as String // 20
        val monthString = DateFormat.format("MMM", dt1) as String // Jun
        val monthNumber = DateFormat.format("MM", dt1) as String // 06
        val year = DateFormat.format("yyyy", dt1) as String // 2013*/



        Picasso.with(context).load(dataModel.imageUrl).into(holder.imgViewItem)

        holder.btnCancel.setOnClickListener()
        {
            holder.btnCancel.alpha=.8f
            holder.btnCancel.isEnabled=false
            holder.btnCancel.text=context.getString(R.string.cancelled)

            (context as DashboardActivity).getCancelRoomBooking(dataModel.bookingId!!)
        }

        holder.btnView.setOnClickListener()
        {
            (context as DashboardActivity).getViewDetailRoomBooking(dataModel.bookingId!!)
        }
    }

    override fun getItemCount(): Int {
        return arrayListTripsBooking.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val itemLayout = itemView.itemLayout
        val txtViewDay = itemView.txtViewDay
        val txtViewDateTime = itemView.txtViewDateTime
        val txtViewName = itemView.txtViewName
        val imgViewItem = itemView.imgViewItem
        val imgViewTop = itemView.imgViewTop
        val txtViewAddress = itemView.txtViewAddress
        val vieLineVertical = itemView.vieLineVertical


        val txtViewCheckInTime = itemView.txtViewCheckInTime
        val txtViewCheckOutTime = itemView.txtViewCheckOutTime
        val btnView = itemView.btnView
        val btnCancel = itemView.btnCancel
    }
}