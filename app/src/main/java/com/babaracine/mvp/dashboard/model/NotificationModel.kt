package com.babaracine.mvp.dashboard.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

/**
 * Created by Braintech on 05-09-2018.
 */

class NotificationModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: ArrayList<Datum>? = null

    inner class Datum {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("message")
        @Expose
        var message: String? = null
        @SerializedName("type")
        @Expose
        var type: String? = null
        @SerializedName("is_read")
        @Expose
        var isRead: Int? = null
        @SerializedName("title")
        @Expose
        var title: String? = null
        @SerializedName("created")
        @Expose
        var created: String? = null
        @SerializedName("modified")
        @Expose
        var modified: String? = null

    }


}
