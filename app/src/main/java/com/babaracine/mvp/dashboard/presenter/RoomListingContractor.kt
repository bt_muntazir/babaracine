package com.babaracine.mvp.dashboard.presenter

import com.babaracine.mvp.dashboard.model.RoomListingModel

/**
 * Created by Braintech on 14-08-2018.
 */
interface RoomListingContractor {

    interface RoomListingPresenter{

        fun onRoomListing(page:Int,search:String,startDate:String,endDate:String,minPrice:String,maxPrice:String,suitableFor:String)

    }

    interface RoomListingView{

        fun onRoomListingSuccess(roomListingModel: RoomListingModel)
        fun onRoomListingUnSuccess(message : String)
        fun onRoomListingInternetError()

    }
}