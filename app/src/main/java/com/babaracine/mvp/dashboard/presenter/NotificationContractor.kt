package com.babaracine.mvp.dashboard.presenter

import com.babaracine.mvp.dashboard.model.NotificationModel

/**
 * Created by Braintech on 05-09-2018.
 */
class NotificationContractor {

    interface NotificationPresenter {

        fun getNotification(page:Int)

    }

    interface NotificationView {

        fun onNotificationSuccess(notificationModel: NotificationModel)
        fun onNotificationUnSuccess(message: String)
        fun onNotificationInternetError()

    }
}