package com.babaracine.mvp.dashboard.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife

import com.babaracine.R
import com.babaracine.mvp.dashboard.adapter.NotificationAdapter
import com.babaracine.mvp.dashboard.model.NotificationModel
import com.babaracine.mvp.dashboard.presenter.NotificationContractor
import com.babaracine.mvp.dashboard.presenter.NotificationPresenterImpl
import kotlinx.android.synthetic.main.fragment_notification.*

class NotificationFragment : Fragment(), NotificationContractor.NotificationView {


    lateinit var notificationPresenter: NotificationPresenterImpl
    lateinit var  notificationAdapter:NotificationAdapter
     lateinit var notificationModel:NotificationModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_notification, container, false)

        ButterKnife.bind(this, view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        notificationPresenter = NotificationPresenterImpl(this!!.activity!!, this)

        setLayoutManager()

        getNotificationDataApi()

    }

    private fun getNotificationDataApi() {
        notificationPresenter.getNotification(1)
    }

    private fun setLayoutManager() {

        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerViewNotification.layoutManager = layoutManager
        recyclerViewNotification.isNestedScrollingEnabled = true
    }

    private fun setAdapter() {
        notificationAdapter = NotificationAdapter(this!!.activity!!, notificationModel.data!!)
        recyclerViewNotification.setAdapter(notificationAdapter)
    }


    override fun onNotificationSuccess(notificationModel: NotificationModel) {
        this.notificationModel=notificationModel

        setAdapter()

    }

    override fun onNotificationUnSuccess(message: String) {

    }

    override fun onNotificationInternetError() {

    }


}
