package com.babaracine.mvp.dashboard.adapter

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babaracine.R
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.dashboard.model.NotificationModel
import com.babaracine.mvp.dashboard.model.TripsModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_notification.view.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Braintech on 06-09-2018.
 */
class NotificationAdapter : RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    var context: Context
    var arrayListNotification: ArrayList<NotificationModel.Datum>

    constructor(context: Context, arrayListNotification: ArrayList<NotificationModel.Datum>) {
        this.context = context
        this.arrayListNotification = arrayListNotification
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel = arrayListNotification[position]

        holder.txtViewTitle!!.text = dataModel.title
        holder.txtViewMessage!!.text = dataModel.message

        if (dataModel.isRead == 0) {

            holder.viewIsReadNotify.setBackgroundColor(ContextCompat.getColor(context, R.color.color_buttton))

        } else {

            holder.viewIsReadNotify.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
        }


        holder.txtViewDate!!.text = dataModel.created

        /* holder.itemLayout.setOnClickListener()
         {
             val intent = Intent(context, RoomDetailActivity::class.java)
             intent.putExtra(ConstIntent.KEY_ROOM_ID, dataModel.roomId)
             context.startActivity(intent)
         }
 */
    }

    override fun getItemCount(): Int {
        return arrayListNotification.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val notificationItemLayout = itemView.notificationItemLayout
        val viewIsReadNotify = itemView.viewIsReadNotify
        val txtViewTitle = itemView.txtViewTitle
        val txtViewDate = itemView.txtViewDate
        val txtViewMessage = itemView.txtViewMessage


    }


}