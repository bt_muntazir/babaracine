package com.babaracine.mvp.dashboard.fragment

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babaracine.R
import com.babaracine.mvp.dashboard.adapter.RoomListingAdapter
import com.babaracine.mvp.dashboard.presenter.RoomListingContractor
import com.babaracine.mvp.dashboard.presenter.RoomListingPresenterImpl
import kotlinx.android.synthetic.main.fragment_dashboard.*
import com.babaracine.mvp.dashboard.model.RoomListingModel
import java.text.SimpleDateFormat
import java.util.*
import com.babaracine.common.adapter.AdapterSpinner
import com.babaracine.common.request_response.Const
import kotlin.collections.ArrayList
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.widget.*
import com.babaracine.R.styleable.RangeSeekBar
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.dashboard.adapter.PlaceArrayAdapter
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.PlaceBuffer
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar


class DashboardFragment : Fragment(), RoomListingContractor.RoomListingView, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    var startDate = ""
    var endDate = ""
    var selectedRoomType = ""
    var searchText = ""
    var isFirstTime = false

    lateinit var roomListingPresenterImpl: RoomListingPresenterImpl
    lateinit var roomListingAdapter: RoomListingAdapter
    lateinit var roomListingModel: RoomListingModel

    lateinit var arrayListSuitable: ArrayList<String>
    lateinit var arrayListHashSuitable: ArrayList<HashMap<String, String>>

    var viewDashboard: View? = null

    var minValue = ""
    var maxValue = ""

    //for paging
    var isEndReached: Boolean = false
    var mIsLoading: Boolean = false
    var page = 0
    lateinit var gridlayoutManager: GridLayoutManager
    lateinit var arrayListRoomData: ArrayList<RoomListingModel.Datum>


    private val LOG_TAG = "MainActivity"
    private val GOOGLE_API_CLIENT_ID = 0

    private lateinit var place: Place
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mPlaceArrayAdapter: PlaceArrayAdapter? = null
    private val BOUNDS_MOUNTAIN_VIEW = LatLngBounds(
            LatLng(37.398160, -122.180831), LatLng(37.430610, -121.972090))


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        viewDashboard = inflater.inflate(R.layout.fragment_dashboard, container, false)

        mGoogleApiClient = GoogleApiClient.Builder(activity!!)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(activity!!, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build()


        return viewDashboard
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val rangeSeekBar = viewDashboard!!.findViewById<View>(R.id.rangeSeekbar) as RangeSeekBar<Int>

        rangeSeekBar.setRangeValues(0, 500)


        rangeSeekBar.setOnRangeSeekBarChangeListener(com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar.OnRangeSeekBarChangeListener<Int> { bar, minValue, maxValue ->
            //Now you have the minValue and maxValue of your RangeSeekbar
            //Toast.makeText(getApplicationContext(), minValue.toString() + "-" + maxValue, Toast.LENGTH_LONG).show()
            this.minValue = minValue.toString()
            this.maxValue = maxValue.toString()
        })

        // Get noticed while dragging
        rangeSeekBar.isNotifyWhileDragging = true

        roomListingPresenterImpl = RoomListingPresenterImpl(this.activity!!, this)

        manageClickEvents()

        setLayoutManager()

        arrayListRoomData = ArrayList()

        bindSuitableList()

        setAdapter()

        setAdapterSection()

        getRoomListingDatApi(true)

        autoCompleteTextViewSearch.setThreshold(3)

        autoCompleteTextViewSearch.setOnItemClickListener(mAutocompleteClickListener)

        mPlaceArrayAdapter = PlaceArrayAdapter(activity, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null)
        autoCompleteTextViewSearch.setAdapter(mPlaceArrayAdapter)


    }

    override fun onPause() {
        super.onPause()
        mGoogleApiClient!!.stopAutoManage(activity!!)
        mGoogleApiClient!!.disconnect()
    }


    override fun onRoomListingSuccess(roomListingModel: RoomListingModel) {

        this.roomListingModel = roomListingModel


        if (roomListingModel.data != null) {

            arrayListRoomData = roomListingModel.data!!

            //setAdapter()

            if (arrayListRoomData != null) {
                if (arrayListRoomData.size < 10) {
                    isEndReached = true
                } else {
                    isEndReached = false
                }
                //setAdapter()
                roomListingAdapter.updateArrayListData(arrayListRoomData, page)

            }

            Handler().postDelayed(Runnable { mIsLoading = false }, 1000)
        }

    }

    override fun onRoomListingUnSuccess(message: String) {
        SnackNotify.showMessage(message, this!!.activity!!, constraintLayout)

        /* mIsLoading = false
         isEndReached = false

         arrayListRoomData.clear()
         recylerViewItems.setAdapter(null)
         roomListingAdapter = RoomListingAdapter(this!!.activity!!, arrayListRoomData)
         recylerViewItems.setAdapter(roomListingAdapter)*/

    }

    override fun onRoomListingInternetError() {
    }


    fun manageClickEvents() {

        //txtViewDates.setOnClickListener(View.OnClickListener { txtViewDateClickEvent() })
        txtViewDate.setOnClickListener(View.OnClickListener { txtViewRoomTypesClickEvent() })
        txtViewPrice.setOnClickListener(View.OnClickListener {
            //txtViewPriceClickEvent()
            constraintLayoutSeekbar.visibility = View.VISIBLE
            viewBackgroundBlur.visibility = View.VISIBLE
        })
        //txtViewSuitable.setOnClickListener(View.OnClickListener { txtViewSuitableClickEvent() })

        txtViewSuitable?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                val selectedId = arrayListHashSuitable.get(position).get(Const.KEY_ID).toString()
                val selectedName = arrayListHashSuitable.get(position).get(Const.KEY_NAME).toString()
                val selectedNamePlusOne = selectedId.toInt() + 1

                if (selectedName.equals(getString(R.string.suitable_for_family))) {

                    page = 0
                    selectedRoomType = "Family"
                    getRoomListingDatApi(true)
                } else if (selectedName.equals(getString(R.string.suitable_for_work))) {
                    selectedRoomType = "Work"
                    page = 0
                    getRoomListingDatApi(true)
                } else {
                    selectedRoomType = ""

                    if (isFirstTime) {
                        page = 0
                        getRoomListingDatApi(true)
                    } else {
                        isFirstTime = true
                    }
                }

              //  Log.e("selectedPageName", "" + selectedNamePlusOne + " alam " + selectedName);
            }
        }

        btnSeekbarOk.setOnClickListener {

            page = 0
            getRoomListingDatApi(true)

            constraintLayoutSeekbar.visibility = View.GONE
            viewBackgroundBlur.visibility = View.GONE
        }

        imgViewClose.setOnClickListener {

            constraintLayoutSeekbar.visibility = View.GONE
            viewBackgroundBlur.visibility = View.GONE
        }

    }

    fun txtViewPriceClickEvent() {

        showDialogForPrice()
    }


    fun showDialogForPrice() {


        val dialogBuilder = AlertDialog.Builder(activity)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_price, null)
        dialogBuilder.setView(dialogView)

        // val rangeview = dialogView!!.findViewById<View>(R.id.rangeview) as SeekBar

        val b = dialogBuilder.create()
        b.show()
    }

    fun bindSuitableList() {

        arrayListHashSuitable = java.util.ArrayList<java.util.HashMap<String, String>>()

        val hashMapMonth = java.util.HashMap<String, String>()
        hashMapMonth.put(Const.KEY_ID, "0")
        hashMapMonth.put(Const.KEY_NAME, getString(R.string.suitable_all))
        arrayListHashSuitable.add(hashMapMonth)

        arrayListSuitable = ArrayList()
        //arrayListSuitable.add("Suitable For All")
        arrayListSuitable.add(getString(R.string.suitable_for_family))
        arrayListSuitable.add(getString(R.string.suitable_for_work))
    }

    private fun setAdapterSection() {

        for (i in 0 until arrayListSuitable.size) {

            val name = arrayListSuitable.get(i)
            val hashMap: HashMap<String, String> = HashMap()
            hashMap.put(Const.KEY_ID, (i + 1).toString())
            hashMap.put(Const.KEY_NAME, name)
            arrayListHashSuitable.add(hashMap)
        }

        val adapterMonth = AdapterSpinner(this.activity!!, R.layout.spinner_item, arrayListHashSuitable, "black")
        txtViewSuitable.setAdapter(adapterMonth)
    }

    fun txtViewRoomTypesClickEvent() {
        openStartDatePicker()

    }

    fun openStartDatePicker() {

        Utils.hideKeyboardIfOpen(activity!!)
        // Get Current Date
        val calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)


        val datePickerDialog = DatePickerDialog(activity,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val calendar = Calendar.getInstance()

                    calendar.set(year, monthOfYear, dayOfMonth)

                    val format = SimpleDateFormat("yyyy-MM-dd")

                    startDate = format.format(calendar.time)
                    openEndDatePicker()
                    Log.e("startDate", startDate)
                    //txtViewDobDate.text = format.format(calendar.time)
                    //dobUpdatedDate = format.format(calendar.time)


                    //txtViewDatePicker.setText(rideDate)
                }, mYear, mMonth, mDay)

        datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        datePickerDialog.setTitle(getString(R.string.starting_date))
        datePickerDialog.show()


    }

    fun openEndDatePicker() {

        Utils.hideKeyboardIfOpen(activity!!)
        // Get Current Date
        val calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)


        val datePickerDialog = DatePickerDialog(activity,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val calendar = Calendar.getInstance()
                    calendar.set(year, monthOfYear, dayOfMonth)

                    val format = SimpleDateFormat("yyyy-MM-dd")

                    endDate = format.format(calendar.time)

                    page = 0
                    getRoomListingDatApi(true)

                    //Log.e("endDate",endDate)
                    //txtViewDobDate.text = format.format(calendar.time)
                    //dobUpdatedDate = format.format(calendar.time)


                    //txtViewDatePicker.setText(rideDate)
                }, mYear, mMonth, mDay)

        datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        datePickerDialog.setTitle(getString(R.string.ending_date))
        datePickerDialog.show()


    }


    fun txtViewDateClickEvent() {

        val cal = Calendar.getInstance()

        val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)


        }


        DatePickerDialog(context, dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show()

        val dp = DatePicker(activity)
        //Set the DatePicker minimum date selection to current date
        dp.setMinDate(cal.getTimeInMillis())

    }

    fun getRoomListingDatApi(isRetry: Boolean) {

        mIsLoading = true
        if (isRetry) {
            if (page == 0) {
                page = 1
            }
            roomListingPresenterImpl.onRoomListing(page, searchText, startDate, endDate, minValue, maxValue, selectedRoomType)
        } else {
            page = page + 1
            roomListingPresenterImpl.onRoomListing(page, searchText, startDate, endDate, minValue, maxValue, selectedRoomType)
        }
    }

    private fun setAdapter() {
        roomListingAdapter = RoomListingAdapter(this!!.activity!!, arrayListRoomData)
        recylerViewItems.setAdapter(roomListingAdapter)
    }

    // implement paging
    private val mRecyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView,
                                          newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = gridlayoutManager.getChildCount()
            val totalItemCount = gridlayoutManager.getItemCount()
            val firstVisibleItemPosition = gridlayoutManager.findFirstVisibleItemPosition()

            if (!mIsLoading && !isEndReached) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                    getRoomListingDatApi(false)
                }
            }
        }
    }

    private fun setLayoutManager() {
        gridlayoutManager = GridLayoutManager(context, 2)
        recylerViewItems.layoutManager = gridlayoutManager as RecyclerView.LayoutManager?
        recylerViewItems.isNestedScrollingEnabled = true
        recylerViewItems.addOnScrollListener(mRecyclerViewOnScrollListener);
    }

    override fun onConnected(p0: Bundle?) {
        mPlaceArrayAdapter!!.setGoogleApiClient(mGoogleApiClient)
    }

    override fun onConnectionSuspended(p0: Int) {
        mPlaceArrayAdapter!!.setGoogleApiClient(null)
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

        //Toast.makeText(this, "Google Places API connection failed with error code:" + connectionResult.getErrorCode(), Toast.LENGTH_LONG).show()
    }

    private val mAutocompleteClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
        val item = mPlaceArrayAdapter!!.getItem(position)
        val placeId = item.placeId.toString()
        val placeResult = Places.GeoDataApi
                .getPlaceById(mGoogleApiClient!!, placeId)
        placeResult.setResultCallback(mUpdatePlaceDetailsCallback)
    }

    private val mUpdatePlaceDetailsCallback = ResultCallback<PlaceBuffer> { places ->
        if (!places.getStatus().isSuccess()) {
            return@ResultCallback
        }
        Utils.hideKeyboardIfOpen(activity!!)
        // Selecting the first object buffer.
        place = places.get(0) as Place
        val attributions = place.getAttributions()


        searchText = autoCompleteTextViewSearch.text.toString()

        getRoomListingDatApi(true)
    }


}
