package com.babaracine.mvp.dashboard.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

/**
 * Created by Braintech on 29-08-2018.
 */

class TripsModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data {

        @SerializedName("bookings")
        @Expose
        var bookings: ArrayList<Booking>? = null


        inner class Booking {

            @SerializedName("booking_id")
            @Expose
            var bookingId: Int? = null
            @SerializedName("room_id")
            @Expose
            var roomId: Int? = null
            @SerializedName("owner_id")
            @Expose
            var ownerId: Int? = null
            @SerializedName("user_id")
            @Expose
            var userId: Int? = null
            @SerializedName("address_1")
            @Expose
            var address1: String? = null
            @SerializedName("address_2")
            @Expose
            var address2: String? = null
            @SerializedName("postal")
            @Expose
            var postal: String? = null
            @SerializedName("formated_address")
            @Expose
            var formatedAddress: String? = null
            @SerializedName("country")
            @Expose
            var country: String? = null
            @SerializedName("state")
            @Expose
            var state: String? = null
            @SerializedName("city")
            @Expose
            var city: String? = null
            @SerializedName("room_name")
            @Expose
            var roomName: String? = null
            @SerializedName("start_date")
            @Expose
            var startDate: String? = null
            @SerializedName("end_date")
            @Expose
            var endDate: String? = null
            @SerializedName("status")
            @Expose
            var status: Int? = null
            @SerializedName("thumb_image_url")
            @Expose
            var thumbImageUrl: String? = null
            @SerializedName("medium_thumb_image_url")
            @Expose
            var mediumThumbImageUrl: String? = null
            @SerializedName("image_url")
            @Expose
            var imageUrl: String? = null

        }


    }

}
