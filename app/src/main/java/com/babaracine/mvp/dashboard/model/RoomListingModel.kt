package com.babaracine.mvp.dashboard.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

/**
 * Created by Braintech on 14-08-2018.
 */

class RoomListingModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: ArrayList<Datum>? = null

    inner class Datum {

        @SerializedName("room_id")
        @Expose
        var roomId: Int? = null
        @SerializedName("room_name")
        @Expose
        var roomName: String? = null
        @SerializedName("currency_code")
        @Expose
        var currencyCode: String? = null
        @SerializedName("nightly_price")
        @Expose
        var nightlyPrice: Int? = null
        @SerializedName("cancellation_type")
        @Expose
        var cancellationType: String? = null
        @SerializedName("cancellation_charge")
        @Expose
        var cancellationCharge: String? = null
        @SerializedName("thumb_image_url")
        @Expose
        var thumbImageUrl: String? = null
        @SerializedName("average_rating")
        @Expose
        var averageRating: Double? = null
    }
}
