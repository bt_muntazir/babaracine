package com.babaracine.mvp.dashboard

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.dashboard.fragment.DashboardFragment
import com.babaracine.mvp.dashboard.fragment.NotificationFragment
import com.babaracine.mvp.dashboard.fragment.ProfileFragment
import com.babaracine.mvp.dashboard.fragment.TripsFragment
import com.babaracine.mvp.your_reservation.YourReservationModel
import com.babaracine.mvp.your_reservation.presenter.ViewRoomDetailBookingModel
import com.babaracine.mvp.your_reservation.presenter.YourReservationContractor
import com.babaracine.mvp.your_reservation.presenter.YourReservationPresenterImpl
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity(), YourReservationContractor.YourReservationView {



    internal lateinit var tabExplore: TextView
    internal lateinit var tabTrips: TextView
    internal lateinit var tabNotification: TextView
    internal lateinit var tabProfile: TextView

    internal lateinit var dialog: Dialog

    var bookingId: Int = 0

    lateinit var viewRoomDetailBookingModel: ViewRoomDetailBookingModel

    lateinit var yourReservationPresenterImpl: YourReservationPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        yourReservationPresenterImpl = YourReservationPresenterImpl(this, this)

        setTabLayout()
    }

    override fun onBackPressed() {


        val currentFragmentId = supportFragmentManager.findFragmentById(R.id.frameLayout)

        if (supportFragmentManager.backStackEntryCount > 1) {
            if (currentFragmentId!!.tag!!.equals(getString(R.string.tag_dashboard))) {
                AlertDialogHelper.alertCloseApp(this)
            } else {
                supportFragmentManager.popBackStack()
            }
        } else {
            AlertDialogHelper.alertCloseApp(this)
        }
    }


    private fun setTabLayout() {
        setTab()

        createTabIcons()
        //createTabIcons();
    }

    private fun createTabIcons() {
        tabExplore = LayoutInflater.from(this).inflate(R.layout.bottom_menu_layout, null) as TextView
        tabExplore.setText(getString(R.string.title_explore))
        tabExplore.setTextColor(ContextCompat.getColor(this, R.color.color_buttton))
        tabExplore.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.ic_explore_active, 0, 0)
        tabLayout.getTabAt(0)!!.setCustomView(tabExplore)

        tabTrips = LayoutInflater.from(this).inflate(R.layout.bottom_menu_layout, null) as TextView
        tabTrips.setText(getString(R.string.title_trips))
        tabTrips.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.ic_trips_tab, 0, 0)
        tabLayout.getTabAt(1)!!.setCustomView(tabTrips)

        tabNotification = LayoutInflater.from(this).inflate(R.layout.bottom_menu_layout, null) as TextView
        tabNotification.setText(getString(R.string.title_notification))
        tabNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.ic_notification_tab, 0, 0)
        tabLayout.getTabAt(2)!!.setCustomView(tabNotification)

        tabProfile = LayoutInflater.from(this).inflate(R.layout.bottom_menu_layout, null) as TextView
        tabProfile.setText(getString(R.string.title_profile))
        tabProfile.setCompoundDrawablesWithIntrinsicBounds(0, R.mipmap.ic_profile_tab, 0, 0)
        tabLayout.getTabAt(3)!!.setCustomView(tabProfile)

    }

    private fun setTab() {

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_explore)))
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_trips)))
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_notification)))
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_profile)))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        //Initialize first fragment
        //Initialize first fragment
        val dshboardFragment = DashboardFragment()
        addFragment(dshboardFragment, getString(R.string.tag_dashboard))

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {

                if (tab.position == 0) {


                    val dashBoardFragment = DashboardFragment()
                    replaceFragment(dashBoardFragment, getString(R.string.tag_dashboard))

                    tabExplore.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_explore, 0, 0)

                    tabExplore.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_buttton))
                    tabTrips.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabNotification.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabProfile.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                } else if (tab.position == 1) {

                    val tripsFragment = TripsFragment()
                    replaceFragment(tripsFragment, getString(R.string.tag_trips))

                    tabTrips.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_trips, 0, 0)

                    tabExplore.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabTrips.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_buttton))
                    tabNotification.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabProfile.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))

                } else if (tab.position == 2) {

                    val notificationFragment = NotificationFragment()
                    replaceFragment(notificationFragment, getString(R.string.tag_notification))

                    tabNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_notification, 0, 0)

                    tabExplore.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabTrips.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabNotification.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_buttton))
                    tabProfile.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                } else if (tab.position == 3) {


                    val profileFragment = ProfileFragment()
                    replaceFragment(profileFragment, getString(R.string.tag_profile))

                    tabProfile.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_profile, 0, 0)

                    tabExplore.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabTrips.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabNotification.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabProfile.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_buttton))
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                if (tab.position == 0) {

                    val dashBoardFragment = DashboardFragment()
                    replaceFragment(dashBoardFragment, getString(R.string.tag_dashboard))

                    tabExplore.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_explore, 0, 0)
                    tabExplore.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_buttton))
                    tabTrips.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabNotification.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabProfile.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))


                } else if (tab.position == 1) {


                    val tripsFragment = TripsFragment()
                    replaceFragment(tripsFragment, getString(R.string.tag_trips))

                    tabTrips.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_trips, 0, 0)

                    tabExplore.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabTrips.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_buttton))
                    tabNotification.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabProfile.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))

                } else if (tab.position == 2) {

                    val notificationFragment = NotificationFragment()
                    replaceFragment(notificationFragment, getString(R.string.tag_notification))

                    tabNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_notification, 0, 0)

                    tabExplore.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabTrips.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabNotification.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_buttton))
                    tabProfile.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                } else if (tab.position == 3) {

                    val profileFragment = ProfileFragment()
                    replaceFragment(profileFragment, getString(R.string.tag_profile))

                    tabProfile.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_profile, 0, 0)

                    tabExplore.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabTrips.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabNotification.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_grey))
                    tabProfile.setTextColor(ContextCompat.getColor(this@DashboardActivity, R.color.color_buttton))
                }
            }
        })
    }

    private fun addFragment(fragment: Fragment, tag: String) {
        Utils.hideKeyboardIfOpen(this)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.frameLayout, fragment, tag).addToBackStack(tag)
        transaction.commitAllowingStateLoss()
    }

    fun replaceFragment(fragment: Fragment, tag: String) {
        Utils.hideKeyboardIfOpen(this)
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, fragment, tag)

        transaction.commitAllowingStateLoss()
    }

    public fun getCancelRoomBooking(bookingId: Int) {

        this.bookingId = bookingId;
        callCancelRoomBooking()
    }

    public fun getViewDetailRoomBooking(bookingId: Int) {

        this.bookingId = bookingId;
        callViewDetailDataApi()
    }

    fun callCancelRoomBooking() {
        yourReservationPresenterImpl.cancelRoomBook(bookingId)
    }

    fun callViewDetailDataApi() {

        yourReservationPresenterImpl.viewDetailRoomBook(bookingId)
    }

    override fun onYourReservationSuccess(yourReservationModel: YourReservationModel) {
        //not used
    }

    override fun onYourReservationUnSuccess(message: String) {
        //not used
    }

    override fun onYourReservationInternetError() {
        //not used
    }

    override fun onConfirmRoomBookSuccess(commonModel: CommonModel) {
        //not used
    }

    override fun onConfirmRoomBookUnSuccess(message: String) {
        //not used
    }

    override fun onConfirmRoomBookInternetError() {
        //not used
    }

    override fun onCancelRoomBookSuccess(commonModel: CommonModel) {

        SnackNotify.showMessage(commonModel.message!!, this, constraintLayoutDashboard)

    }

    override fun onCancelRoomBookUnSuccess(message: String) {
        SnackNotify.showMessage(message!!, this, constraintLayoutDashboard)
    }

    override fun onCancelRoomBookInternetError() {
        SnackNotify.checkConnection(onRetryCancelBookingApi, constraintLayoutDashboard)
    }

    internal var onRetryCancelBookingApi: OnClickInterface = object : OnClickInterface {
        override fun onClick() {

            callCancelRoomBooking()
        }
    }

    override fun onViewDetailRoomBookSuccess(viewRoomDetailBookingModel: ViewRoomDetailBookingModel) {

        this.viewRoomDetailBookingModel = viewRoomDetailBookingModel

        showDialogForViewRoomdetails()
    }

    override fun onViewDetailRoomBookUnSuccess(message: String) {
        SnackNotify.showMessage(message!!, this, constraintLayoutDashboard)
    }

    override fun onViewDetailRoomBookInternetError() {

        SnackNotify.checkConnection(onRetryViewDetailRoomBookingApi, constraintLayoutDashboard)
    }

    internal var onRetryViewDetailRoomBookingApi: OnClickInterface = object : OnClickInterface {
        override fun onClick() {

            callViewDetailDataApi()
        }
    }


    fun showDialogForViewRoomdetails() {

        dialog = Dialog(this, R.style.CustomDialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_view_room_book)
        dialog.getWindow()!!.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.alpha(Color.BLACK)))

        val imgViewClose = dialog.findViewById<View>(R.id.imgViewClose) as ImageView

        val linLayoutBookingId = dialog.findViewById<View>(R.id.linLayoutBookingId) as LinearLayout
        val txtViewBookingId = dialog.findViewById<View>(R.id.txtViewBookingId) as TextView

        val linLayoutBookingStatus = dialog.findViewById<View>(R.id.linLayoutBookingStatus) as LinearLayout
        val txtViewBookingStatus = dialog.findViewById<View>(R.id.txtViewBookingStatus) as TextView

        val linLayoutCheckIn = dialog.findViewById<View>(R.id.linLayoutCheckIn) as LinearLayout
        val txtViewCheckIn = dialog.findViewById<View>(R.id.txtViewCheckIn) as TextView

        val linLayoutCheckOut = dialog.findViewById<View>(R.id.linLayoutCheckOut) as LinearLayout
        val txtViewCheckOut = dialog.findViewById<View>(R.id.txtViewCheckOut) as TextView

        val linLayoutRoomName = dialog.findViewById<View>(R.id.linLayoutRoomName) as LinearLayout
        val txtViewRoomName = dialog.findViewById<View>(R.id.txtViewRoomName) as TextView

        val linLayoutBedRooms = dialog.findViewById<View>(R.id.linLayoutBedRooms) as LinearLayout
        val txtViewBedRooms = dialog.findViewById<View>(R.id.txtViewBedRooms) as TextView

        val linLayoutAddress = dialog.findViewById<View>(R.id.linLayoutAddress) as LinearLayout
        val txtViewAddress = dialog.findViewById<View>(R.id.txtViewAddress) as TextView

        val linLayoutCity = dialog.findViewById<View>(R.id.linLayoutCity) as LinearLayout
        val txtViewCity = dialog.findViewById<View>(R.id.txtViewCity) as TextView

        val linLayoutState = dialog.findViewById<View>(R.id.linLayoutState) as LinearLayout
        val txtViewState = dialog.findViewById<View>(R.id.txtViewState) as TextView

        val linLayoutCountry = dialog.findViewById<View>(R.id.linLayoutCountry) as LinearLayout
        val txtViewCountry = dialog.findViewById<View>(R.id.txtViewCountry) as TextView

        val linLayoutZipCode = dialog.findViewById<View>(R.id.linLayoutZipCode) as LinearLayout
        val txtViewZipCode = dialog.findViewById<View>(R.id.txtViewZipCode) as TextView

        val linLayoutOwnerName = dialog.findViewById<View>(R.id.linLayoutOwnerName) as LinearLayout
        val txtViewOwnerName = dialog.findViewById<View>(R.id.txtViewOwnerName) as TextView

        val linLayoutEmail = dialog.findViewById<View>(R.id.linLayoutEmail) as LinearLayout
        val txtViewEmail = dialog.findViewById<View>(R.id.txtViewEmail) as TextView

        val linLayoutOwnerMobileNumber = dialog.findViewById<View>(R.id.linLayoutOwnerMobileNumber) as LinearLayout
        val txtViewOwnerMobileNumber = dialog.findViewById<View>(R.id.txtViewOwnerMobileNumber) as TextView

        val linLayoutRoomCharge = dialog.findViewById<View>(R.id.linLayoutRoomCharge) as LinearLayout
        val txtViewRoomCharge = dialog.findViewById<View>(R.id.txtViewRoomCharge) as TextView

        val linLayoutTaxAmount = dialog.findViewById<View>(R.id.linLayoutTaxAmount) as LinearLayout
        val txtViewTaxAmount = dialog.findViewById<View>(R.id.txtViewTaxAmount) as TextView

        val linLayoutGrandTotal = dialog.findViewById<View>(R.id.linLayoutGrandTotal) as LinearLayout
        val txtViewGrandTotal = dialog.findViewById<View>(R.id.txtViewGrandTotal) as TextView

        val linLayoutDepositAmount = dialog.findViewById<View>(R.id.linLayoutDepositAmount) as LinearLayout
        val txtViewDepositAmount = dialog.findViewById<View>(R.id.txtViewDepositAmount) as TextView

        val linLayoutRemainingAmount = dialog.findViewById<View>(R.id.linLayoutRemainingAmount) as LinearLayout
        val txtViewRemainingAmount = dialog.findViewById<View>(R.id.txtViewRemainingAmount) as TextView

        val linLayoutTransactionId = dialog.findViewById<View>(R.id.linLayoutTransactionId) as LinearLayout
        val txtViewTransactionId = dialog.findViewById<View>(R.id.txtViewTransactionId) as TextView

        val linLayoutTransactionStatus = dialog.findViewById<View>(R.id.linLayoutTransactionStatus) as LinearLayout
        val txtViewTransactionStatus = dialog.findViewById<View>(R.id.txtViewTransactionStatus) as TextView

        //setLayoutManager()

        /*set the booking Id*/
        if (viewRoomDetailBookingModel.data!!.room!!.id!! > 0) {

            linLayoutBookingId.visibility = View.VISIBLE
            txtViewBookingId.text = viewRoomDetailBookingModel.data!!.room!!.id!!.toString()

        } else {

            linLayoutBookingId.visibility = View.GONE
        }


        /*set the booking Status*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.status!!)) {

            linLayoutBookingStatus.visibility = View.VISIBLE
            txtViewBookingStatus.text = viewRoomDetailBookingModel.data!!.room!!.status!!

        } else {

            linLayoutBookingStatus.visibility = View.GONE
        }

        /*set the Check in Date*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.bookings!!.startDate!!)) {

            linLayoutCheckIn.visibility = View.VISIBLE
            txtViewCheckIn.text = viewRoomDetailBookingModel.data!!.bookings!!.startDate!!

        } else {

            linLayoutCheckIn.visibility = View.GONE
        }

        /*set the Check out Date*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.bookings!!.endDate!!)) {

            linLayoutCheckOut.visibility = View.VISIBLE
            txtViewCheckOut.text = viewRoomDetailBookingModel.data!!.bookings!!.endDate!!

        } else {

            linLayoutCheckOut.visibility = View.GONE
        }

        /*set the Bed Room Name*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.name!!)) {

            linLayoutRoomName.visibility = View.VISIBLE
            txtViewRoomName.text = viewRoomDetailBookingModel.data!!.room!!.name!!

        } else {

            linLayoutRoomName.visibility = View.GONE
        }


        /*set the Bed Room*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.bedrooms!!)) {

            linLayoutBedRooms.visibility = View.VISIBLE
            txtViewBedRooms.text = viewRoomDetailBookingModel.data!!.room!!.bedrooms!!

        } else {

            linLayoutCheckOut.visibility = View.GONE
        }

        /*set the Address*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.address1!!)) {

            linLayoutAddress.visibility = View.VISIBLE
            txtViewAddress.text = viewRoomDetailBookingModel.data!!.room!!.address1!!

        } else {

            linLayoutAddress.visibility = View.GONE
        }

        /*set the City*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.city!!)) {

            linLayoutCity.visibility = View.VISIBLE
            txtViewCity.text = viewRoomDetailBookingModel.data!!.room!!.city!!

        } else {

            linLayoutCity.visibility = View.GONE
        }

        /*set the State*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.state!!)) {

            linLayoutState.visibility = View.VISIBLE
            txtViewState.text = viewRoomDetailBookingModel.data!!.room!!.state!!

        } else {

            linLayoutState.visibility = View.GONE
        }

        /*set the Country*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.country!!)) {

            linLayoutCountry.visibility = View.VISIBLE
            txtViewCountry.text = viewRoomDetailBookingModel.data!!.room!!.country!!

        } else {

            linLayoutCountry.visibility = View.GONE
        }

        /*  set the postal code*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.room!!.postal!!)) {

            linLayoutZipCode.visibility = View.VISIBLE
            txtViewZipCode.text = viewRoomDetailBookingModel.data!!.room!!.postal!!.toString()

        } else {

            linLayoutZipCode.visibility = View.GONE
        }

        /*set the owner name*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.owner!!.username!!)) {

            linLayoutOwnerName.visibility = View.VISIBLE
            txtViewOwnerName.text = viewRoomDetailBookingModel.data!!.owner!!.username!!.toString()

        } else {

            linLayoutOwnerName.visibility = View.GONE
        }

        /*set the email*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.owner!!.email!!)) {

            linLayoutEmail.visibility = View.VISIBLE
            txtViewEmail.text = viewRoomDetailBookingModel.data!!.owner!!.email!!.toString()

        } else {

            linLayoutEmail.visibility = View.GONE
        }


        /*set the mobile numbner*/
        if (!Utils.isEmptyOrNull(viewRoomDetailBookingModel.data!!.owner!!.mobileNumber!!)) {

            linLayoutOwnerMobileNumber.visibility = View.VISIBLE
            txtViewOwnerMobileNumber.text = viewRoomDetailBookingModel.data!!.owner!!.mobileNumber!!.toString()

        } else {

            linLayoutOwnerMobileNumber.visibility = View.GONE
        }

        /*set the room charge*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.grandTotal!! > 0) {

            linLayoutRoomCharge.visibility = View.VISIBLE
            txtViewRoomCharge.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.grandTotal!!.toString()

        } else {

            linLayoutRoomCharge.visibility = View.GONE
        }

        /*set the tax amount*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.taxAmount!! > 0) {

            linLayoutTaxAmount.visibility = View.VISIBLE
            txtViewTaxAmount.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.taxAmount!!.toString()

        } else {

            linLayoutTaxAmount.visibility = View.GONE
        }

        /*set the grand total*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.grandTotal!! > 0) {

            linLayoutGrandTotal.visibility = View.VISIBLE
            txtViewGrandTotal.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.grandTotal!!.toString()

        } else {

            linLayoutGrandTotal.visibility = View.GONE
        }

        /*set the deposit amount*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.depositAmount!! > 0) {

            linLayoutDepositAmount.visibility = View.VISIBLE
            txtViewDepositAmount.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.depositAmount!!.toString()

        } else {

            linLayoutDepositAmount.visibility = View.GONE
        }

        /*set the remaining amount*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.remainingAmount!! > 0) {

            linLayoutRemainingAmount.visibility = View.VISIBLE
            txtViewRemainingAmount.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingInvoice!!.remainingAmount!!.toString()

        } else {

            linLayoutRemainingAmount.visibility = View.GONE
        }

        /*  set the transaction Id*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingTransactions!!.size > 0) {

            linLayoutTransactionId.visibility = View.VISIBLE
            txtViewTransactionId.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingTransactions!!.get(0)!!.txnId

        } else {

            linLayoutTransactionId.visibility = View.GONE
        }

        /*   set the transaction status*/
        if (viewRoomDetailBookingModel.data!!.bookings!!.bookingTransactions!!.size > 0) {

            linLayoutTransactionStatus.visibility = View.VISIBLE
            txtViewTransactionStatus.text = viewRoomDetailBookingModel.data!!.bookings!!.bookingTransactions!!.get(0)!!.status

        } else {

            linLayoutTransactionStatus.visibility = View.GONE
        }


        imgViewClose.setOnClickListener { dialog.dismiss() }

        dialog.show()
    }


    /* private fun replaceFragment(fragment: Fragment, tag: String) {

         val index = supportFragmentManager.backStackEntryCount - 1
         val backEntry = supportFragmentManager.getBackStackEntryAt(index)
         val tagFragment = backEntry.name

       *//*  if (tag != tagFragment) {*//*
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.frameLayout, fragment, tag).addToBackStack(tag)
            fragmentTransaction.commitAllowingStateLoss()
        //}
    }*/

}
