package com.babaracine.mvp.dashboard.presenter

import android.app.Activity
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.dashboard.fragment.TripsFragment
import com.babaracine.mvp.dashboard.model.TripsModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Braintech on 29-08-2018.
 */
class TripsPresenterImpl : TripsContractor.TripsPresenter {


    var activity: Activity
    var tripsView: TripsContractor.TripsView

    constructor(activity: Activity, tripsView: TripsContractor.TripsView) {

        this.activity = activity
        this.tripsView = tripsView
    }

    override fun onTrips() {

        try {
            ApiAdapter.getInstance(activity)
            callYourTrips()
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            tripsView.onTripsInternetError()
        }

    }

    private fun callYourTrips() {

        Progress.start(activity)

        val url = Const.BASE_URL + "api/users/yourBooking.json"

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val getLoginOutput = ApiAdapter.getApiService()!!.getYourTrips("application/json", "no-cache", preferredLanguage,authorizationToken, url)

        getLoginOutput.enqueue(object : Callback<TripsModel> {
            override fun onResponse(call: Call<TripsModel>, response: Response<TripsModel>) {

                Progress.stop()

                try {
                    //getting whole data from response
                    val TripsModel = response.body()
                    val message = TripsModel.message!!
                    if (TripsModel.status == 1) {
                        tripsView.onTripsSuccess(TripsModel)
                    }else if(TripsModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    } else {
                        tripsView.onTripsUnSuccess(message)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    tripsView.onTripsUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<TripsModel>, t: Throwable) {
                Progress.stop()
                tripsView.onTripsUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }
}