package com.babaracine.mvp.dashboard.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import com.babaracine.R
import com.babaracine.common.helpers.CircleTransform
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.mvp.change_password.ChangePasswordActivity
import com.babaracine.mvp.dashboard.DashboardActivity
import com.babaracine.mvp.manage_room.ManageRoomActivity
import com.babaracine.mvp.property_list.PropertyListActivity
import com.babaracine.mvp.setting.SettingActivity
import com.babaracine.mvp.update_profile.UpdateProfileActivity
import com.babaracine.mvp.welcome.WelcomeActivity
import com.babaracine.mvp.your_reservation.YourReservationActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setData()


        txtViewListYourReservation.setOnClickListener(View.OnClickListener { onClickYourReservation() })
        txtViewHome.setOnClickListener(View.OnClickListener { onClickHome() })

        txtViewListPropertyList.setOnClickListener(View.OnClickListener { onClickPropertyList() })

        constraintLayoutListYourProperty.setOnClickListener(View.OnClickListener { onClickListYourProperty() })

        txtViewEditProfile.setOnClickListener(View.OnClickListener { onClickViewAndEditProfile() })

        txtViewSettings.setOnClickListener(View.OnClickListener { onChangePwdClick() })

        txtViewChangePassword.setOnClickListener(View.OnClickListener { onClickSetting() })
        txtViewListAddProperty.setOnClickListener(View.OnClickListener { onClickAddProperty() })

        txtViewLogout.setOnClickListener {
            LoginManagerSession.logoutUser()
            val intent = Intent(activity!!, WelcomeActivity::class.java)
            activity!!.startActivity(intent)
        }
    }

    private fun onClickAddProperty() {
        LoginManagerSession.setIsManageFrom(false)
        val intent = Intent(activity!!,ManageRoomActivity::class.java)
        startActivity(intent)
    }

    private fun onChangePwdClick() {
        val intent = Intent(activity, SettingActivity::class.java)
        activity!!.startActivity(intent)
    }

    private fun onClickHome() {
        val intent = Intent(activity, DashboardActivity::class.java)
        activity!!.startActivity(intent)
        activity!!.finishAffinity()
    }


    private fun onClickSetting() {

        val intent = Intent(activity, ChangePasswordActivity::class.java)
        activity!!.startActivity(intent)
    }

    private fun setData() {

        val name = LoginManagerSession.getUserData().data!!.user!!.fullName
        txtViewName.text = name

        Picasso.with(activity).load(LoginManagerSession.getUserData().data!!.user!!.fullImageUrl).into(imgViewProfileImage)

        Picasso.with(activity).load(LoginManagerSession.getUserData().data!!.user!!.fullImageUrl).transform(CircleTransform()).into(imgViewProfileImage)
    }

    private fun onClickViewAndEditProfile() {
        val intent = Intent(activity!!, UpdateProfileActivity::class.java)
        activity!!.startActivity(intent)
    }


    private fun onClickListYourProperty() {

        if (imgViewListPropertyArrowDown.visibility == View.VISIBLE) {
            imgViewListPropertyArrowDown.visibility = View.GONE
            imgViewListPropertyArrowUp.visibility = View.VISIBLE
            constraintLayoutListPropertyItem.visibility = View.VISIBLE
        } else {
            imgViewListPropertyArrowDown.visibility = View.VISIBLE
            imgViewListPropertyArrowUp.visibility = View.GONE
            constraintLayoutListPropertyItem.visibility = View.GONE
        }

    }

    private fun onClickYourReservation() {

        val intent = Intent(activity, YourReservationActivity::class.java)
        startActivity(intent)
    }

    private fun onClickPropertyList() {

        val intent = Intent(activity, PropertyListActivity::class.java)
        startActivity(intent)
    }

}
