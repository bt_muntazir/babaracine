package com.babaracine.mvp.dashboard.presenter

import android.app.Activity
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.dashboard.model.RoomListingModel
import com.babaracine.mvp.login.presenter.LoginContractor
import com.babaracine.mvp.login.presenter.LoginModel
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Braintech on 14-08-2018.
 */
class RoomListingPresenterImpl : RoomListingContractor.RoomListingPresenter {

    var activity: Activity
    var roomListingView: RoomListingContractor.RoomListingView

    constructor(activity: Activity, roomListingView: RoomListingContractor.RoomListingView) {

        this.activity = activity
        this.roomListingView = roomListingView
    }

    override fun onRoomListing(page:Int,search:String,startDate:String,endDate:String,minPrice:String,maxPrice:String,suitableFor:String) {

        try {
            ApiAdapter.getInstance(activity)
            callRoomListing(page,search,startDate,endDate,minPrice,maxPrice,suitableFor)
        } catch (e: ApiAdapter.Companion.NoInternetException) {
            roomListingView.onRoomListingInternetError()
        }

    }

    private fun callRoomListing(page:Int,search:String,startDate:String,endDate:String,minPrice:String,maxPrice:String,suitableFor:String) {

        Progress.start(activity)

       // val url = Const.BASE_URL + "api/rooms/listing.json"

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()
        val getLoginOutput = ApiAdapter.getApiService()!!.getRoomListing("application/json", "no-cache", preferredLanguage,authorizationToken,search,startDate,endDate,minPrice,maxPrice,suitableFor,page )

        getLoginOutput.enqueue(object : Callback<RoomListingModel> {
            override fun onResponse(call: Call<RoomListingModel>, response: Response<RoomListingModel>) {

                try {

                    Progress.stop()
                    //getting whole data from response
                    val roomListingModel = response.body()
                    val message = roomListingModel.message!!
                    if (roomListingModel.status == 1) {
                        roomListingView.onRoomListingSuccess(roomListingModel)
                    }else if(roomListingModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    } else {
                        roomListingView.onRoomListingUnSuccess(message)
                    }
                } catch (exp: NullPointerException) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    Progress.stop()

                    roomListingView.onRoomListingUnSuccess(activity.getString(R.string.error_server))
                }

            }

            override fun onFailure(call: Call<RoomListingModel>, t: Throwable) {
                Progress.stop()
                roomListingView.onRoomListingUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }
}