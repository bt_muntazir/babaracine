package com.babaracine.mvp.dashboard.fragment

import android.app.DatePickerDialog
import android.content.Context
import android.widget.DatePicker

class MyDatePickerDialog(context: Context, callBack: DatePickerDialog.OnDateSetListener, year: Int, monthOfYear: Int, dayOfMonth: Int, private val title: CharSequence) : DatePickerDialog(context, callBack, year, monthOfYear, dayOfMonth) {

    init {
        setTitle(title)
    }

    override fun onDateChanged(view: DatePicker, year: Int, month: Int, day: Int) {
        super.onDateChanged(view, year, month, day)
        if (datePicker.calendarViewShown) {
            setTitle(title)
        }
    }


}
