package com.babaracine.mvp.splash

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.location.*
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Base64
import android.util.Log
import android.view.Window
import android.view.WindowManager
import com.babaracine.R
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.Const
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.Payment.PaymentActivity
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.dashboard.DashboardActivity
import com.babaracine.mvp.setting.SettingActivity
import com.babaracine.mvp.submit_id_proof.SubmitIdProofActivity

import com.babaracine.mvp.welcome.WelcomeActivity
import java.io.ByteArrayInputStream
import java.io.IOException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.cert.CertificateEncodingException
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate

class SplashActivity : BaseActivity(), LocationListener {

    companion object {
        val SPLASH_TIME_OUT = 2000
    }

    private var locationManager: LocationManager? = null
    internal var isAskedPermission = false
    private val PERMISSION_CODE__LOCATION = 2910

    internal lateinit var handler : Handler
    internal lateinit var myRunnable : Runnable

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_splash)

        getHashKey()
        getCertificateSHA1Fingerprint()
        //getCurrentLocation()
        handler = Handler()
        myRunnable = Runnable {


            /*val intent = Intent(this@SplashActivity, ChangePasswordActivity::class.java)
            startActivity(intent)*/
           /* if(Utils.isEmptyOrNull(LoginManagerSession.getPreferredLanguage())) {

                    LoginManagerSession.setPreferredLanguage(Const.KEY_DEFAULT_LANGUAGE)
            }*/


            if(LoginManagerSession.isLoggedIn()){
                val intent = Intent(this@SplashActivity, DashboardActivity::class.java)
                //val intent = Intent(this@SplashActivity, PaymentActivity::class.java)
                startActivity(intent)
            }else{
                val intent = Intent(this@SplashActivity, SettingActivity::class.java)
                startActivity(intent)
            }

            finish()
        }
        handler.postDelayed(myRunnable, SPLASH_TIME_OUT.toLong())
    }

    override fun onLocationChanged(location: Location?) {

        Progress.stop()
        var countryName = ""
        if (location != null) {
            val geocoder = Geocoder(this)
            try {
                val address = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0)
                countryName = address.countryName
                //Log.e("name",name)
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
        }


       /* if(Utils.isEmptyOrNull(LoginManagerSession.getPreferredLanguage())) {

            if (countryName.equals(getString(R.string.title_spain), true)) {
                LoginManagerSession.setPreferredLanguage(Const.KEY_SPAIN_LANGUAGE)
            } else if (countryName.equals(getString(R.string.title_france), true)) {
                LoginManagerSession.setPreferredLanguage(Const.KEY_FRANCE_LANGUAGE)
            } else {
                LoginManagerSession.setPreferredLanguage(Const.KEY_DEFAULT_LANGUAGE)
            }
        }*/

        if (LoginManagerSession.isLoggedIn()) {
            val intent = Intent(this@SplashActivity, DashboardActivity::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this@SplashActivity, WelcomeActivity::class.java)
            startActivity(intent)
        }

        finish()
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

    }

    override fun onProviderEnabled(p0: String?) {

    }

    override fun onProviderDisabled(p0: String?) {

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        // requestCode (2908 for pick image from gallary), (2909 for camera) and 2910 is for selectedItemLocation permission

        when (requestCode) {

            PERMISSION_CODE__LOCATION -> { // selectedItemLocation enable

                val permissionCheckLoc = ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)

                val permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)

                if (permissionCheckLoc == 0 && permissionCoarseLoc == 0) {
                    getCurrentLocation()
                } else {
                    //SnackNotify.showMessage("Please provide security permission from aap setting.", coordinateLayout);
                    isAskedPermission = false
                    alertEnableLocation("We need location to continue this application.Do you want to enable it?", false)
                }
                return
            }


        }
    }

    private fun getCurrentLocation() {

        Progress.start(this)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            isAskedPermission = false

            alertEnableLocation(getString(R.string.title_gps), true)

            // return;
        } else if (!checkIsPermissionGranted()) {
            askLocationPermission()
        } else {
            getLocation()
        }
    }

    fun alertEnableLocation(msg: String, isEnableLocation: Boolean) {
        val builder = AlertDialog.Builder(this@SplashActivity)
        builder.setMessage(msg)
                .setCancelable(false)

                .setNegativeButton(getString(R.string.title_exit)) { dialog, which -> finishAffinity() }

                .setPositiveButton(getString(R.string.title_go_to_setting)) { dialog, id ->
                    if (isEnableLocation) {
                        startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    } else {
                        startActivity(Intent(Settings.ACTION_APPLICATION_SETTINGS))
                    }

                    isAskedPermission = true

                    dialog.dismiss()
                }
        /*.setNegativeButton(context.getString(R.string.title_no), new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                System.exit(0);
                            }
                        });*/
        val alert = builder.create()
        alert.show()
    }

    private fun askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), PERMISSION_CODE__LOCATION)
        } else {
            getLocation()
        }
    }

    private fun getLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val networkLocationEnabled = locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        val gpsLocationEnabled = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            return
        }

        if (networkLocationEnabled || gpsLocationEnabled) {

            if (locationManager!!.getAllProviders().contains(LocationManager.NETWORK_PROVIDER) && networkLocationEnabled) {
                locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 2000f, this)
                locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
            } else if (locationManager!!.getAllProviders().contains(LocationManager.GPS_PROVIDER) && gpsLocationEnabled) {
                locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 2000f, this)
                locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            }
        }
    }

    private fun checkIsPermissionGranted(): Boolean {

        val permissionCheckLoc = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)

        val permissionCoarseLoc = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)

        return if (permissionCheckLoc == PackageManager.PERMISSION_GRANTED && permissionCoarseLoc == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }


    private fun getHashKey() {
        try {
            val info = packageManager.getPackageInfo(
                    "com.babaracine",
                    PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

    }
    private fun getCertificateSHA1Fingerprint(): String? {
        val pm = this.packageManager
        val packageName = this.packageName
        val flags = PackageManager.GET_SIGNATURES
        var packageInfo: PackageInfo? = null
        try {
            packageInfo = pm.getPackageInfo(packageName, flags)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        val signatures = packageInfo!!.signatures
        val cert = signatures[0].toByteArray()
        val input = ByteArrayInputStream(cert)
        var cf: CertificateFactory? = null
        try {
            cf = CertificateFactory.getInstance("X509")
        } catch (e: CertificateException) {
            e.printStackTrace()
        }

        var c: X509Certificate? = null
        try {
            c = cf!!.generateCertificate(input) as X509Certificate
        } catch (e: CertificateException) {
            e.printStackTrace()
        }

        var hexString: String? = null
        try {
            val md = MessageDigest.getInstance("SHA1")
            val publicKey = md.digest(c!!.encoded)
            hexString = byte2HexFormatted(publicKey)
        } catch (e1: NoSuchAlgorithmException) {
            e1.printStackTrace()
        } catch (e: CertificateEncodingException) {
            e.printStackTrace()
        }

        Log.e("SHA1 ", hexString)
        return hexString
    }

    fun byte2HexFormatted(arr: ByteArray): String {
        val str = StringBuilder(arr.size * 2)
        for (i in arr.indices) {
            var h = Integer.toHexString(arr[i].toInt())
            val l = h.length
            if (l == 1) h = "0$h"
            if (l > 2) h = h.substring(l - 2, l)
            str.append(h.toUpperCase())
            if (i < arr.size - 1) str.append(':')
        }
        return str.toString()
    }
}
