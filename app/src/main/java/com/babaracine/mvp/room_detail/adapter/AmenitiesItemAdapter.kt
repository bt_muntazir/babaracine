package com.babaracine.mvp.room_detail.adapter

import android.app.Activity

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import java.util.ArrayList
import com.babaracine.R
import com.babaracine.mvp.room_detail.presenter.RoomDetailModel
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.item_room_detail_amenities.view.*


/**
 * Created by Braintech on 7/4/2018.
 */
class AmenitiesItemAdapter(val context: Context,val arrayListRoomDetailAmenity : ArrayList<RoomDetailModel.Data.RoomAmenity>) : RecyclerView.Adapter<AmenitiesItemAdapter.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_room_detail_amenities, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val dataModel = arrayListRoomDetailAmenity[position]

        val itemName = dataModel.amenity!!.name

        holder.txtViewAmenitiesItemName!!.text = itemName
        Picasso.with(context).load(dataModel.amenity!!.imageFullUrl).into(holder.imgViewAmenitiesItem)

    }



    override fun getItemCount(): Int {
        return arrayListRoomDetailAmenity.size
    }




    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val imgViewAmenitiesItem = itemView.imgViewAmenitiesItem
        val txtViewAmenitiesItemName = itemView.txtViewAmenitiesItemName

    }


}