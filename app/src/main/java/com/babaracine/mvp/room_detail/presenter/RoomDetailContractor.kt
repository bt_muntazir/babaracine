package com.babaracine.mvp.room_detail.presenter

interface RoomDetailContractor {

    interface RoomDetailPresenter{

        fun getRoomDetailData(roomId : String)

    }

    interface RoomDetailView{

        fun onRoomDetailSuccess(roomDetailModel: RoomDetailModel)
        fun onRoomDetailUnSuccess(message : String)
        fun onRoomDetailInternetError()

    }
}