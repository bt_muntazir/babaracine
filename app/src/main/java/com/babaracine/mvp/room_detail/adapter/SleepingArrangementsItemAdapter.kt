package com.babaracine.mvp.room_detail.adapter

import android.app.Activity

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import java.util.ArrayList
import com.babaracine.R
import kotlinx.android.synthetic.main.item_sleeping_arrangement.view.*


/**
 * Created by Braintech on 7/4/2018.
 */
class SleepingArrangementsItemAdapter(val context: Context, val arrayListBeds : ArrayList<String>) : RecyclerView.Adapter<SleepingArrangementsItemAdapter.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_sleeping_arrangement, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val bedName = arrayListBeds[position]

        //val itemName = dataModel.amenity!!.name

        holder.txtViewBedName!!.text = bedName
        //Picasso.with(context).load(dataModel.amenity!!.imageFullUrl).into(holder.imgViewAmenitiesItem)

    }



    override fun getItemCount(): Int {
        return arrayListBeds.size
    }




    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val imgViewBeds = itemView.imgViewBed
        val txtViewBedName = itemView.txtViewBedName

    }


}