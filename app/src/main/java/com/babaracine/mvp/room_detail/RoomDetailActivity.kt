package com.babaracine.mvp.room_detail

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager


import android.view.View
import com.babaracine.R
import com.babaracine.common.helpers.TextViewMoreLess
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.mvp.review_steps.RoomCheckAvailabilityActivity
import com.babaracine.mvp.room_detail.adapter.AmenitiesItemAdapter
import com.babaracine.mvp.room_detail.presenter.RoomDetailContractor
import com.babaracine.mvp.room_detail.presenter.RoomDetailModel
import com.babaracine.mvp.room_detail.presenter.RoomDetailPresenterImpl
import kotlinx.android.synthetic.main.activity_room_detail.*
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.room_detail.adapter.SleepingArrangementsItemAdapter
import com.babaracine.mvp.room_detail.adapter.SlidingImageAdapter
import java.util.*
import kotlin.collections.ArrayList


class RoomDetailActivity : BaseActivity(), RoomDetailContractor.RoomDetailView {

    var roomId = ""
    lateinit var roomDetailPresenterImpl: RoomDetailPresenterImpl

   /* val CONST_GUEST = " "+ getString(R.string.guest)
    val CONST_BEDS = " "+ getString(R.string.beds)
    val CONST_BEDROOMS = " "+ getString(R.string.bathrooms)
    val CONST_BATH = " "+ getString(R.string.bath)*/

    lateinit var arrayListSleepingBeds: ArrayList<String>

    lateinit var arrayListRoomImages: ArrayList<String>

    lateinit var roomDetailModel: RoomDetailModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room_detail)

        roomDetailPresenterImpl = RoomDetailPresenterImpl(this, this)
        indicator.setupWithViewPager(viewPagerRoomImage, true);
        setLayoutManager()
        getIntentData()
        getRoomDetailData()
        manageClickEvents()
    }

    override fun onRoomDetailSuccess(roomDetailModel: RoomDetailModel) {
        this.roomDetailModel = roomDetailModel

        setDataFromApi()
    }

    private fun setLayoutManager() {
        val linearLayoutManager = LinearLayoutManager(this)
        recyclerViewAmenitiesItem.layoutManager = linearLayoutManager!!


        val gridlayoutManager = GridLayoutManager(this, 3)
        recyclerViewSleepingArrangement.layoutManager = gridlayoutManager as RecyclerView.LayoutManager?
        recyclerViewSleepingArrangement.isNestedScrollingEnabled = true


    }

    private fun setAmenitiesAdapter() {
        val adGraphicAdapter = AmenitiesItemAdapter(this, roomDetailModel.data!!.roomAmenities!!)
        recyclerViewAmenitiesItem.setAdapter(adGraphicAdapter)
    }

    private fun setDataFromApi() {

        val data = roomDetailModel.data!!

        if (data != null) {
            txtViewDetails.text = data.description
            if (TextViewMoreLess.isViewMoreRequired(txtViewDetails)) {
                TextViewMoreLess.doResizeTextView(txtViewDetails, 3, getString(R.string.read_more), true)
            }

            txtViewRoomName.text = data.name

            txtViewGuest.text = data.maxOccupants!!.toString() + " "+ getString(R.string.guest)
            txtViewBedrooms.text = data.bedrooms.toString() + " "+ getString(R.string.bedrooms)

            val beds: String
            if (data.beds!!.toInt() == 16) {
                beds = "16+" + " "+ getString(R.string.beds)
            } else {
                beds = data.beds!! + " "+ getString(R.string.beds)
            }

            txtViewBeds.text = beds
            txtViewBath.text = data.bathrooms.toString() + " "+ getString(R.string.bath)

            // set Amenities item
            setAmenitiesAdapter()

            val cancelationType = data.roomPricing!!.cancellationType!!
            txtViewCancellationType.text = cancelationType + " " + getString(R.string.title_cancellation)

            if (cancelationType.equals(getString(R.string.title_free), true)) {
                txtViewCancellation.text = getString(R.string.title_cancellation) + getString(R.string.open_bracket) + cancelationType + getString(R.string.close_bracket)
            } else {
                txtViewCancellation.text = getString(R.string.title_cancellation) + getString(R.string.open_bracket) + cancelationType + " - " + data.roomPricing!!.cancellationCharge + "%" + getString(R.string.close_bracket)
            }




            txtViewCancellationDesc.text = data.roomPricing!!.cancellationDesc

            ratingBar.rating = data.averageRating!!.toFloat()

            val webContent = data.policy
            webViewHousingRules.loadData(webContent, "text/html", "UTF-8")

            Collections.sort(data.roomPhotos!!, SortbyFeaturedBit())
            Collections.sort(data.roomPhotos!!, Collections.reverseOrder())

            viewPagerRoomImage.setAdapter(SlidingImageAdapter(this@RoomDetailActivity, data.roomPhotos!!))


            txtViewPrice.text = Utils.getCurrenctSymbol(data.roomPricing!!.currencyCode!!, this) + " " + data.roomPricing!!.nightlyPrice!!.toString()

            /*val indicator = findViewById<View>(R.id.indicatorRoomImage) as CirclePageIndicator

            indicator.setViewPager(viewPagerRoomImage)

            val density = resources.displayMetrics.density
            var currentPage = 0
            var NUM_PAGES = 0
//Set circle indicator radius
            indicator.setRadius(5 * density)

            NUM_PAGES = data.roomPhotos!!.size

            // Auto start of viewpager
            val handler = Handler()
            val Update = Runnable {
                if (currentPage === NUM_PAGES) {
                    currentPage = 0
                }
                viewPagerRoomImage.setCurrentItem(currentPage++, true)
            }
            val swipeTimer = Timer()
            swipeTimer.schedule(object : TimerTask() {
                override fun run() {
                    handler.post(Update)
                }
            }, 3000, 3000)

            // Pager listener over indicator
            indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

                override fun onPageSelected(position: Int) {
                    currentPage = position

                }

                override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

                }

                override fun onPageScrollStateChanged(pos: Int) {

                }
            })

*/

            setSleepingArrangementsApadter()


        }


    }

    private fun setSleepingArrangementsApadter() {

        arrayListSleepingBeds = ArrayList()
        val numberOfBeds = roomDetailModel.data!!.beds!!.toInt()

        for (i in 1..numberOfBeds) {
            if (i == 1) {
                arrayListSleepingBeds.add(getString(R.string.first_bed))
            } else if (i == 2) {
                arrayListSleepingBeds.add(getString(R.string.first_bed))
            } else if (i == 3) {
                arrayListSleepingBeds.add(getString(R.string.first_bed))
            } else {
                arrayListSleepingBeds.add("" + i + getString(R.string.th_bed))
            }
        }

        val sleepingArrangementsItemAdapter = SleepingArrangementsItemAdapter(this, arrayListSleepingBeds)
        recyclerViewSleepingArrangement.setAdapter(sleepingArrangementsItemAdapter)
    }

    override fun onRoomDetailUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, constrainLayoutRoomDetail)
    }

    override fun onRoomDetailInternetError() {
        SnackNotify.checkConnection(onRetryRoomDetail, constrainLayoutRoomDetail)
    }


    private fun getRoomDetailData() {
        roomDetailPresenterImpl.getRoomDetailData(roomId)
    }

    private fun getIntentData() {
        if (intent != null) {
            if (intent.hasExtra(ConstIntent.KEY_ROOM_ID)) {
                roomId = intent.getIntExtra(ConstIntent.KEY_ROOM_ID, 0).toString()
            }
        }
    }

    private fun manageClickEvents() {
        btnNext.setOnClickListener(View.OnClickListener { onClickCheckAvailability() })

        imgViewBack.setOnClickListener { finish() }

        imgViewAmenitiesPlus.setOnClickListener(View.OnClickListener { onClickPlusAmenities() })
        imgViewAmenitiesMinus.setOnClickListener(View.OnClickListener { onClickMinusAmenities() })

        imgViewCancellationPlus.setOnClickListener(View.OnClickListener { onClickCancellationPlus() })
        imgViewCancellationMinus.setOnClickListener(View.OnClickListener { onClickCancellationMinus() })

        imgViewHouseRulesPlus.setOnClickListener(View.OnClickListener { onClickHousingRulePlus() })
        imgViewHouseRulesMinus.setOnClickListener(View.OnClickListener { onClickHousingRuleMinus() })

        imgViewSleepingArrangementPlus.setOnClickListener(View.OnClickListener { onClickSleepingArrangementPlus() })
        imgViewSleepingArrangementMinus.setOnClickListener(View.OnClickListener { onClickSleepingArrangementMinus() })

    }

    private fun onClickSleepingArrangementMinus() {
        constraintLayoutItemSleepingArrangements.visibility = View.GONE
        imgViewSleepingArrangementPlus.visibility = View.VISIBLE
        imgViewSleepingArrangementMinus.visibility = View.GONE
    }

    private fun onClickSleepingArrangementPlus() {
        constraintLayoutItemSleepingArrangements.visibility = View.VISIBLE
        imgViewSleepingArrangementPlus.visibility = View.GONE
        imgViewSleepingArrangementMinus.visibility = View.VISIBLE

        closeAllItemsExceptSleepingArrangements()
    }

    private fun closeAllItemsExceptSleepingArrangements() {
        imgViewHouseRulesMinus.visibility = View.GONE
        imgViewHouseRulesPlus.visibility = View.VISIBLE

        imgViewCancellationMinus.visibility = View.GONE
        imgViewCancellationPlus.visibility = View.VISIBLE

        imgViewAmenitiesMinus.visibility = View.GONE
        imgViewAmenitiesPlus.visibility = View.VISIBLE

        webViewHousingRules.visibility = View.GONE
        recyclerViewAmenitiesItem.visibility = View.GONE
        txtViewCancellationDesc.visibility = View.GONE
    }

    private fun onClickHousingRuleMinus() {
        webViewHousingRules.visibility = View.GONE
        imgViewHouseRulesPlus.visibility = View.VISIBLE
        imgViewHouseRulesMinus.visibility = View.GONE


    }

    private fun onClickHousingRulePlus() {
        webViewHousingRules.visibility = View.VISIBLE
        imgViewHouseRulesPlus.visibility = View.GONE
        imgViewHouseRulesMinus.visibility = View.VISIBLE

        closeAllItemsExceptHousingRules()
    }

    private fun closeAllItemsExceptHousingRules() {
        imgViewSleepingArrangementMinus.visibility = View.GONE
        imgViewSleepingArrangementPlus.visibility = View.VISIBLE

        imgViewCancellationMinus.visibility = View.GONE
        imgViewCancellationPlus.visibility = View.VISIBLE

        imgViewAmenitiesMinus.visibility = View.GONE
        imgViewAmenitiesPlus.visibility = View.VISIBLE

        constraintLayoutItemSleepingArrangements.visibility = View.GONE
        recyclerViewAmenitiesItem.visibility = View.GONE
        txtViewCancellationDesc.visibility = View.GONE


    }

    private fun onClickCancellationMinus() {
        imgViewCancellationPlus.visibility = View.VISIBLE
        imgViewCancellationMinus.visibility = View.GONE
        txtViewCancellationDesc.visibility = View.GONE

    }

    private fun onClickCancellationPlus() {

        imgViewCancellationPlus.visibility = View.GONE
        imgViewCancellationMinus.visibility = View.VISIBLE
        txtViewCancellationDesc.visibility = View.VISIBLE

        closeAllItemsExceptCancellation()

    }

    private fun closeAllItemsExceptCancellation() {
        imgViewSleepingArrangementMinus.visibility = View.GONE
        imgViewSleepingArrangementPlus.visibility = View.VISIBLE

        imgViewHouseRulesMinus.visibility = View.GONE
        imgViewHouseRulesPlus.visibility = View.VISIBLE

        imgViewAmenitiesMinus.visibility = View.GONE
        imgViewAmenitiesPlus.visibility = View.VISIBLE

        constraintLayoutItemSleepingArrangements.visibility = View.GONE
        recyclerViewAmenitiesItem.visibility = View.GONE
        webViewHousingRules.visibility = View.GONE


    }

    private fun onClickMinusAmenities() {
        recyclerViewAmenitiesItem.visibility = View.GONE
        imgViewAmenitiesMinus.visibility = View.GONE
        imgViewAmenitiesPlus.visibility = View.VISIBLE

    }

    private fun onClickPlusAmenities() {
        recyclerViewAmenitiesItem.visibility = View.VISIBLE
        imgViewAmenitiesMinus.visibility = View.VISIBLE
        imgViewAmenitiesPlus.visibility = View.GONE

        closeAllItemsExceptAmenities()


    }

    private fun closeAllItemsExceptAmenities() {
        imgViewSleepingArrangementMinus.visibility = View.GONE
        imgViewSleepingArrangementPlus.visibility = View.VISIBLE

        imgViewHouseRulesMinus.visibility = View.GONE
        imgViewHouseRulesPlus.visibility = View.VISIBLE

        imgViewCancellationMinus.visibility = View.GONE
        imgViewCancellationPlus.visibility = View.VISIBLE

        constraintLayoutItemSleepingArrangements.visibility = View.GONE
        txtViewCancellationDesc.visibility = View.GONE
        webViewHousingRules.visibility = View.GONE

    }

    private fun onClickCheckAvailability() {
        val data = roomDetailModel.data!!
        var featuredImageUrl = ""
        for (i in 1..data.roomPhotos!!.size - 1) {
            if (data.roomPhotos!![i].featured == 1) {
                featuredImageUrl = data.roomPhotos!![i].imageUrl!!
            }
        }

        val intent = Intent(this, RoomCheckAvailabilityActivity::class.java)
        intent.putExtra(ConstIntent.KEY_RATING, data.averageRating)
        intent.putExtra(ConstIntent.KEY_PRICE, data.roomPricing!!.nightlyPrice)
        intent.putExtra(ConstIntent.KEY_CURRENCY_CODE, data.roomPricing!!.currencyCode)
        intent.putExtra(ConstIntent.KEY_IMGAGE_URL, featuredImageUrl)
        intent.putExtra(ConstIntent.KEY_CANCELATION_TYPE, data.roomPricing!!.cancellationType)
        intent.putExtra(ConstIntent.KEY_CANCELATION_CHARGE, data.roomPricing!!.cancellationCharge)
        intent.putExtra(ConstIntent.KEY_ROOM_NAME, data.name!!)
        intent.putExtra(ConstIntent.KEY_ROOM_ID, roomId)
        intent.putExtra(ConstIntent.KEY_ROOM_DETAILS, data.description!!)
        intent.putExtra(ConstIntent.KEY_HOUSING_RULES, data.policy!!)

        intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_DEPOSIT_PERCENTAGE, data.roomPricing!!.depositPercent)
        intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_PERCENTAGE, data.roomPricing!!.taxPercentage)
        intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_NAME, data.roomPricing!!.taxName)

        intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_MINIMUM_DAY, data.roomPricing!!.minimumDay)
        intent.putExtra(ConstIntent.KEY_ROOM_LONG_BOOKING_NIGHTLY_PRICE, data.roomPricing!!.longTermNightlyPrice)
        intent.putExtra(ConstIntent.KEY_MAXIMUM_GUEST_ALLOWED, data.maxOccupants!!)
        intent.putExtra(ConstIntent.KEY_CHECK_IN_DISABLE_DATES, data.checkInDisableDates!!)
        intent.putExtra(ConstIntent.KEY_CHECK_OUT_DISABLE_DATES, data.checkOutDisableDates!!)
        startActivity(intent)
    }

    internal val onRetryRoomDetail: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getRoomDetailData()
        }

    }
}

internal class SortbyFeaturedBit : Comparator<RoomDetailModel.Data.RoomPhoto> {
    // Used for sorting in ascending order of
    // featured bit
    override fun compare(a: RoomDetailModel.Data.RoomPhoto, b: RoomDetailModel.Data.RoomPhoto): Int {
        return a.featured!! - b.featured!!
    }
}
