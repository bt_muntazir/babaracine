package com.babaracine.mvp.room_detail.presenter

import android.app.Activity
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import okhttp3.Callback
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class RoomDetailPresenterImpl(val activity : Activity,val roomDetailView : RoomDetailContractor.RoomDetailView) : RoomDetailContractor.RoomDetailPresenter {
    override fun getRoomDetailData(roomId: String) {
        try {
            ApiAdapter.getInstance(activity)
            roomDetailApi(roomId)
        }catch (exp : ApiAdapter.Companion.NoInternetException){
            roomDetailView.onRoomDetailInternetError()
        }
    }

    private fun roomDetailApi(roomId: String) {

        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_ROOM_ID,roomId)
        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject!!.toString())

        val getRoomDetailResult = ApiAdapter.getApiService()!!.getRoomDetail("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomDetailResult.enqueue(object : retrofit2.Callback<RoomDetailModel>{

            override fun onResponse(call: Call<RoomDetailModel>?, response: Response<RoomDetailModel>?) {

                Progress.stop()
                val roomDetailModel = response!!.body()
                val message = roomDetailModel.message!!
                try {
                    if(roomDetailModel.status == 1){
                        roomDetailView.onRoomDetailSuccess(roomDetailModel)
                    }else if(roomDetailModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        roomDetailView.onRoomDetailUnSuccess(message)
                    }
                }catch (exp : NullPointerException){

                    exp!!.printStackTrace()
                    roomDetailView.onRoomDetailUnSuccess(activity.getString(R.string.error_server))
                }
            }

            override fun onFailure(call: Call<RoomDetailModel>?, t: Throwable?) {
                Progress.stop()
                t!!.printStackTrace()
                roomDetailView.onRoomDetailUnSuccess(activity.getString(R.string.error_server))
            }
        })

    }
}