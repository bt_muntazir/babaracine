package com.babaracine.mvp.room_detail.presenter

import android.util.Log
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*


class RoomDetailModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    inner class Data {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("name")
        @Expose
        var name: String? = null
        @SerializedName("description")
        @Expose
        var description: String? = null
        @SerializedName("bedrooms")
        @Expose
        var bedrooms: String? = null
        @SerializedName("beds")
        @Expose
        var beds: String? = null
        @SerializedName("bathrooms")
        @Expose
        var bathrooms: String? = null
        @SerializedName("max_occupants")
        @Expose
        var maxOccupants: Int? = null
        @SerializedName("property_type_id")
        @Expose
        var propertyTypeId: Int? = null
        @SerializedName("suitable_for")
        @Expose
        var suitableFor: String? = null
        @SerializedName("room_type_id")
        @Expose
        var roomTypeId: Int? = null
        @SerializedName("bed_type_id")
        @Expose
        var bedTypeId: Int? = null
        @SerializedName("country")
        @Expose
        var country: String? = null
        @SerializedName("city")
        @Expose
        var city: String? = null
        @SerializedName("state")
        @Expose
        var state: String? = null
        @SerializedName("postal")
        @Expose
        var postal: String? = null
        @SerializedName("address_1")
        @Expose
        var address1: String? = null
        @SerializedName("address_2")
        @Expose
        var address2: String? = null
        @SerializedName("formated_address")
        @Expose
        var formatedAddress: String? = null
        @SerializedName("latitude")
        @Expose
        var latitude: String? = null
        @SerializedName("longitude")
        @Expose
        var longitude: String? = null
        @SerializedName("policy")
        @Expose
        var policy: String? = null
        @SerializedName("is_premium")
        @Expose
        var isPremium: String? = null
        @SerializedName("is_listed")
        @Expose
        var isListed: String? = null
        @SerializedName("currency")
        @Expose
        var currency: String? = null
        @SerializedName("status")
        @Expose
        var status: String? = null
        @SerializedName("steps_completed")
        @Expose
        var stepsCompleted: Int? = null
        @SerializedName("total_view")
        @Expose
        var totalView: Int? = null
        @SerializedName("deleted_at")
        @Expose
        var deletedAt: Any? = null
        @SerializedName("created")
        @Expose
        var created: String? = null
        @SerializedName("modified")
        @Expose
        var modified: String? = null
        @SerializedName("user")
        @Expose
        var user: User? = null
        @SerializedName("bed_type")
        @Expose
        var bedType: BedType? = null
        @SerializedName("room_type")
        @Expose
        var roomType: RoomType? = null
        @SerializedName("property_type")
        @Expose
        var propertyType: PropertyType? = null
        @SerializedName("room_pricing")
        @Expose
        var roomPricing: RoomPricing? = null
        @SerializedName("room_amenities")
        @Expose
        var roomAmenities: ArrayList<RoomAmenity>? = null
        @SerializedName("room_photos")
        @Expose
        var roomPhotos: ArrayList<RoomPhoto>? = null
        @SerializedName("_locale")
        @Expose
        var locale: String? = null
        @SerializedName("average_rating")
        @Expose
        var averageRating: Double? = null
        @SerializedName("check_in_disable_dates")
        @Expose
        var checkInDisableDates: ArrayList<String>? = null
        @SerializedName("check_out_disable_dates")
        @Expose
        var checkOutDisableDates: ArrayList<String>? = null

        inner class RoomPhoto : Comparable<RoomPhoto> {
            override fun compareTo(other: RoomPhoto): Int {
                return 0
            }

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("room_id")
            @Expose
            var roomId: Int? = null
            @SerializedName("name")
            @Expose
            var name: String? = null
            @SerializedName("dir")
            @Expose
            var dir: String? = null
            @SerializedName("description")
            @Expose
            var description: String? = null
            @SerializedName("featured")
            @Expose
            var featured: Int? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("medium_thumb_image_url")
            @Expose
            var mediumThumbImageUrl: String? = null
            @SerializedName("thumb_image_url")
            @Expose
            var thumbImageUrl: String? = null
            @SerializedName("image_url")
            @Expose
            var imageUrl: String? = null

        }


        inner class RoomPricing {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("currency_code")
            @Expose
            var currencyCode: String? = null
            @SerializedName("room_id")
            @Expose
            var roomId: Int? = null
            @SerializedName("nightly_price")
            @Expose
            var nightlyPrice: Int? = null
            @SerializedName("deposit_frequency")
            @Expose
            var depositFrequency: String? = null
            @SerializedName("deposit_percent")
            @Expose
            var depositPercent: Int? = null
            @SerializedName("deposit_enable")
            @Expose
            var depositEnable: String? = null
            @SerializedName("cancellation_type")
            @Expose
            var cancellationType: String? = null
            @SerializedName("cancellation_charge")
            @Expose
            var cancellationCharge: String? = null
            @SerializedName("cancellation_desc")
            @Expose
            var cancellationDesc: String? = null
            @SerializedName("tax_name")
            @Expose
            var taxName: String? = null
            @SerializedName("tax_percentage")
            @Expose
            var taxPercentage: Int? = null
            @SerializedName("minimum_day")
            @Expose
            var minimumDay: Int? = null
            @SerializedName("long_term_nightly_price")
            @Expose
            var longTermNightlyPrice: Int? = null
            @SerializedName("deleted_at")
            @Expose
            var deletedAt: Any? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null

        }


        inner class RoomType {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("name")
            @Expose
            var name: String? = null
            @SerializedName("status")
            @Expose
            var status: String? = null
            @SerializedName("deleted_at")
            @Expose
            var deletedAt: Any? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("_locale")
            @Expose
            var locale: String? = null

        }

        inner class User {

            @SerializedName("first_name")
            @Expose
            var firstName: String? = null
            @SerializedName("last_name")
            @Expose
            var lastName: String? = null
            @SerializedName("profile_picture")
            @Expose
            var profilePicture: String? = null
            @SerializedName("full_name")
            @Expose
            var fullName: String? = null
            @SerializedName("profile_picture_path")
            @Expose
            var profilePicturePath: String? = null
            @SerializedName("full_image_url")
            @Expose
            var fullImageUrl: String? = null
            @SerializedName("id_proof_path")
            @Expose
            var idProofPath: Boolean? = null

        }

        inner class PropertyType {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("name")
            @Expose
            var name: String? = null
            @SerializedName("status")
            @Expose
            var status: String? = null
            @SerializedName("deleted_at")
            @Expose
            var deletedAt: Any? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("_locale")
            @Expose
            var locale: String? = null

        }


        inner class RoomAmenity {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("room_id")
            @Expose
            var roomId: Int? = null
            @SerializedName("amenity_id")
            @Expose
            var amenityId: Int? = null
            @SerializedName("deleted_at")
            @Expose
            var deletedAt: Any? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("amenity")
            @Expose
            var amenity: Amenity? = null

            inner class Amenity {

                @SerializedName("id")
                @Expose
                var id: Int? = null
                @SerializedName("name")
                @Expose
                var name: String? = null
                @SerializedName("image")
                @Expose
                var image: String? = null
                @SerializedName("status")
                @Expose
                var status: String? = null
                @SerializedName("deleted_at")
                @Expose
                var deletedAt: Any? = null
                @SerializedName("created")
                @Expose
                var created: String? = null
                @SerializedName("modified")
                @Expose
                var modified: String? = null
                @SerializedName("_locale")
                @Expose
                var locale: String? = null
                @SerializedName("image_full_url")
                @Expose
                var imageFullUrl: String? = null

            }

        }

        inner class BedType {

            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("name")
            @Expose
            var name: String? = null
            @SerializedName("status")
            @Expose
            var status: String? = null
            @SerializedName("deleted_at")
            @Expose
            var deletedAt: Any? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("_locale")
            @Expose
            var locale: String? = null

        }

    }


}











