package com.babaracine.mvp.room_detail.adapter

import android.content.Context
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.babaracine.R
import com.babaracine.mvp.room_detail.presenter.RoomDetailModel
import com.squareup.picasso.Picasso

import java.util.ArrayList

class SlidingImageAdapter(private val context: Context, private val arrayListRoomPhoto: ArrayList<RoomDetailModel.Data.RoomPhoto>) : PagerAdapter() {

    private val inflater: LayoutInflater

    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return arrayListRoomPhoto.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.sliding_images_room_details, view, false)!!

        val imageRoomDetail = imageLayout.findViewById<View>(R.id.imageRoomDetail) as ImageView

        val imagePath = arrayListRoomPhoto[position].imageUrl
        Picasso.with(context).load(imagePath).into(imageRoomDetail)
        //imageView.setImageResource(IMAGES[position])

        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

}
