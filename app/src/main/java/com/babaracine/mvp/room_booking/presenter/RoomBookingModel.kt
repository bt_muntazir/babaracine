package com.babaracine.mvp.room_booking.presenter


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList

class RoomBookingModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null
    @SerializedName("invoice_data")
    @Expose
    var invoiceData: InvoiceData? = null


    inner class Data {

        @SerializedName("booking_details")
        @Expose
        var bookingDetails: BookingDetails? = null

        inner class BookingDetails {

            @SerializedName("booking_invoice")
            @Expose
            var bookingInvoice: BookingInvoice? = null
            @SerializedName("associated")
            @Expose
            var associated: ArrayList<String>? = null
            @SerializedName("booking_session_id")
            @Expose
            var bookingSessionId: String? = null
            @SerializedName("first_name")
            @Expose
            var firstName: String? = null
            @SerializedName("last_name")
            @Expose
            var lastName: String? = null
            @SerializedName("email_address")
            @Expose
            var emailAddress: String? = null
            @SerializedName("address")
            @Expose
            var address: String? = null
            @SerializedName("country")
            @Expose
            var country: String? = null
            @SerializedName("state")
            @Expose
            var state: String? = null
            @SerializedName("city")
            @Expose
            var city: String? = null
            @SerializedName("zip_code")
            @Expose
            var zipCode: String? = null
            @SerializedName("phone")
            @Expose
            var phone: String? = null
            @SerializedName("room_id")
            @Expose
            var roomId: Int? = null
            @SerializedName("number_of_adults")
            @Expose
            var numberOfAdults: Int? = null
            @SerializedName("number_of_children")
            @Expose
            var numberOfChildren: Int? = null
            @SerializedName("start_date")
            @Expose
            var startDate: String? = null
            @SerializedName("end_date")
            @Expose
            var endDate: String? = null
            @SerializedName("user_id")
            @Expose
            var userId: Int? = null
            @SerializedName("booking_traveller")
            @Expose
            var bookingTraveller: BookingTraveller? = null
            @SerializedName("owner_id")
            @Expose
            var ownerId: Int? = null
            @SerializedName("created")
            @Expose
            var created: String? = null
            @SerializedName("modified")
            @Expose
            var modified: String? = null
            @SerializedName("id")
            @Expose
            var id: Int? = null

            inner class BookingInvoice {

                @SerializedName("deposit_percent")
                @Expose
                var depositPercent: Int? = null
                @SerializedName("deposit_amount")
                @Expose
                var depositAmount: Double? = null
                @SerializedName("grand_total")
                @Expose
                var grandTotal: Int? = null
                @SerializedName("remaining_amount")
                @Expose
                var remainingAmount: Double? = null
                @SerializedName("tax_amount")
                @Expose
                var taxAmount: Int? = null
                @SerializedName("tax_name")
                @Expose
                var taxName: String? = null
                @SerializedName("tax_percent")
                @Expose
                var taxPercent: Int? = null
                @SerializedName("booking_id")
                @Expose
                var bookingId: Int? = null
                @SerializedName("created")
                @Expose
                var created: String? = null
                @SerializedName("modified")
                @Expose
                var modified: String? = null
                @SerializedName("id")
                @Expose
                var id: Int? = null

            }


            inner class BookingTraveller {

                @SerializedName("user_id")
                @Expose
                var userId: Int? = null
                @SerializedName("first_name")
                @Expose
                var firstName: String? = null
                @SerializedName("last_name")
                @Expose
                var lastName: String? = null
                @SerializedName("email_address")
                @Expose
                var emailAddress: String? = null
                @SerializedName("address")
                @Expose
                var address: String? = null
                @SerializedName("city")
                @Expose
                var city: String? = null
                @SerializedName("state")
                @Expose
                var state: String? = null
                @SerializedName("zip_code")
                @Expose
                var zipCode: String? = null
                @SerializedName("country")
                @Expose
                var country: String? = null
                @SerializedName("phone")
                @Expose
                var phone: String? = null
                @SerializedName("booking_id")
                @Expose
                var bookingId: Int? = null
                @SerializedName("created")
                @Expose
                var created: String? = null
                @SerializedName("modified")
                @Expose
                var modified: String? = null
                @SerializedName("id")
                @Expose
                var id: Int? = null
                @SerializedName("full_name")
                @Expose
                var fullName: String? = null

            }

        }

    }


    inner class InvoiceData {

        @SerializedName("deposit_percent")
        @Expose
        var depositPercent: Int? = null
        @SerializedName("deposit_amount")
        @Expose
        var depositAmount: Double? = null
        @SerializedName("grand_total")
        @Expose
        var grandTotal: Int? = null
        @SerializedName("remaining_amount")
        @Expose
        var remainingAmount: Double? = null
        @SerializedName("tax_amount")
        @Expose
        var taxAmount: Int? = null
        @SerializedName("tax_name")
        @Expose
        var taxName: String? = null
        @SerializedName("tax_percent")
        @Expose
        var taxPercent: Int? = null

    }


}








