package com.babaracine.mvp.room_booking

import android.app.Dialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import com.babaracine.R
import com.babaracine.common.PayPalConfig
import com.babaracine.common.adapter.AdapterSpinner

import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.review_steps.presenter.presenter_country.GetCountryModel
import com.babaracine.mvp.review_steps.presenter.presenter_country.CountryContractor
import com.babaracine.mvp.review_steps.presenter.presenter_country.CountryPresenterImpl
import com.paypal.android.sdk.payments.PayPalConfiguration
import java.util.HashMap
import com.paypal.android.sdk.payments.PayPalService
import android.content.Intent
import com.paypal.android.sdk.payments.PayPalPayment
import java.math.BigDecimal
import com.paypal.android.sdk.i
import android.app.Activity
import android.util.Log
import com.babaracine.BuildConfig
import com.babaracine.R.id.*
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.*
import com.babaracine.mvp.Payment.PaymentActivity
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.review_steps.CongratulationActivity
import com.babaracine.mvp.room_booking.paypal_details.PaypalPaymentDetailModel
import com.babaracine.mvp.room_booking.paypal_details.PaypalTokenResponseModel
import com.babaracine.mvp.room_booking.presenter.*
import com.babaracine.mvp.submit_id_proof.SubmitIdProofActivity

import kotlinx.android.synthetic.main.activity_room_booking.*
import kotlinx.android.synthetic.main.toolbar_with_back_and_title.*

import com.paypal.android.sdk.e
import org.json.JSONException

import com.paypal.android.sdk.payments.PaymentConfirmation
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.custom_dialog_price_details.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RoomBookingActivity : BaseActivity(), CountryContractor.CountryView, RoomBookingContractor.RoomBookingView, UserIdStatusContractor.View {


    val PAYPAL_REQUEST_CODE = 123

    lateinit var globalPaymentId: String
    lateinit var payerId: String
    lateinit var bookingId: String

    var roomBookingLongTermPrice: Int = 0
    var roomBookingMinimumDay: Int = 0
    lateinit var roomSessionId: String
    lateinit var roomDetails: String
    lateinit var housingPolicy: String
    lateinit var startDate: String
    lateinit var endDate: String
    lateinit var numberOfDays: String


    lateinit var taxName: String
    var taxPercentage: Int = 0
    var depositPercentage: Int = 0

    var roomPerNightPrice: Int = 0
    lateinit var cancelationType: String
    lateinit var cancellationCharge: String
    lateinit var currencyCode: String
    lateinit var roomId: String

    var selectedCountryId = "0"
    lateinit var selectedCountryName: String
    lateinit var arrayListCountry: ArrayList<HashMap<String, String>>

    lateinit var countryPresenterImpl: CountryPresenterImpl


    lateinit var arrayListCountryList: ArrayList<GetCountryModel.Datum>

    lateinit var roomBookingPresenterImpl: RoomBookingPresenterImpl

    val config = PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID)

    lateinit var paymentAmount: String

    var documentUploadStatus = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
/*

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

*/

        setContentView(R.layout.activity_room_booking)
        countryPresenterImpl = CountryPresenterImpl(this, this)
        roomBookingPresenterImpl = RoomBookingPresenterImpl(this, this)
        getAllCountry()
        getIntentData()
        manageClickEvents()
        setCountryAdapter()


        val intent = Intent(this, PayPalService::class.java)

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)

        startService(intent)

    }

    override fun onRoomBookingSuccess(roomBookingModel: RoomBookingModel) {

        paymentAmount = roomBookingModel.invoiceData!!.depositAmount!!.toString()

        bookingId = roomBookingModel.data!!.bookingDetails!!.bookingTraveller!!.bookingId!!.toString()


        val intent = Intent(this, PaymentActivity::class.java)

        intent.putExtra(ConstIntent.KEY_ROOM_SESSION_ID, roomSessionId)
        intent.putExtra(ConstIntent.KEY_ROOM_BOOKING_ID, bookingId)



        startActivity(intent)
    }

    override fun onRoomBookingUnSuccess(message: String) {
        SnackNotify.showMessage(message, this, constraintLayoutTravellerDetails)
    }

    override fun onRoomBookingInternetError() {
        SnackNotify.checkConnection(onRetryRoomBooking, constraintLayoutTravellerDetails)
    }

    override fun onRoomBookingConfirmAfterPaymentSuccess(roomBookingConfirmAfterPaymentModel: RoomBookingConfirmAfterPaymentModel) {
        //Not Used
    }

    override fun onRoomBookingConfirmAfterPaymentUnSuccess(message: String) {
        //Not Used
    }

    override fun onRoomBookingConfirmAfterPaymentInternetError() {
        //Not Used
    }


    public override fun onDestroy() {
        stopService(Intent(this, PayPalService::class.java))
        super.onDestroy()
    }

    public fun getUserIdStatus() {
        var presenter = UserIdStatusPresenterImpl(this, this)
        presenter.getIdUploadStatus()
    }

    /* private fun getPayment() {
         //Getting the amount from editText


         //Creating a paypalpayment
         val payment = PayPalPayment(BigDecimal(paymentAmount), "USD", "Room Booking Amount",
                 PayPalPayment.PAYMENT_INTENT_SALE)

         //Creating Paypal Payment activity intent
         val intent = Intent(this, PaymentActivity::class.java)

         //putting the paypal configuration to the intent
         intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)

         //Puting paypal payment to the intent
         intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment)

         //Starting the intent activity for result
         //the request code will be used on the method onActivityResult
         startActivityForResult(intent, PAYPAL_REQUEST_CODE)
     }*/


    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                *//*val confirm = data.getParcelableExtra<PaymentConfirmation>(PaymentActivity.EXTRA_RESULT_CONFIRMATION)

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        val paymentDetails = confirm.toJSONObject().toString(4)
                        Log.e("paymentExample", paymentDetails)

                        //Starting a new activity for the payment details and also putting the payment details with intent
                        //startActivity(Intent(this, ConfirmationActivity::class.java)
                          //      .putExtra("PaymentDetails", paymentDetails)
                            //    .putExtra("PaymentAmount", paymentAmount))
*//*

                val confirm = data
                        .getParcelableExtra<PaymentConfirmation>(PaymentActivity.EXTRA_RESULT_CONFIRMATION)

                if (confirm != null) {
                    try {
                        //println("Response$confirm")

                        Log.e("Paypal Response", "-------->$confirm")

                        //String payment_client = confirm.getPayment().toJSONObject().toString();
                        //  String payment_clienty = confirm.getProofOfPayment().toJSONObject().toString();
                        //String transId = confirm.getProofOfPayment().toJSONObject().toString();

                        // Log.e("payment_client", "----->" + payment_client);
                        // Log.e("payment_clienty", "-------->" + payment_clienty);
                        // Log.e("transId", "-------->" + transId);


                        val jsonObj = JSONObject(confirm.toJSONObject().toString())

                        val response = jsonObj.getJSONObject("response").toString()
                        var paymentId = jsonObj.getJSONObject("response").getString("id")
                        println("payment id:-==$paymentId")
                        Log.e("paymentId", paymentId)
                        Log.e("response", "-------->$response")
                        // Toast.makeText(getActivity(), paymentId, Toast.LENGTH_LONG).show();
                        paymentId = paymentId

                        // call token api
                        getPaypalToken(paymentId)

                    } catch (e: JSONException) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e)
                    }

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e("paymentExample", "The user canceled.")
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.e("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.")
            }
        }
    }*/

    private fun getPaypalToken(paymentId: String) {

        var apiInterface: ApiService? = null
        try {
            apiInterface = ApiAdapterForPaypal.getInstance(this, true, "")
        } catch (e: ApiAdapterForPaypal.NoInternetException) {
            e.printStackTrace()
        }

        Progress.start(this)

        apiInterface!!.getAuthentionToken("client_credentials").enqueue(object : Callback<PaypalTokenResponseModel> {
            override fun onResponse(call: Call<PaypalTokenResponseModel>, response: Response<PaypalTokenResponseModel>) {
                Log.e("response", response.body().toString())

                Progress.stop()
                try {

                    val paymentSuccessModel = response.body()

                    if (paymentSuccessModel != null) {

                        Log.e("token ", "-------->" + paymentSuccessModel.accessToken!!)

                        // call payment detail api
                        getPaymentDetail(paymentSuccessModel.accessToken!!, paymentId)

                    } else {

                        //SnackNotify.showMessage("Paypal token error.", frameLayout)
                    }
                } catch (exp: NullPointerException) {
                    exp.printStackTrace()
                }

            }

            override fun onFailure(call: Call<PaypalTokenResponseModel>, t: Throwable) {
                Progress.stop()
                Log.e("response", t.message)

                //Remove it after testing
                if (BuildConfig.DEBUG) {
                    openIdProofActivity()
                }
                //SnackNotify.showMessage("Paypal server not responding.", frameLayout)
            }
        })
    }

    private fun getPaymentDetail(accessToken: String, paymentId: String) {

        var apiInterface: ApiService? = null
        try {
            apiInterface = ApiAdapterForPaymentTransaction.getInstance(this, false, accessToken)
        } catch (e: ApiAdapterForPaymentTransaction.NoInternetException) {
            e.printStackTrace()
        }

        Progress.start(this)

        val getUpcomingAppointmentResult = apiInterface!!.getPaypalPaymentDetail(paymentId)

        getUpcomingAppointmentResult.enqueue(object : Callback<PaypalPaymentDetailModel> {
            override fun onResponse(call: Call<PaypalPaymentDetailModel>, response: Response<PaypalPaymentDetailModel>) {
                Progress.stop()

                Log.e("response", response.body().toString())

                try {
                    val paypalPaymentDetailModel = response.body()

                    if (paypalPaymentDetailModel != null) {

                        for (i in 0 until paypalPaymentDetailModel.payments!!.size) {

                            if (paypalPaymentDetailModel.payments!!.get(i).id.equals(paymentId)) {

                                globalPaymentId = paypalPaymentDetailModel.payments!!.get(i).id!!
                                payerId = paypalPaymentDetailModel.payments!![i].payer!!.payerInfo!!.payerId!!

                                Log.e("payment id", paymentId)
                                // save transaction id to server
                                Log.e("paymentId matched", "------->$paymentId")
                                break
                            }
                        }
                    }

                } catch (ex: NullPointerException) {
                    ex.printStackTrace()
                    //SnackNotify.showMessage(getActivity()!!.getString(R.string.server_error), frameLayout)
                }

            }

            override fun onFailure(call: Call<PaypalPaymentDetailModel>, t: Throwable) {
                Progress.stop()
                //SnackNotify.showMessage(getActivity()!!.getString(R.string.server_error), frameLayout)
            }
        })
    }

    private fun getIntentData() {

        if (intent.hasExtra(ConstIntent.KEY_ROOM_SESSION_ID)) {
            roomSessionId = intent.getStringExtra(ConstIntent.KEY_ROOM_SESSION_ID)
            roomDetails = intent.getStringExtra(ConstIntent.KEY_ROOM_DETAILS)
            housingPolicy = intent.getStringExtra(ConstIntent.KEY_HOUSING_RULES)
            startDate = intent.getStringExtra(ConstIntent.KEY_CHECK_IN_DATE)
            endDate = intent.getStringExtra(ConstIntent.KEY_CHECK_OUT_DATE)
            roomId = intent.getStringExtra(ConstIntent.KEY_ROOM_ID)
            cancelationType = intent.getStringExtra(ConstIntent.KEY_CANCELATION_TYPE)
            cancellationCharge = intent.getStringExtra(ConstIntent.KEY_CANCELATION_CHARGE)
            roomPerNightPrice = intent.getIntExtra(ConstIntent.KEY_PRICE, 0)
            currencyCode = intent.getStringExtra(ConstIntent.KEY_CURRENCY_CODE)

            taxName = intent.getStringExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_NAME)
            numberOfDays = intent.getStringExtra(ConstIntent.KEY_NUMBER_OF_DAYS)
            taxPercentage = intent.getIntExtra(ConstIntent.KEY_ROOM_BOOKING_TAX_PERCENTAGE, 0)
            depositPercentage = intent.getIntExtra(ConstIntent.KEY_ROOM_BOOKING_DEPOSIT_PERCENTAGE, 0)


            roomBookingLongTermPrice = intent.getIntExtra(ConstIntent.KEY_ROOM_LONG_BOOKING_NIGHTLY_PRICE, 0)
            roomBookingMinimumDay = intent.getIntExtra(ConstIntent.KEY_ROOM_BOOKING_MINIMUM_DAY, 0)

            setData()
        }
    }

    private fun setData() {
        val completePrice: String
        if (numberOfDays.toInt() >= roomBookingMinimumDay) {
            completePrice = Utils.getCurrenctSymbol(currencyCode, this) + " " + (roomBookingLongTermPrice * numberOfDays.toInt()) + " for " + numberOfDays + " night."
        } else {
            completePrice = Utils.getCurrenctSymbol(currencyCode, this) + " " + (roomPerNightPrice * numberOfDays.toInt()) + " for " + numberOfDays + " night."
        }
        txtViewPrice.text = completePrice
    }

    private fun getAllCountry() {
        countryPresenterImpl.getCountryList()
    }

    override fun onGetCountrySuccess(countryModel: GetCountryModel) {
        this.arrayListCountryList = countryModel.data!!

        bindCountryInDropdown()

        getUserIdStatus()
    }

    private fun bindCountryInDropdown() {

        for (i in 0 until arrayListCountryList.size) {

            val name = arrayListCountryList.get(i).countryName!!
            val id = arrayListCountryList.get(i).countryId!!.toString()
            val hashMap: HashMap<String, String> = HashMap()
            hashMap.put(Const.KEY_ID, id)
            hashMap.put(Const.KEY_NAME, name)
            arrayListCountry.add(hashMap)
        }

        val adapterMonth = AdapterSpinner(this, R.layout.spinner_item, arrayListCountry, "black")
        spinnerCountry.setAdapter(adapterMonth)
    }

    override fun onGetCountryUnsuccess(message: String) {
        SnackNotify.showMessage(message, this, constraintLayoutTravellerDetails)
    }

    override fun onGetCountryInternetError() {
        SnackNotify.checkConnection(onRetryCountry, constraintLayoutTravellerDetails)
    }


    private fun setCountryAdapter() {

        arrayListCountry = ArrayList()
        val hashMapMonth = java.util.HashMap<String, String>()
        hashMapMonth.put(Const.KEY_ID, "0")
        hashMapMonth.put(Const.KEY_NAME, getString(R.string.hint_country))
        arrayListCountry.add(hashMapMonth)

        val adapterMonth = AdapterSpinner(this, R.layout.spinner_item, arrayListCountry, "black")
        spinnerCountry.setAdapter(adapterMonth)

    }

    private fun manageClickEvents() {

        imgViewToolBack.setOnClickListener { finish() }

        btnBook.setOnClickListener(View.OnClickListener { onClickBtnBook() })

        spinnerCountry?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                selectedCountryId = arrayListCountry.get(position).get(Const.KEY_ID).toString()
                selectedCountryName = arrayListCountry.get(position).get(Const.KEY_NAME).toString()
            }
        }

        txtViewSeePricingDetails.setOnClickListener(View.OnClickListener { showAlertForSeePricingDetails() })

    }

    private fun onClickBtnBook() {

        //getPayment()

        val firstName = edtTextFirstName.text.toString().trim()
        val lastName = edtTextLastName.text.toString().trim()
        val email = edtTextEmail.text.toString().trim()
        val phoneNumber = edtTextPhone.text.toString().trim()
        val address = edtTextAddress.text.toString().trim()
        val state = edtTextState.text.toString().trim()
        val city = edtTextCity.text.toString().trim()
        val zipCode = edtTextZipCode.text.toString().trim()
        val bookingNotes = edtTextBookingNotes.text.toString().trim()

        if (isValidate(firstName, lastName, email, phoneNumber, address, state, city, zipCode, bookingNotes)) {


            roomBookingPresenterImpl.getRoomBookingData(roomSessionId, firstName, lastName, email, address, state, city, zipCode, phoneNumber, selectedCountryName, bookingNotes)

        }

        /* val intent = Intent(this,CongratulationActivity::class.java)
         startActivity(intent)*/

    }

    private fun isValidate(firstName: String, lastName: String, email: String, phoneNumber: String, address: String, state: String, city: String, zipCode: String, bookingNotes: String): Boolean {

        if (Utils.isEmptyOrNull(firstName)) {
            edtTextFirstName.setError(getString(R.string.error_first_name))
            edtTextFirstName.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(lastName)) {
            edtTextLastName.setError(getString(R.string.error_last_name))
            edtTextLastName.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(email)) {
            edtTextEmail.setError(getString(R.string.error_email))
            edtTextEmail.requestFocus()
            return false
        } else if (!Utils.isValidEmail(email)) {
            edtTextEmail.setError(getString(R.string.error_valid_email))
            edtTextEmail.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(phoneNumber)) {
            edtTextPhone.setError(getString(R.string.error_mobile_number))
            edtTextPhone.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(address)) {
            edtTextAddress.setError(getString(R.string.error_address))
            edtTextAddress.requestFocus()
            return false
        } else if (selectedCountryId.equals("0")) {
            SnackNotify.showMessage(getString(R.string.error_country), this, constraintLayoutTravellerDetails)
            return false
        } else if (Utils.isEmptyOrNull(state)) {
            edtTextState.setError(getString(R.string.error_state))
            edtTextState.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(city)) {
            edtTextCity.setError(getString(R.string.error_city))
            edtTextCity.requestFocus()
            return false
        } else if (Utils.isEmptyOrNull(zipCode)) {
            edtTextZipCode.setError(getString(R.string.error_zip_code))
            edtTextZipCode.requestFocus()
            return false
        }


        return true
    }

    fun openIdProofActivity() {
        if (!documentUploadStatus) {
            val intent = Intent(this@RoomBookingActivity, SubmitIdProofActivity::class.java)
            startActivity(intent)
        }
    }

    fun showAlertForSeePricingDetails() {

        try {

            val customDialog = Dialog(this)

            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            customDialog.setContentView(R.layout.custom_dialog_price_details)
            customDialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            //customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            customDialog.setCancelable(true)

            val price: String
            val totalPrice: Int
            if (numberOfDays.toInt() >= roomBookingMinimumDay) {
                price = Utils.getCurrenctSymbol(currencyCode, this) + " " + roomBookingLongTermPrice.toString()
                totalPrice = numberOfDays.toInt() * roomBookingLongTermPrice
            } else {
                price = Utils.getCurrenctSymbol(currencyCode, this) + " " + roomPerNightPrice.toString()
                totalPrice = numberOfDays.toInt() * roomPerNightPrice
            }

            customDialog.txtViewPricePerNight.text = price
            customDialog.txtViewNoOfNights.text = numberOfDays

            customDialog.txtViewTotal.text = Utils.getCurrenctSymbol(currencyCode, this) + " " + totalPrice.toString()
            val gstPercentage = getString(R.string.gst) + " (" + taxPercentage + "%)"
            customDialog.txtViewTaxTitle.text = gstPercentage

            val calculateTaxAmount = ((totalPrice * taxPercentage) / 100)
            customDialog.txtViewTax.text = Utils.getCurrenctSymbol(currencyCode, this) + " " + calculateTaxAmount

            val depositPercentageTitle = getString(R.string.deposit) + " (" + depositPercentage + "%)"

            customDialog.txtViewDepositTitle.text = depositPercentageTitle
            val calculateDepositAmount = ((totalPrice * depositPercentage) / 100)
            customDialog.txtViewDeposit.text = Utils.getCurrenctSymbol(currencyCode, this) + " " + calculateDepositAmount

            val calculateRemainingAmount = (totalPrice + calculateTaxAmount - calculateDepositAmount)
            customDialog.txtViewRemainingAmount.text = Utils.getCurrenctSymbol(currencyCode, this) + " " + calculateRemainingAmount

            val cancellationChargeAmount: String
            if (cancelationType.equals("free", true)) {
                cancellationChargeAmount = getString(R.string.free_cancellation)
            } else {
                cancellationChargeAmount = getString(R.string.cancellation_charge) + " (" + cancellationCharge + "%)"
            }

            customDialog.txtViewCancellationCharge.text = cancellationChargeAmount

            customDialog.imgViewClose.setOnClickListener {
                customDialog.dismiss()
            }


            customDialog.create()
            customDialog.show()
        } catch (e: RuntimeException) {
            e.printStackTrace()
        }

    }


    private fun getRoomBookingStatus() {
    }

    internal val onRetryCountry: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getAllCountry()
        }
    }

    //Id status
    override fun idAllReadyAvailable() {
        documentUploadStatus = true
    }

    override fun idNotAvailable() {
        documentUploadStatus = false
    }

    override fun internetError() {
        AlertDialogHelper.alertInternetError(this, onRetry)
    }

    internal val onRetry: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getUserIdStatus()
        }

    }

    internal val onRetryRoomBooking: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            getRoomBookingStatus()
        }

    }
}
