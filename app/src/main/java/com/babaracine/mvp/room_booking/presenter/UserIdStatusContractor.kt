package com.babaracine.mvp.room_booking.presenter

interface UserIdStatusContractor {

    interface Presenter {
        fun getIdUploadStatus()
    }

    interface View {
        fun idAllReadyAvailable()
        fun idNotAvailable()
        fun internetError()
    }
}