package com.babaracine.mvp.room_booking.presenter

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.mvp.room_booking.RoomBookingActivity
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response

class RoomBookingPresenterImpl(val activity: Activity, val roomBookingView : RoomBookingContractor.RoomBookingView) : RoomBookingContractor.RoomBookingPresenter {
    override fun afterPaymentRoomConfirm(bookingSessionId: String, tokenId: String, bookingId: String) {
        try {
            ApiAdapter.getInstance(activity)
            afterPaymentRoomBookingApi(bookingSessionId,tokenId,bookingId)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            roomBookingView.onRoomBookingConfirmAfterPaymentInternetError()
        }
    }

    private fun afterPaymentRoomBookingApi(bookingSessionId: String, tokenId: String, bookingId: String) {
        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_BOOKING_SESSION_ID,bookingSessionId)
            jsonObject.put(Const.PARAM_STRIPE_TOKEN,tokenId)
            //jsonObject.put(Const.PARAM_STRIPE_TOKEN,"tok_ca")
            jsonObject.put(Const.PARAM_BOOKING_ID,bookingId)

        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject.toString())

        val getRoomConfirmationAferPayment = ApiAdapter.getApiService()!!.stripePayment("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomConfirmationAferPayment.enqueue(object : retrofit2.Callback<RoomBookingConfirmAfterPaymentModel>{


            override fun onResponse(call: retrofit2.Call<RoomBookingConfirmAfterPaymentModel>?, response: Response<RoomBookingConfirmAfterPaymentModel>?) {

                Progress.stop()
                val roomBookingConfirmAfterPaymentModel = response!!.body()


                val message = roomBookingConfirmAfterPaymentModel.message!!
                try {
                    if(roomBookingConfirmAfterPaymentModel.status == 1){
                        roomBookingView.onRoomBookingConfirmAfterPaymentSuccess(roomBookingConfirmAfterPaymentModel)
                    }else if(roomBookingConfirmAfterPaymentModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        roomBookingView.onRoomBookingConfirmAfterPaymentUnSuccess(message)
                    }
                }catch (ex : NullPointerException){
                    roomBookingView.onRoomBookingConfirmAfterPaymentUnSuccess(activity.getString(R.string.error_server))
                }

            }


            override fun onFailure(call: retrofit2.Call<RoomBookingConfirmAfterPaymentModel>?, t: Throwable?) {
                Progress.stop()
                roomBookingView.onRoomBookingConfirmAfterPaymentUnSuccess(activity.getString(R.string.error_server))
            }


        })
    }

    override fun getRoomBookingData(bookingSessionId: String, firstName: String, lastName: String, emailAddress: String, address: String, state: String, city: String, zipCode: String, phone: String, country: String, bookingNotes: String) {
        try {
            ApiAdapter.getInstance(activity)
            roomBookingApi(bookingSessionId,firstName,lastName,emailAddress,address,country,state,city,zipCode,phone,bookingNotes)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            roomBookingView.onRoomBookingInternetError()
        }
    }

    private fun roomBookingApi(bookingSessionId: String, firstName: String, lastName: String, emailAddress: String, address: String, country: String, state: String, city: String, zipCode: String, phone: String, bookingNotes: String) {

        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_BOOKING_SESSION_ID,bookingSessionId)
            jsonObject.put(Const.PARAM_FIRST_NAME,firstName)
            jsonObject.put(Const.PARAM_LAST_NAME,lastName)
            jsonObject.put(Const.PARAM_EMAIL_ADDRESS,emailAddress)
            jsonObject.put(Const.PARAM_ADDRESS,address)
            jsonObject.put(Const.PARAM_COUNTRY,country)
            jsonObject.put(Const.PARAM_STATE,state)
            jsonObject.put(Const.PARAM_CITY,city)
            jsonObject.put(Const.PARAM_ZIP_CODE,zipCode)
            jsonObject.put(Const.PARAM_PHONE,phone)
            jsonObject.put(Const.PARAM_BOOKING_NOTES,bookingNotes)
        }catch (ex : JSONException){
            ex.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject.toString())

        val getRoomBookingResult = ApiAdapter.getApiService()!!.roomBooking("application/json","no-cache",preferredLanguage,authorizationToken,body)

        getRoomBookingResult.enqueue(object : retrofit2.Callback<RoomBookingModel>{


            override fun onResponse(call: retrofit2.Call<RoomBookingModel>?, response: Response<RoomBookingModel>?) {

                Progress.stop()

                val roomBookingModel = response!!.body()

                val message = roomBookingModel.message!!
                try {
                    if(roomBookingModel.status == 1){
                        roomBookingView.onRoomBookingSuccess(roomBookingModel)
                    }else if(roomBookingModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else if(roomBookingModel.status == 3){
                        callSubmitIdProof(message)
                    }else{
                        roomBookingView.onRoomBookingUnSuccess(message)
                    }
                }catch (ex : NullPointerException){

                    ex!!.printStackTrace()
                    roomBookingView.onRoomBookingUnSuccess(activity.getString(R.string.error_server))
                }


            }


            override fun onFailure(call: retrofit2.Call<RoomBookingModel>?, t: Throwable?) {
                Progress.stop()

                t!!.printStackTrace()
                roomBookingView.onRoomBookingUnSuccess(activity.getString(R.string.error_server))
            }


        })
    }

    fun callSubmitIdProof(msg: String) {

        val alertDialoge = AlertDialog.Builder(activity)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(msg)

        alertDialoge.setPositiveButton(activity.getString(R.string.title_ok)) { dialog, which ->
            (activity as RoomBookingActivity).openIdProofActivity()
        }
        alertDialoge.setNegativeButton(activity.getString(R.string.title_cancel)) { dialog, which ->

            dialog.dismiss()
        }

        alertDialoge.show()


    }
}