package com.babaracine.mvp.room_booking.presenter

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class IDProofModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Any? = null

}