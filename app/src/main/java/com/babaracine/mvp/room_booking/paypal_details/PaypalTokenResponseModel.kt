package com.babaracine.mvp.room_booking.paypal_details

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PaypalTokenResponseModel {

    @SerializedName("scope")
    @Expose
    var scope: String? = null
    @SerializedName("nonce")
    @Expose
    var nonce: String? = null
    @SerializedName("access_token")
    @Expose
    var accessToken: String? = null
    @SerializedName("token_type")
    @Expose
    var tokenType: String? = null
    @SerializedName("app_id")
    @Expose
    var appId: String? = null
    @SerializedName("expires_in")
    @Expose
    var expiresIn: Int? = null

}