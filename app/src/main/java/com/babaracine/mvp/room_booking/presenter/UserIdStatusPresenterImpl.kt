package com.babaracine.mvp.room_booking.presenter

import android.app.Activity
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response

class UserIdStatusPresenterImpl(var activity: Activity, var view: UserIdStatusContractor.View) : UserIdStatusContractor.Presenter {


    override fun getIdUploadStatus() {
        try {
            ApiAdapter.getInstance(activity)
            getIdUploadIdStatus()
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            view.internetError()
        }

    }

    private fun getIdUploadIdStatus(){
        Progress.start(activity)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()


        var jsonObject = JSONObject()


        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString())

        val getRoomBookingResult = ApiAdapter.getApiService()!!.getIdProof("application/json", "no-cache", preferredLanguage, authorizationToken, body)

        getRoomBookingResult.enqueue(object : retrofit2.Callback<IDProofModel> {


            override fun onResponse(call: retrofit2.Call<IDProofModel>?, response: Response<IDProofModel>?) {

                Progress.stop()

                val idProofModel = response!!.body()


                try {
                    if (idProofModel.status == 1) {
                        view.idAllReadyAvailable()
                    } else if (idProofModel.status == 0) {
                        view.idNotAvailable()
                    } else if (idProofModel.status == 2) {
                        val message = idProofModel.message!!
                        AlertDialogHelper.showMessageToLogout(message, activity)
                    }
                } catch (ex: NullPointerException) {
                  //  view.uploadFail(activity.getString(R.string.error_server))
                    ex!!.printStackTrace()
                    // roomBookingView.onRoomBookingUnSuccess(activity.getString(R.string.error_server))
                }


            }


            override fun onFailure(call: retrofit2.Call<IDProofModel>?, t: Throwable?) {
                Progress.stop()
               // view.uploadFail(activity.getString(R.string.error_server))
                t!!.printStackTrace()
                // roomBookingView.onRoomBookingUnSuccess(activity.getString(R.string.error_server))
            }


        })
    }

}