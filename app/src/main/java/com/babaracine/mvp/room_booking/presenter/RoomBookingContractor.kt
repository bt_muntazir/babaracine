package com.babaracine.mvp.room_booking.presenter

interface RoomBookingContractor {

    interface RoomBookingPresenter{

        fun getRoomBookingData(bookingSessionId : String, firstName : String,lastName : String,emailAddress : String,address : String,state : String,city : String,zipCode : String, phone : String,country : String,bookingNotes : String)
        fun afterPaymentRoomConfirm(bookingSessionId : String, tokenId : String,bookingId : String)

    }

    interface RoomBookingView{

        fun onRoomBookingSuccess(roomBookingModel : RoomBookingModel)
        fun onRoomBookingUnSuccess(message : String)
        fun onRoomBookingInternetError()

        fun onRoomBookingConfirmAfterPaymentSuccess(roomBookingConfirmAfterPaymentModel : RoomBookingConfirmAfterPaymentModel)
        fun onRoomBookingConfirmAfterPaymentUnSuccess(message : String)
        fun onRoomBookingConfirmAfterPaymentInternetError()
    }

}