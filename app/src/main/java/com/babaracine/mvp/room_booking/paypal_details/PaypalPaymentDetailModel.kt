package com.babaracine.mvp.room_booking.paypal_details

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.util.ArrayList


class PaypalPaymentDetailModel {

    @SerializedName("payments")
    @Expose
    var payments: ArrayList<Payment>? = null
    @SerializedName("count")
    @Expose
    var count: Int? = null
    @SerializedName("next_id")
    @Expose
    var nextId: String? = null


    inner class Details {

        @SerializedName("subtotal")
        @Expose
        var subtotal: String? = null

    }


    inner class ItemArrayList {

        @SerializedName("items")
        @Expose
        var items: ArrayList<Any>? = null
        @SerializedName("shipping_address")
        @Expose
        var shippingAddress: ShippingAddress? = null

    }


    inner class Link {

        @SerializedName("href")
        @Expose
        var href: String? = null
        @SerializedName("rel")
        @Expose
        var rel: String? = null
        @SerializedName("method")
        @Expose
        var method: String? = null

    }

    inner class Payee {

        @SerializedName("merchant_id")
        @Expose
        var merchantId: String? = null

    }

    inner class Payer {

        @SerializedName("payment_method")
        @Expose
        var paymentMethod: String? = null
        @SerializedName("status")
        @Expose
        var status: String? = null
        @SerializedName("payer_info")
        @Expose
        var payerInfo: PayerInfo? = null

    }


    inner class PayerInfo {

        @SerializedName("email")
        @Expose
        var email: String? = null
        @SerializedName("first_name")
        @Expose
        var firstName: String? = null
        @SerializedName("last_name")
        @Expose
        var lastName: String? = null
        @SerializedName("payer_id")
        @Expose
        var payerId: String? = null
        @SerializedName("shipping_address")
        @Expose
        var shippingAddress: ShippingAddress? = null
        @SerializedName("phone")
        @Expose
        var phone: String? = null
        @SerializedName("country_code")
        @Expose
        var countryCode: String? = null

    }

    inner class Payment {

        @SerializedName("id")
        @Expose
        var id: String? = null
        @SerializedName("intent")
        @Expose
        var intent: String? = null
        @SerializedName("state")
        @Expose
        var state: String? = null
        @SerializedName("cart")
        @Expose
        var cart: String? = null
        @SerializedName("payer")
        @Expose
        var payer: Payer? = null
        @SerializedName("transactions")
        @Expose
        var transactions: ArrayList<Transaction>? = null
        @SerializedName("create_time")
        @Expose
        var createTime: String? = null
        @SerializedName("links")
        @Expose
        var links: ArrayList<Link>? = null

    }


    inner class RelatedResource {

        @SerializedName("sale")
        @Expose
        var sale: Sale? = null

    }


    inner class Sale {

        @SerializedName("id")
        @Expose
        var id: String? = null
        @SerializedName("state")
        @Expose
        var state: String? = null
        @SerializedName("amount")
        @Expose
        var amount: Amount? = null
        @SerializedName("payment_mode")
        @Expose
        var paymentMode: String? = null
        @SerializedName("protection_eligibility")
        @Expose
        var protectionEligibility: String? = null
        @SerializedName("protection_eligibility_type")
        @Expose
        var protectionEligibilityType: String? = null
        @SerializedName("transaction_fee")
        @Expose
        var transactionFee: TransactionFee? = null
        @SerializedName("parent_payment")
        @Expose
        var parentPayment: String? = null
        @SerializedName("create_time")
        @Expose
        var createTime: String? = null
        @SerializedName("update_time")
        @Expose
        var updateTime: String? = null
        @SerializedName("links")
        @Expose
        var links: ArrayList<Link>? = null

    }

    inner class ShippingAddress {

        @SerializedName("recipient_name")
        @Expose
        var recipientName: String? = null

    }


    inner class Transaction {

        @SerializedName("amount")
        @Expose
        var amount: Amount? = null
        @SerializedName("payee")
        @Expose
        var payee: Payee? = null
        @SerializedName("description")
        @Expose
        var description: String? = null
        @SerializedName("item_ArrayList")
        @Expose
        var itemArrayList: ItemArrayList? = null
        @SerializedName("related_resources")
        @Expose
        var relatedResources: ArrayList<RelatedResource>? = null

    }

    inner class TransactionFee {

        @SerializedName("value")
        @Expose
        var value: String? = null
        @SerializedName("currency")
        @Expose
        var currency: String? = null

    }

    inner class Amount {

        @SerializedName("total")
        @Expose
        var total: String? = null
        @SerializedName("currency")
        @Expose
        var currency: String? = null
        @SerializedName("details")
        @Expose
        var details: Details? = null

    }

}



