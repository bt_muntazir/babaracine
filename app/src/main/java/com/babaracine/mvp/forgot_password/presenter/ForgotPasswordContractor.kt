package com.babaracine.mvp.forgot_password.presenter

interface ForgotPasswordContractor {

    interface ForgotPasswordPresenter{

        fun forgotPasswordApi(email : String)

    }
    interface ForgotPasswordView{

        fun onForgotPasswordSuccess(forgotPasswordModel: ForgotPasswordModel)
        fun onForgotPasswordUnSuccess(message : String)
        fun onForgotPasswordInternetError()

    }

}