package com.babaracine.mvp.forgot_password

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.utility.SnackNotify
import com.babaracine.common.utility.Utils
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.forgot_password.presenter.ForgotPasswordContractor
import com.babaracine.mvp.forgot_password.presenter.ForgotPasswordModel
import com.babaracine.mvp.forgot_password.presenter.ForgotPasswordPresenterImpl
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : BaseActivity() ,ForgotPasswordContractor.ForgotPasswordView{

    lateinit var forgotPasswordPresenterImpl : ForgotPasswordPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        forgotPasswordPresenterImpl = ForgotPasswordPresenterImpl(this,this)
        manageClickEvent()
    }

    override fun onForgotPasswordSuccess(forgotPasswordModel: ForgotPasswordModel) {
        AlertDialogHelper.showMessageToFinish(forgotPasswordModel.message!!,this)
    }

    override fun onForgotPasswordUnSuccess(message: String) {
        SnackNotify.showMessage(message,this,constraintLayoutForgotPassword)
    }

    override fun onForgotPasswordInternetError() {
        SnackNotify.checkConnection(onRetryForgotPassword,constraintLayoutForgotPassword)
    }

    private fun manageClickEvent() {

        btnForgotPasswordSubmit.setOnClickListener(View.OnClickListener { onClickBtnSubmit() })
        imgViewBack.setOnClickListener(View.OnClickListener { imgViewBackClick() })
    }

    private fun imgViewBackClick() {
        finish()
    }

    private fun onClickBtnSubmit() {

        val email = edtTextEmail.text.toString().trim()

        if(isValidateEmail(email)){
            forgotPasswordPresenterImpl.forgotPasswordApi(email)
        }
    }

    private fun isValidateEmail(email: String): Boolean {

        if(Utils.isEmptyOrNull(email)){
            edtTextEmail.setError(getString(R.string.error_email))
            edtTextEmail.requestFocus()
            return false
        }else if(!Utils.isValidEmail(email)){
            edtTextEmail.setError(getString(R.string.error_valid_email))
            edtTextEmail.requestFocus()
            return false
        }
        return true
    }

    internal val onRetryForgotPassword : OnClickInterface = object : OnClickInterface{
        override fun onClick() {
            onClickBtnSubmit()
        }

    }
}
