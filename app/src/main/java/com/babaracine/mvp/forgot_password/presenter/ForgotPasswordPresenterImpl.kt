package com.babaracine.mvp.forgot_password.presenter

import android.app.Activity
import com.babaracine.BuildConfig
import com.babaracine.R
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import okhttp3.Callback
import okhttp3.RequestBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class ForgotPasswordPresenterImpl(val activity: Activity, val forgotPasswordView: ForgotPasswordContractor.ForgotPasswordView) : ForgotPasswordContractor.ForgotPasswordPresenter {
    override fun forgotPasswordApi(email: String) {
        try {
            ApiAdapter.getInstance(activity)
            getForgotPasswordData(email)
        }catch (ex : ApiAdapter.Companion.NoInternetException){
            forgotPasswordView.onForgotPasswordInternetError()
        }
    }

    private fun getForgotPasswordData(email: String) {

        Progress.start(activity)

        val acceptedLanguage = LoginManagerSession.getPreferredLanguage()

        var jsonObject : JSONObject? = null
        try {
            jsonObject = JSONObject()
            jsonObject.put(Const.PARAM_EMAIL,email)
        }catch (exp : JSONException){
            exp.printStackTrace()
        }

        val body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),jsonObject.toString())

        val getForgotPasswordResult = ApiAdapter.getApiService()!!.forgotPassword("application/json","no-cache",acceptedLanguage,body)

        getForgotPasswordResult.enqueue(object : retrofit2.Callback<ForgotPasswordModel>{

            override fun onResponse(call: Call<ForgotPasswordModel>?, response: Response<ForgotPasswordModel>?) {

                Progress.stop()

                val forgotPasswordModel = response!!.body()
                val message = forgotPasswordModel.message!!
                try {
                    if(forgotPasswordModel.status == 1){
                        forgotPasswordView.onForgotPasswordSuccess(forgotPasswordModel)
                    }else if(forgotPasswordModel.status == 2){
                        AlertDialogHelper.showMessageToLogout(message,activity)
                    }else{
                        forgotPasswordView.onForgotPasswordUnSuccess(message)
                    }
                }catch (exp : NullPointerException){
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace()
                    forgotPasswordView.onForgotPasswordUnSuccess(activity.getString(R.string.error_server))
                }


            }



            override fun onFailure(call: Call<ForgotPasswordModel>?, t: Throwable?) {
                forgotPasswordView.onForgotPasswordUnSuccess(activity.getString(R.string.error_server))
            }


        })

    }
}