package com.babaracine.mvp.setting

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.babaracine.R
import com.babaracine.mvp.base_activity.BaseActivity
import com.babaracine.mvp.setting.adapter.LanguageAdapter
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.toolbar_with_back_and_title.*

class SettingActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        recyclerViewLang.layoutManager = GridLayoutManager(this, 3)
        recyclerViewLang.adapter = LanguageAdapter(this)

        txtViewToolTitle.text = getString(R.string.setting)

        imgViewToolBack.setOnClickListener { finish() }
    }
}
