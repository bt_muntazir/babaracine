package com.babaracine.mvp.setting.adapter

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.babaracine.R
import com.babaracine.common.application.BabaracineApplication
import com.babaracine.common.helpers.LoginManagerSession

import com.babaracine.common.request_response.Const
import com.babaracine.common.utility.PrefUtil
import com.babaracine.mvp.dashboard.DashboardActivity
import com.babaracine.mvp.setting.SettingActivity
import com.babaracine.mvp.welcome.WelcomeActivity
import com.facebook.login.LoginManager
import kotlinx.android.synthetic.main.item_language.view.*

class LanguageAdapter(var context: Context) : RecyclerView.Adapter<LanguageAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_language, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var selectedLocale: String = "en"


        //set language name
        holder!!.txtViewLanguage.text = Const.strLanguage.get(position)

        //Drawable color change according to current language
        if (Const.strLocales[position].equals(Const.strLocales[PrefUtil.getInt(BabaracineApplication.getInstance() as Context, Const.KEY_LANGUAGE, 0)])) {
            holder!!.txtViewLanguage.background = ContextCompat.getDrawable(context, R.drawable.curved_drawable_orange)


        } else {
            holder!!.txtViewLanguage.background = ContextCompat.getDrawable(context, R.drawable.curved_drawable_gray)
        }

        //handle click event of user to change language
        holder!!.txtViewLanguage.setOnClickListener(View.OnClickListener {

            selectedLocale = Const.strLocales[position]

            alertConfirmationChangeLang(position, selectedLocale)

        })
    }

/*

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {

    }
*/

    override fun getItemCount(): Int {
        return Const.strLanguage.size
    }

    /* override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

     }
 */
    private fun updateLanguage(position: Int, selectedLocale: String) {

        PrefUtil.putInt(BabaracineApplication.getInstance() as Context, Const.KEY_LANGUAGE, position)

        LoginManagerSession.setPreferredLanguage(selectedLocale)


        if(LoginManagerSession.isLoggedIn()){
            context.startActivity(Intent(context, DashboardActivity::class.java))
            (context as Activity).finishAffinity()
        }else{
            context.startActivity(Intent(context, WelcomeActivity::class.java))
            (context as Activity).finishAffinity()

        }


    }


    inner class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var txtViewLanguage = view.txtViewLanguage
    }

    fun alertConfirmationChangeLang(position: Int, selectedLocale: String) {


        val alertDialoge = AlertDialog.Builder(context)

        alertDialoge.setCancelable(false)
        alertDialoge.setMessage(context.getString(R.string.change_application_language)+" " + Const.strLanguage[position])

        alertDialoge.setPositiveButton(context.getString(R.string.title_ok)) { dialog, which ->
            updateLanguage(position, selectedLocale)
        }

        alertDialoge.setNegativeButton(context.getString(R.string.title_cancel)) { dialog, which ->

        }


        alertDialoge.show()
    }
}