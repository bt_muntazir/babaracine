package com.babaracine.mvp.submit_id_proof

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SnapHelper
import android.view.View
import android.widget.Toast
import com.babaracine.R
import com.babaracine.common.application.BabaracineApplication
import com.babaracine.common.helpers.AlertDialogHelper
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.common.listener.InternetExceptionRetry
import com.babaracine.common.request_response.ConstIntent
import com.babaracine.common.utility.SnackNotify
import com.babaracine.mvp.Payment.PaymentActivity
import com.babaracine.mvp.submit_id_proof.presenter.SubmitIdProofContractor
import com.babaracine.mvp.submit_id_proof.presenter.SubmitIdProofPresenterImpl
import kotlinx.android.synthetic.main.activity_submit_id_proof.*

import kotlinx.android.synthetic.main.bottom_sheet_camera.*
import kotlinx.android.synthetic.main.content_submit_id_proof.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class SubmitIdProofActivity : AppCompatActivity(), SubmitIdProofContractor.View {


    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    var lastSelectedFeature: Int = 0

    lateinit var roomSessionId: String
    lateinit var bookingId: String


    private val IMAGE_DIRECTORY = "/Babaracine"

    var isNewImageUploading: Boolean = false

    lateinit var image_uri: Uri


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_submit_id_proof)
        bottomSheet()
        manageClickEvent()
    }


    private fun manageClickEvent() {
        imgViewUploadidProof.setOnClickListener { openBottomSheet() }
        relLayImageFromGallery.setOnClickListener { selectImageInAlbum() }
        relLayImageFromCamera.setOnClickListener { takePhoto() }
        btnDone.setOnClickListener(View.OnClickListener { uploadFile(image_uri) })

    }

    private fun bottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheetBehavior.setPeekHeight(0)
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    bottomSheetBehavior.setPeekHeight(0)
                    touch_outside.setVisibility(View.GONE)
                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    touch_outside.setVisibility(View.VISIBLE)
                } else {
                    touch_outside.setVisibility(View.GONE)

                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    private fun collapseBottomSheet() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)

        }
    }

    private fun openBottomSheet() {

        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        } else {
            bottomSheetBehavior.setPeekHeight(0)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
        }

    }

    fun selectImageInAlbum() {


        if (setupPermissions()) {
            isNewImageUploading = true

            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(intent, REQUEST_SELECT_IMAGE_IN_ALBUM)
            }
        } else {
            lastSelectedFeature = REQUEST_SELECT_IMAGE_IN_ALBUM
            makeRequest()
        }

        collapseBottomSheet()
    }

    fun takePhoto() {


        if (setupPermissions()) {

            isNewImageUploading = true

            val intent1 = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent1.resolveActivity(packageManager) != null) {
                startActivityForResult(intent1, REQUEST_TAKE_PHOTO)
            }
        } else {
            lastSelectedFeature = REQUEST_TAKE_PHOTO
            makeRequest()
        }

        collapseBottomSheet()
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                2909)
    }

    private fun setupPermissions(): Boolean {
        val permission_camera = ContextCompat.checkSelfPermission(BabaracineApplication.getInstance()!!,
                Manifest.permission.CAMERA)

        val permission_write_storage = ContextCompat.checkSelfPermission(BabaracineApplication.getInstance()!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if ((permission_camera != PackageManager.PERMISSION_GRANTED) ||
                (permission_write_storage != PackageManager.PERMISSION_GRANTED)) {
            //Log.i(TAG, "Permission to record denied")
            return false
        } else {
            return true
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (setupPermissions()) {
            if (lastSelectedFeature == REQUEST_SELECT_IMAGE_IN_ALBUM) {
                selectImageInAlbum()
            } else if (lastSelectedFeature == REQUEST_TAKE_PHOTO) {
                takePhoto()
            }
        } else {
            isNewImageUploading = false
            //show message to enable it from setting
        }
    }

    companion object {
        private val REQUEST_TAKE_PHOTO = 321
        private val REQUEST_SELECT_IMAGE_IN_ALBUM = 123
    }


    //Only for main user
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_TAKE_PHOTO) {

            if (resultCode == RESULT_OK) {
                val thumbnail = data!!.getExtras().get("data") as Bitmap
                // profileImageView.setImageBitmap(thumbnail)


                saveImage(thumbnail)


                // Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == REQUEST_SELECT_IMAGE_IN_ALBUM) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    val contentURI = data.data
                    try {
                        //val bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), contentURI)
                        //val path = saveImage(bitmap)

                        btnDone.isEnabled = true
                        image_uri = contentURI
                        //
                        //Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
                        //  profileImageView.setImageBitmap(bitmap)

                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show()
                    }

                }
            }
        }
    }

    //only for main user
    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        var path = android.os.Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY


        val wallpaperDirectory = File(path)
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }

        try {
            val f = File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis().toString() + ".jpg")
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this,
                    arrayOf(f.getPath()),
                    arrayOf("image/jpeg"), null)
            fo.close()

            image_uri = Uri.fromFile(f)
            btnDone.isEnabled = true
            // uploadFile(Uri.fromFile(f))
            // Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    private fun uploadFile(file: Uri?) {

        var presenter: SubmitIdProofContractor.Presenter = SubmitIdProofPresenterImpl(this, this)
        presenter.uploadIdProof(file!!)
    }


    //Upload file response
    override fun uploadSuccess(message: String) {

        finish()

        //SnackNotify.showMessage(message,this@SubmitIdProofActivity,coordinator)
    }

    override fun uploadFail(message: String) {
        SnackNotify.showMessage(message, this@SubmitIdProofActivity, coordinator)
    }

    override fun internetError() {
        AlertDialogHelper.alertInternetError(this, onRetry)
    }

    internal val onRetry: OnClickInterface = object : OnClickInterface {
        override fun onClick() {
            uploadFile(image_uri)
        }

    }
}
