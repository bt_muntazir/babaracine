package com.babaracine.mvp.submit_id_proof.presenter

import android.net.Uri


interface SubmitIdProofContractor {

    interface Presenter {
        fun uploadIdProof(filePath: Uri)
    }

    interface View {
        fun uploadSuccess(message: String);
        fun uploadFail(message: String)
        fun internetError()
    }
}