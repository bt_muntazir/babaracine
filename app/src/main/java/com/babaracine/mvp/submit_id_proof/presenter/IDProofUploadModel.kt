package com.babaracine.mvp.submit_id_proof.presenter


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class IDProofUploadModel {

    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("data")
    @Expose
    var data: Any? = null

}