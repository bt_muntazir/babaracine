package com.babaracine.mvp.submit_id_proof.presenter

import android.app.Activity
import android.net.Uri
import com.babaracine.R
import com.babaracine.common.application.BabaracineApplication
import com.babaracine.common.helpers.LoginManagerSession
import com.babaracine.common.helpers.Progress
import com.babaracine.common.request_response.ApiAdapter
import com.babaracine.common.request_response.Const
import com.babaracine.common.utility.FileUtils
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import okhttp3.MultipartBody


class SubmitIdProofPresenterImpl(var mView: SubmitIdProofContractor.View, var activity: Activity) : SubmitIdProofContractor.Presenter {
    override fun uploadIdProof(filePath: Uri) {

        try {
            ApiAdapter.getInstance(activity)
            uploadFileOnServer(filePath)
        } catch (ex: ApiAdapter.Companion.NoInternetException) {
            mView.internetError()
        }

    }


    private fun uploadFileOnServer(filePath: Uri) {

        Progress.start(activity)

        val file = FileUtils.getFile(BabaracineApplication.getInstance()!!, filePath)
        val fbody = RequestBody.create(MediaType.parse("image/*"), file)
        val body = MultipartBody.Part.createFormData("id_proof", file!!.name, fbody)

        val preferredLanguage = LoginManagerSession.getPreferredLanguage()
        val authorizationToken = Const.KEY_BEARER + LoginManagerSession.getAuthenticationToken()

        val call = ApiAdapter.getApiService()!!.uploadIdProof(authorizationToken, preferredLanguage, body)


        call.enqueue(object : Callback<IDProofUploadModel> {
            override fun onResponse(call: Call<IDProofUploadModel>?, response: Response<IDProofUploadModel>?) {

                Progress.stop()
                val view = response!!.body()
                try {
                    if (view.status == 1) {
                        if (!view.message.isNullOrEmpty())
                            mView.uploadSuccess(view.message!!)
                    } else {
                        if (!view.message.isNullOrEmpty())
                            mView.uploadFail(view.message!!)
                    }
                } catch (ex: NullPointerException) {
                    mView.uploadFail(activity.getString(R.string.error_server))
                    ex.printStackTrace()
                }

            }

            override fun onFailure(call: Call<IDProofUploadModel>?, t: Throwable?) {
                mView.uploadFail(activity.getString(R.string.error_server))
                Progress.stop()
                t!!.printStackTrace()
            }


        })
    }
}