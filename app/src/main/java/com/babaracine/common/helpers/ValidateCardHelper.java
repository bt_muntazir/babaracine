package com.babaracine.common.helpers;

import android.content.Context;

import com.babaracine.R;
import com.stripe.android.model.Card;

/**
 * Created by Braintech on 25-04-2018.
 */

public class ValidateCardHelper {

    public static String validateCardData(Context context, Card card) {

        if (!card.validateNumber()) {
            return context.getString(R.string.invalid_card);
        }


        if (!card.validateExpiryDate()) {
            return context.getString(R.string.empty_exp_month);
        }

      /*  if (!card.validateExpYear()) {
            return context.getString(R.string.empty_exp_year) ;
        }*/

        if (!card.validateCVC()) {
            return context.getString(R.string.invalid_cvv);
        }

        if (!card.validateCard()) {
            return context.getString(R.string.empty_card_name);
        }


        return "true";
    }
}
