package com.babaracine.common.helpers

import android.os.Build
import android.view.ViewTreeObserver
import android.text.Html
import android.text.Spanned
import android.widget.TextView
import android.text.style.ClickableSpan
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.view.View


class TextViewMoreLess {

    companion object {


        // ----
        fun doResizeTextView(tv: TextView, maxLine: Int, expandText: String, viewMore: Boolean) {

            if (tv.tag == null) {
                tv.tag = tv.text
            }
            val vto = tv.viewTreeObserver
            vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    try {
                        val obs = tv.viewTreeObserver
                        obs.removeOnGlobalLayoutListener(this)
                        if (maxLine == 0) {
                            val lineEndIndex = tv.layout.getLineEnd(0)
                            val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + " " + expandText
                            tv.text = text
                            tv.movementMethod = LinkMovementMethod.getInstance()
                            tv.setText(
                                    addClickablePartTextViewResizable(/*getHtmlText(*/tv.text.toString()/*)*/, tv, maxLine, expandText,
                                            viewMore), TextView.BufferType.SPANNABLE)
                        } else if (maxLine > 0 && tv.lineCount >= maxLine) {
                            val lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                            val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + " " + expandText
                            tv.text = text
                            tv.movementMethod = LinkMovementMethod.getInstance()
                            tv.setText(
                                    addClickablePartTextViewResizable(/*getHtmlText(*/tv.text.toString()/*)*/, tv, maxLine, expandText,
                                            viewMore), TextView.BufferType.SPANNABLE)
                        } else {
                            val lineEndIndex = tv.layout.getLineEnd(tv.layout.lineCount - 1)
                            val text = tv.text.subSequence(0, lineEndIndex).toString() + " " + expandText
                            tv.text = text
                            tv.movementMethod = LinkMovementMethod.getInstance()
                            tv.setText(
                                    addClickablePartTextViewResizable(/*getHtmlText(*/tv.text.toString()/*)*/, tv, lineEndIndex, expandText,
                                            viewMore), TextView.BufferType.SPANNABLE)
                        }
                    } catch (e: StringIndexOutOfBoundsException) {
                        e.printStackTrace()
                    }

                }
            })

        }

        private fun addClickablePartTextViewResizable(strSpanned:/*Spanned*/ String, tv: TextView,
                                                      maxLine: Int, spanableText: String, viewMore: Boolean): SpannableStringBuilder {
            val ssb = SpannableStringBuilder(strSpanned)

            if (strSpanned.contains(spanableText)) {
                ssb.setSpan(object : ClickableSpan() {

                    override fun onClick(widget: View) {

                        if (viewMore) {
                            tv.layoutParams = tv.layoutParams
                            tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                            tv.invalidate()
                            doResizeTextView(tv, -1, "read it", false)
                        } else {
                            tv.layoutParams = tv.layoutParams
                            tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                            tv.invalidate()
                            doResizeTextView(tv, 3, "read more", true)
                        }

                    }
                }, strSpanned.indexOf(spanableText), strSpanned.indexOf(spanableText) + spanableText.length, 0)

            }
            return ssb

        }


        fun isViewMoreRequired(textView: TextView): Boolean {
            return if (textView.lineCount > 2 || textView.text.length > 250) {
                true
            } else {
                false
            }
        }


        private fun getHtmlText(text: String): Spanned {
            return if (Build.VERSION.SDK_INT >= 24) {
                Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY) // for 24 api and more
            } else {
                Html.fromHtml(text) // or for older api
            }
        }

        private fun removeOnGlobalLayoutListener(v: View, listener: ViewTreeObserver.OnGlobalLayoutListener) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                v.getViewTreeObserver().removeGlobalOnLayoutListener(listener)
            } else {
                v.getViewTreeObserver().removeOnGlobalLayoutListener(listener)
            }
        }




    }



}