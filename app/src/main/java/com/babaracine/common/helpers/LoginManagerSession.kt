package com.babaracine.common.helpers

import com.babaracine.common.application.BabaracineApplication
import com.google.gson.Gson
import com.babaracine.common.utility.PrefUtil
import com.babaracine.mvp.login.presenter.LoginModel

/**
 * Created by Braintech on 7/2/2018.
 */
class LoginManagerSession{

    companion object {

        var KEY_USER_DATA = "user_data"
        var KEY_USER_DETAIL = "user_detail"
        var IS_LOGIN = "is_login"
        var USER_PASSWORD = "user_pass"
        var PREFERRED_LANGUAGE = "preferred_language"
        var AUTHENTICATION_TOKEN = "authentication_token"
        var ROOM_ID = "room_id"
        var IS_ADD_OR_MANAGE_ROOM = "is_add_or_manage_room"



        fun logoutUser() {
            // Clearing all data from Shared Preferences
            //PrefUtil.clear(ctx);
            PrefUtil.remove(BabaracineApplication.getInstance()!!, KEY_USER_DATA)
            PrefUtil.remove(BabaracineApplication.getInstance()!!, IS_LOGIN)
            PrefUtil.remove(BabaracineApplication.getInstance()!!, KEY_USER_DETAIL)

        }

        fun isLoggedIn(): Boolean {
            return PrefUtil.getBoolean(BabaracineApplication.getInstance()!!, IS_LOGIN, false)
        }

        fun createLoginSession(data: LoginModel) {
            PrefUtil.putBoolean(BabaracineApplication.getInstance()!!, IS_LOGIN, true)
            PrefUtil.putString(BabaracineApplication.getInstance()!!, KEY_USER_DATA, Gson().toJson(data))
        }

        fun setPreferredLanguage(language : String){
            PrefUtil.putString(BabaracineApplication.getInstance()!!,PREFERRED_LANGUAGE,language)
        }

        fun getPreferredLanguage() : String{
            return PrefUtil.getString(BabaracineApplication.getInstance()!!, PREFERRED_LANGUAGE,"")!!
        }

        fun setAuthenticationToken(token : String){
            PrefUtil.putString(BabaracineApplication.getInstance()!!,AUTHENTICATION_TOKEN,token)
        }

        fun getAuthenticationToken() : String{
            return PrefUtil.getString(BabaracineApplication.getInstance()!!, AUTHENTICATION_TOKEN,"empty")!!
        }


        fun setRoomIdForManageRoom(roomId : String){
            PrefUtil.putString(BabaracineApplication.getInstance()!!,ROOM_ID,roomId)
        }

        fun getRoomIdForManageRoom() : String{
            return PrefUtil.getString(BabaracineApplication.getInstance()!!, ROOM_ID,"empty")!!
        }

        fun setIsManageFrom(bitForCheck : Boolean){
            PrefUtil.putBoolean(BabaracineApplication.getInstance()!!,IS_ADD_OR_MANAGE_ROOM,bitForCheck)
        }

        fun getIsManageFrom() : Boolean{
            return PrefUtil.getBoolean(BabaracineApplication.getInstance()!!, IS_ADD_OR_MANAGE_ROOM,false)
        }


        fun getUserData(): LoginModel {
            return Gson().fromJson(PrefUtil.getString(BabaracineApplication.getInstance()!!, KEY_USER_DATA, ""), LoginModel::class.java)
        }


    }





}