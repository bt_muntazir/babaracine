package com.babaracine.common.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by Braintech on 7/2/2018.
 */
class NetworkHelper{

    companion object {

        @SuppressLint("MissingPermission")
        fun isNetworkAvaialble(context: Context) : Boolean{

            var isWifiConnected : Boolean = true

            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo

            return if (activeNetworkInfo != null && activeNetworkInfo.isConnected)
                true
            else
                false
        }
    }



}