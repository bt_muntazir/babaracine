package com.netqube.common.helpers

import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView

/**
 * Created by Braintech on 7/2/2018.
 */
class FontHelper{

   public enum class FontType(type : String){


        FONT_REGULAR_POPPINS("fonts/Poppins-Regular.otf"),
        FONT_BOLD_POPPINS("fonts/Poppins-Bold.otf"),
        FONT_EXTRABOLD_POPPINS("fonts/Poppins-ExtraBold.otf"),
        FONT_EXTRALIGHT_POPPINS("fonts/Poppins-ExtraLight.otf"),
        FONT_LIGHT_POPPINS("fonts/Poppins-Light.otf"),
        FONT_MEDIUM_POPPINS("fonts/Poppins-Medium.otf"),
        FONT_SEMIBOLD_POPPINS("fonts/Poppins-SemiBold.otf"),
        FONT_BLACK_PLAYFAIR_DISPLAY("fonts/PlayfairDisplay-Black.otf"),
        FONT_BOLD_PLAYFAIR_DISPLAY("fonts/PlayfairDisplay-Bold.otf"),
        FONT_REGULAR_PLAYFAIR_DISPLAY("fonts/PlayfairDisplay-Regular.otf"),
        FONT_BOLD_ITALIC_PLAYFAIR_DISPLAY("fonts/PlayfairDisplay-BoldItalic.otf"),
        FONT_BLACK_ITALIC_PLAYFAIR_DISPLAY("fonts/PlayfairDisplay-BlackItalic.otf");


        lateinit var type : String

       companion object {

           fun fromType(fontType: FontType?): String? {
               if (fontType != null) {
                   for (typeEnum in FontType.values()) {
                       if (fontType == typeEnum) {
                           return typeEnum.type
                       }
                   }
               }
               return null
           }


       }

    }

    companion object {

        fun setFontFace(tv : TextView, fontType : FontType, context: Context){

            val type = Typeface.createFromAsset(context.assets, FontType.fromType(fontType))

            tv.typeface = type

        }

        fun applyFont(context: Context, root: View, fontType: FontType) {
            try {
                if (root is ViewGroup) {
                    for (i in 0 until root.childCount) {
                        applyFont(context, root, fontType)

                    }
                } else if (root is EditText) {
                    root.typeface = Typeface.createFromAsset(context.assets, FontType.fromType(fontType))
                } else if (root is TextView) {
                    root.typeface = Typeface.createFromAsset(context.assets, FontType.fromType(fontType))
                } else if (root is Button) {
                    root.typeface = Typeface.createFromAsset(context.assets, FontType.fromType(fontType))
                } else if (root is RadioButton) {
                    root.typeface = Typeface.createFromAsset(context.assets, FontType.fromType(fontType))
                }
            } catch (e: Exception) {
                // Log.e("Font error", String.format("Error occuerd when trying to apply %s font for %s view", FontType.fromType(fontType), root));
                e.printStackTrace()
            }

        }

    }






}