package com.babaracine.common.helpers

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.View
import com.babaracine.R
import com.babaracine.common.interfaces.OnClickInterface
import com.babaracine.mvp.login.LoginActivity

/**
 * Created by Braintech on 7/2/2018.
 */
class AlertDialogHelper {

    companion object {

        fun showMessageToFinish(msg: String, context: Context) {

            val alertDialog = AlertDialog.Builder(context)
            alertDialog.setCancelable(false);

            alertDialog.setMessage(msg)

            //alertDialog.setPositiveButton(context.getString(R.string.title_ok),DialogInterface.OnClickListener()->)

            alertDialog.setPositiveButton(context.getString(R.string.title_ok)) { dialog, which ->
                (context as Activity).finish()
                dialog.dismiss()
            }
            alertDialog.show()
        }

        fun showMessageToLogout(msg: String, context: Context) {

            val alertDialog = AlertDialog.Builder(context)
            alertDialog.setCancelable(false)
            alertDialog.setTitle("Session Expired")
            alertDialog.setMessage(msg)

            //alertDialog.setPositiveButton(context.getString(R.string.title_ok),DialogInterface.OnClickListener()->)

            alertDialog.setPositiveButton(context.getString(R.string.title_ok)) { dialog, which ->
                dialog.dismiss()
                val intent = Intent(context,LoginActivity::class.java)
                LoginManagerSession.logoutUser()
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                context.startActivity(intent)
            }
            alertDialog.show()
        }


        fun showMessage(msg: String, context: Context) {

            val alertDialoge = AlertDialog.Builder(context)

            alertDialoge.setCancelable(false)
            alertDialoge.setMessage(msg)

            alertDialoge.setPositiveButton(context.getString(R.string.title_ok)) { dialog, which ->

                dialog.dismiss()
            }

            alertDialoge.show()


        }


        fun alertInternetError(context: Context, onClick: OnClickInterface) {


            val alertDialoge = AlertDialog.Builder(context)

            alertDialoge.setCancelable(false)
            alertDialoge.setMessage(context.getString(R.string.error_internet))

            alertDialoge.setPositiveButton(context.getString(R.string.title_retry)) { dialog, which ->

                onClick.onClick()
                dialog.dismiss()
            }

            alertDialoge.setNegativeButton(context.getString(R.string.title_cancel)){ dialog, which ->
                (context as Activity).finishAffinity()
            }


            alertDialoge.show()
        }


        fun alertCloseApp(context: Context) {


            val alertDialoge = AlertDialog.Builder(context)

            alertDialoge.setCancelable(false)
            alertDialoge.setMessage(context.getString(R.string.app_exit))

            alertDialoge.setPositiveButton(context.getString(R.string.title_ok)) { dialog, which ->
                (context as Activity).finish()
                dialog.dismiss()
            }

            alertDialoge.setNegativeButton(context.getString(R.string.title_cancel)){ dialog, which ->
                dialog.dismiss()
            }


            alertDialoge.show()
        }

        fun alertEnableLocation(context: Context) {

            val builder = android.support.v7.app.AlertDialog.Builder(context)
            builder.setMessage(context.getString(R.string.title_gps))
                    .setCancelable(false)
                    .setPositiveButton(context.getString(R.string.title_yes)) { dialog, id ->
                        context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                        dialog.dismiss()
                    }
                    .setNegativeButton(context.getString(R.string.title_no)) { dialog, id -> System.exit(0) }
            val alert = builder.create()
            alert.show()
        }


        fun alertOpenAppSetting(msg: String, context: Context) {
            val alertDialog = android.support.v7.app.AlertDialog.Builder(context)

            // Setting Dialog Message
            alertDialog.setMessage(msg)
            alertDialog.setCancelable(false)
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton(context.getString(R.string.title_go_to_setting)) { dialog, which ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", context.packageName, null)
                intent.data = uri
                context.startActivity(intent)
            }

            alertDialog.setNegativeButton(context.getString(R.string.title_cancel)) { dialogInterface, i -> System.exit(0) }

            // Showing Alert Message
            alertDialog.show()
        }

        fun alertOpenAppCallSetting(msg: String, context: Context) {
            val alertDialog = android.support.v7.app.AlertDialog.Builder(context)

            // Setting Dialog Message
            alertDialog.setMessage(msg)
            alertDialog.setCancelable(false)
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton(context.getString(R.string.title_go_to_setting)) { dialog, which ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", context.packageName, null)
                intent.data = uri
                context.startActivity(intent)
            }

            alertDialog.setNegativeButton(context.getString(R.string.title_cancel)) { dialogInterface, i -> dialogInterface.dismiss() }

            // Showing Alert Message
            alertDialog.show()
        }


        fun alertInflateError(context: Context, onClick: OnClickInterface, msg: String) {


            val alertDialog = android.support.v7.app.AlertDialog.Builder(context)

            // Setting Dialog Message
            alertDialog.setMessage(msg)
            alertDialog.setCancelable(false)
            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton(context.getString(R.string.title_retry)) { dialog, which ->
                onClick.onClick()
                dialog.dismiss()
            }

            // Showing Alert Message
            alertDialog.show()


        }


    }


}