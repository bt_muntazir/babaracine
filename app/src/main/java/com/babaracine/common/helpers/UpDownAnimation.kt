package com.babaracine.common.helpers

import android.view.View
import android.view.animation.Animation
import android.view.WindowManager
import android.view.animation.Transformation



/**
 * Created by Braintech on 7/16/2018.
 */
class UpDownAnimation {

    companion object {

        fun expand(v: View) {
            v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            val targetHeight = v.getMeasuredHeight()

            // Older versions of android (pre API 21) cancel animations for views with a height of 0.
            v.getLayoutParams().height = 1
            v.setVisibility(View.VISIBLE)
            val a = object : Animation() {
                override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                    v.getLayoutParams().height = if (interpolatedTime == 1f)
                        WindowManager.LayoutParams.WRAP_CONTENT
                    else
                        (targetHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }

                override fun willChangeBounds(): Boolean {
                    return true
                }
            }

            // 1dp/ms
            a.setDuration((targetHeight / v.context.resources.displayMetrics.density).toLong())
            //a.duration = ((targetHeight / v.getContext().getResources().getDisplayMetrics().density) as Int).toLong()
            v.startAnimation(a)
        }

        fun collapse(v: View) {
            val initialHeight = v.getMeasuredHeight()

            val a = object : Animation() {
                override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                    if (interpolatedTime == 1f) {
                        v.setVisibility(View.GONE)
                    } else {
                        v.getLayoutParams().height = initialHeight - (initialHeight * interpolatedTime).toInt()
                        v.requestLayout()
                    }
                }

                override fun willChangeBounds(): Boolean {
                    return true
                }
            }

            // 1dp/ms
            a.duration = ((initialHeight / v.getContext().getResources().getDisplayMetrics().density) as Int).toLong()
            v.startAnimation(a)
        }
    }
}