package com.babaracine.common.push


import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.babaracine.common.session.FcmSession

/**
 * Created by Braintech on 8/8/2016.
 */
class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {



    override fun onTokenRefresh() {

        //Getting registration tokerefreshedTokenn
        val refreshedToken = FirebaseInstanceId.getInstance().token

        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken)

    }

    private fun sendRegistrationToServer(token: String?) {
        val fcmSession = FcmSession(this)
        fcmSession.saveFcmToken(token!!)
    }

    companion object {
        private val TAG = "MyFirebaseIIDService"
    }
}
