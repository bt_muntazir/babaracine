package com.babaracine.common.push

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.babaracine.R
import com.babaracine.mvp.splash.SplashActivity

/**
 * Created by Braintech on 06-07-2018.
 */

class MyFirebaseMessagingService : FirebaseMessagingService() {

    val TAG = "Service"
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

      //  Log.d(TAG, "From: " + remoteMessage!!.from)
        //Log.d(TAG, "Notification Message Body: " + remoteMessage.notification!!.body!!)
        //sendNotification(remoteMessage)

        if (remoteMessage!!.data != null) {
            val data = remoteMessage.data
            Log.e("FireBase Data", "From: " + remoteMessage.data)


            sendNotification(remoteMessage.data!!["message"]!!, remoteMessage.data!!["title"]!!)

        } else if (remoteMessage.notification!!.body != null) {
            Log.e("Firebase body ", remoteMessage.notification!!.body)
            sendNotification(remoteMessage.notification!!.body!!, remoteMessage.notification!!.title!!)
        }

    }

    private fun sendNotification(messageBody: String, title: String) {

        val intent = Intent(this, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(
                    "1001", "name", NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(mChannel)
        }
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationBuilder = NotificationCompat.Builder(this, "1001")
                .setSmallIcon(R.mipmap.ic_app_icon)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.mipmap.ic_app_icon))
                .setContentTitle(getString(R.string.app_name))
                .setStyle(NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }
}




