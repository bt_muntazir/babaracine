package com.babaracine.common.application

import android.content.Context
import android.content.Intent
import org.acra.ReportField
import org.acra.collector.CrashReportData
import org.acra.sender.ReportSender
import org.acra.sender.ReportSenderException


/**
 * Created by Braintech on 7/2/2018.
 */

internal class AcraReportSender(context: Context) : ReportSender {


    @Throws(ReportSenderException :: class)
    override fun send(context: Context, report: CrashReportData) {

        // Extract the required data out of the crash report.
        val reportBody = createCrashReport(report)

        val sendIntent = Intent(Intent.ACTION_VIEW)
        sendIntent.type = "plain/text"
        sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail")
        sendIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("himanshu@braintechnosys.com"))
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "CrashReport")
        sendIntent.putExtra(Intent.EXTRA_TEXT, reportBody)
        sendIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(sendIntent)
    }


    /**
     * Extract the required data out of the crash report.
     */
    private fun createCrashReport(report: CrashReportData): String {

        // I've extracted only basic information.
        // U can add loads more data using the enum ReportField. See below.
        val body = StringBuilder()
        body
                .append("Device : " + report.getProperty(ReportField.BRAND) + "-" + report.getProperty(ReportField.PHONE_MODEL))
                .append("\n")
                .append("Android Version :" + report.getProperty(ReportField.ANDROID_VERSION))
                .append("\n")
                .append("App Version : " + 1.1)
                .append("\n")
                .append("STACK TRACE : \n" + report.getProperty(ReportField.STACK_TRACE))


        return body.toString()
    }
}
