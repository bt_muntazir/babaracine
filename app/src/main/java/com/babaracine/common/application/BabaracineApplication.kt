package com.babaracine.common.application

import android.app.Application
import android.content.Context
import com.babaracine.common.utility.TypeFaceUtil

/**
 * Created by Braintech on 7/2/2018.
 */

class BabaracineApplication : Application() {



    override fun onCreate() {
        super.onCreate()

        context = this

        TypeFaceUtil.setDefaultFont(this, "MONOSPACE", "fonts/Poppins-Regular.otf");

        // ACRA.init(this);
    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
    }

    companion object {

        private var context: Context? = null
        fun getInstance(): Context? {
            return context
        }
    }

    /*companion object {

        var instance: Context? = null
            private set
    }*/
}
