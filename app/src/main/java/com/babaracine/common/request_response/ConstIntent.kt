package com.babaracine.common.request_response

/**
 * Created by Braintech on 7/2/2018.
 */
class ConstIntent{

    companion object {

        var KEY_ROOM_ID = "roomId"

        var KEY_RATING = "rating"
        var KEY_PRICE = "price"
        var KEY_CURRENCY_CODE = "price_type"
        var KEY_IMGAGE_URL = "image_url"
        var KEY_CANCELATION_TYPE = "cancelation_type"
        var KEY_CANCELATION_CHARGE = "cancelation_charge"
        var KEY_ROOM_NAME = "room_name"
        var KEY_ROOM_SESSION_ID = "room_session_id"
        var KEY_ROOM_BOOKING_ID = "room_booking_id"

        var KEY_ROOM_BOOKING_DEPOSIT_PERCENTAGE = "room_booking_deposit_percentage"
        var KEY_ROOM_BOOKING_TAX_PERCENTAGE = "room_booking_tax_percentage"
        var KEY_ROOM_BOOKING_TAX_NAME = "room_booking_tax_name"

        var KEY_ROOM_BOOKING_MINIMUM_DAY = "room_booking_minimum_day"
        var KEY_ROOM_LONG_BOOKING_NIGHTLY_PRICE = "long_booking_nightly_price"
        var KEY_NUMBER_OF_DAYS = "number_of_days"
        var KEY_MAXIMUM_GUEST_ALLOWED = "maximum_guest_allowed"
        var KEY_CHECK_IN_DISABLE_DATES = "checkInDisableDates"
        var KEY_CHECK_OUT_DISABLE_DATES = "checkOutDisableDates"


        var KEY_ROOM_DETAILS = "room_details"
        var KEY_HOUSING_RULES = "housing_rules"


        var KEY_CHECK_IN_DATE = "check_in_date"
        var KEY_CHECK_OUT_DATE = "check_out_date"
        var KEY_STARTING_MONTH_NAME = "starting_month_name"
        var KEY_ENDING_MONTH_NAME = "ending_month_name"
        var KEY_ROOM_CONFIRMATION_MESSAGE = "room_confirmation_message"

        // manage room fragmenst key

        var KEY_BASIC_DATA_FRAGMENT = "basic_data_fragment"
        var KEY_BASIC_DETAIL_FRAGMENT = "basic_detail_fragment"
        var KEY_AMENITY_FRAGMENT = "amenity_fragment"
        var KEY_PRICING_FRAGMENT = "pricing_fragment"
        var KEY_LOCATION_PAGE_COUNTRY_LIST = "loaction_page_country_list"
        var KEY_SELECTED_AMENITY_DATA = "selected_amenity_data"
        var KEY_PRICING_DETAILS = "pricing_details"
        var KEY_REVIEW_STEPS = "review_steps"
        var KEY_ROOM_PHOTOS = "room_photos"
        var KEY_ARRAY_LIST_REVIEW = "arraylist_review"
        var KEY_ARRAY_LIST_DISABLE_ROOM = "arraylist_disable_room"


    }


}