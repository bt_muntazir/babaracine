package com.babaracine.common.request_response

import android.content.Context

import com.babaracine.BuildConfig
import com.babaracine.common.helpers.NetworkHelper

import java.io.IOException
import java.util.concurrent.TimeUnit

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by Braintech on 6/10/2016.
 */
internal class ApiAdapterForPaymentTransaction {
    var isForToken: Boolean = false


    class NoInternetException(message: String) : Exception(message)

    companion object {

        private var sInstance: ApiService? = null

        @Throws(NoInternetException::class)
        fun getInstance(context: Context, isForToken: Boolean, accessToken: String): ApiService {

            return if (NetworkHelper.isNetworkAvaialble(context)) {
                getApiService(isForToken, accessToken)
            } else {
                throw NoInternetException("No Internet connection available")
            }
        }

        fun getApiService(isForToken: Boolean, accessToken: String): ApiService {
            if (sInstance == null) {
                synchronized(ApiAdapterForPaymentTransaction::class.java) {
                    if (sInstance == null) {

                        sInstance = Retrofit.Builder()
                                .baseUrl("https://api.sandbox.paypal.com/v1/")
                                .client(getOkHttpClient(isForToken, accessToken)).addConverterFactory(GsonConverterFactory.create()).build()
                                .create(ApiService::class.java)
                    }
                }
            }
            return sInstance!!
        }

        private fun getOkHttpClient(isForToken: Boolean, accessToken: String?): OkHttpClient {

            val builder = OkHttpClient.Builder()
                    .retryOnConnectionFailure(true)

            val interceptor = Interceptor { chain ->
                val original = chain.request()

                // Request customization: add request headers
                val requestBuilder = original.newBuilder()
                requestBuilder.header("Content-Type", "application/json")

                if (accessToken != null) {
                    val credentials = "Bearer $accessToken"
                    requestBuilder.header("Authorization", credentials)
                }

                val request = requestBuilder.build()
                chain.proceed(request)
            }

            builder.addInterceptor(interceptor)

            if (BuildConfig.DEBUG) {

                //Print Log
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(httpLoggingInterceptor)

            }
            return builder.readTimeout(20000, TimeUnit.SECONDS)
                    .connectTimeout(20000, TimeUnit.SECONDS)
                    .build()

        }
    }

}
