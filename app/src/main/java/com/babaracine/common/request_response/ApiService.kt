package com.babaracine.common.request_response


import com.babaracine.common.request_response.Const.Companion.PARAM_ROOM_ID
import com.babaracine.mvp.common_model.CommonModel
import com.babaracine.mvp.dashboard.model.NotificationModel
import com.babaracine.mvp.dashboard.model.RoomListingModel
import com.babaracine.mvp.dashboard.model.TripsModel
import com.babaracine.mvp.forgot_password.presenter.ForgotPasswordModel
import com.babaracine.mvp.update_profile.presenter.GetProfileDataModel
import com.babaracine.mvp.login.presenter.LoginModel
import com.babaracine.mvp.manage_room.model.PricingDetailModel
import com.babaracine.mvp.manage_room.model.SelectedAmenityModel
import com.babaracine.mvp.manage_room.model.SelectedRoomPhotoModel
import com.babaracine.mvp.manage_room.model.UpdateBasicDetailModel
import com.babaracine.mvp.manage_room.presenter.BasicDataModel
import com.babaracine.mvp.manage_room.presenter.BasicLocationAndPolicyModel
import com.babaracine.mvp.manage_room.presenter_disable_room.DisableRoomListModel
import com.babaracine.mvp.manage_room.presenter_manage_reviews.ManageReviewModel
import com.babaracine.mvp.manage_room.presenter_update_room.UpdateRoomDetailModel
import com.babaracine.mvp.property_list.PropertyListModel
import com.babaracine.mvp.register.presenter.SignupModel
import com.babaracine.mvp.review_steps.presenter.CheckAvailabilityModel
import com.babaracine.mvp.review_steps.presenter.presenter_country.GetCountryModel
import com.babaracine.mvp.room_booking.paypal_details.PaypalPaymentDetailModel
import com.babaracine.mvp.room_booking.paypal_details.PaypalTokenResponseModel
import com.babaracine.mvp.room_booking.presenter.IDProofModel
import com.babaracine.mvp.room_booking.presenter.RoomBookingConfirmAfterPaymentModel
import com.babaracine.mvp.room_booking.presenter.RoomBookingModel
import com.babaracine.mvp.room_detail.presenter.RoomDetailModel
import com.babaracine.mvp.submit_id_proof.presenter.IDProofUploadModel
import com.babaracine.mvp.terms_and_condition.presenter.TermsAndConditionModel
import com.babaracine.mvp.your_reservation.YourReservationModel
import com.babaracine.mvp.your_reservation.presenter.ViewRoomDetailBookingModel
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.POST
import retrofit2.http.Multipart
import okhttp3.MultipartBody


/**
 * Created by Braintech on 7/2/2018.
 */
interface ApiService {


    //<----------- Login Api ------------------------------>
    @POST("api/users/login.json")
    fun login(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Body params: RequestBody): retrofit2.Call<LoginModel>

    //<----------- Signup Api ------------------------------>
    @POST("api/users/register.json")
    fun signup(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Body params: RequestBody): retrofit2.Call<SignupModel>

    //<----------- edit profile Api ------------------------------>
    @POST("api/users/edit.json")
    fun editProfile(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<LoginModel>

    //<----------- get profile Api ------------------------------>
    @POST("api/users/getUser.json")
    fun getProfile(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<GetProfileDataModel>

    //<----------- social login Api ------------------------------>
    @POST("api/users/socialLogin.json")
    fun socialLogin(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Body params: RequestBody): retrofit2.Call<LoginModel>

    //<----------- change password Api ------------------------------>
    @POST("api/users/changePassword.json")
    fun changePassword(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<CommonModel>

    //<----------- room detail Api ------------------------------>
    @POST("api/rooms/roomDetails.json")
    fun getRoomDetail(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<RoomDetailModel>

    //<----------- check room availability Api ------------------------------>
    @POST("api/bookings/roomAvailability.json")
    fun checkAvailabilityRoom(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<CheckAvailabilityModel>

    //<----------- room booking Api ------------------------------>
    @POST("api/bookings/book.json")
    fun roomBooking(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<RoomBookingModel>

    //<----------- room booking Api ------------------------------>
    @POST("api/Bookings/confirm.json")
    fun roomBookingConfirmAfterPayment(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<RoomBookingConfirmAfterPaymentModel>

    //<----------- get basic data location and policy data Api ------------------------------>
    @POST("api/manageRooms/getBasicDetails.json")
    fun getBasicLocationAndPolicyData(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<BasicLocationAndPolicyModel>

    //<----------- get pricing detail data Api ------------------------------>
    @POST("api/manageRooms/getPricingDetails.json")
    fun getPricingDetails(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<PricingDetailModel>

    //<----------- get selected amenity data Api ------------------------------>
    @POST("api/manageRooms/getAmenitiesDetails.json")
    fun getSelectedAmenity(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<SelectedAmenityModel>

    //<----------- get selected photo data Api ------------------------------>
    @POST("api/manageRooms/getPhotosDetails.json")
    fun getPhotoForManageRoom(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<SelectedRoomPhotoModel>

    //<----------- confirm booking Api ------------------------------>
    @POST("api/bookings/ownerConfirm.json")
    fun confirmRoomBook(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<CommonModel>


    //<----------- Cancel booking Api ------------------------------>
    @POST("api/bookings/cancelBooking.json")
    fun cancelRoomBook(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<CommonModel>


    //<----------- View Detail room booking Api ------------------------------>
    @POST("api/bookings/viewBooking.json")
    fun viewDetailRoomBook(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<ViewRoomDetailBookingModel>


    //<----------- forgot password Api ------------------------------>
    @POST("api/users/forgotPassword.json")
    fun forgotPassword(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Body params: RequestBody): retrofit2.Call<ForgotPasswordModel>


    //<----------- your reservation list Api ------------------------------>
    @GET
    fun yourReservation(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Url url: String): retrofit2.Call<YourReservationModel>

    //<----------- get basic detail list Api ------------------------------>
    @GET
    fun getBasicDetailForManageRoom(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Url url: String): retrofit2.Call<BasicDataModel>


    //<----------- terms and condition Api ------------------------------>
    @GET
    fun termsAndCondition(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Url url: String): retrofit2.Call<TermsAndConditionModel>


    //<----------- property list list Api ------------------------------>
    @GET("api/Properties/listing.json")
    fun propertyList(@Header("Content-Type") contentType: String,
                     @Header("Cache-Control") cache: String,
                     @Header("Accept-Language") acceptLanguage: String,
                     @Header("BABARACINEAUTH") authorizationToken: String,
                     @Query("room_name") filterType: String,
                     @Query("page") page: Int
    ): retrofit2.Call<PropertyListModel>

    @GET("api/users/notifications.json")
    fun ownerNotification(@Header("Content-Type") contentType: String,
                          @Header("Cache-Control") cache: String,
                          @Header("Accept-Language") acceptLanguage: String,
                          @Header("BABARACINEAUTH") authorizationToken: String,
                          @Query("page") page: Int
    ): retrofit2.Call<NotificationModel>


    @GET("api/rooms/listing.json")
    fun getRoomListing(@Header("Content-Type") contentType: String,
                       @Header("Cache-Control") cache: String,
                       @Header("Accept-Language") acceptLanguage: String,
                       @Header("BABARACINEAUTH") authorizationToken: String,
                       @Query("q") search: String,
                       @Query("startDate") startDate: String,
                       @Query("endDate") endDate: String,
                       @Query("min_price") min_price: String,
                       @Query("max_price") max_price: String,
                       @Query("suitable_for") suitable_for: String,
                       @Query("page") page: Int
    ): retrofit2.Call<RoomListingModel>

    @GET
    fun getYourTrips(@Header("Content-Type") contentType: String,
                     @Header("Cache-Control") cache: String,
                     @Header("Accept-Language") acceptLanguage: String,
                     @Header("BABARACINEAUTH") authorizationToken: String,
                     @Url url: String): retrofit2.Call<TripsModel>

    @GET
    fun countryList(@Header("Content-Type") contentType: String,
                    @Header("Cache-Control") cache: String,
                    @Header("Accept-Language") acceptLanguage: String,
                    @Header("BABARACINEAUTH") authorizationToken: String,
                    @Url url: String): retrofit2.Call<GetCountryModel>


    /*-------------Calling Paypal api to get token using ClientId and Secrete Key-------------*/
    @POST("oauth2/token")
    abstract fun getAuthentionToken(@Query("grant_type") grant_type: String): Call<PaypalTokenResponseModel>

    /*-------------Calling Paypal api to get payment detail using token and paymentId-------------*/
    @GET("payments/payment")
    abstract fun getPaypalPaymentDetail(@Query("paymentId") paymentId: String): Call<PaypalPaymentDetailModel>


    /*Check users ID Proof status*/
    @POST("api/users/getIdProof.json")
    fun getIdProof(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<IDProofModel>

    
    /*Upload users ID Proof*/
    @Multipart
    @POST("api/users/uploadIdProof.json")
    fun uploadIdProof(@Header("BABARACINEAUTH") authorizationToken: String, @Header("Accept-Language") acceptLanguage: String, @Part image: MultipartBody.Part): Call<IDProofUploadModel>


    //<----------- update basic room details Api ------------------------------>
    @POST("api/manageRooms/editBasicDetails.json")
    fun updateBasicDetailsOfRoom(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<UpdateBasicDetailModel>

    //<----------- update location detail in manage room Api ------------------------------>
    @POST("api/manageRooms/editLocation.json")
    fun updateLocationDetails(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<UpdateRoomDetailModel>

     //<----------- update policy detail in manage room Api ------------------------------>
    @POST("api/manageRooms/editPolicy.json")
    fun updatePolicyDetails(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<UpdateRoomDetailModel>


    //<----------- update policy detail in manage room Api ------------------------------>
    @POST("api/manageRooms/editAmenities.json")
    fun updateAmenityDetails(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<UpdateRoomDetailModel>

    //<----------- update pricing detail in manage room Api ------------------------------>
    @POST("api/manageRooms/editPricing.json")
    fun updatePricingDetail(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<UpdateRoomDetailModel>

    //<----------- update featured photo in manage room Api ------------------------------>
    @POST("api/manageRooms/markedFeatured.json")
    fun updateFeaturedPhoto(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<UpdateRoomDetailModel>

    //<----------- delete photo in manage room Api ------------------------------>
    @POST("api/manageRooms/deletePhoto.json")
    fun deletePhotoInRoomUpdate(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<SelectedRoomPhotoModel>


    //<----------- stripe payment Api ------------------------------>
    @POST("api/bookings/stripePayment.json")
    fun stripePayment(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<RoomBookingConfirmAfterPaymentModel>

    /*Upload room photo*/
    @Multipart
    @POST("api/manageRooms/uploadPhotos.json")
    fun uploadRoomPhoto(@Header("BABARACINEAUTH") authorizationToken: String, @Header("Accept-Language") acceptLanguage: String, @Part("room_id") roomIdRequestBody: RequestBody, @Part image: MultipartBody.Part): Call<SelectedRoomPhotoModel>

    //<----------- get disable room list in manage room Api ------------------------------>
    @POST("api/rooms/disableRoomsList.json")
    fun getDisableRoomList(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<DisableRoomListModel>

    //<----------- delete disable room from list in manage room Api ------------------------------>
    @POST("api/rooms/deleteDisable.json")
    fun deletDisableRoom(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<CommonModel>

    //<----------- add disable room in list in manage room Api ------------------------------>
    @POST("api/rooms/addDisable.json")
    fun addDisableRoom(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<DisableRoomListModel>

    //<----------- get all reviews in manage room Api ------------------------------>
    @POST("api/rooms/reviewList.json")
    fun getAllReviews(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<ManageReviewModel>

    //<----------- delete review from list in manage room Api ------------------------------>
    @POST("api/rooms/deleteReview.json")
    fun deletReviewFromList(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<CommonModel>


    //<----------- change status room list  ------------------------------>
    @POST("api/rooms/changeStatus.json")
    fun updateRoomStatusListedUnlisted(@Header("Content-Type") contentType: String, @Header("Cache-Control") cache: String, @Header("Accept-Language") acceptLanguage: String, @Header("BABARACINEAUTH") authorizationToken: String, @Body params: RequestBody): retrofit2.Call<CommonModel>


}