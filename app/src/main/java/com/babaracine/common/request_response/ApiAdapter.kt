package com.babaracine.common.request_response

import android.content.Context
import com.babaracine.BuildConfig
import com.babaracine.common.helpers.NetworkHelper
import com.babaracine.common.request_response.ApiService
import com.babaracine.common.request_response.Const
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

class ApiAdapter {

    companion object {

        @Volatile
        private var instance: ApiService? = null

        class NoInternetException(override var message: String) : Exception(message)


        fun getInstance(context: Context): ApiService? {

            return if (NetworkHelper.isNetworkAvaialble(context)) {
                getApiService()
            } else {
                throw NoInternetException("No internet connection")
            }
        }

        fun getApiService(): ApiService? {
            if (instance == null) {
                if (instance == null) { //if there is no instance available... create new one
                    synchronized(ApiAdapter::class.java)
                    {
                        instance = Retrofit.Builder()
                                .baseUrl(Const.BASE_URL)
                                .client(getOkHttpClient()).addConverterFactory(GsonConverterFactory.create()).build()
                                .create(ApiService::class.java)
                    }
                }
            }
            return instance
        }

        fun getOkHttpClient():OkHttpClient
        {
            val builder = OkHttpClient.Builder()
                    .retryOnConnectionFailure(true);

            val interceptor= Interceptor { chain ->

                val original=chain.request()
                val requestBuilder=original.newBuilder().header(Const.KEY_APPKEY,Const.KEY_APPKEY_VALUE)
                val request=requestBuilder.build()
                 chain.proceed(request)
            }

            builder.addInterceptor(interceptor)

            if (BuildConfig.DEBUG) {
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(httpLoggingInterceptor)
            }
            return builder.readTimeout(30000, TimeUnit.SECONDS)
                    .connectTimeout(30000, TimeUnit.SECONDS)
                    .build()
        }

    }


}