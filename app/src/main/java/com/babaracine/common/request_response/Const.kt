package com.babaracine.common.request_response

/**
 * Created by Braintech on 7/2/2018.
 */
class Const {

    companion object {
       // var BASE_URL = "http://onlineprojectprogress.com/babaracine/"
        var BASE_URL = "https://www.babaracine.com/"

        var PAYPAL_CLIENT_ID = "AbsusShKjvWQi7qsAH1vAoiSYo-CZlj9iCSJmP-AaVgU-441VRQAp1cEQcdO5xq32m2Q_BZN4I78P4jk"
        var PAYPAL_SECRET_KEY = "EDYOKHNpzwooHhvIWvrBwG6PTuU8YGkW2drIDK2Qzm8RNdu8-VK2OeK93zaABLniOPFJ1k8sH9wbDSUm"


        var KEY_APPKEY = "APPKEY"
        var KEY_APPKEY_VALUE = "b2abcd2d2f2cb62b16Babaracine786786"

        var KEY_NAME = "name"
        var KEY_ID = "id"
        var KEY_BEARER = "Bearer "


        var KEY_DEFAULT_LANGUAGE = "en"
        var KEY_SPAIN_LANGUAGE = "es"
        var KEY_FRANCE_LANGUAGE = "fr"

        // login api
        var PARAM_EMAIL = "email"
        var PARAM_PASSWORD = "password"
        var PARAM_DEVICE_TYPE = "device_type"
        var PARAM_DEVICE_ID = "device_id"
        var PARAM_DEVICE_TOKEN = "device_token"

        // social login api extra parameter
        var PARAM_PROVIDER = "provider"
        var PARAM_IDENTIFIER = "identifier"


        // sign up api
        var PARAM_FIRST_NAME = "first_name"
        var PARAM_LAST_NAME = "last_name"
        var PARAM_MOBILE_NUMBER = "mobile_number"
        var PARAM_GENDER = "gender"
        var PARAM_DOB = "dob"
        var PARAM_CONFIRM_PASSWORD = "confirm_password"
        var PARAM_SUBSCRIPTION = "subscription"
        var PARAM_USED_ID = "user_id"

        // change password api
        var PARAM_OLD_PASSWORD = "old_password"
        var PARAM_NEW_PASSWORD = "password"

        // room detail api
        var PARAM_ROOM_ID = "room_id"
        var PARAM_BOOKING_NOTES = "booking_notes"

        // check availability api
        var PARAM_START_DATE = "start_date"
        var PARAM_END_DATE = "end_date"
        var PARAM_NUMBER_OF_ADULT = "number_of_adults"
        var PARAM_NUMBER_OF_CHILDREN = "number_of_children"

        // room booking parameter api

        var PARAM_BOOKING_SESSION_ID = "booking_session_id"
        var PARAM_EMAIL_ADDRESS = "email_address"
        var PARAM_ADDRESS = "address"
        var PARAM_COUNTRY = "country"
        var PARAM_STATE = "state"
        var PARAM_CITY = "city"
        var PARAM_ZIP_CODE = "zip_code"
        var PARAM_PHONE = "phone"

        // after payment room booking parameter api

        var PARAM_PAYER_ID = "payer_id"
        var PARAM_PAYMENT_ID = "authorization_id"
        var PARAM_TOKEN_ID = "token_id"
        var PARAM_STRIPE_TOKEN = "stripeToken"
        var PARAM_BOOKING_ID = "booking_id"
        var PARAM_REQUESTED_BY = "request_by"


        var PARAM_STATUS = "status"
        var STATUS_CONFIRMED = 1
        var STATUS_ARRIVED = 2
        var STATUS_DEPARTED = 3
        var STATUS_ARCHIVED = 4
        var STATUS_CANCEL = 5
        var STATUS_DISABLED = 6
        var STATUS_PAYMENT_PENDING = 7
        var STATUS_EXPIRE = 8

        // manage room basic detail api

        var PARAM_BEDROOMS = "bedrooms"
        var PARAM_BEDS = "beds"
        var PARAM_BED_TYPE_ID = "bed_type_id"
        var PARAM_BATHROOMS = "bathrooms"
        var PARAM_PROPERTY_TYPE_ID = "property_type_id"
        var PARAM_ROOM_TYPE_ID = "room_type_id"
        var PARAM_MAX_OCCUPANTS = "max_occupants"
        var PARAM_NAME = "name"
        var PARAM_DESCRIPTION = "description"
        var PARAM_SUITABLE_FOR = "suitable_for"
        var PARAM_TRANSLATIONS = "_translations"


        // location update in manage room

        var PARAM_ADDRESS_1 = "address_1"
        var PARAM_ADDRESS_2 = "address_2"
        var PARAM_POSTAL = "postal"
        var PARAM_LATITUDE = "latitude"
        var PARAM_LONGITUDE = "longitude"

        // policy update in manage room
        var PARAM_POLICY = "policy"

        // amenity in manage room
        var PARAM_ARRAYLIST_SELECTED_AMENITY = "amenities"

        // pricing details in manage room
        var PARAM_CURRENCY_CODE = "currency_code"
        var PARAM_NIGHTLY_PRICE = "nightly_price"
        var PARAM_MINIMUM_DAY = "minimum_day"
        var PARAM_LONG_TERM_NIGHTLY_PRICE = "long_term_nightly_price"
        var PARAM_TAX_NAME = "tax_name"
        var PARAM_TAX_PERCENTAGE = "tax_percentage"
        var PARAM_DEPOSIT_PERCENTAGE = "deposit_percent"
        var PARAM_CANCELLATION_TYPE = "cancellation_type"
        var PARAM_CANCELLATION_CHARGE = "cancellation_charge"
        var PARAM_CANCELLATION_DESC = "cancellation_desc"

        // featured photo update

        var PARAM_PHOTO_ID = "photo_id"

        // disable room

        var PARAM_DISABLE_ID = "disable_id"
        var PARAM_REVIEW_ID = "review_id"


        //language
        var KEY_LANGUAGE = "language";
        var strLocales = arrayOf("en", "fr", "es")
        var strLanguage = arrayOf("English", "French", "Spanish")
    }

}