package com.babaracine.common.request_response

import android.content.Context
import android.util.Base64

import com.babaracine.BuildConfig
import com.babaracine.common.helpers.NetworkHelper

import java.io.IOException
import java.util.concurrent.TimeUnit

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import com.babaracine.common.request_response.Const
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by Braintech on 6/10/2016.
 */
class ApiAdapterForPaypal {
    internal var isForToken: Boolean = false


    class NoInternetException(message: String) : Exception(message)

    companion object {

        private var sInstance: ApiService? = null

        @Throws(NoInternetException::class)
        fun getInstance(context: Context, isForToken: Boolean, accessToken: String): ApiService {
            var isForToken = isForToken


            if (NetworkHelper.isNetworkAvaialble(context)) {
                isForToken = isForToken

                return getApiService(isForToken, accessToken)
            } else {
                throw NoInternetException("No Internet connection available")
            }
        }

        fun getApiService(isForToken: Boolean, accessToken: String): ApiService {
            if (sInstance == null) {
                synchronized(ApiAdapterForPaypal::class.java) {
                    if (sInstance == null) {

                        sInstance = Retrofit.Builder()
                                .baseUrl("https://api.sandbox.paypal.com/v1/")
                                .client(getOkHttpClient(isForToken, accessToken)).addConverterFactory(GsonConverterFactory.create()).build()
                                .create(ApiService::class.java)
                    }
                }
            }
            return sInstance!!
        }


        private fun getOkHttpClient(isForToken: Boolean, accessToken: String?): OkHttpClient {

            val builder = OkHttpClient.Builder()
                    .retryOnConnectionFailure(true)

            val interceptor = Interceptor { chain ->
                val original = chain.request()

                // Request customization: add request headers
                val requestBuilder = original.newBuilder()
                requestBuilder.header("Accept", "application/json")

                if (isForToken) {
                    requestBuilder.header("Accept-Language", " en_US")
                    requestBuilder.header("Content-Type", "application/x-www-form-urlencoded")

                    //String clientId = "AacYa2ZnSBnW_Wq503Q2V15LgyltY7pueH08qEj4rtTCYuxtwSf_hD0ed5FSGJOcI8-GoJKFjTTftgnA";
                    //String clientSecret = "EDWfEDRwGnRiPWTXSPqX_r7-6HNtl45i8hm34iCj-WrvPyj_XHLhzlgaaqIzfDkuQlulFIMz3aoqGt2x";

                    val clientId = "AeoIHEbVcvtAIU0vFBVcfxReJUVGrlx3V-TiW8a5nGMCBwbYjOBPHZ1t9vvTUFSbeomqMxR8hAN38Suy"
                    //val clientId = "AbsusShKjvWQi7qsAH1vAoiSYo-CZlj9iCSJmP-AaVgU-441VRQAp1cEQcdO5xq32m2Q_BZN4I78P4jk"
                    val clientSecret = "ECPf5RkzOFSqegLoVylMCNIMF0l6EthIm4OwHA3w4ddbp4znEu0aOQEYbbFBCZ-MyyUt31itijJp7DYk"
                    //val clientSecret = "EDYOKHNpzwooHhvIWvrBwG6PTuU8YGkW2drIDK2Qzm8RNdu8-VK2OeK93zaABLniOPFJ1k8sH9wbDSUm"

                    val credentials = "$clientId:$clientSecret"
                    val basic = "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)

                    // requestBuilder.header("Authorization", "Basic QWFjWWEyWm5TQm5XX1dxNTAzUTJWMTVMZ3lsdFk3cHVlSDA4cUVqNHJ0VENZdXh0d1NmX2hEMGVkNUZTR0pPY0k4LUdvSktGalRUZnRnbkE6RURXZkVEUndHblJpUFdUWFNQcVhfcjctNkhOdGw0NWk4aG0zNGlDai1XcnZQeWpfWEhMaHpsZ2FhcUl6ZkRrdVFsdWxGSU16M2FvcUd0Mng=");
                    requestBuilder.header("Authorization", basic)

                } else {

                    if (accessToken != null) {
                        val credentials = "Bearer $accessToken"
                        requestBuilder.header("Authorization", credentials)
                    }
                }


                val request = requestBuilder.build()
                chain.proceed(request)
            }

            builder.addInterceptor(interceptor)

            if (BuildConfig.DEBUG) {

                //Print Log
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(httpLoggingInterceptor)

            }
            return builder.readTimeout(20000, TimeUnit.SECONDS)
                    .connectTimeout(20000, TimeUnit.SECONDS)
                    .build()

        }
    }

}
