package com.babaracine.common.adapter

import android.app.Activity
import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.babaracine.R
import com.babaracine.common.request_response.Const
import java.util.ArrayList
import java.util.HashMap

/**
 * Created by Braintech on 7/2/2018.
 */

class AdapterSpinner(internal var context: Context, txtViewResourceId: Int,
                     internal var list: ArrayList<HashMap<String, String>>,internal var bitForChangeColor : String) : ArrayAdapter<String>(context, txtViewResourceId) {

    override fun getItem(index: Int): String? {
        return list[index][Const.KEY_NAME]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getDropDownView(position: Int, cnvtView: View?, prnt: ViewGroup): View {
        return getCustomViewDropdown(position, cnvtView, prnt)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    fun getCustomView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        /*View mySpinner = inflater.inflate(R.layout.layout_spinner_dropdown, parent, false);*/
        val textView = View.inflate(context, R.layout.spinner_selected_view, null) as TextView

        if(bitForChangeColor.equals("white",true)) {
            textView.setTextColor(ContextCompat.getColor(context, R.color.color_white))
        }else{
            textView.setTextColor(ContextCompat.getColor(context, R.color.color_grey))
        }
        val events = list[position]

        if (events[Const.KEY_NAME] != null)
            textView.text = events[Const.KEY_NAME].toString()


        return textView
    }

    fun getCustomViewDropdown(position: Int, convertView: View?, parent: ViewGroup): View {


        val inflater = context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val textView = View.inflate(context, R.layout.spinner_item, null) as TextView

        /* TextView textView = (TextView) mySpinner
                .findViewById(R.id.txtView);*/

        val events = list[position]

        if (events[Const.KEY_NAME] != null)
            textView.text = events[Const.KEY_NAME].toString()

        return textView
    }

    override fun getCount(): Int {
        return list.size
    }
}
