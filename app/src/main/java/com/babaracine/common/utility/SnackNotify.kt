package com.babaracine.common.utility

import android.content.Context
import android.graphics.Color
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.babaracine.R
import com.babaracine.common.interfaces.OnClickInterface

class SnackNotify {

    companion object {

        fun checkConnection(onRetryClick: OnClickInterface, coordinatorLayout: View) {

            val snackbar = Snackbar
                    .make(coordinatorLayout, "No internet connection!.", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Retry" , View.OnClickListener{
               onRetryClick.onClick()
            })
            // Changing message text color
            snackbar.setActionTextColor(Color.RED)

            // Changing action button text color
            val sbView = snackbar.view
            val textView = sbView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
            // Fonts.robotoRegular(activity, textView);
            textView.setTextColor(Color.YELLOW)
            snackbar.show()
        }

        fun showSnakeBarForBackPress(coordinatorLayout: View) {
            val snackbar = Snackbar
                    .make(coordinatorLayout, "Please click BACK again to exit.", Snackbar.LENGTH_LONG)

            // Changing action button text color
            val sbView = snackbar.view
            val textView = sbView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
            // Fonts.robotoRegular(activity, textView);
            textView.setTextColor(Color.YELLOW)
            snackbar.show()

        }

        /*fun showMessage(msg: String, coordinatorLayout: View) {
            val snackbar = Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_LONG)

            // Changing action button text color
            val sbView = snackbar.view
            val textView = sbView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
            // Fonts.robotoRegular(activity, textView);
            textView.setTextColor(Color.YELLOW)

            snackbar.show()

        }*/

        fun showMessage(msg: String,context : Context, coordinatorLayout: View) {

            // Create the Snackbar
            val snackbar = Snackbar.make(coordinatorLayout, "", Snackbar.LENGTH_LONG)
            // Get the Snackbar's layout view
            val layout = snackbar.view as Snackbar.SnackbarLayout

            layout.setPadding(15, 0, 15, 15)

            // Changing action button text color
            layout.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent))

            // Hide the text
            val textView = layout.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
            textView.visibility = View.INVISIBLE

            // Inflate our custom view
            val mInflater = LayoutInflater.from(context)
            val snackView = mInflater.inflate(R.layout.layout_snackbar, null)

            // Configure the view
            val txtViewMsg = snackView.findViewById<View>(R.id.txtViewMsg) as TextView
            txtViewMsg.text = msg
            txtViewMsg.setTextColor(Color.WHITE)

            // Add the view to the Snackbar's layout
            layout.addView(snackView, 0)
            // Show the Snackbar
            snackbar.show()
        }

        fun showMsgOnAlertDialog(msg: String, alertView: View) {

            val snackbar = Snackbar.make(alertView, msg, Snackbar.LENGTH_LONG)

            // Changing action button text color
            val sbView = snackbar.view
            val textView = sbView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
            // Fonts.robotoRegular(activity, textView);
            textView.setTextColor(Color.YELLOW)
            snackbar.show()
        }


        fun sGetPermission(msg: String, btnName: String, onRetryClick: OnClickInterface, coordinatorLayout: View) {

            val snackbar = Snackbar
                    .make(coordinatorLayout, msg, Snackbar.LENGTH_INDEFINITE)
                    .setAction(btnName) { onRetryClick.onClick() }

            // Changing message text color
            snackbar.setActionTextColor(Color.RED)

            // Changing action button text color
            val sbView = snackbar.view
            val textView = sbView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
            // Fonts.robotoRegular(activity, textView);
            textView.setTextColor(Color.YELLOW)
            snackbar.show()
        }

    }
}