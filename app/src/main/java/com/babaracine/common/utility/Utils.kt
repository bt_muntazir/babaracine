package com.babaracine.common.utility

import android.app.Activity
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.app.Fragment
import android.text.Html
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.text.style.AbsoluteSizeSpan
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import com.babaracine.R
import java.io.File
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Braintech on 7/2/2018.
 */
class Utils{

    companion object {
        // for keyboard down
        fun hideKeyboardIfOpen(activity: Activity) {


            val view = activity.currentFocus

            if (view != null) {
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }

        }


        fun isValidEmail(email: String): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }


        fun isEmptyOrNull(value: String?): Boolean {
            return if (value == null || value.isEmpty())
                true
            else
                false
        }

        fun getDeviceId(context: Context): String {

            return Settings.Secure.getString(context.contentResolver,
                    Settings.Secure.ANDROID_ID)
        }

        fun isPasswordMatch(password: String, confirmPassword: String): Boolean {
            return if (password == confirmPassword)
                true
            else
                false
        }

        fun getCurrenctSymbol(currencyCode :String,context :Context):String {

            val data = Currency.getInstance(currencyCode)
            var str: String
            str = if (Build.VERSION.SDK_INT >= 24) data.getSymbol(Locale.getDefault(Locale.Category.DISPLAY))
            else data.getSymbol(context.resources.configuration.locale)

            return  str


        }

        fun convertDateFormat(date: String, inputFormat: String, outputFormat: String): String {
            val sdf = SimpleDateFormat(inputFormat)
            var testDate: Date? = null
            var newFormat = "N/A"
            try {
                testDate = sdf.parse(date)
                val formatter = SimpleDateFormat(outputFormat)
                newFormat = formatter.format(testDate)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            return newFormat
        }


        fun isVisibleOrNull(fragment: Fragment?): Boolean {
            return if (fragment != null) {
                if (fragment.isVisible)
                    true
                else
                    false
            } else
                false
        }


        fun getRealPathFromURI(contentURI: String, context: Context): String {
            val contentUri = Uri.parse(contentURI)
            val cursor = context.contentResolver.query(contentUri, null, null, null, null)
            if (cursor == null) {
                return contentUri.path
            } else {
                cursor.moveToFirst()
                val index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                return cursor.getString(index)
            }
        }

        fun resizeImageForImageView(bitmap: Bitmap, rotation: Float): Bitmap? {

            var resizedBitmap: Bitmap? = null
            val scaleSize = 1024
            val originalWidth = bitmap.width
            val originalHeight = bitmap.height
            var newWidth = -1
            var newHeight = -1
            var multFactor = -1.0f
            if (originalHeight > originalWidth) {
                newHeight = scaleSize
                multFactor = originalWidth.toFloat() / originalHeight.toFloat()
                newWidth = (newHeight * multFactor).toInt()
            } else if (originalWidth > originalHeight) {
                newWidth = scaleSize
                multFactor = originalHeight.toFloat() / originalWidth.toFloat()
                newHeight = (newWidth * multFactor).toInt()
            } else if (originalHeight == originalWidth) {
                newHeight = scaleSize
                newWidth = scaleSize
            }


            val matrix = Matrix()
            matrix.postRotate(rotation)

            resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, newWidth, newHeight, matrix, true)

            //resizedBitmap = createScaledBitmap(bitmap, newWidth, newHeight, false, rotation);
            return resizedBitmap
        }

        fun rotateImage(bitmap: Bitmap, rotation: Float): Bitmap {

            val resizedBitmap: Bitmap? = null
            val scaleSize = 1024
            val originalWidth = bitmap.width
            val originalHeight = bitmap.height


            val matrix = Matrix()
            matrix.postRotate(rotation)

            return Bitmap.createBitmap(bitmap, 0, 0, originalWidth, originalHeight, matrix, true)

        }

        fun formatCurrencyUk(amount: Double?): String? {

            var s: String? = null
            try {

                val n = NumberFormat.getCurrencyInstance(Locale.UK)
                s = n.format(amount)

            } catch (ex: NumberFormatException) {
                ex.printStackTrace()
            }

            return s
        }


        fun formatDecimal(amount: Double): String {
            val form = DecimalFormat("0.00")
            return form.format(amount)
        }

        fun getColoredSpanned(text: String, color: String): String {
            return "<font color=$color>$text</font>"
        }

        fun fromHtml(text: String): Spanned {
            val result: Spanned

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                result = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
            } else {
                result = Html.fromHtml(text)
            }

            return result
        }

       /* fun setFontsize(context: Context, firstString: String, secondString: String): CharSequence {

            val textSize1 = context.resources.getDimensionPixelSize(R.dimen.text_size_40_sp)
            val textSize2 = context.resources.getDimensionPixelSize(R.dimen.text_size_30_sp)

            val span1 = SpannableString(firstString)
            span1.setSpan(AbsoluteSizeSpan(textSize1), 0, firstString.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)

            val span2 = SpannableString(secondString)
            span2.setSpan(AbsoluteSizeSpan(textSize2), 0, secondString.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)

            // let's put both spans together with a separator and all
            val finalText = TextUtils.concat(span1, span2)

            return trimTrailingWhitespace(finalText)
        }
*/
        fun trimTrailingWhitespace(source: CharSequence?): CharSequence {

            if (source == null)
                return ""

            var i = source.length

            // loop back to the first non-whitespace character
            while (--i >= 0 && Character.isWhitespace(source[i])) {
            }

            return source.subSequence(0, i + 1)
        }

        fun getVersionNumber(context: Context): String {
            val manager = context.packageManager
            var info: PackageInfo? = null
            var version = ""
            try {
                info = manager.getPackageInfo(
                        context.packageName, 0)
                version = info!!.versionName
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }

            return version

        }

        fun fileSizeExceed(path: String): Boolean {
            val file = File(path)

            // Get length of file in bytes
            val fileSizeInBytes = file.length()

            // Convert the bytes to Kilobytes (ic_splash KB = 1024 Bytes)
            val fileSizeInKB = fileSizeInBytes / 1024

            // Convert the KB to MegaBytes (ic_splash MB = 1024 KBytes)
            val fileSizeInMB = fileSizeInKB / 1024

            Log.e("filesize", "" + fileSizeInMB)

            return if (fileSizeInMB > 50) {
                true
            } else false
        }

        fun getMimeType(url: String): String? {
            var type: String? = null
            val extension = MimeTypeMap.getFileExtensionFromUrl(url)
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            }
            return type
        }

        fun isInPortraitMode(path: String): Boolean {
            val metaRetriever = MediaMetadataRetriever()
            metaRetriever.setDataSource(path)
            val height = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT)
            val width = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH)

            return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                true
            } else {
                try {
                    // Log.d("Width ", width + " Height " + height);
                    if (Integer.parseInt(height) > Integer.parseInt(width)) {
                        //   Log.d("Mode ", " portrait ");
                        true
                    } else {
                        //  Log.d("Mode ", " landscape");
                        false
                    }
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                    false

                }

            }
        }

    }


}