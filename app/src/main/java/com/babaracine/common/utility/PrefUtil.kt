package com.babaracine.common.utility

import android.content.Context
import android.preference.PreferenceManager

/**
 * Created by Braintech on 7/2/2018.
 */
class PrefUtil{

    companion object {


        fun getInt(context: Context, key: String, defaultValue: Int): Int {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return prefs.getInt(key, defaultValue)
        }

        fun putInt(context: Context, key: String, value: Int) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            prefs.edit().putInt(key, value).apply()
        }

        fun getString(context: Context, key: String, defaultValue: String): String? {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return prefs.getString(key, defaultValue)
        }

        fun putString(context: Context, key: String, value: String) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            prefs.edit().putString(key, value).commit()
        }

        fun getBoolean(context: Context, key: String, defaultValue: Boolean): Boolean {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return prefs.getBoolean(key, defaultValue)
        }

        fun putBoolean(context: Context, key: String, value: Boolean) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            prefs.edit().putBoolean(key, value).apply()
        }

        fun remove(context: Context, key: String) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            prefs.edit().remove(key).commit()
        }

        fun clear(context: Context) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            prefs.edit().clear().commit()
        }

        fun logoutUser(context: Context) {
            /*SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        remove(context, ConstIntent.KEY_IS_LOGGED_IN);
        remove(context, ConstIntent.KEY_USER_ID);
        remove(context, ConstIntent.KEY_USERNAME);
        remove(context, ConstIntent.KEY_EMAIL);
        prefs.edit().commit();*/


        }


    }



}