package com.babaracine.common.utility

import android.content.Context
import android.graphics.Typeface

/**
 * Created by Braintech on 7/2/2018.
 */
class TypeFaceUtil{

    companion object {

        fun setDefaultFont(context: Context, staticTypefaceFieldName: String, fontAssetName: String) {
            val regular = Typeface.createFromAsset(context.assets, fontAssetName)
            replaceFont(staticTypefaceFieldName, regular)
        }

        protected fun replaceFont(staticTypefaceFieldName: String, newTypeface: Typeface) {
            try {
                val staticField = Typeface::class.java
                        .getDeclaredField(staticTypefaceFieldName)
                staticField.isAccessible = true
                staticField.set(null, newTypeface)
            } catch (e: NoSuchFieldException) {
                e.printStackTrace()
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            }

        }

    }


}