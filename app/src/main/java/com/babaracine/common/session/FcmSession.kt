package com.babaracine.common.session

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Braintech on 1/27/2017.
 */

class FcmSession(internal var context: Context) {
    internal var pref: SharedPreferences
    internal var editor: SharedPreferences.Editor
    internal var PRIVATE_MODE = 0

    val fcmToken: String?
        get() = pref.getString(FCM_TOKEN, null)

    val fcmMessage: String?
        get() = pref.getString(FCM_MESSAGE, null)

    val fcmCount: Int
        get() = pref.getInt(FCM_COUNT, 0)

    init {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun saveFcmToken(token: String) {
        editor.putString(FCM_TOKEN, token)
        editor.commit()
    }

    fun saveFcmMessage(message: String) {
        editor.putString(FCM_MESSAGE, message)
        editor.commit()
    }

    fun saveFcmCount(count: Int) {
        editor.putInt(FCM_COUNT, count)
        editor.commit()
    }

    companion object {
        val FCM_TOKEN = "fcmToken"
        val FCM_COUNT = "fcmCount"
        val FCM_MESSAGE = "fcmMessage"

        // Shared preferences file name
        private val PREF_NAME = "fcm_pref"
    }


}
